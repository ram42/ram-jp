if (function(t, e) {
    "use strict";
    "object" == typeof module && "object" == typeof module.exports ? module.exports = t.document ? e(t, !0) : function(t) {
      if (!t.document) throw new Error("jQuery requires a window with a document");
      return e(t)
    } : e(t)
  }("undefined" != typeof window ? window : this, function(t, e) {
    "use strict";

    function n(t, e) {
      var n = (e = e || K).createElement("script");
      n.text = t, e.head.appendChild(n).parentNode.removeChild(n)
    }

    function i(t) {
      var e = !!t && "length" in t && t.length,
        n = at.type(t);
      return "function" !== n && !at.isWindow(t) && ("array" === n || 0 === e || "number" == typeof e && e > 0 && e - 1 in t)
    }

    function o(t, e, n) {
      return at.isFunction(e) ? at.grep(t, function(t, i) {
        return !!e.call(t, i, t) !== n
      }) : e.nodeType ? at.grep(t, function(t) {
        return t === e !== n
      }) : "string" != typeof e ? at.grep(t, function(t) {
        return tt.call(e, t) > -1 !== n
      }) : vt.test(e) ? at.filter(e, t, n) : (e = at.filter(e, t), at.grep(t, function(t) {
        return tt.call(e, t) > -1 !== n && 1 === t.nodeType
      }))
    }

    function r(t, e) {
      for (;
        (t = t[e]) && 1 !== t.nodeType;);
      return t
    }

    function s(t) {
      return t
    }

    function a(t) {
      throw t
    }

    function l(t, e, n) {
      var i;
      try {
        t && at.isFunction(i = t.promise) ? i.call(t).done(e).fail(n) : t && at.isFunction(i = t.then) ? i.call(t, e, n) : e.call(void 0, t)
      } catch (t) {
        n.call(void 0, t)
      }
    }

    function u() {
      K.removeEventListener("DOMContentLoaded", u), t.removeEventListener("load", u), at.ready()
    }

    function c() {
      this.expando = at.expando + c.uid++
    }

    function d(t, e, n) {
      var i;
      if (void 0 === n && 1 === t.nodeType)
        if (i = "data-" + e.replace(Dt, "-$&").toLowerCase(), "string" == typeof(n = t.getAttribute(i))) {
          try {
            n = function(t) {
              return "true" === t || "false" !== t && ("null" === t ? null : t === +t + "" ? +t : At.test(t) ? JSON.parse(t) : t)
            }(n)
          } catch (t) {}
          It.set(t, e, n)
        } else n = void 0;
      return n
    }

    function f(t, e, n, i) {
      var o, r = 1,
        s = 20,
        a = i ? function() {
          return i.cur()
        } : function() {
          return at.css(t, e, "")
        },
        l = a(),
        u = n && n[3] || (at.cssNumber[e] ? "" : "px"),
        c = (at.cssNumber[e] || "px" !== u && +l) && Ot.exec(at.css(t, e));
      if (c && c[3] !== u) {
        u = u || c[3], n = n || [], c = +l || 1;
        do {
          c /= r = r || ".5", at.style(t, e, c + u)
        } while (r !== (r = a() / l) && 1 !== r && --s)
      }
      return n && (c = +c || +l || 0, o = n[1] ? c + (n[1] + 1) * n[2] : +n[2], i && (i.unit = u, i.start = c, i.end = o)), o
    }

    function h(t) {
      var e, n = t.ownerDocument,
        i = t.nodeName,
        o = zt[i];
      return o || (e = n.body.appendChild(n.createElement(i)), o = at.css(e, "display"), e.parentNode.removeChild(e), "none" === o && (o = "block"), zt[i] = o, o)
    }

    function p(t, e) {
      for (var n, i, o = [], r = 0, s = t.length; r < s; r++)(i = t[r]).style && (n = i.style.display, e ? ("none" === n && (o[r] = kt.get(i, "display") || null, o[r] || (i.style.display = "")), "" === i.style.display && Nt(i) && (o[r] = h(i))) : "none" !== n && (o[r] = "none", kt.set(i, "display", n)));
      for (r = 0; r < s; r++) null != o[r] && (t[r].style.display = o[r]);
      return t
    }

    function m(t, e) {
      var n;
      return n = void 0 !== t.getElementsByTagName ? t.getElementsByTagName(e || "*") : void 0 !== t.querySelectorAll ? t.querySelectorAll(e || "*") : [], void 0 === e || e && at.nodeName(t, e) ? at.merge([t], n) : n
    }

    function g(t, e) {
      for (var n = 0, i = t.length; n < i; n++) kt.set(t[n], "globalEval", !e || kt.get(e[n], "globalEval"))
    }

    function v(t, e, n, i, o) {
      for (var r, s, a, l, u, c, d = e.createDocumentFragment(), f = [], h = 0, p = t.length; h < p; h++)
        if ((r = t[h]) || 0 === r)
          if ("object" === at.type(r)) at.merge(f, r.nodeType ? [r] : r);
          else if (Bt.test(r)) {
        for (s = s || d.appendChild(e.createElement("div")), a = ($t.exec(r) || ["", ""])[1].toLowerCase(), l = Ft[a] || Ft._default, s.innerHTML = l[1] + at.htmlPrefilter(r) + l[2], c = l[0]; c--;) s = s.lastChild;
        at.merge(f, s.childNodes), (s = d.firstChild).textContent = ""
      } else f.push(e.createTextNode(r));
      for (d.textContent = "", h = 0; r = f[h++];)
        if (i && at.inArray(r, i) > -1) o && o.push(r);
        else if (u = at.contains(r.ownerDocument, r), s = m(d.appendChild(r), "script"), u && g(s), n)
        for (c = 0; r = s[c++];) Rt.test(r.type || "") && n.push(r);
      return d
    }

    function y() {
      return !0
    }

    function b() {
      return !1
    }

    function _() {
      try {
        return K.activeElement
      } catch (t) {}
    }

    function w(t, e, n, i, o, r) {
      var s, a;
      if ("object" == typeof e) {
        for (a in "string" != typeof n && (i = i || n, n = void 0), e) w(t, a, n, i, e[a], r);
        return t
      }
      if (null == i && null == o ? (o = n, i = n = void 0) : null == o && ("string" == typeof n ? (o = i, i = void 0) : (o = i, i = n, n = void 0)), !1 === o) o = b;
      else if (!o) return t;
      return 1 === r && (s = o, (o = function(t) {
        return at().off(t), s.apply(this, arguments)
      }).guid = s.guid || (s.guid = at.guid++)), t.each(function() {
        at.event.add(this, e, o, i, n)
      })
    }

    function x(t, e) {
      return at.nodeName(t, "table") && at.nodeName(11 !== e.nodeType ? e : e.firstChild, "tr") && t.getElementsByTagName("tbody")[0] || t
    }

    function C(t) {
      return t.type = (null !== t.getAttribute("type")) + "/" + t.type, t
    }

    function E(t) {
      var e = Kt.exec(t.type);
      return e ? t.type = e[1] : t.removeAttribute("type"), t
    }

    function T(t, e) {
      var n, i, o, r, s, a, l, u;
      if (1 === e.nodeType) {
        if (kt.hasData(t) && (r = kt.access(t), s = kt.set(e, r), u = r.events))
          for (o in delete s.handle, s.events = {}, u)
            for (n = 0, i = u[o].length; n < i; n++) at.event.add(e, o, u[o][n]);
        It.hasData(t) && (a = It.access(t), l = at.extend({}, a), It.set(e, l))
      }
    }

    function S(t, e) {
      var n = e.nodeName.toLowerCase();
      "input" === n && Mt.test(t.type) ? e.checked = t.checked : "input" !== n && "textarea" !== n || (e.defaultValue = t.defaultValue)
    }

    function k(t, e, i, o) {
      e = X.apply([], e);
      var r, s, a, l, u, c, d = 0,
        f = t.length,
        h = f - 1,
        p = e[0],
        g = at.isFunction(p);
      if (g || f > 1 && "string" == typeof p && !st.checkClone && Gt.test(p)) return t.each(function(n) {
        var r = t.eq(n);
        g && (e[0] = p.call(this, n, r.html())), k(r, e, i, o)
      });
      if (f && (s = (r = v(e, t[0].ownerDocument, !1, t, o)).firstChild, 1 === r.childNodes.length && (r = s), s || o)) {
        for (l = (a = at.map(m(r, "script"), C)).length; d < f; d++) u = r, d !== h && (u = at.clone(u, !0, !0), l && at.merge(a, m(u, "script"))), i.call(t[d], u, d);
        if (l)
          for (c = a[a.length - 1].ownerDocument, at.map(a, E), d = 0; d < l; d++) u = a[d], Rt.test(u.type || "") && !kt.access(u, "globalEval") && at.contains(c, u) && (u.src ? at._evalUrl && at._evalUrl(u.src) : n(u.textContent.replace(Yt, ""), c))
      }
      return t
    }

    function I(t, e, n) {
      for (var i, o = e ? at.filter(e, t) : t, r = 0; null != (i = o[r]); r++) n || 1 !== i.nodeType || at.cleanData(m(i)), i.parentNode && (n && at.contains(i.ownerDocument, i) && g(m(i, "script")), i.parentNode.removeChild(i));
      return t
    }

    function A(t, e, n) {
      var i, o, r, s, a = t.style;
      return (n = n || Jt(t)) && ("" !== (s = n.getPropertyValue(e) || n[e]) || at.contains(t.ownerDocument, t) || (s = at.style(t, e)), !st.pixelMarginRight() && Xt.test(s) && Zt.test(e) && (i = a.width, o = a.minWidth, r = a.maxWidth, a.minWidth = a.maxWidth = a.width = s, s = n.width, a.width = i, a.minWidth = o, a.maxWidth = r)), void 0 !== s ? s + "" : s
    }

    function D(t, e) {
      return {
        get: function() {
          return t() ? void delete this.get : (this.get = e).apply(this, arguments)
        }
      }
    }

    function L(t) {
      if (t in oe) return t;
      for (var e = t[0].toUpperCase() + t.slice(1), n = ie.length; n--;)
        if ((t = ie[n] + e) in oe) return t
    }

    function O(t, e, n) {
      var i = Ot.exec(e);
      return i ? Math.max(0, i[2] - (n || 0)) + (i[3] || "px") : e
    }

    function P(t, e, n, i, o) {
      var r, s = 0;
      for (r = n === (i ? "border" : "content") ? 4 : "width" === e ? 1 : 0; r < 4; r += 2) "margin" === n && (s += at.css(t, n + Pt[r], !0, o)), i ? ("content" === n && (s -= at.css(t, "padding" + Pt[r], !0, o)), "margin" !== n && (s -= at.css(t, "border" + Pt[r] + "Width", !0, o))) : (s += at.css(t, "padding" + Pt[r], !0, o), "padding" !== n && (s += at.css(t, "border" + Pt[r] + "Width", !0, o)));
      return s
    }

    function N(t, e, n) {
      var i, o = !0,
        r = Jt(t),
        s = "border-box" === at.css(t, "boxSizing", !1, r);
      if (t.getClientRects().length && (i = t.getBoundingClientRect()[e]), i <= 0 || null == i) {
        if (((i = A(t, e, r)) < 0 || null == i) && (i = t.style[e]), Xt.test(i)) return i;
        o = s && (st.boxSizingReliable() || i === t.style[e]), i = parseFloat(i) || 0
      }
      return i + P(t, e, n || (s ? "border" : "content"), o, r) + "px"
    }

    function j(t, e, n, i, o) {
      return new j.prototype.init(t, e, n, i, o)
    }

    function z() {
      se && (t.requestAnimationFrame(z), at.fx.tick())
    }

    function M() {
      return t.setTimeout(function() {
        re = void 0
      }), re = at.now()
    }

    function $(t, e) {
      var n, i = 0,
        o = {
          height: t
        };
      for (e = e ? 1 : 0; i < 4; i += 2 - e) o["margin" + (n = Pt[i])] = o["padding" + n] = t;
      return e && (o.opacity = o.width = t), o
    }

    function R(t, e, n) {
      for (var i, o = (F.tweeners[e] || []).concat(F.tweeners["*"]), r = 0, s = o.length; r < s; r++)
        if (i = o[r].call(n, e, t)) return i
    }

    function F(t, e, n) {
      var i, o, r = 0,
        s = F.prefilters.length,
        a = at.Deferred().always(function() {
          delete l.elem
        }),
        l = function() {
          if (o) return !1;
          for (var e = re || M(), n = Math.max(0, u.startTime + u.duration - e), i = 1 - (n / u.duration || 0), r = 0, s = u.tweens.length; r < s; r++) u.tweens[r].run(i);
          return a.notifyWith(t, [u, i, n]), i < 1 && s ? n : (a.resolveWith(t, [u]), !1)
        },
        u = a.promise({
          elem: t,
          props: at.extend({}, e),
          opts: at.extend(!0, {
            specialEasing: {},
            easing: at.easing._default
          }, n),
          originalProperties: e,
          originalOptions: n,
          startTime: re || M(),
          duration: n.duration,
          tweens: [],
          createTween: function(e, n) {
            var i = at.Tween(t, u.opts, e, n, u.opts.specialEasing[e] || u.opts.easing);
            return u.tweens.push(i), i
          },
          stop: function(e) {
            var n = 0,
              i = e ? u.tweens.length : 0;
            if (o) return this;
            for (o = !0; n < i; n++) u.tweens[n].run(1);
            return e ? (a.notifyWith(t, [u, 1, 0]), a.resolveWith(t, [u, e])) : a.rejectWith(t, [u, e]), this
          }
        }),
        c = u.props;
      for (function(t, e) {
          var n, i, o, r, s;
          for (n in t)
            if (o = e[i = at.camelCase(n)], r = t[n], at.isArray(r) && (o = r[1], r = t[n] = r[0]), n !== i && (t[i] = r, delete t[n]), (s = at.cssHooks[i]) && "expand" in s)
              for (n in r = s.expand(r), delete t[i], r) n in t || (t[n] = r[n], e[n] = o);
            else e[i] = o
        }(c, u.opts.specialEasing); r < s; r++)
        if (i = F.prefilters[r].call(u, t, c, u.opts)) return at.isFunction(i.stop) && (at._queueHooks(u.elem, u.opts.queue).stop = at.proxy(i.stop, i)), i;
      return at.map(c, R, u), at.isFunction(u.opts.start) && u.opts.start.call(t, u), at.fx.timer(at.extend(l, {
        elem: t,
        anim: u,
        queue: u.opts.queue
      })), u.progress(u.opts.progress).done(u.opts.done, u.opts.complete).fail(u.opts.fail).always(u.opts.always)
    }

    function B(t) {
      return (t.match(xt) || []).join(" ")
    }

    function H(t) {
      return t.getAttribute && t.getAttribute("class") || ""
    }

    function W(t, e, n, i) {
      var o;
      if (at.isArray(e)) at.each(e, function(e, o) {
        n || ye.test(t) ? i(t, o) : W(t + "[" + ("object" == typeof o && null != o ? e : "") + "]", o, n, i)
      });
      else if (n || "object" !== at.type(e)) i(t, e);
      else
        for (o in e) W(t + "[" + o + "]", e[o], n, i)
    }

    function q(t) {
      return function(e, n) {
        "string" != typeof e && (n = e, e = "*");
        var i, o = 0,
          r = e.toLowerCase().match(xt) || [];
        if (at.isFunction(n))
          for (; i = r[o++];) "+" === i[0] ? (i = i.slice(1) || "*", (t[i] = t[i] || []).unshift(n)) : (t[i] = t[i] || []).push(n)
      }
    }

    function V(t, e, n, i) {
      function o(a) {
        var l;
        return r[a] = !0, at.each(t[a] || [], function(t, a) {
          var u = a(e, n, i);
          return "string" != typeof u || s || r[u] ? s ? !(l = u) : void 0 : (e.dataTypes.unshift(u), o(u), !1)
        }), l
      }
      var r = {},
        s = t === Ae;
      return o(e.dataTypes[0]) || !r["*"] && o("*")
    }

    function U(t, e) {
      var n, i, o = at.ajaxSettings.flatOptions || {};
      for (n in e) void 0 !== e[n] && ((o[n] ? t : i || (i = {}))[n] = e[n]);
      return i && at.extend(!0, t, i), t
    }

    function Q(t) {
      return at.isWindow(t) ? t : 9 === t.nodeType && t.defaultView
    }
    var G = [],
      K = t.document,
      Y = Object.getPrototypeOf,
      Z = G.slice,
      X = G.concat,
      J = G.push,
      tt = G.indexOf,
      et = {},
      nt = et.toString,
      it = et.hasOwnProperty,
      ot = it.toString,
      rt = ot.call(Object),
      st = {},
      at = function(t, e) {
        return new at.fn.init(t, e)
      },
      lt = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,
      ut = /^-ms-/,
      ct = /-([a-z])/g,
      dt = function(t, e) {
        return e.toUpperCase()
      };
    at.fn = at.prototype = {
      jquery: "3.1.1",
      constructor: at,
      length: 0,
      toArray: function() {
        return Z.call(this)
      },
      get: function(t) {
        return null == t ? Z.call(this) : t < 0 ? this[t + this.length] : this[t]
      },
      pushStack: function(t) {
        var e = at.merge(this.constructor(), t);
        return e.prevObject = this, e
      },
      each: function(t) {
        return at.each(this, t)
      },
      map: function(t) {
        return this.pushStack(at.map(this, function(e, n) {
          return t.call(e, n, e)
        }))
      },
      slice: function() {
        return this.pushStack(Z.apply(this, arguments))
      },
      first: function() {
        return this.eq(0)
      },
      last: function() {
        return this.eq(-1)
      },
      eq: function(t) {
        var e = this.length,
          n = +t + (t < 0 ? e : 0);
        return this.pushStack(n >= 0 && n < e ? [this[n]] : [])
      },
      end: function() {
        return this.prevObject || this.constructor()
      },
      push: J,
      sort: G.sort,
      splice: G.splice
    }, at.extend = at.fn.extend = function() {
      var t, e, n, i, o, r, s = arguments[0] || {},
        a = 1,
        l = arguments.length,
        u = !1;
      for ("boolean" == typeof s && (u = s, s = arguments[a] || {}, a++), "object" == typeof s || at.isFunction(s) || (s = {}), a === l && (s = this, a--); a < l; a++)
        if (null != (t = arguments[a]))
          for (e in t) n = s[e], s !== (i = t[e]) && (u && i && (at.isPlainObject(i) || (o = at.isArray(i))) ? (o ? (o = !1, r = n && at.isArray(n) ? n : []) : r = n && at.isPlainObject(n) ? n : {}, s[e] = at.extend(u, r, i)) : void 0 !== i && (s[e] = i));
      return s
    }, at.extend({
      expando: "jQuery" + ("3.1.1" + Math.random()).replace(/\D/g, ""),
      isReady: !0,
      error: function(t) {
        throw new Error(t)
      },
      noop: function() {},
      isFunction: function(t) {
        return "function" === at.type(t)
      },
      isArray: Array.isArray,
      isWindow: function(t) {
        return null != t && t === t.window
      },
      isNumeric: function(t) {
        var e = at.type(t);
        return ("number" === e || "string" === e) && !isNaN(t - parseFloat(t))
      },
      isPlainObject: function(t) {
        var e, n;
        return !(!t || "[object Object]" !== nt.call(t) || (e = Y(t)) && (n = it.call(e, "constructor") && e.constructor, "function" != typeof n || ot.call(n) !== rt))
      },
      isEmptyObject: function(t) {
        var e;
        for (e in t) return !1;
        return !0
      },
      type: function(t) {
        return null == t ? t + "" : "object" == typeof t || "function" == typeof t ? et[nt.call(t)] || "object" : typeof t
      },
      globalEval: function(t) {
        n(t)
      },
      camelCase: function(t) {
        return t.replace(ut, "ms-").replace(ct, dt)
      },
      nodeName: function(t, e) {
        return t.nodeName && t.nodeName.toLowerCase() === e.toLowerCase()
      },
      each: function(t, e) {
        var n, o = 0;
        if (i(t))
          for (n = t.length; o < n && !1 !== e.call(t[o], o, t[o]); o++);
        else
          for (o in t)
            if (!1 === e.call(t[o], o, t[o])) break;
        return t
      },
      trim: function(t) {
        return null == t ? "" : (t + "").replace(lt, "")
      },
      makeArray: function(t, e) {
        var n = e || [];
        return null != t && (i(Object(t)) ? at.merge(n, "string" == typeof t ? [t] : t) : J.call(n, t)), n
      },
      inArray: function(t, e, n) {
        return null == e ? -1 : tt.call(e, t, n)
      },
      merge: function(t, e) {
        for (var n = +e.length, i = 0, o = t.length; i < n; i++) t[o++] = e[i];
        return t.length = o, t
      },
      grep: function(t, e, n) {
        for (var i = [], o = 0, r = t.length, s = !n; o < r; o++) !e(t[o], o) !== s && i.push(t[o]);
        return i
      },
      map: function(t, e, n) {
        var o, r, s = 0,
          a = [];
        if (i(t))
          for (o = t.length; s < o; s++) null != (r = e(t[s], s, n)) && a.push(r);
        else
          for (s in t) null != (r = e(t[s], s, n)) && a.push(r);
        return X.apply([], a)
      },
      guid: 1,
      proxy: function(t, e) {
        var n, i, o;
        if ("string" == typeof e && (n = t[e], e = t, t = n), at.isFunction(t)) return i = Z.call(arguments, 2), (o = function() {
          return t.apply(e || this, i.concat(Z.call(arguments)))
        }).guid = t.guid = t.guid || at.guid++, o
      },
      now: Date.now,
      support: st
    }), "function" == typeof Symbol && (at.fn[Symbol.iterator] = G[Symbol.iterator]), at.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "), function(t, e) {
      et["[object " + e + "]"] = e.toLowerCase()
    });
    var ft = function(t) {
      function e(t, e, n, i) {
        var o, r, s, a, l, u, c, f = e && e.ownerDocument,
          p = e ? e.nodeType : 9;
        if (n = n || [], "string" != typeof t || !t || 1 !== p && 9 !== p && 11 !== p) return n;
        if (!i && ((e ? e.ownerDocument || e : B) !== P && O(e), e = e || P, j)) {
          if (11 !== p && (l = vt.exec(t)))
            if (o = l[1]) {
              if (9 === p) {
                if (!(s = e.getElementById(o))) return n;
                if (s.id === o) return n.push(s), n
              } else if (f && (s = f.getElementById(o)) && R(e, s) && s.id === o) return n.push(s), n
            } else {
              if (l[2]) return X.apply(n, e.getElementsByTagName(t)), n;
              if ((o = l[3]) && x.getElementsByClassName && e.getElementsByClassName) return X.apply(n, e.getElementsByClassName(o)), n
            } if (x.qsa && !U[t + " "] && (!z || !z.test(t))) {
            if (1 !== p) f = e, c = t;
            else if ("object" !== e.nodeName.toLowerCase()) {
              for ((a = e.getAttribute("id")) ? a = a.replace(wt, xt) : e.setAttribute("id", a = F), r = (u = S(t)).length; r--;) u[r] = "#" + a + " " + h(u[r]);
              c = u.join(","), f = yt.test(t) && d(e.parentNode) || e
            }
            if (c) try {
              return X.apply(n, f.querySelectorAll(c)), n
            } catch (t) {} finally {
              a === F && e.removeAttribute("id")
            }
          }
        }
        return I(t.replace(at, "$1"), e, n, i)
      }

      function n() {
        var t = [];
        return function e(n, i) {
          return t.push(n + " ") > C.cacheLength && delete e[t.shift()], e[n + " "] = i
        }
      }

      function i(t) {
        return t[F] = !0, t
      }

      function o(t) {
        var e = P.createElement("fieldset");
        try {
          return !!t(e)
        } catch (t) {
          return !1
        } finally {
          e.parentNode && e.parentNode.removeChild(e), e = null
        }
      }

      function r(t, e) {
        for (var n = t.split("|"), i = n.length; i--;) C.attrHandle[n[i]] = e
      }

      function s(t, e) {
        var n = e && t,
          i = n && 1 === t.nodeType && 1 === e.nodeType && t.sourceIndex - e.sourceIndex;
        if (i) return i;
        if (n)
          for (; n = n.nextSibling;)
            if (n === e) return -1;
        return t ? 1 : -1
      }

      function a(t) {
        return function(e) {
          return "input" === e.nodeName.toLowerCase() && e.type === t
        }
      }

      function l(t) {
        return function(e) {
          var n = e.nodeName.toLowerCase();
          return ("input" === n || "button" === n) && e.type === t
        }
      }

      function u(t) {
        return function(e) {
          return "form" in e ? e.parentNode && !1 === e.disabled ? "label" in e ? "label" in e.parentNode ? e.parentNode.disabled === t : e.disabled === t : e.isDisabled === t || e.isDisabled !== !t && Et(e) === t : e.disabled === t : "label" in e && e.disabled === t
        }
      }

      function c(t) {
        return i(function(e) {
          return e = +e, i(function(n, i) {
            for (var o, r = t([], n.length, e), s = r.length; s--;) n[o = r[s]] && (n[o] = !(i[o] = n[o]))
          })
        })
      }

      function d(t) {
        return t && void 0 !== t.getElementsByTagName && t
      }

      function f() {}

      function h(t) {
        for (var e = 0, n = t.length, i = ""; e < n; e++) i += t[e].value;
        return i
      }

      function p(t, e, n) {
        var i = e.dir,
          o = e.next,
          r = o || i,
          s = n && "parentNode" === r,
          a = W++;
        return e.first ? function(e, n, o) {
          for (; e = e[i];)
            if (1 === e.nodeType || s) return t(e, n, o);
          return !1
        } : function(e, n, l) {
          var u, c, d, f = [H, a];
          if (l) {
            for (; e = e[i];)
              if ((1 === e.nodeType || s) && t(e, n, l)) return !0
          } else
            for (; e = e[i];)
              if (1 === e.nodeType || s)
                if (c = (d = e[F] || (e[F] = {}))[e.uniqueID] || (d[e.uniqueID] = {}), o && o === e.nodeName.toLowerCase()) e = e[i] || e;
                else {
                  if ((u = c[r]) && u[0] === H && u[1] === a) return f[2] = u[2];
                  if (c[r] = f, f[2] = t(e, n, l)) return !0
                } return !1
        }
      }

      function m(t) {
        return t.length > 1 ? function(e, n, i) {
          for (var o = t.length; o--;)
            if (!t[o](e, n, i)) return !1;
          return !0
        } : t[0]
      }

      function g(t, n, i) {
        for (var o = 0, r = n.length; o < r; o++) e(t, n[o], i);
        return i
      }

      function v(t, e, n, i, o) {
        for (var r, s = [], a = 0, l = t.length, u = null != e; a < l; a++)(r = t[a]) && (n && !n(r, i, o) || (s.push(r), u && e.push(a)));
        return s
      }

      function y(t, e, n, o, r, s) {
        return o && !o[F] && (o = y(o)), r && !r[F] && (r = y(r, s)), i(function(i, s, a, l) {
          var u, c, d, f = [],
            h = [],
            p = s.length,
            m = i || g(e || "*", a.nodeType ? [a] : a, []),
            y = !t || !i && e ? m : v(m, f, t, a, l),
            b = n ? r || (i ? t : p || o) ? [] : s : y;
          if (n && n(y, b, a, l), o)
            for (u = v(b, h), o(u, [], a, l), c = u.length; c--;)(d = u[c]) && (b[h[c]] = !(y[h[c]] = d));
          if (i) {
            if (r || t) {
              if (r) {
                for (u = [], c = b.length; c--;)(d = b[c]) && u.push(y[c] = d);
                r(null, b = [], u, l)
              }
              for (c = b.length; c--;)(d = b[c]) && (u = r ? tt(i, d) : f[c]) > -1 && (i[u] = !(s[u] = d))
            }
          } else b = v(b === s ? b.splice(p, b.length) : b), r ? r(null, s, b, l) : X.apply(s, b)
        })
      }

      function b(t) {
        for (var e, n, i, o = t.length, r = C.relative[t[0].type], s = r || C.relative[" "], a = r ? 1 : 0, l = p(function(t) {
            return t === e
          }, s, !0), u = p(function(t) {
            return tt(e, t) > -1
          }, s, !0), c = [function(t, n, i) {
            var o = !r && (i || n !== A) || ((e = n).nodeType ? l(t, n, i) : u(t, n, i));
            return e = null, o
          }]; a < o; a++)
          if (n = C.relative[t[a].type]) c = [p(m(c), n)];
          else {
            if ((n = C.filter[t[a].type].apply(null, t[a].matches))[F]) {
              for (i = ++a; i < o && !C.relative[t[i].type]; i++);
              return y(a > 1 && m(c), a > 1 && h(t.slice(0, a - 1).concat({
                value: " " === t[a - 2].type ? "*" : ""
              })).replace(at, "$1"), n, a < i && b(t.slice(a, i)), i < o && b(t = t.slice(i)), i < o && h(t))
            }
            c.push(n)
          } return m(c)
      }

      function _(t, n) {
        var o = n.length > 0,
          r = t.length > 0,
          s = function(i, s, a, l, u) {
            var c, d, f, h = 0,
              p = "0",
              m = i && [],
              g = [],
              y = A,
              b = i || r && C.find.TAG("*", u),
              _ = H += null == y ? 1 : Math.random() || .1,
              w = b.length;
            for (u && (A = s === P || s || u); p !== w && null != (c = b[p]); p++) {
              if (r && c) {
                for (d = 0, s || c.ownerDocument === P || (O(c), a = !j); f = t[d++];)
                  if (f(c, s || P, a)) {
                    l.push(c);
                    break
                  } u && (H = _)
              }
              o && ((c = !f && c) && h--, i && m.push(c))
            }
            if (h += p, o && p !== h) {
              for (d = 0; f = n[d++];) f(m, g, s, a);
              if (i) {
                if (h > 0)
                  for (; p--;) m[p] || g[p] || (g[p] = Y.call(l));
                g = v(g)
              }
              X.apply(l, g), u && !i && g.length > 0 && h + n.length > 1 && e.uniqueSort(l)
            }
            return u && (H = _, A = y), m
          };
        return o ? i(s) : s
      }
      var w, x, C, E, T, S, k, I, A, D, L, O, P, N, j, z, M, $, R, F = "sizzle" + 1 * new Date,
        B = t.document,
        H = 0,
        W = 0,
        q = n(),
        V = n(),
        U = n(),
        Q = function(t, e) {
          return t === e && (L = !0), 0
        },
        G = {}.hasOwnProperty,
        K = [],
        Y = K.pop,
        Z = K.push,
        X = K.push,
        J = K.slice,
        tt = function(t, e) {
          for (var n = 0, i = t.length; n < i; n++)
            if (t[n] === e) return n;
          return -1
        },
        et = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
        nt = "[\\x20\\t\\r\\n\\f]",
        it = "(?:\\\\.|[\\w-]|[^\0-\\xa0])+",
        ot = "\\[" + nt + "*(" + it + ")(?:" + nt + "*([*^$|!~]?=)" + nt + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + it + "))|)" + nt + "*\\]",
        rt = ":(" + it + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + ot + ")*)|.*)\\)|)",
        st = new RegExp(nt + "+", "g"),
        at = new RegExp("^" + nt + "+|((?:^|[^\\\\])(?:\\\\.)*)" + nt + "+$", "g"),
        lt = new RegExp("^" + nt + "*," + nt + "*"),
        ut = new RegExp("^" + nt + "*([>+~]|" + nt + ")" + nt + "*"),
        ct = new RegExp("=" + nt + "*([^\\]'\"]*?)" + nt + "*\\]", "g"),
        dt = new RegExp(rt),
        ft = new RegExp("^" + it + "$"),
        ht = {
          ID: new RegExp("^#(" + it + ")"),
          CLASS: new RegExp("^\\.(" + it + ")"),
          TAG: new RegExp("^(" + it + "|[*])"),
          ATTR: new RegExp("^" + ot),
          PSEUDO: new RegExp("^" + rt),
          CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + nt + "*(even|odd|(([+-]|)(\\d*)n|)" + nt + "*(?:([+-]|)" + nt + "*(\\d+)|))" + nt + "*\\)|)", "i"),
          bool: new RegExp("^(?:" + et + ")$", "i"),
          needsContext: new RegExp("^" + nt + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + nt + "*((?:-\\d)?\\d*)" + nt + "*\\)|)(?=[^-]|$)", "i")
        },
        pt = /^(?:input|select|textarea|button)$/i,
        mt = /^h\d$/i,
        gt = /^[^{]+\{\s*\[native \w/,
        vt = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
        yt = /[+~]/,
        bt = new RegExp("\\\\([\\da-f]{1,6}" + nt + "?|(" + nt + ")|.)", "ig"),
        _t = function(t, e, n) {
          var i = "0x" + e - 65536;
          return i != i || n ? e : i < 0 ? String.fromCharCode(i + 65536) : String.fromCharCode(i >> 10 | 55296, 1023 & i | 56320)
        },
        wt = /([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g,
        xt = function(t, e) {
          return e ? "\0" === t ? "�" : t.slice(0, -1) + "\\" + t.charCodeAt(t.length - 1).toString(16) + " " : "\\" + t
        },
        Ct = function() {
          O()
        },
        Et = p(function(t) {
          return !0 === t.disabled && ("form" in t || "label" in t)
        }, {
          dir: "parentNode",
          next: "legend"
        });
      try {
        X.apply(K = J.call(B.childNodes), B.childNodes), K[B.childNodes.length].nodeType
      } catch (t) {
        X = {
          apply: K.length ? function(t, e) {
            Z.apply(t, J.call(e))
          } : function(t, e) {
            for (var n = t.length, i = 0; t[n++] = e[i++];);
            t.length = n - 1
          }
        }
      }
      for (w in x = e.support = {}, T = e.isXML = function(t) {
          var e = t && (t.ownerDocument || t).documentElement;
          return !!e && "HTML" !== e.nodeName
        }, O = e.setDocument = function(t) {
          var e, n, i = t ? t.ownerDocument || t : B;
          return i !== P && 9 === i.nodeType && i.documentElement ? (N = (P = i).documentElement, j = !T(P), B !== P && (n = P.defaultView) && n.top !== n && (n.addEventListener ? n.addEventListener("unload", Ct, !1) : n.attachEvent && n.attachEvent("onunload", Ct)), x.attributes = o(function(t) {
            return t.className = "i", !t.getAttribute("className")
          }), x.getElementsByTagName = o(function(t) {
            return t.appendChild(P.createComment("")), !t.getElementsByTagName("*").length
          }), x.getElementsByClassName = gt.test(P.getElementsByClassName), x.getById = o(function(t) {
            return N.appendChild(t).id = F, !P.getElementsByName || !P.getElementsByName(F).length
          }), x.getById ? (C.filter.ID = function(t) {
            var e = t.replace(bt, _t);
            return function(t) {
              return t.getAttribute("id") === e
            }
          }, C.find.ID = function(t, e) {
            if (void 0 !== e.getElementById && j) {
              var n = e.getElementById(t);
              return n ? [n] : []
            }
          }) : (C.filter.ID = function(t) {
            var e = t.replace(bt, _t);
            return function(t) {
              var n = void 0 !== t.getAttributeNode && t.getAttributeNode("id");
              return n && n.value === e
            }
          }, C.find.ID = function(t, e) {
            if (void 0 !== e.getElementById && j) {
              var n, i, o, r = e.getElementById(t);
              if (r) {
                if ((n = r.getAttributeNode("id")) && n.value === t) return [r];
                for (o = e.getElementsByName(t), i = 0; r = o[i++];)
                  if ((n = r.getAttributeNode("id")) && n.value === t) return [r]
              }
              return []
            }
          }), C.find.TAG = x.getElementsByTagName ? function(t, e) {
            return void 0 !== e.getElementsByTagName ? e.getElementsByTagName(t) : x.qsa ? e.querySelectorAll(t) : void 0
          } : function(t, e) {
            var n, i = [],
              o = 0,
              r = e.getElementsByTagName(t);
            if ("*" === t) {
              for (; n = r[o++];) 1 === n.nodeType && i.push(n);
              return i
            }
            return r
          }, C.find.CLASS = x.getElementsByClassName && function(t, e) {
            if (void 0 !== e.getElementsByClassName && j) return e.getElementsByClassName(t)
          }, M = [], z = [], (x.qsa = gt.test(P.querySelectorAll)) && (o(function(t) {
            N.appendChild(t).innerHTML = "<a id='" + F + "'></a><select id='" + F + "-\r\\' msallowcapture=''><option selected=''></option></select>", t.querySelectorAll("[msallowcapture^='']").length && z.push("[*^$]=" + nt + "*(?:''|\"\")"), t.querySelectorAll("[selected]").length || z.push("\\[" + nt + "*(?:value|" + et + ")"), t.querySelectorAll("[id~=" + F + "-]").length || z.push("~="), t.querySelectorAll(":checked").length || z.push(":checked"), t.querySelectorAll("a#" + F + "+*").length || z.push(".#.+[+~]")
          }), o(function(t) {
            t.innerHTML = "<a href='' disabled='disabled'></a><select disabled='disabled'><option/></select>";
            var e = P.createElement("input");
            e.setAttribute("type", "hidden"), t.appendChild(e).setAttribute("name", "D"), t.querySelectorAll("[name=d]").length && z.push("name" + nt + "*[*^$|!~]?="), 2 !== t.querySelectorAll(":enabled").length && z.push(":enabled", ":disabled"), N.appendChild(t).disabled = !0, 2 !== t.querySelectorAll(":disabled").length && z.push(":enabled", ":disabled"), t.querySelectorAll("*,:x"), z.push(",.*:")
          })), (x.matchesSelector = gt.test($ = N.matches || N.webkitMatchesSelector || N.mozMatchesSelector || N.oMatchesSelector || N.msMatchesSelector)) && o(function(t) {
            x.disconnectedMatch = $.call(t, "*"), $.call(t, "[s!='']:x"), M.push("!=", rt)
          }), z = z.length && new RegExp(z.join("|")), M = M.length && new RegExp(M.join("|")), e = gt.test(N.compareDocumentPosition), R = e || gt.test(N.contains) ? function(t, e) {
            var n = 9 === t.nodeType ? t.documentElement : t,
              i = e && e.parentNode;
            return t === i || !(!i || 1 !== i.nodeType || !(n.contains ? n.contains(i) : t.compareDocumentPosition && 16 & t.compareDocumentPosition(i)))
          } : function(t, e) {
            if (e)
              for (; e = e.parentNode;)
                if (e === t) return !0;
            return !1
          }, Q = e ? function(t, e) {
            if (t === e) return L = !0, 0;
            var n = !t.compareDocumentPosition - !e.compareDocumentPosition;
            return n || (1 & (n = (t.ownerDocument || t) === (e.ownerDocument || e) ? t.compareDocumentPosition(e) : 1) || !x.sortDetached && e.compareDocumentPosition(t) === n ? t === P || t.ownerDocument === B && R(B, t) ? -1 : e === P || e.ownerDocument === B && R(B, e) ? 1 : D ? tt(D, t) - tt(D, e) : 0 : 4 & n ? -1 : 1)
          } : function(t, e) {
            if (t === e) return L = !0, 0;
            var n, i = 0,
              o = t.parentNode,
              r = e.parentNode,
              a = [t],
              l = [e];
            if (!o || !r) return t === P ? -1 : e === P ? 1 : o ? -1 : r ? 1 : D ? tt(D, t) - tt(D, e) : 0;
            if (o === r) return s(t, e);
            for (n = t; n = n.parentNode;) a.unshift(n);
            for (n = e; n = n.parentNode;) l.unshift(n);
            for (; a[i] === l[i];) i++;
            return i ? s(a[i], l[i]) : a[i] === B ? -1 : l[i] === B ? 1 : 0
          }, P) : P
        }, e.matches = function(t, n) {
          return e(t, null, null, n)
        }, e.matchesSelector = function(t, n) {
          if ((t.ownerDocument || t) !== P && O(t), n = n.replace(ct, "='$1']"), x.matchesSelector && j && !U[n + " "] && (!M || !M.test(n)) && (!z || !z.test(n))) try {
            var i = $.call(t, n);
            if (i || x.disconnectedMatch || t.document && 11 !== t.document.nodeType) return i
          } catch (t) {}
          return e(n, P, null, [t]).length > 0
        }, e.contains = function(t, e) {
          return (t.ownerDocument || t) !== P && O(t), R(t, e)
        }, e.attr = function(t, e) {
          (t.ownerDocument || t) !== P && O(t);
          var n = C.attrHandle[e.toLowerCase()],
            i = n && G.call(C.attrHandle, e.toLowerCase()) ? n(t, e, !j) : void 0;
          return void 0 !== i ? i : x.attributes || !j ? t.getAttribute(e) : (i = t.getAttributeNode(e)) && i.specified ? i.value : null
        }, e.escape = function(t) {
          return (t + "").replace(wt, xt)
        }, e.error = function(t) {
          throw new Error("Syntax error, unrecognized expression: " + t)
        }, e.uniqueSort = function(t) {
          var e, n = [],
            i = 0,
            o = 0;
          if (L = !x.detectDuplicates, D = !x.sortStable && t.slice(0), t.sort(Q), L) {
            for (; e = t[o++];) e === t[o] && (i = n.push(o));
            for (; i--;) t.splice(n[i], 1)
          }
          return D = null, t
        }, E = e.getText = function(t) {
          var e, n = "",
            i = 0,
            o = t.nodeType;
          if (o) {
            if (1 === o || 9 === o || 11 === o) {
              if ("string" == typeof t.textContent) return t.textContent;
              for (t = t.firstChild; t; t = t.nextSibling) n += E(t)
            } else if (3 === o || 4 === o) return t.nodeValue
          } else
            for (; e = t[i++];) n += E(e);
          return n
        }, (C = e.selectors = {
          cacheLength: 50,
          createPseudo: i,
          match: ht,
          attrHandle: {},
          find: {},
          relative: {
            ">": {
              dir: "parentNode",
              first: !0
            },
            " ": {
              dir: "parentNode"
            },
            "+": {
              dir: "previousSibling",
              first: !0
            },
            "~": {
              dir: "previousSibling"
            }
          },
          preFilter: {
            ATTR: function(t) {
              return t[1] = t[1].replace(bt, _t), t[3] = (t[3] || t[4] || t[5] || "").replace(bt, _t), "~=" === t[2] && (t[3] = " " + t[3] + " "), t.slice(0, 4)
            },
            CHILD: function(t) {
              return t[1] = t[1].toLowerCase(), "nth" === t[1].slice(0, 3) ? (t[3] || e.error(t[0]), t[4] = +(t[4] ? t[5] + (t[6] || 1) : 2 * ("even" === t[3] || "odd" === t[3])), t[5] = +(t[7] + t[8] || "odd" === t[3])) : t[3] && e.error(t[0]), t
            },
            PSEUDO: function(t) {
              var e, n = !t[6] && t[2];
              return ht.CHILD.test(t[0]) ? null : (t[3] ? t[2] = t[4] || t[5] || "" : n && dt.test(n) && (e = S(n, !0)) && (e = n.indexOf(")", n.length - e) - n.length) && (t[0] = t[0].slice(0, e), t[2] = n.slice(0, e)), t.slice(0, 3))
            }
          },
          filter: {
            TAG: function(t) {
              var e = t.replace(bt, _t).toLowerCase();
              return "*" === t ? function() {
                return !0
              } : function(t) {
                return t.nodeName && t.nodeName.toLowerCase() === e
              }
            },
            CLASS: function(t) {
              var e = q[t + " "];
              return e || (e = new RegExp("(^|" + nt + ")" + t + "(" + nt + "|$)")) && q(t, function(t) {
                return e.test("string" == typeof t.className && t.className || void 0 !== t.getAttribute && t.getAttribute("class") || "")
              })
            },
            ATTR: function(t, n, i) {
              return function(o) {
                var r = e.attr(o, t);
                return null == r ? "!=" === n : !n || (r += "", "=" === n ? r === i : "!=" === n ? r !== i : "^=" === n ? i && 0 === r.indexOf(i) : "*=" === n ? i && r.indexOf(i) > -1 : "$=" === n ? i && r.slice(-i.length) === i : "~=" === n ? (" " + r.replace(st, " ") + " ").indexOf(i) > -1 : "|=" === n && (r === i || r.slice(0, i.length + 1) === i + "-"))
              }
            },
            CHILD: function(t, e, n, i, o) {
              var r = "nth" !== t.slice(0, 3),
                s = "last" !== t.slice(-4),
                a = "of-type" === e;
              return 1 === i && 0 === o ? function(t) {
                return !!t.parentNode
              } : function(e, n, l) {
                var u, c, d, f, h, p, m = r !== s ? "nextSibling" : "previousSibling",
                  g = e.parentNode,
                  v = a && e.nodeName.toLowerCase(),
                  y = !l && !a,
                  b = !1;
                if (g) {
                  if (r) {
                    for (; m;) {
                      for (f = e; f = f[m];)
                        if (a ? f.nodeName.toLowerCase() === v : 1 === f.nodeType) return !1;
                      p = m = "only" === t && !p && "nextSibling"
                    }
                    return !0
                  }
                  if (p = [s ? g.firstChild : g.lastChild], s && y) {
                    for (b = (h = (u = (c = (d = (f = g)[F] || (f[F] = {}))[f.uniqueID] || (d[f.uniqueID] = {}))[t] || [])[0] === H && u[1]) && u[2], f = h && g.childNodes[h]; f = ++h && f && f[m] || (b = h = 0) || p.pop();)
                      if (1 === f.nodeType && ++b && f === e) {
                        c[t] = [H, h, b];
                        break
                      }
                  } else if (y && (b = h = (u = (c = (d = (f = e)[F] || (f[F] = {}))[f.uniqueID] || (d[f.uniqueID] = {}))[t] || [])[0] === H && u[1]), !1 === b)
                    for (;
                      (f = ++h && f && f[m] || (b = h = 0) || p.pop()) && ((a ? f.nodeName.toLowerCase() !== v : 1 !== f.nodeType) || !++b || (y && ((c = (d = f[F] || (f[F] = {}))[f.uniqueID] || (d[f.uniqueID] = {}))[t] = [H, b]), f !== e)););
                  return (b -= o) === i || b % i == 0 && b / i >= 0
                }
              }
            },
            PSEUDO: function(t, n) {
              var o, r = C.pseudos[t] || C.setFilters[t.toLowerCase()] || e.error("unsupported pseudo: " + t);
              return r[F] ? r(n) : r.length > 1 ? (o = [t, t, "", n], C.setFilters.hasOwnProperty(t.toLowerCase()) ? i(function(t, e) {
                for (var i, o = r(t, n), s = o.length; s--;) t[i = tt(t, o[s])] = !(e[i] = o[s])
              }) : function(t) {
                return r(t, 0, o)
              }) : r
            }
          },
          pseudos: {
            not: i(function(t) {
              var e = [],
                n = [],
                o = k(t.replace(at, "$1"));
              return o[F] ? i(function(t, e, n, i) {
                for (var r, s = o(t, null, i, []), a = t.length; a--;)(r = s[a]) && (t[a] = !(e[a] = r))
              }) : function(t, i, r) {
                return e[0] = t, o(e, null, r, n), e[0] = null, !n.pop()
              }
            }),
            has: i(function(t) {
              return function(n) {
                return e(t, n).length > 0
              }
            }),
            contains: i(function(t) {
              return t = t.replace(bt, _t),
                function(e) {
                  return (e.textContent || e.innerText || E(e)).indexOf(t) > -1
                }
            }),
            lang: i(function(t) {
              return ft.test(t || "") || e.error("unsupported lang: " + t), t = t.replace(bt, _t).toLowerCase(),
                function(e) {
                  var n;
                  do {
                    if (n = j ? e.lang : e.getAttribute("xml:lang") || e.getAttribute("lang")) return (n = n.toLowerCase()) === t || 0 === n.indexOf(t + "-")
                  } while ((e = e.parentNode) && 1 === e.nodeType);
                  return !1
                }
            }),
            target: function(e) {
              var n = t.location && t.location.hash;
              return n && n.slice(1) === e.id
            },
            root: function(t) {
              return t === N
            },
            focus: function(t) {
              return t === P.activeElement && (!P.hasFocus || P.hasFocus()) && !!(t.type || t.href || ~t.tabIndex)
            },
            enabled: u(!1),
            disabled: u(!0),
            checked: function(t) {
              var e = t.nodeName.toLowerCase();
              return "input" === e && !!t.checked || "option" === e && !!t.selected
            },
            selected: function(t) {
              return t.parentNode && t.parentNode.selectedIndex, !0 === t.selected
            },
            empty: function(t) {
              for (t = t.firstChild; t; t = t.nextSibling)
                if (t.nodeType < 6) return !1;
              return !0
            },
            parent: function(t) {
              return !C.pseudos.empty(t)
            },
            header: function(t) {
              return mt.test(t.nodeName)
            },
            input: function(t) {
              return pt.test(t.nodeName)
            },
            button: function(t) {
              var e = t.nodeName.toLowerCase();
              return "input" === e && "button" === t.type || "button" === e
            },
            text: function(t) {
              var e;
              return "input" === t.nodeName.toLowerCase() && "text" === t.type && (null == (e = t.getAttribute("type")) || "text" === e.toLowerCase())
            },
            first: c(function() {
              return [0]
            }),
            last: c(function(t, e) {
              return [e - 1]
            }),
            eq: c(function(t, e, n) {
              return [n < 0 ? n + e : n]
            }),
            even: c(function(t, e) {
              for (var n = 0; n < e; n += 2) t.push(n);
              return t
            }),
            odd: c(function(t, e) {
              for (var n = 1; n < e; n += 2) t.push(n);
              return t
            }),
            lt: c(function(t, e, n) {
              for (var i = n < 0 ? n + e : n; --i >= 0;) t.push(i);
              return t
            }),
            gt: c(function(t, e, n) {
              for (var i = n < 0 ? n + e : n; ++i < e;) t.push(i);
              return t
            })
          }
        }).pseudos.nth = C.pseudos.eq, {
          radio: !0,
          checkbox: !0,
          file: !0,
          password: !0,
          image: !0
        }) C.pseudos[w] = a(w);
      for (w in {
          submit: !0,
          reset: !0
        }) C.pseudos[w] = l(w);
      return f.prototype = C.filters = C.pseudos, C.setFilters = new f, S = e.tokenize = function(t, n) {
        var i, o, r, s, a, l, u, c = V[t + " "];
        if (c) return n ? 0 : c.slice(0);
        for (a = t, l = [], u = C.preFilter; a;) {
          for (s in i && !(o = lt.exec(a)) || (o && (a = a.slice(o[0].length) || a), l.push(r = [])), i = !1, (o = ut.exec(a)) && (i = o.shift(), r.push({
              value: i,
              type: o[0].replace(at, " ")
            }), a = a.slice(i.length)), C.filter) !(o = ht[s].exec(a)) || u[s] && !(o = u[s](o)) || (i = o.shift(), r.push({
            value: i,
            type: s,
            matches: o
          }), a = a.slice(i.length));
          if (!i) break
        }
        return n ? a.length : a ? e.error(t) : V(t, l).slice(0)
      }, k = e.compile = function(t, e) {
        var n, i = [],
          o = [],
          r = U[t + " "];
        if (!r) {
          for (e || (e = S(t)), n = e.length; n--;)(r = b(e[n]))[F] ? i.push(r) : o.push(r);
          (r = U(t, _(o, i))).selector = t
        }
        return r
      }, I = e.select = function(t, e, n, i) {
        var o, r, s, a, l, u = "function" == typeof t && t,
          c = !i && S(t = u.selector || t);
        if (n = n || [], 1 === c.length) {
          if ((r = c[0] = c[0].slice(0)).length > 2 && "ID" === (s = r[0]).type && 9 === e.nodeType && j && C.relative[r[1].type]) {
            if (!(e = (C.find.ID(s.matches[0].replace(bt, _t), e) || [])[0])) return n;
            u && (e = e.parentNode), t = t.slice(r.shift().value.length)
          }
          for (o = ht.needsContext.test(t) ? 0 : r.length; o-- && (s = r[o], !C.relative[a = s.type]);)
            if ((l = C.find[a]) && (i = l(s.matches[0].replace(bt, _t), yt.test(r[0].type) && d(e.parentNode) || e))) {
              if (r.splice(o, 1), !(t = i.length && h(r))) return X.apply(n, i), n;
              break
            }
        }
        return (u || k(t, c))(i, e, !j, n, !e || yt.test(t) && d(e.parentNode) || e), n
      }, x.sortStable = F.split("").sort(Q).join("") === F, x.detectDuplicates = !!L, O(), x.sortDetached = o(function(t) {
        return 1 & t.compareDocumentPosition(P.createElement("fieldset"))
      }), o(function(t) {
        return t.innerHTML = "<a href='#'></a>", "#" === t.firstChild.getAttribute("href")
      }) || r("type|href|height|width", function(t, e, n) {
        if (!n) return t.getAttribute(e, "type" === e.toLowerCase() ? 1 : 2)
      }), x.attributes && o(function(t) {
        return t.innerHTML = "<input/>", t.firstChild.setAttribute("value", ""), "" === t.firstChild.getAttribute("value")
      }) || r("value", function(t, e, n) {
        if (!n && "input" === t.nodeName.toLowerCase()) return t.defaultValue
      }), o(function(t) {
        return null == t.getAttribute("disabled")
      }) || r(et, function(t, e, n) {
        var i;
        if (!n) return !0 === t[e] ? e.toLowerCase() : (i = t.getAttributeNode(e)) && i.specified ? i.value : null
      }), e
    }(t);
    at.find = ft, at.expr = ft.selectors, at.expr[":"] = at.expr.pseudos, at.uniqueSort = at.unique = ft.uniqueSort, at.text = ft.getText, at.isXMLDoc = ft.isXML, at.contains = ft.contains, at.escapeSelector = ft.escape;
    var ht = function(t, e, n) {
        for (var i = [], o = void 0 !== n;
          (t = t[e]) && 9 !== t.nodeType;)
          if (1 === t.nodeType) {
            if (o && at(t).is(n)) break;
            i.push(t)
          } return i
      },
      pt = function(t, e) {
        for (var n = []; t; t = t.nextSibling) 1 === t.nodeType && t !== e && n.push(t);
        return n
      },
      mt = at.expr.match.needsContext,
      gt = /^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i,
      vt = /^.[^:#\[\.,]*$/;
    at.filter = function(t, e, n) {
      var i = e[0];
      return n && (t = ":not(" + t + ")"), 1 === e.length && 1 === i.nodeType ? at.find.matchesSelector(i, t) ? [i] : [] : at.find.matches(t, at.grep(e, function(t) {
        return 1 === t.nodeType
      }))
    }, at.fn.extend({
      find: function(t) {
        var e, n, i = this.length,
          o = this;
        if ("string" != typeof t) return this.pushStack(at(t).filter(function() {
          for (e = 0; e < i; e++)
            if (at.contains(o[e], this)) return !0
        }));
        for (n = this.pushStack([]), e = 0; e < i; e++) at.find(t, o[e], n);
        return i > 1 ? at.uniqueSort(n) : n
      },
      filter: function(t) {
        return this.pushStack(o(this, t || [], !1))
      },
      not: function(t) {
        return this.pushStack(o(this, t || [], !0))
      },
      is: function(t) {
        return !!o(this, "string" == typeof t && mt.test(t) ? at(t) : t || [], !1).length
      }
    });
    var yt, bt = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/;
    (at.fn.init = function(t, e, n) {
      var i, o;
      if (!t) return this;
      if (n = n || yt, "string" == typeof t) {
        if (!(i = "<" === t[0] && ">" === t[t.length - 1] && t.length >= 3 ? [null, t, null] : bt.exec(t)) || !i[1] && e) return !e || e.jquery ? (e || n).find(t) : this.constructor(e).find(t);
        if (i[1]) {
          if (e = e instanceof at ? e[0] : e, at.merge(this, at.parseHTML(i[1], e && e.nodeType ? e.ownerDocument || e : K, !0)), gt.test(i[1]) && at.isPlainObject(e))
            for (i in e) at.isFunction(this[i]) ? this[i](e[i]) : this.attr(i, e[i]);
          return this
        }
        return (o = K.getElementById(i[2])) && (this[0] = o, this.length = 1), this
      }
      return t.nodeType ? (this[0] = t, this.length = 1, this) : at.isFunction(t) ? void 0 !== n.ready ? n.ready(t) : t(at) : at.makeArray(t, this)
    }).prototype = at.fn, yt = at(K);
    var _t = /^(?:parents|prev(?:Until|All))/,
      wt = {
        children: !0,
        contents: !0,
        next: !0,
        prev: !0
      };
    at.fn.extend({
      has: function(t) {
        var e = at(t, this),
          n = e.length;
        return this.filter(function() {
          for (var t = 0; t < n; t++)
            if (at.contains(this, e[t])) return !0
        })
      },
      closest: function(t, e) {
        var n, i = 0,
          o = this.length,
          r = [],
          s = "string" != typeof t && at(t);
        if (!mt.test(t))
          for (; i < o; i++)
            for (n = this[i]; n && n !== e; n = n.parentNode)
              if (n.nodeType < 11 && (s ? s.index(n) > -1 : 1 === n.nodeType && at.find.matchesSelector(n, t))) {
                r.push(n);
                break
              } return this.pushStack(r.length > 1 ? at.uniqueSort(r) : r)
      },
      index: function(t) {
        return t ? "string" == typeof t ? tt.call(at(t), this[0]) : tt.call(this, t.jquery ? t[0] : t) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1
      },
      add: function(t, e) {
        return this.pushStack(at.uniqueSort(at.merge(this.get(), at(t, e))))
      },
      addBack: function(t) {
        return this.add(null == t ? this.prevObject : this.prevObject.filter(t))
      }
    }), at.each({
      parent: function(t) {
        var e = t.parentNode;
        return e && 11 !== e.nodeType ? e : null
      },
      parents: function(t) {
        return ht(t, "parentNode")
      },
      parentsUntil: function(t, e, n) {
        return ht(t, "parentNode", n)
      },
      next: function(t) {
        return r(t, "nextSibling")
      },
      prev: function(t) {
        return r(t, "previousSibling")
      },
      nextAll: function(t) {
        return ht(t, "nextSibling")
      },
      prevAll: function(t) {
        return ht(t, "previousSibling")
      },
      nextUntil: function(t, e, n) {
        return ht(t, "nextSibling", n)
      },
      prevUntil: function(t, e, n) {
        return ht(t, "previousSibling", n)
      },
      siblings: function(t) {
        return pt((t.parentNode || {}).firstChild, t)
      },
      children: function(t) {
        return pt(t.firstChild)
      },
      contents: function(t) {
        return t.contentDocument || at.merge([], t.childNodes)
      }
    }, function(t, e) {
      at.fn[t] = function(n, i) {
        var o = at.map(this, e, n);
        return "Until" !== t.slice(-5) && (i = n), i && "string" == typeof i && (o = at.filter(i, o)), this.length > 1 && (wt[t] || at.uniqueSort(o), _t.test(t) && o.reverse()), this.pushStack(o)
      }
    });
    var xt = /[^\x20\t\r\n\f]+/g;
    at.Callbacks = function(t) {
      t = "string" == typeof t ? function(t) {
        var e = {};
        return at.each(t.match(xt) || [], function(t, n) {
          e[n] = !0
        }), e
      }(t) : at.extend({}, t);
      var e, n, i, o, r = [],
        s = [],
        a = -1,
        l = function() {
          for (o = t.once, i = e = !0; s.length; a = -1)
            for (n = s.shift(); ++a < r.length;) !1 === r[a].apply(n[0], n[1]) && t.stopOnFalse && (a = r.length, n = !1);
          t.memory || (n = !1), e = !1, o && (r = n ? [] : "")
        },
        u = {
          add: function() {
            return r && (n && !e && (a = r.length - 1, s.push(n)), function e(n) {
              at.each(n, function(n, i) {
                at.isFunction(i) ? t.unique && u.has(i) || r.push(i) : i && i.length && "string" !== at.type(i) && e(i)
              })
            }(arguments), n && !e && l()), this
          },
          remove: function() {
            return at.each(arguments, function(t, e) {
              for (var n;
                (n = at.inArray(e, r, n)) > -1;) r.splice(n, 1), n <= a && a--
            }), this
          },
          has: function(t) {
            return t ? at.inArray(t, r) > -1 : r.length > 0
          },
          empty: function() {
            return r && (r = []), this
          },
          disable: function() {
            return o = s = [], r = n = "", this
          },
          disabled: function() {
            return !r
          },
          lock: function() {
            return o = s = [], n || e || (r = n = ""), this
          },
          locked: function() {
            return !!o
          },
          fireWith: function(t, n) {
            return o || (n = [t, (n = n || []).slice ? n.slice() : n], s.push(n), e || l()), this
          },
          fire: function() {
            return u.fireWith(this, arguments), this
          },
          fired: function() {
            return !!i
          }
        };
      return u
    }, at.extend({
      Deferred: function(e) {
        var n = [
            ["notify", "progress", at.Callbacks("memory"), at.Callbacks("memory"), 2],
            ["resolve", "done", at.Callbacks("once memory"), at.Callbacks("once memory"), 0, "resolved"],
            ["reject", "fail", at.Callbacks("once memory"), at.Callbacks("once memory"), 1, "rejected"]
          ],
          i = "pending",
          o = {
            state: function() {
              return i
            },
            always: function() {
              return r.done(arguments).fail(arguments), this
            },
            catch: function(t) {
              return o.then(null, t)
            },
            pipe: function() {
              var t = arguments;
              return at.Deferred(function(e) {
                at.each(n, function(n, i) {
                  var o = at.isFunction(t[i[4]]) && t[i[4]];
                  r[i[1]](function() {
                    var t = o && o.apply(this, arguments);
                    t && at.isFunction(t.promise) ? t.promise().progress(e.notify).done(e.resolve).fail(e.reject) : e[i[0] + "With"](this, o ? [t] : arguments)
                  })
                }), t = null
              }).promise()
            },
            then: function(e, i, o) {
              function r(e, n, i, o) {
                return function() {
                  var u = this,
                    c = arguments,
                    d = function() {
                      var t, d;
                      if (!(e < l)) {
                        if ((t = i.apply(u, c)) === n.promise()) throw new TypeError("Thenable self-resolution");
                        d = t && ("object" == typeof t || "function" == typeof t) && t.then, at.isFunction(d) ? o ? d.call(t, r(l, n, s, o), r(l, n, a, o)) : (l++, d.call(t, r(l, n, s, o), r(l, n, a, o), r(l, n, s, n.notifyWith))) : (i !== s && (u = void 0, c = [t]), (o || n.resolveWith)(u, c))
                      }
                    },
                    f = o ? d : function() {
                      try {
                        d()
                      } catch (t) {
                        at.Deferred.exceptionHook && at.Deferred.exceptionHook(t, f.stackTrace), e + 1 >= l && (i !== a && (u = void 0, c = [t]), n.rejectWith(u, c))
                      }
                    };
                  e ? f() : (at.Deferred.getStackHook && (f.stackTrace = at.Deferred.getStackHook()), t.setTimeout(f))
                }
              }
              var l = 0;
              return at.Deferred(function(t) {
                n[0][3].add(r(0, t, at.isFunction(o) ? o : s, t.notifyWith)), n[1][3].add(r(0, t, at.isFunction(e) ? e : s)), n[2][3].add(r(0, t, at.isFunction(i) ? i : a))
              }).promise()
            },
            promise: function(t) {
              return null != t ? at.extend(t, o) : o
            }
          },
          r = {};
        return at.each(n, function(t, e) {
          var s = e[2],
            a = e[5];
          o[e[1]] = s.add, a && s.add(function() {
            i = a
          }, n[3 - t][2].disable, n[0][2].lock), s.add(e[3].fire), r[e[0]] = function() {
            return r[e[0] + "With"](this === r ? void 0 : this, arguments), this
          }, r[e[0] + "With"] = s.fireWith
        }), o.promise(r), e && e.call(r, r), r
      },
      when: function(t) {
        var e = arguments.length,
          n = e,
          i = Array(n),
          o = Z.call(arguments),
          r = at.Deferred(),
          s = function(t) {
            return function(n) {
              i[t] = this, o[t] = arguments.length > 1 ? Z.call(arguments) : n, --e || r.resolveWith(i, o)
            }
          };
        if (e <= 1 && (l(t, r.done(s(n)).resolve, r.reject), "pending" === r.state() || at.isFunction(o[n] && o[n].then))) return r.then();
        for (; n--;) l(o[n], s(n), r.reject);
        return r.promise()
      }
    });
    var Ct = /^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;
    at.Deferred.exceptionHook = function(e, n) {
      t.console && t.console.warn && e && Ct.test(e.name) && t.console.warn("jQuery.Deferred exception: " + e.message, e.stack, n)
    }, at.readyException = function(e) {
      e = e;
      t.setTimeout(function() {
        throw e
      })
    };
    var Et = at.Deferred();
    at.fn.ready = function(t) {
      return Et.then(t).catch(function(t) {
        at.readyException(t)
      }), this
    }, at.extend({
      isReady: !1,
      readyWait: 1,
      holdReady: function(t) {
        t ? at.readyWait++ : at.ready(!0)
      },
      ready: function(t) {
        (!0 === t ? --at.readyWait : at.isReady) || (at.isReady = !0, !0 !== t && --at.readyWait > 0 || Et.resolveWith(K, [at]))
      }
    }), at.ready.then = Et.then, "complete" === K.readyState || "loading" !== K.readyState && !K.documentElement.doScroll ? t.setTimeout(at.ready) : (K.addEventListener("DOMContentLoaded", u), t.addEventListener("load", u));
    var Tt = function(t, e, n, i, o, r, s) {
        var a = 0,
          l = t.length,
          u = null == n;
        if ("object" === at.type(n))
          for (a in o = !0, n) Tt(t, e, a, n[a], !0, r, s);
        else if (void 0 !== i && (o = !0, at.isFunction(i) || (s = !0), u && (s ? (e.call(t, i), e = null) : (u = e, e = function(t, e, n) {
            return u.call(at(t), n)
          })), e))
          for (; a < l; a++) e(t[a], n, s ? i : i.call(t[a], a, e(t[a], n)));
        return o ? t : u ? e.call(t) : l ? e(t[0], n) : r
      },
      St = function(t) {
        return 1 === t.nodeType || 9 === t.nodeType || !+t.nodeType
      };
    c.uid = 1, c.prototype = {
      cache: function(t) {
        var e = t[this.expando];
        return e || (e = {}, St(t) && (t.nodeType ? t[this.expando] = e : Object.defineProperty(t, this.expando, {
          value: e,
          configurable: !0
        }))), e
      },
      set: function(t, e, n) {
        var i, o = this.cache(t);
        if ("string" == typeof e) o[at.camelCase(e)] = n;
        else
          for (i in e) o[at.camelCase(i)] = e[i];
        return o
      },
      get: function(t, e) {
        return void 0 === e ? this.cache(t) : t[this.expando] && t[this.expando][at.camelCase(e)]
      },
      access: function(t, e, n) {
        return void 0 === e || e && "string" == typeof e && void 0 === n ? this.get(t, e) : (this.set(t, e, n), void 0 !== n ? n : e)
      },
      remove: function(t, e) {
        var n, i = t[this.expando];
        if (void 0 !== i) {
          if (void 0 !== e) {
            at.isArray(e) ? e = e.map(at.camelCase) : e = (e = at.camelCase(e)) in i ? [e] : e.match(xt) || [], n = e.length;
            for (; n--;) delete i[e[n]]
          }(void 0 === e || at.isEmptyObject(i)) && (t.nodeType ? t[this.expando] = void 0 : delete t[this.expando])
        }
      },
      hasData: function(t) {
        var e = t[this.expando];
        return void 0 !== e && !at.isEmptyObject(e)
      }
    };
    var kt = new c,
      It = new c,
      At = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
      Dt = /[A-Z]/g;
    at.extend({
      hasData: function(t) {
        return It.hasData(t) || kt.hasData(t)
      },
      data: function(t, e, n) {
        return It.access(t, e, n)
      },
      removeData: function(t, e) {
        It.remove(t, e)
      },
      _data: function(t, e, n) {
        return kt.access(t, e, n)
      },
      _removeData: function(t, e) {
        kt.remove(t, e)
      }
    }), at.fn.extend({
      data: function(t, e) {
        var n, i, o, r = this[0],
          s = r && r.attributes;
        if (void 0 === t) {
          if (this.length && (o = It.get(r), 1 === r.nodeType && !kt.get(r, "hasDataAttrs"))) {
            for (n = s.length; n--;) s[n] && (0 === (i = s[n].name).indexOf("data-") && (i = at.camelCase(i.slice(5)), d(r, i, o[i])));
            kt.set(r, "hasDataAttrs", !0)
          }
          return o
        }
        return "object" == typeof t ? this.each(function() {
          It.set(this, t)
        }) : Tt(this, function(e) {
          var n;
          if (r && void 0 === e) {
            if (void 0 !== (n = It.get(r, t))) return n;
            if (void 0 !== (n = d(r, t))) return n
          } else this.each(function() {
            It.set(this, t, e)
          })
        }, null, e, arguments.length > 1, null, !0)
      },
      removeData: function(t) {
        return this.each(function() {
          It.remove(this, t)
        })
      }
    }), at.extend({
      queue: function(t, e, n) {
        var i;
        if (t) return e = (e || "fx") + "queue", i = kt.get(t, e), n && (!i || at.isArray(n) ? i = kt.access(t, e, at.makeArray(n)) : i.push(n)), i || []
      },
      dequeue: function(t, e) {
        e = e || "fx";
        var n = at.queue(t, e),
          i = n.length,
          o = n.shift(),
          r = at._queueHooks(t, e);
        "inprogress" === o && (o = n.shift(), i--), o && ("fx" === e && n.unshift("inprogress"), delete r.stop, o.call(t, function() {
          at.dequeue(t, e)
        }, r)), !i && r && r.empty.fire()
      },
      _queueHooks: function(t, e) {
        var n = e + "queueHooks";
        return kt.get(t, n) || kt.access(t, n, {
          empty: at.Callbacks("once memory").add(function() {
            kt.remove(t, [e + "queue", n])
          })
        })
      }
    }), at.fn.extend({
      queue: function(t, e) {
        var n = 2;
        return "string" != typeof t && (e = t, t = "fx", n--), arguments.length < n ? at.queue(this[0], t) : void 0 === e ? this : this.each(function() {
          var n = at.queue(this, t, e);
          at._queueHooks(this, t), "fx" === t && "inprogress" !== n[0] && at.dequeue(this, t)
        })
      },
      dequeue: function(t) {
        return this.each(function() {
          at.dequeue(this, t)
        })
      },
      clearQueue: function(t) {
        return this.queue(t || "fx", [])
      },
      promise: function(t, e) {
        var n, i = 1,
          o = at.Deferred(),
          r = this,
          s = this.length,
          a = function() {
            --i || o.resolveWith(r, [r])
          };
        for ("string" != typeof t && (e = t, t = void 0), t = t || "fx"; s--;)(n = kt.get(r[s], t + "queueHooks")) && n.empty && (i++, n.empty.add(a));
        return a(), o.promise(e)
      }
    });
    var Lt = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,
      Ot = new RegExp("^(?:([+-])=|)(" + Lt + ")([a-z%]*)$", "i"),
      Pt = ["Top", "Right", "Bottom", "Left"],
      Nt = function(t, e) {
        return "none" === (t = e || t).style.display || "" === t.style.display && at.contains(t.ownerDocument, t) && "none" === at.css(t, "display")
      },
      jt = function(t, e, n, i) {
        var o, r, s = {};
        for (r in e) s[r] = t.style[r], t.style[r] = e[r];
        for (r in o = n.apply(t, i || []), e) t.style[r] = s[r];
        return o
      },
      zt = {};
    at.fn.extend({
      show: function() {
        return p(this, !0)
      },
      hide: function() {
        return p(this)
      },
      toggle: function(t) {
        return "boolean" == typeof t ? t ? this.show() : this.hide() : this.each(function() {
          Nt(this) ? at(this).show() : at(this).hide()
        })
      }
    });
    var Mt = /^(?:checkbox|radio)$/i,
      $t = /<([a-z][^\/\0>\x20\t\r\n\f]+)/i,
      Rt = /^$|\/(?:java|ecma)script/i,
      Ft = {
        option: [1, "<select multiple='multiple'>", "</select>"],
        thead: [1, "<table>", "</table>"],
        col: [2, "<table><colgroup>", "</colgroup></table>"],
        tr: [2, "<table><tbody>", "</tbody></table>"],
        td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
        _default: [0, "", ""]
      };
    Ft.optgroup = Ft.option, Ft.tbody = Ft.tfoot = Ft.colgroup = Ft.caption = Ft.thead, Ft.th = Ft.td;
    var Bt = /<|&#?\w+;/;
    ! function() {
      var t = K.createDocumentFragment().appendChild(K.createElement("div")),
        e = K.createElement("input");
      e.setAttribute("type", "radio"), e.setAttribute("checked", "checked"), e.setAttribute("name", "t"), t.appendChild(e), st.checkClone = t.cloneNode(!0).cloneNode(!0).lastChild.checked, t.innerHTML = "<textarea>x</textarea>", st.noCloneChecked = !!t.cloneNode(!0).lastChild.defaultValue
    }();
    var Ht = K.documentElement,
      Wt = /^key/,
      qt = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,
      Vt = /^([^.]*)(?:\.(.+)|)/;
    at.event = {
      global: {},
      add: function(t, e, n, i, o) {
        var r, s, a, l, u, c, d, f, h, p, m, g = kt.get(t);
        if (g)
          for (n.handler && (n = (r = n).handler, o = r.selector), o && at.find.matchesSelector(Ht, o), n.guid || (n.guid = at.guid++), (l = g.events) || (l = g.events = {}), (s = g.handle) || (s = g.handle = function(e) {
              return void 0 !== at && at.event.triggered !== e.type ? at.event.dispatch.apply(t, arguments) : void 0
            }), u = (e = (e || "").match(xt) || [""]).length; u--;) h = m = (a = Vt.exec(e[u]) || [])[1], p = (a[2] || "").split(".").sort(), h && (d = at.event.special[h] || {}, h = (o ? d.delegateType : d.bindType) || h, d = at.event.special[h] || {}, c = at.extend({
            type: h,
            origType: m,
            data: i,
            handler: n,
            guid: n.guid,
            selector: o,
            needsContext: o && at.expr.match.needsContext.test(o),
            namespace: p.join(".")
          }, r), (f = l[h]) || ((f = l[h] = []).delegateCount = 0, d.setup && !1 !== d.setup.call(t, i, p, s) || t.addEventListener && t.addEventListener(h, s)), d.add && (d.add.call(t, c), c.handler.guid || (c.handler.guid = n.guid)), o ? f.splice(f.delegateCount++, 0, c) : f.push(c), at.event.global[h] = !0)
      },
      remove: function(t, e, n, i, o) {
        var r, s, a, l, u, c, d, f, h, p, m, g = kt.hasData(t) && kt.get(t);
        if (g && (l = g.events)) {
          for (u = (e = (e || "").match(xt) || [""]).length; u--;)
            if (h = m = (a = Vt.exec(e[u]) || [])[1], p = (a[2] || "").split(".").sort(), h) {
              for (d = at.event.special[h] || {}, f = l[h = (i ? d.delegateType : d.bindType) || h] || [], a = a[2] && new RegExp("(^|\\.)" + p.join("\\.(?:.*\\.|)") + "(\\.|$)"), s = r = f.length; r--;) c = f[r], !o && m !== c.origType || n && n.guid !== c.guid || a && !a.test(c.namespace) || i && i !== c.selector && ("**" !== i || !c.selector) || (f.splice(r, 1), c.selector && f.delegateCount--, d.remove && d.remove.call(t, c));
              s && !f.length && (d.teardown && !1 !== d.teardown.call(t, p, g.handle) || at.removeEvent(t, h, g.handle), delete l[h])
            } else
              for (h in l) at.event.remove(t, h + e[u], n, i, !0);
          at.isEmptyObject(l) && kt.remove(t, "handle events")
        }
      },
      dispatch: function(t) {
        var e, n, i, o, r, s, a = at.event.fix(t),
          l = new Array(arguments.length),
          u = (kt.get(this, "events") || {})[a.type] || [],
          c = at.event.special[a.type] || {};
        for (l[0] = a, e = 1; e < arguments.length; e++) l[e] = arguments[e];
        if (a.delegateTarget = this, !c.preDispatch || !1 !== c.preDispatch.call(this, a)) {
          for (s = at.event.handlers.call(this, a, u), e = 0;
            (o = s[e++]) && !a.isPropagationStopped();)
            for (a.currentTarget = o.elem, n = 0;
              (r = o.handlers[n++]) && !a.isImmediatePropagationStopped();) a.rnamespace && !a.rnamespace.test(r.namespace) || (a.handleObj = r, a.data = r.data, void 0 !== (i = ((at.event.special[r.origType] || {}).handle || r.handler).apply(o.elem, l)) && !1 === (a.result = i) && (a.preventDefault(), a.stopPropagation()));
          return c.postDispatch && c.postDispatch.call(this, a), a.result
        }
      },
      handlers: function(t, e) {
        var n, i, o, r, s, a = [],
          l = e.delegateCount,
          u = t.target;
        if (l && u.nodeType && !("click" === t.type && t.button >= 1))
          for (; u !== this; u = u.parentNode || this)
            if (1 === u.nodeType && ("click" !== t.type || !0 !== u.disabled)) {
              for (r = [], s = {}, n = 0; n < l; n++) void 0 === s[o = (i = e[n]).selector + " "] && (s[o] = i.needsContext ? at(o, this).index(u) > -1 : at.find(o, this, null, [u]).length), s[o] && r.push(i);
              r.length && a.push({
                elem: u,
                handlers: r
              })
            } return u = this, l < e.length && a.push({
          elem: u,
          handlers: e.slice(l)
        }), a
      },
      addProp: function(t, e) {
        Object.defineProperty(at.Event.prototype, t, {
          enumerable: !0,
          configurable: !0,
          get: at.isFunction(e) ? function() {
            if (this.originalEvent) return e(this.originalEvent)
          } : function() {
            if (this.originalEvent) return this.originalEvent[t]
          },
          set: function(e) {
            Object.defineProperty(this, t, {
              enumerable: !0,
              configurable: !0,
              writable: !0,
              value: e
            })
          }
        })
      },
      fix: function(t) {
        return t[at.expando] ? t : new at.Event(t)
      },
      special: {
        load: {
          noBubble: !0
        },
        focus: {
          trigger: function() {
            if (this !== _() && this.focus) return this.focus(), !1
          },
          delegateType: "focusin"
        },
        blur: {
          trigger: function() {
            if (this === _() && this.blur) return this.blur(), !1
          },
          delegateType: "focusout"
        },
        click: {
          trigger: function() {
            if ("checkbox" === this.type && this.click && at.nodeName(this, "input")) return this.click(), !1
          },
          _default: function(t) {
            return at.nodeName(t.target, "a")
          }
        },
        beforeunload: {
          postDispatch: function(t) {
            void 0 !== t.result && t.originalEvent && (t.originalEvent.returnValue = t.result)
          }
        }
      }
    }, at.removeEvent = function(t, e, n) {
      t.removeEventListener && t.removeEventListener(e, n)
    }, at.Event = function(t, e) {
      return this instanceof at.Event ? (t && t.type ? (this.originalEvent = t, this.type = t.type, this.isDefaultPrevented = t.defaultPrevented || void 0 === t.defaultPrevented && !1 === t.returnValue ? y : b, this.target = t.target && 3 === t.target.nodeType ? t.target.parentNode : t.target, this.currentTarget = t.currentTarget, this.relatedTarget = t.relatedTarget) : this.type = t, e && at.extend(this, e), this.timeStamp = t && t.timeStamp || at.now(), void(this[at.expando] = !0)) : new at.Event(t, e)
    }, at.Event.prototype = {
      constructor: at.Event,
      isDefaultPrevented: b,
      isPropagationStopped: b,
      isImmediatePropagationStopped: b,
      isSimulated: !1,
      preventDefault: function() {
        var t = this.originalEvent;
        this.isDefaultPrevented = y, t && !this.isSimulated && t.preventDefault()
      },
      stopPropagation: function() {
        var t = this.originalEvent;
        this.isPropagationStopped = y, t && !this.isSimulated && t.stopPropagation()
      },
      stopImmediatePropagation: function() {
        var t = this.originalEvent;
        this.isImmediatePropagationStopped = y, t && !this.isSimulated && t.stopImmediatePropagation(), this.stopPropagation()
      }
    }, at.each({
      altKey: !0,
      bubbles: !0,
      cancelable: !0,
      changedTouches: !0,
      ctrlKey: !0,
      detail: !0,
      eventPhase: !0,
      metaKey: !0,
      pageX: !0,
      pageY: !0,
      shiftKey: !0,
      view: !0,
      char: !0,
      charCode: !0,
      key: !0,
      keyCode: !0,
      button: !0,
      buttons: !0,
      clientX: !0,
      clientY: !0,
      offsetX: !0,
      offsetY: !0,
      pointerId: !0,
      pointerType: !0,
      screenX: !0,
      screenY: !0,
      targetTouches: !0,
      toElement: !0,
      touches: !0,
      which: function(t) {
        var e = t.button;
        return null == t.which && Wt.test(t.type) ? null != t.charCode ? t.charCode : t.keyCode : !t.which && void 0 !== e && qt.test(t.type) ? 1 & e ? 1 : 2 & e ? 3 : 4 & e ? 2 : 0 : t.which
      }
    }, at.event.addProp), at.each({
      mouseenter: "mouseover",
      mouseleave: "mouseout",
      pointerenter: "pointerover",
      pointerleave: "pointerout"
    }, function(t, e) {
      at.event.special[t] = {
        delegateType: e,
        bindType: e,
        handle: function(t) {
          var n, i = t.relatedTarget,
            o = t.handleObj;
          return i && (i === this || at.contains(this, i)) || (t.type = o.origType, n = o.handler.apply(this, arguments), t.type = e), n
        }
      }
    }), at.fn.extend({
      on: function(t, e, n, i) {
        return w(this, t, e, n, i)
      },
      one: function(t, e, n, i) {
        return w(this, t, e, n, i, 1)
      },
      off: function(t, e, n) {
        var i, o;
        if (t && t.preventDefault && t.handleObj) return i = t.handleObj, at(t.delegateTarget).off(i.namespace ? i.origType + "." + i.namespace : i.origType, i.selector, i.handler), this;
        if ("object" == typeof t) {
          for (o in t) this.off(o, e, t[o]);
          return this
        }
        return !1 !== e && "function" != typeof e || (n = e, e = void 0), !1 === n && (n = b), this.each(function() {
          at.event.remove(this, t, n, e)
        })
      }
    });
    var Ut = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([a-z][^\/\0>\x20\t\r\n\f]*)[^>]*)\/>/gi,
      Qt = /<script|<style|<link/i,
      Gt = /checked\s*(?:[^=]|=\s*.checked.)/i,
      Kt = /^true\/(.*)/,
      Yt = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;
    at.extend({
      htmlPrefilter: function(t) {
        return t.replace(Ut, "<$1></$2>")
      },
      clone: function(t, e, n) {
        var i, o, r, s, a = t.cloneNode(!0),
          l = at.contains(t.ownerDocument, t);
        if (!(st.noCloneChecked || 1 !== t.nodeType && 11 !== t.nodeType || at.isXMLDoc(t)))
          for (s = m(a), i = 0, o = (r = m(t)).length; i < o; i++) S(r[i], s[i]);
        if (e)
          if (n)
            for (r = r || m(t), s = s || m(a), i = 0, o = r.length; i < o; i++) T(r[i], s[i]);
          else T(t, a);
        return (s = m(a, "script")).length > 0 && g(s, !l && m(t, "script")), a
      },
      cleanData: function(t) {
        for (var e, n, i, o = at.event.special, r = 0; void 0 !== (n = t[r]); r++)
          if (St(n)) {
            if (e = n[kt.expando]) {
              if (e.events)
                for (i in e.events) o[i] ? at.event.remove(n, i) : at.removeEvent(n, i, e.handle);
              n[kt.expando] = void 0
            }
            n[It.expando] && (n[It.expando] = void 0)
          }
      }
    }), at.fn.extend({
      detach: function(t) {
        return I(this, t, !0)
      },
      remove: function(t) {
        return I(this, t)
      },
      text: function(t) {
        return Tt(this, function(t) {
          return void 0 === t ? at.text(this) : this.empty().each(function() {
            1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || (this.textContent = t)
          })
        }, null, t, arguments.length)
      },
      append: function() {
        return k(this, arguments, function(t) {
          1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || x(this, t).appendChild(t)
        })
      },
      prepend: function() {
        return k(this, arguments, function(t) {
          if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
            var e = x(this, t);
            e.insertBefore(t, e.firstChild)
          }
        })
      },
      before: function() {
        return k(this, arguments, function(t) {
          this.parentNode && this.parentNode.insertBefore(t, this)
        })
      },
      after: function() {
        return k(this, arguments, function(t) {
          this.parentNode && this.parentNode.insertBefore(t, this.nextSibling)
        })
      },
      empty: function() {
        for (var t, e = 0; null != (t = this[e]); e++) 1 === t.nodeType && (at.cleanData(m(t, !1)), t.textContent = "");
        return this
      },
      clone: function(t, e) {
        return t = null != t && t, e = null == e ? t : e, this.map(function() {
          return at.clone(this, t, e)
        })
      },
      html: function(t) {
        return Tt(this, function(t) {
          var e = this[0] || {},
            n = 0,
            i = this.length;
          if (void 0 === t && 1 === e.nodeType) return e.innerHTML;
          if ("string" == typeof t && !Qt.test(t) && !Ft[($t.exec(t) || ["", ""])[1].toLowerCase()]) {
            t = at.htmlPrefilter(t);
            try {
              for (; n < i; n++) 1 === (e = this[n] || {}).nodeType && (at.cleanData(m(e, !1)), e.innerHTML = t);
              e = 0
            } catch (t) {}
          }
          e && this.empty().append(t)
        }, null, t, arguments.length)
      },
      replaceWith: function() {
        var t = [];
        return k(this, arguments, function(e) {
          var n = this.parentNode;
          at.inArray(this, t) < 0 && (at.cleanData(m(this)), n && n.replaceChild(e, this))
        }, t)
      }
    }), at.each({
      appendTo: "append",
      prependTo: "prepend",
      insertBefore: "before",
      insertAfter: "after",
      replaceAll: "replaceWith"
    }, function(t, e) {
      at.fn[t] = function(t) {
        for (var n, i = [], o = at(t), r = o.length - 1, s = 0; s <= r; s++) n = s === r ? this : this.clone(!0), at(o[s])[e](n), J.apply(i, n.get());
        return this.pushStack(i)
      }
    });
    var Zt = /^margin/,
      Xt = new RegExp("^(" + Lt + ")(?!px)[a-z%]+$", "i"),
      Jt = function(e) {
        var n = e.ownerDocument.defaultView;
        return n && n.opener || (n = t), n.getComputedStyle(e)
      };
    ! function() {
      function e() {
        if (a) {
          a.style.cssText = "box-sizing:border-box;position:relative;display:block;margin:auto;border:1px;padding:1px;top:1%;width:50%", a.innerHTML = "", Ht.appendChild(s);
          var e = t.getComputedStyle(a);
          n = "1%" !== e.top, r = "2px" === e.marginLeft, i = "4px" === e.width, a.style.marginRight = "50%", o = "4px" === e.marginRight, Ht.removeChild(s), a = null
        }
      }
      var n, i, o, r, s = K.createElement("div"),
        a = K.createElement("div");
      a.style && (a.style.backgroundClip = "content-box", a.cloneNode(!0).style.backgroundClip = "", st.clearCloneStyle = "content-box" === a.style.backgroundClip, s.style.cssText = "border:0;width:8px;height:0;top:0;left:-9999px;padding:0;margin-top:1px;position:absolute", s.appendChild(a), at.extend(st, {
        pixelPosition: function() {
          return e(), n
        },
        boxSizingReliable: function() {
          return e(), i
        },
        pixelMarginRight: function() {
          return e(), o
        },
        reliableMarginLeft: function() {
          return e(), r
        }
      }))
    }();
    var te = /^(none|table(?!-c[ea]).+)/,
      ee = {
        position: "absolute",
        visibility: "hidden",
        display: "block"
      },
      ne = {
        letterSpacing: "0",
        fontWeight: "400"
      },
      ie = ["Webkit", "Moz", "ms"],
      oe = K.createElement("div").style;
    at.extend({
      cssHooks: {
        opacity: {
          get: function(t, e) {
            if (e) {
              var n = A(t, "opacity");
              return "" === n ? "1" : n
            }
          }
        }
      },
      cssNumber: {
        animationIterationCount: !0,
        columnCount: !0,
        fillOpacity: !0,
        flexGrow: !0,
        flexShrink: !0,
        fontWeight: !0,
        lineHeight: !0,
        opacity: !0,
        order: !0,
        orphans: !0,
        widows: !0,
        zIndex: !0,
        zoom: !0
      },
      cssProps: {
        float: "cssFloat"
      },
      style: function(t, e, n, i) {
        if (t && 3 !== t.nodeType && 8 !== t.nodeType && t.style) {
          var o, r, s, a = at.camelCase(e),
            l = t.style;
          return e = at.cssProps[a] || (at.cssProps[a] = L(a) || a), s = at.cssHooks[e] || at.cssHooks[a], void 0 === n ? s && "get" in s && void 0 !== (o = s.get(t, !1, i)) ? o : l[e] : ("string" === (r = typeof n) && (o = Ot.exec(n)) && o[1] && (n = f(t, e, o), r = "number"), void(null != n && n == n && ("number" === r && (n += o && o[3] || (at.cssNumber[a] ? "" : "px")), st.clearCloneStyle || "" !== n || 0 !== e.indexOf("background") || (l[e] = "inherit"), s && "set" in s && void 0 === (n = s.set(t, n, i)) || (l[e] = n))))
        }
      },
      css: function(t, e, n, i) {
        var o, r, s, a = at.camelCase(e);
        return e = at.cssProps[a] || (at.cssProps[a] = L(a) || a), (s = at.cssHooks[e] || at.cssHooks[a]) && "get" in s && (o = s.get(t, !0, n)), void 0 === o && (o = A(t, e, i)), "normal" === o && e in ne && (o = ne[e]), "" === n || n ? (r = parseFloat(o), !0 === n || isFinite(r) ? r || 0 : o) : o
      }
    }), at.each(["height", "width"], function(t, e) {
      at.cssHooks[e] = {
        get: function(t, n, i) {
          if (n) return !te.test(at.css(t, "display")) || t.getClientRects().length && t.getBoundingClientRect().width ? N(t, e, i) : jt(t, ee, function() {
            return N(t, e, i)
          })
        },
        set: function(t, n, i) {
          var o, r = i && Jt(t),
            s = i && P(t, e, i, "border-box" === at.css(t, "boxSizing", !1, r), r);
          return s && (o = Ot.exec(n)) && "px" !== (o[3] || "px") && (t.style[e] = n, n = at.css(t, e)), O(0, n, s)
        }
      }
    }), at.cssHooks.marginLeft = D(st.reliableMarginLeft, function(t, e) {
      if (e) return (parseFloat(A(t, "marginLeft")) || t.getBoundingClientRect().left - jt(t, {
        marginLeft: 0
      }, function() {
        return t.getBoundingClientRect().left
      })) + "px"
    }), at.each({
      margin: "",
      padding: "",
      border: "Width"
    }, function(t, e) {
      at.cssHooks[t + e] = {
        expand: function(n) {
          for (var i = 0, o = {}, r = "string" == typeof n ? n.split(" ") : [n]; i < 4; i++) o[t + Pt[i] + e] = r[i] || r[i - 2] || r[0];
          return o
        }
      }, Zt.test(t) || (at.cssHooks[t + e].set = O)
    }), at.fn.extend({
      css: function(t, e) {
        return Tt(this, function(t, e, n) {
          var i, o, r = {},
            s = 0;
          if (at.isArray(e)) {
            for (i = Jt(t), o = e.length; s < o; s++) r[e[s]] = at.css(t, e[s], !1, i);
            return r
          }
          return void 0 !== n ? at.style(t, e, n) : at.css(t, e)
        }, t, e, arguments.length > 1)
      }
    }), at.Tween = j, j.prototype = {
      constructor: j,
      init: function(t, e, n, i, o, r) {
        this.elem = t, this.prop = n, this.easing = o || at.easing._default, this.options = e, this.start = this.now = this.cur(), this.end = i, this.unit = r || (at.cssNumber[n] ? "" : "px")
      },
      cur: function() {
        var t = j.propHooks[this.prop];
        return t && t.get ? t.get(this) : j.propHooks._default.get(this)
      },
      run: function(t) {
        var e, n = j.propHooks[this.prop];
        return this.options.duration ? this.pos = e = at.easing[this.easing](t, this.options.duration * t, 0, 1, this.options.duration) : this.pos = e = t, this.now = (this.end - this.start) * e + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), n && n.set ? n.set(this) : j.propHooks._default.set(this), this
      }
    }, j.prototype.init.prototype = j.prototype, j.propHooks = {
      _default: {
        get: function(t) {
          var e;
          return 1 !== t.elem.nodeType || null != t.elem[t.prop] && null == t.elem.style[t.prop] ? t.elem[t.prop] : (e = at.css(t.elem, t.prop, "")) && "auto" !== e ? e : 0
        },
        set: function(t) {
          at.fx.step[t.prop] ? at.fx.step[t.prop](t) : 1 !== t.elem.nodeType || null == t.elem.style[at.cssProps[t.prop]] && !at.cssHooks[t.prop] ? t.elem[t.prop] = t.now : at.style(t.elem, t.prop, t.now + t.unit)
        }
      }
    }, j.propHooks.scrollTop = j.propHooks.scrollLeft = {
      set: function(t) {
        t.elem.nodeType && t.elem.parentNode && (t.elem[t.prop] = t.now)
      }
    }, at.easing = {
      linear: function(t) {
        return t
      },
      swing: function(t) {
        return .5 - Math.cos(t * Math.PI) / 2
      },
      _default: "swing"
    }, at.fx = j.prototype.init, at.fx.step = {};
    var re, se, ae = /^(?:toggle|show|hide)$/,
      le = /queueHooks$/;
    at.Animation = at.extend(F, {
        tweeners: {
          "*": [function(t, e) {
            var n = this.createTween(t, e);
            return f(n.elem, t, Ot.exec(e), n), n
          }]
        },
        tweener: function(t, e) {
          at.isFunction(t) ? (e = t, t = ["*"]) : t = t.match(xt);
          for (var n, i = 0, o = t.length; i < o; i++) n = t[i], F.tweeners[n] = F.tweeners[n] || [], F.tweeners[n].unshift(e)
        },
        prefilters: [function(t, e, n) {
          var i, o, r, s, a, l, u, c, d = "width" in e || "height" in e,
            f = this,
            h = {},
            m = t.style,
            g = t.nodeType && Nt(t),
            v = kt.get(t, "fxshow");
          for (i in n.queue || (null == (s = at._queueHooks(t, "fx")).unqueued && (s.unqueued = 0, a = s.empty.fire, s.empty.fire = function() {
              s.unqueued || a()
            }), s.unqueued++, f.always(function() {
              f.always(function() {
                s.unqueued--, at.queue(t, "fx").length || s.empty.fire()
              })
            })), e)
            if (o = e[i], ae.test(o)) {
              if (delete e[i], r = r || "toggle" === o, o === (g ? "hide" : "show")) {
                if ("show" !== o || !v || void 0 === v[i]) continue;
                g = !0
              }
              h[i] = v && v[i] || at.style(t, i)
            } if ((l = !at.isEmptyObject(e)) || !at.isEmptyObject(h))
            for (i in d && 1 === t.nodeType && (n.overflow = [m.overflow, m.overflowX, m.overflowY], null == (u = v && v.display) && (u = kt.get(t, "display")), "none" === (c = at.css(t, "display")) && (u ? c = u : (p([t], !0), u = t.style.display || u, c = at.css(t, "display"), p([t]))), ("inline" === c || "inline-block" === c && null != u) && "none" === at.css(t, "float") && (l || (f.done(function() {
                m.display = u
              }), null == u && (c = m.display, u = "none" === c ? "" : c)), m.display = "inline-block")), n.overflow && (m.overflow = "hidden", f.always(function() {
                m.overflow = n.overflow[0], m.overflowX = n.overflow[1], m.overflowY = n.overflow[2]
              })), l = !1, h) l || (v ? "hidden" in v && (g = v.hidden) : v = kt.access(t, "fxshow", {
              display: u
            }), r && (v.hidden = !g), g && p([t], !0), f.done(function() {
              for (i in g || p([t]), kt.remove(t, "fxshow"), h) at.style(t, i, h[i])
            })), l = R(g ? v[i] : 0, i, f), i in v || (v[i] = l.start, g && (l.end = l.start, l.start = 0))
        }],
        prefilter: function(t, e) {
          e ? F.prefilters.unshift(t) : F.prefilters.push(t)
        }
      }), at.speed = function(t, e, n) {
        var i = t && "object" == typeof t ? at.extend({}, t) : {
          complete: n || !n && e || at.isFunction(t) && t,
          duration: t,
          easing: n && e || e && !at.isFunction(e) && e
        };
        return at.fx.off || K.hidden ? i.duration = 0 : "number" != typeof i.duration && (i.duration in at.fx.speeds ? i.duration = at.fx.speeds[i.duration] : i.duration = at.fx.speeds._default), null != i.queue && !0 !== i.queue || (i.queue = "fx"), i.old = i.complete, i.complete = function() {
          at.isFunction(i.old) && i.old.call(this), i.queue && at.dequeue(this, i.queue)
        }, i
      }, at.fn.extend({
        fadeTo: function(t, e, n, i) {
          return this.filter(Nt).css("opacity", 0).show().end().animate({
            opacity: e
          }, t, n, i)
        },
        animate: function(t, e, n, i) {
          var o = at.isEmptyObject(t),
            r = at.speed(e, n, i),
            s = function() {
              var e = F(this, at.extend({}, t), r);
              (o || kt.get(this, "finish")) && e.stop(!0)
            };
          return s.finish = s, o || !1 === r.queue ? this.each(s) : this.queue(r.queue, s)
        },
        stop: function(t, e, n) {
          var i = function(t) {
            var e = t.stop;
            delete t.stop, e(n)
          };
          return "string" != typeof t && (n = e, e = t, t = void 0), e && !1 !== t && this.queue(t || "fx", []), this.each(function() {
            var e = !0,
              o = null != t && t + "queueHooks",
              r = at.timers,
              s = kt.get(this);
            if (o) s[o] && s[o].stop && i(s[o]);
            else
              for (o in s) s[o] && s[o].stop && le.test(o) && i(s[o]);
            for (o = r.length; o--;) r[o].elem !== this || null != t && r[o].queue !== t || (r[o].anim.stop(n), e = !1, r.splice(o, 1));
            !e && n || at.dequeue(this, t)
          })
        },
        finish: function(t) {
          return !1 !== t && (t = t || "fx"), this.each(function() {
            var e, n = kt.get(this),
              i = n[t + "queue"],
              o = n[t + "queueHooks"],
              r = at.timers,
              s = i ? i.length : 0;
            for (n.finish = !0, at.queue(this, t, []), o && o.stop && o.stop.call(this, !0), e = r.length; e--;) r[e].elem === this && r[e].queue === t && (r[e].anim.stop(!0), r.splice(e, 1));
            for (e = 0; e < s; e++) i[e] && i[e].finish && i[e].finish.call(this);
            delete n.finish
          })
        }
      }), at.each(["toggle", "show", "hide"], function(t, e) {
        var n = at.fn[e];
        at.fn[e] = function(t, i, o) {
          return null == t || "boolean" == typeof t ? n.apply(this, arguments) : this.animate($(e, !0), t, i, o)
        }
      }), at.each({
        slideDown: $("show"),
        slideUp: $("hide"),
        slideToggle: $("toggle"),
        fadeIn: {
          opacity: "show"
        },
        fadeOut: {
          opacity: "hide"
        },
        fadeToggle: {
          opacity: "toggle"
        }
      }, function(t, e) {
        at.fn[t] = function(t, n, i) {
          return this.animate(e, t, n, i)
        }
      }), at.timers = [], at.fx.tick = function() {
        var t, e = 0,
          n = at.timers;
        for (re = at.now(); e < n.length; e++)(t = n[e])() || n[e] !== t || n.splice(e--, 1);
        n.length || at.fx.stop(), re = void 0
      }, at.fx.timer = function(t) {
        at.timers.push(t), t() ? at.fx.start() : at.timers.pop()
      }, at.fx.interval = 13, at.fx.start = function() {
        se || (se = t.requestAnimationFrame ? t.requestAnimationFrame(z) : t.setInterval(at.fx.tick, at.fx.interval))
      }, at.fx.stop = function() {
        t.cancelAnimationFrame ? t.cancelAnimationFrame(se) : t.clearInterval(se), se = null
      }, at.fx.speeds = {
        slow: 600,
        fast: 200,
        _default: 400
      }, at.fn.delay = function(e, n) {
        return e = at.fx && at.fx.speeds[e] || e, n = n || "fx", this.queue(n, function(n, i) {
          var o = t.setTimeout(n, e);
          i.stop = function() {
            t.clearTimeout(o)
          }
        })
      },
      function() {
        var t = K.createElement("input"),
          e = K.createElement("select").appendChild(K.createElement("option"));
        t.type = "checkbox", st.checkOn = "" !== t.value, st.optSelected = e.selected, (t = K.createElement("input")).value = "t", t.type = "radio", st.radioValue = "t" === t.value
      }();
    var ue, ce = at.expr.attrHandle;
    at.fn.extend({
      attr: function(t, e) {
        return Tt(this, at.attr, t, e, arguments.length > 1)
      },
      removeAttr: function(t) {
        return this.each(function() {
          at.removeAttr(this, t)
        })
      }
    }), at.extend({
      attr: function(t, e, n) {
        var i, o, r = t.nodeType;
        if (3 !== r && 8 !== r && 2 !== r) return void 0 === t.getAttribute ? at.prop(t, e, n) : (1 === r && at.isXMLDoc(t) || (o = at.attrHooks[e.toLowerCase()] || (at.expr.match.bool.test(e) ? ue : void 0)), void 0 !== n ? null === n ? void at.removeAttr(t, e) : o && "set" in o && void 0 !== (i = o.set(t, n, e)) ? i : (t.setAttribute(e, n + ""), n) : o && "get" in o && null !== (i = o.get(t, e)) ? i : null == (i = at.find.attr(t, e)) ? void 0 : i)
      },
      attrHooks: {
        type: {
          set: function(t, e) {
            if (!st.radioValue && "radio" === e && at.nodeName(t, "input")) {
              var n = t.value;
              return t.setAttribute("type", e), n && (t.value = n), e
            }
          }
        }
      },
      removeAttr: function(t, e) {
        var n, i = 0,
          o = e && e.match(xt);
        if (o && 1 === t.nodeType)
          for (; n = o[i++];) t.removeAttribute(n)
      }
    }), ue = {
      set: function(t, e, n) {
        return !1 === e ? at.removeAttr(t, n) : t.setAttribute(n, n), n
      }
    }, at.each(at.expr.match.bool.source.match(/\w+/g), function(t, e) {
      var n = ce[e] || at.find.attr;
      ce[e] = function(t, e, i) {
        var o, r, s = e.toLowerCase();
        return i || (r = ce[s], ce[s] = o, o = null != n(t, e, i) ? s : null, ce[s] = r), o
      }
    });
    var de = /^(?:input|select|textarea|button)$/i,
      fe = /^(?:a|area)$/i;
    at.fn.extend({
      prop: function(t, e) {
        return Tt(this, at.prop, t, e, arguments.length > 1)
      },
      removeProp: function(t) {
        return this.each(function() {
          delete this[at.propFix[t] || t]
        })
      }
    }), at.extend({
      prop: function(t, e, n) {
        var i, o, r = t.nodeType;
        if (3 !== r && 8 !== r && 2 !== r) return 1 === r && at.isXMLDoc(t) || (e = at.propFix[e] || e, o = at.propHooks[e]), void 0 !== n ? o && "set" in o && void 0 !== (i = o.set(t, n, e)) ? i : t[e] = n : o && "get" in o && null !== (i = o.get(t, e)) ? i : t[e]
      },
      propHooks: {
        tabIndex: {
          get: function(t) {
            var e = at.find.attr(t, "tabindex");
            return e ? parseInt(e, 10) : de.test(t.nodeName) || fe.test(t.nodeName) && t.href ? 0 : -1
          }
        }
      },
      propFix: {
        for: "htmlFor",
        class: "className"
      }
    }), st.optSelected || (at.propHooks.selected = {
      get: function(t) {
        var e = t.parentNode;
        return e && e.parentNode && e.parentNode.selectedIndex, null
      },
      set: function(t) {
        var e = t.parentNode;
        e && (e.selectedIndex, e.parentNode && e.parentNode.selectedIndex)
      }
    }), at.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function() {
      at.propFix[this.toLowerCase()] = this
    }), at.fn.extend({
      addClass: function(t) {
        var e, n, i, o, r, s, a, l = 0;
        if (at.isFunction(t)) return this.each(function(e) {
          at(this).addClass(t.call(this, e, H(this)))
        });
        if ("string" == typeof t && t)
          for (e = t.match(xt) || []; n = this[l++];)
            if (o = H(n), i = 1 === n.nodeType && " " + B(o) + " ") {
              for (s = 0; r = e[s++];) i.indexOf(" " + r + " ") < 0 && (i += r + " ");
              o !== (a = B(i)) && n.setAttribute("class", a)
            } return this
      },
      removeClass: function(t) {
        var e, n, i, o, r, s, a, l = 0;
        if (at.isFunction(t)) return this.each(function(e) {
          at(this).removeClass(t.call(this, e, H(this)))
        });
        if (!arguments.length) return this.attr("class", "");
        if ("string" == typeof t && t)
          for (e = t.match(xt) || []; n = this[l++];)
            if (o = H(n), i = 1 === n.nodeType && " " + B(o) + " ") {
              for (s = 0; r = e[s++];)
                for (; i.indexOf(" " + r + " ") > -1;) i = i.replace(" " + r + " ", " ");
              o !== (a = B(i)) && n.setAttribute("class", a)
            } return this
      },
      toggleClass: function(t, e) {
        var n = typeof t;
        return "boolean" == typeof e && "string" === n ? e ? this.addClass(t) : this.removeClass(t) : at.isFunction(t) ? this.each(function(n) {
          at(this).toggleClass(t.call(this, n, H(this), e), e)
        }) : this.each(function() {
          var e, i, o, r;
          if ("string" === n)
            for (i = 0, o = at(this), r = t.match(xt) || []; e = r[i++];) o.hasClass(e) ? o.removeClass(e) : o.addClass(e);
          else void 0 !== t && "boolean" !== n || ((e = H(this)) && kt.set(this, "__className__", e), this.setAttribute && this.setAttribute("class", e || !1 === t ? "" : kt.get(this, "__className__") || ""))
        })
      },
      hasClass: function(t) {
        var e, n, i = 0;
        for (e = " " + t + " "; n = this[i++];)
          if (1 === n.nodeType && (" " + B(H(n)) + " ").indexOf(e) > -1) return !0;
        return !1
      }
    });
    var he = /\r/g;
    at.fn.extend({
      val: function(t) {
        var e, n, i, o = this[0];
        return arguments.length ? (i = at.isFunction(t), this.each(function(n) {
          var o;
          1 === this.nodeType && (null == (o = i ? t.call(this, n, at(this).val()) : t) ? o = "" : "number" == typeof o ? o += "" : at.isArray(o) && (o = at.map(o, function(t) {
            return null == t ? "" : t + ""
          })), (e = at.valHooks[this.type] || at.valHooks[this.nodeName.toLowerCase()]) && "set" in e && void 0 !== e.set(this, o, "value") || (this.value = o))
        })) : o ? (e = at.valHooks[o.type] || at.valHooks[o.nodeName.toLowerCase()]) && "get" in e && void 0 !== (n = e.get(o, "value")) ? n : "string" == typeof(n = o.value) ? n.replace(he, "") : null == n ? "" : n : void 0
      }
    }), at.extend({
      valHooks: {
        option: {
          get: function(t) {
            var e = at.find.attr(t, "value");
            return null != e ? e : B(at.text(t))
          }
        },
        select: {
          get: function(t) {
            var e, n, i, o = t.options,
              r = t.selectedIndex,
              s = "select-one" === t.type,
              a = s ? null : [],
              l = s ? r + 1 : o.length;
            for (i = r < 0 ? l : s ? r : 0; i < l; i++)
              if (((n = o[i]).selected || i === r) && !n.disabled && (!n.parentNode.disabled || !at.nodeName(n.parentNode, "optgroup"))) {
                if (e = at(n).val(), s) return e;
                a.push(e)
              } return a
          },
          set: function(t, e) {
            for (var n, i, o = t.options, r = at.makeArray(e), s = o.length; s--;)((i = o[s]).selected = at.inArray(at.valHooks.option.get(i), r) > -1) && (n = !0);
            return n || (t.selectedIndex = -1), r
          }
        }
      }
    }), at.each(["radio", "checkbox"], function() {
      at.valHooks[this] = {
        set: function(t, e) {
          if (at.isArray(e)) return t.checked = at.inArray(at(t).val(), e) > -1
        }
      }, st.checkOn || (at.valHooks[this].get = function(t) {
        return null === t.getAttribute("value") ? "on" : t.value
      })
    });
    var pe = /^(?:focusinfocus|focusoutblur)$/;
    at.extend(at.event, {
      trigger: function(e, n, i, o) {
        var r, s, a, l, u, c, d, f = [i || K],
          h = it.call(e, "type") ? e.type : e,
          p = it.call(e, "namespace") ? e.namespace.split(".") : [];
        if (s = a = i = i || K, 3 !== i.nodeType && 8 !== i.nodeType && !pe.test(h + at.event.triggered) && (h.indexOf(".") > -1 && (p = h.split("."), h = p.shift(), p.sort()), u = h.indexOf(":") < 0 && "on" + h, (e = e[at.expando] ? e : new at.Event(h, "object" == typeof e && e)).isTrigger = o ? 2 : 3, e.namespace = p.join("."), e.rnamespace = e.namespace ? new RegExp("(^|\\.)" + p.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, e.result = void 0, e.target || (e.target = i), n = null == n ? [e] : at.makeArray(n, [e]), d = at.event.special[h] || {}, o || !d.trigger || !1 !== d.trigger.apply(i, n))) {
          if (!o && !d.noBubble && !at.isWindow(i)) {
            for (l = d.delegateType || h, pe.test(l + h) || (s = s.parentNode); s; s = s.parentNode) f.push(s), a = s;
            a === (i.ownerDocument || K) && f.push(a.defaultView || a.parentWindow || t)
          }
          for (r = 0;
            (s = f[r++]) && !e.isPropagationStopped();) e.type = r > 1 ? l : d.bindType || h, (c = (kt.get(s, "events") || {})[e.type] && kt.get(s, "handle")) && c.apply(s, n), (c = u && s[u]) && c.apply && St(s) && (e.result = c.apply(s, n), !1 === e.result && e.preventDefault());
          return e.type = h, o || e.isDefaultPrevented() || d._default && !1 !== d._default.apply(f.pop(), n) || !St(i) || u && at.isFunction(i[h]) && !at.isWindow(i) && ((a = i[u]) && (i[u] = null), at.event.triggered = h, i[h](), at.event.triggered = void 0, a && (i[u] = a)), e.result
        }
      },
      simulate: function(t, e, n) {
        var i = at.extend(new at.Event, n, {
          type: t,
          isSimulated: !0
        });
        at.event.trigger(i, null, e)
      }
    }), at.fn.extend({
      trigger: function(t, e) {
        return this.each(function() {
          at.event.trigger(t, e, this)
        })
      },
      triggerHandler: function(t, e) {
        var n = this[0];
        if (n) return at.event.trigger(t, e, n, !0)
      }
    }), at.each("blur focus focusin focusout resize scroll click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup contextmenu".split(" "), function(t, e) {
      at.fn[e] = function(t, n) {
        return arguments.length > 0 ? this.on(e, null, t, n) : this.trigger(e)
      }
    }), at.fn.extend({
      hover: function(t, e) {
        return this.mouseenter(t).mouseleave(e || t)
      }
    }), st.focusin = "onfocusin" in t, st.focusin || at.each({
      focus: "focusin",
      blur: "focusout"
    }, function(t, e) {
      var n = function(t) {
        at.event.simulate(e, t.target, at.event.fix(t))
      };
      at.event.special[e] = {
        setup: function() {
          var i = this.ownerDocument || this,
            o = kt.access(i, e);
          o || i.addEventListener(t, n, !0), kt.access(i, e, (o || 0) + 1)
        },
        teardown: function() {
          var i = this.ownerDocument || this,
            o = kt.access(i, e) - 1;
          o ? kt.access(i, e, o) : (i.removeEventListener(t, n, !0), kt.remove(i, e))
        }
      }
    });
    var me = t.location,
      ge = at.now(),
      ve = /\?/;
    at.parseXML = function(e) {
      var n;
      if (!e || "string" != typeof e) return null;
      try {
        n = (new t.DOMParser).parseFromString(e, "text/xml")
      } catch (t) {
        n = void 0
      }
      return n && !n.getElementsByTagName("parsererror").length || at.error("Invalid XML: " + e), n
    };
    var ye = /\[\]$/,
      be = /\r?\n/g,
      _e = /^(?:submit|button|image|reset|file)$/i,
      we = /^(?:input|select|textarea|keygen)/i;
    at.param = function(t, e) {
      var n, i = [],
        o = function(t, e) {
          var n = at.isFunction(e) ? e() : e;
          i[i.length] = encodeURIComponent(t) + "=" + encodeURIComponent(null == n ? "" : n)
        };
      if (at.isArray(t) || t.jquery && !at.isPlainObject(t)) at.each(t, function() {
        o(this.name, this.value)
      });
      else
        for (n in t) W(n, t[n], e, o);
      return i.join("&")
    }, at.fn.extend({
      serialize: function() {
        return at.param(this.serializeArray())
      },
      serializeArray: function() {
        return this.map(function() {
          var t = at.prop(this, "elements");
          return t ? at.makeArray(t) : this
        }).filter(function() {
          var t = this.type;
          return this.name && !at(this).is(":disabled") && we.test(this.nodeName) && !_e.test(t) && (this.checked || !Mt.test(t))
        }).map(function(t, e) {
          var n = at(this).val();
          return null == n ? null : at.isArray(n) ? at.map(n, function(t) {
            return {
              name: e.name,
              value: t.replace(be, "\r\n")
            }
          }) : {
            name: e.name,
            value: n.replace(be, "\r\n")
          }
        }).get()
      }
    });
    var xe = /%20/g,
      Ce = /#.*$/,
      Ee = /([?&])_=[^&]*/,
      Te = /^(.*?):[ \t]*([^\r\n]*)$/gm,
      Se = /^(?:GET|HEAD)$/,
      ke = /^\/\//,
      Ie = {},
      Ae = {},
      De = "*/".concat("*"),
      Le = K.createElement("a");
    Le.href = me.href, at.extend({
      active: 0,
      lastModified: {},
      etag: {},
      ajaxSettings: {
        url: me.href,
        type: "GET",
        isLocal: /^(?:about|app|app-storage|.+-extension|file|res|widget):$/.test(me.protocol),
        global: !0,
        processData: !0,
        async: !0,
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",
        accepts: {
          "*": De,
          text: "text/plain",
          html: "text/html",
          xml: "application/xml, text/xml",
          json: "application/json, text/javascript"
        },
        contents: {
          xml: /\bxml\b/,
          html: /\bhtml/,
          json: /\bjson\b/
        },
        responseFields: {
          xml: "responseXML",
          text: "responseText",
          json: "responseJSON"
        },
        converters: {
          "* text": String,
          "text html": !0,
          "text json": JSON.parse,
          "text xml": at.parseXML
        },
        flatOptions: {
          url: !0,
          context: !0
        }
      },
      ajaxSetup: function(t, e) {
        return e ? U(U(t, at.ajaxSettings), e) : U(at.ajaxSettings, t)
      },
      ajaxPrefilter: q(Ie),
      ajaxTransport: q(Ae),
      ajax: function(e, n) {
        function i(e, n, i, a) {
          var u, f, h, _, w, x = n;
          c || (c = !0, l && t.clearTimeout(l), o = void 0, s = a || "", C.readyState = e > 0 ? 4 : 0, u = e >= 200 && e < 300 || 304 === e, i && (_ = function(t, e, n) {
            for (var i, o, r, s, a = t.contents, l = t.dataTypes;
              "*" === l[0];) l.shift(), void 0 === i && (i = t.mimeType || e.getResponseHeader("Content-Type"));
            if (i)
              for (o in a)
                if (a[o] && a[o].test(i)) {
                  l.unshift(o);
                  break
                } if (l[0] in n) r = l[0];
            else {
              for (o in n) {
                if (!l[0] || t.converters[o + " " + l[0]]) {
                  r = o;
                  break
                }
                s || (s = o)
              }
              r = r || s
            }
            if (r) return r !== l[0] && l.unshift(r), n[r]
          }(p, C, i)), _ = function(t, e, n, i) {
            var o, r, s, a, l, u = {},
              c = t.dataTypes.slice();
            if (c[1])
              for (s in t.converters) u[s.toLowerCase()] = t.converters[s];
            for (r = c.shift(); r;)
              if (t.responseFields[r] && (n[t.responseFields[r]] = e), !l && i && t.dataFilter && (e = t.dataFilter(e, t.dataType)), l = r, r = c.shift())
                if ("*" === r) r = l;
                else if ("*" !== l && l !== r) {
              if (!(s = u[l + " " + r] || u["* " + r]))
                for (o in u)
                  if ((a = o.split(" "))[1] === r && (s = u[l + " " + a[0]] || u["* " + a[0]])) {
                    !0 === s ? s = u[o] : !0 !== u[o] && (r = a[0], c.unshift(a[1]));
                    break
                  } if (!0 !== s)
                if (s && t.throws) e = s(e);
                else try {
                  e = s(e)
                } catch (t) {
                  return {
                    state: "parsererror",
                    error: s ? t : "No conversion from " + l + " to " + r
                  }
                }
            }
            return {
              state: "success",
              data: e
            }
          }(p, _, C, u), u ? (p.ifModified && ((w = C.getResponseHeader("Last-Modified")) && (at.lastModified[r] = w), (w = C.getResponseHeader("etag")) && (at.etag[r] = w)), 204 === e || "HEAD" === p.type ? x = "nocontent" : 304 === e ? x = "notmodified" : (x = _.state, f = _.data, u = !(h = _.error))) : (h = x, !e && x || (x = "error", e < 0 && (e = 0))), C.status = e, C.statusText = (n || x) + "", u ? v.resolveWith(m, [f, x, C]) : v.rejectWith(m, [C, x, h]), C.statusCode(b), b = void 0, d && g.trigger(u ? "ajaxSuccess" : "ajaxError", [C, p, u ? f : h]), y.fireWith(m, [C, x]), d && (g.trigger("ajaxComplete", [C, p]), --at.active || at.event.trigger("ajaxStop")))
        }
        "object" == typeof e && (n = e, e = void 0), n = n || {};
        var o, r, s, a, l, u, c, d, f, h, p = at.ajaxSetup({}, n),
          m = p.context || p,
          g = p.context && (m.nodeType || m.jquery) ? at(m) : at.event,
          v = at.Deferred(),
          y = at.Callbacks("once memory"),
          b = p.statusCode || {},
          _ = {},
          w = {},
          x = "canceled",
          C = {
            readyState: 0,
            getResponseHeader: function(t) {
              var e;
              if (c) {
                if (!a)
                  for (a = {}; e = Te.exec(s);) a[e[1].toLowerCase()] = e[2];
                e = a[t.toLowerCase()]
              }
              return null == e ? null : e
            },
            getAllResponseHeaders: function() {
              return c ? s : null
            },
            setRequestHeader: function(t, e) {
              return null == c && (t = w[t.toLowerCase()] = w[t.toLowerCase()] || t, _[t] = e), this
            },
            overrideMimeType: function(t) {
              return null == c && (p.mimeType = t), this
            },
            statusCode: function(t) {
              var e;
              if (t)
                if (c) C.always(t[C.status]);
                else
                  for (e in t) b[e] = [b[e], t[e]];
              return this
            },
            abort: function(t) {
              var e = t || x;
              return o && o.abort(e), i(0, e), this
            }
          };
        if (v.promise(C), p.url = ((e || p.url || me.href) + "").replace(ke, me.protocol + "//"), p.type = n.method || n.type || p.method || p.type, p.dataTypes = (p.dataType || "*").toLowerCase().match(xt) || [""], null == p.crossDomain) {
          u = K.createElement("a");
          try {
            u.href = p.url, u.href = u.href, p.crossDomain = Le.protocol + "//" + Le.host != u.protocol + "//" + u.host
          } catch (t) {
            p.crossDomain = !0
          }
        }
        if (p.data && p.processData && "string" != typeof p.data && (p.data = at.param(p.data, p.traditional)), V(Ie, p, n, C), c) return C;
        for (f in (d = at.event && p.global) && 0 == at.active++ && at.event.trigger("ajaxStart"), p.type = p.type.toUpperCase(), p.hasContent = !Se.test(p.type), r = p.url.replace(Ce, ""), p.hasContent ? p.data && p.processData && 0 === (p.contentType || "").indexOf("application/x-www-form-urlencoded") && (p.data = p.data.replace(xe, "+")) : (h = p.url.slice(r.length), p.data && (r += (ve.test(r) ? "&" : "?") + p.data, delete p.data), !1 === p.cache && (r = r.replace(Ee, "$1"), h = (ve.test(r) ? "&" : "?") + "_=" + ge++ + h), p.url = r + h), p.ifModified && (at.lastModified[r] && C.setRequestHeader("If-Modified-Since", at.lastModified[r]), at.etag[r] && C.setRequestHeader("If-None-Match", at.etag[r])), (p.data && p.hasContent && !1 !== p.contentType || n.contentType) && C.setRequestHeader("Content-Type", p.contentType), C.setRequestHeader("Accept", p.dataTypes[0] && p.accepts[p.dataTypes[0]] ? p.accepts[p.dataTypes[0]] + ("*" !== p.dataTypes[0] ? ", " + De + "; q=0.01" : "") : p.accepts["*"]), p.headers) C.setRequestHeader(f, p.headers[f]);
        if (p.beforeSend && (!1 === p.beforeSend.call(m, C, p) || c)) return C.abort();
        if (x = "abort", y.add(p.complete), C.done(p.success), C.fail(p.error), o = V(Ae, p, n, C)) {
          if (C.readyState = 1, d && g.trigger("ajaxSend", [C, p]), c) return C;
          p.async && p.timeout > 0 && (l = t.setTimeout(function() {
            C.abort("timeout")
          }, p.timeout));
          try {
            c = !1, o.send(_, i)
          } catch (t) {
            if (c) throw t;
            i(-1, t)
          }
        } else i(-1, "No Transport");
        return C
      },
      getJSON: function(t, e, n) {
        return at.get(t, e, n, "json")
      },
      getScript: function(t, e) {
        return at.get(t, void 0, e, "script")
      }
    }), at.each(["get", "post"], function(t, e) {
      at[e] = function(t, n, i, o) {
        return at.isFunction(n) && (o = o || i, i = n, n = void 0), at.ajax(at.extend({
          url: t,
          type: e,
          dataType: o,
          data: n,
          success: i
        }, at.isPlainObject(t) && t))
      }
    }), at._evalUrl = function(t) {
      return at.ajax({
        url: t,
        type: "GET",
        dataType: "script",
        cache: !0,
        async: !1,
        global: !1,
        throws: !0
      })
    }, at.fn.extend({
      wrapAll: function(t) {
        var e;
        return this[0] && (at.isFunction(t) && (t = t.call(this[0])), e = at(t, this[0].ownerDocument).eq(0).clone(!0), this[0].parentNode && e.insertBefore(this[0]), e.map(function() {
          for (var t = this; t.firstElementChild;) t = t.firstElementChild;
          return t
        }).append(this)), this
      },
      wrapInner: function(t) {
        return at.isFunction(t) ? this.each(function(e) {
          at(this).wrapInner(t.call(this, e))
        }) : this.each(function() {
          var e = at(this),
            n = e.contents();
          n.length ? n.wrapAll(t) : e.append(t)
        })
      },
      wrap: function(t) {
        var e = at.isFunction(t);
        return this.each(function(n) {
          at(this).wrapAll(e ? t.call(this, n) : t)
        })
      },
      unwrap: function(t) {
        return this.parent(t).not("body").each(function() {
          at(this).replaceWith(this.childNodes)
        }), this
      }
    }), at.expr.pseudos.hidden = function(t) {
      return !at.expr.pseudos.visible(t)
    }, at.expr.pseudos.visible = function(t) {
      return !!(t.offsetWidth || t.offsetHeight || t.getClientRects().length)
    }, at.ajaxSettings.xhr = function() {
      try {
        return new t.XMLHttpRequest
      } catch (t) {}
    };
    var Oe = {
        0: 200,
        1223: 204
      },
      Pe = at.ajaxSettings.xhr();
    st.cors = !!Pe && "withCredentials" in Pe, st.ajax = Pe = !!Pe, at.ajaxTransport(function(e) {
      var n, i;
      if (st.cors || Pe && !e.crossDomain) return {
        send: function(o, r) {
          var s, a = e.xhr();
          if (a.open(e.type, e.url, e.async, e.username, e.password), e.xhrFields)
            for (s in e.xhrFields) a[s] = e.xhrFields[s];
          for (s in e.mimeType && a.overrideMimeType && a.overrideMimeType(e.mimeType), e.crossDomain || o["X-Requested-With"] || (o["X-Requested-With"] = "XMLHttpRequest"), o) a.setRequestHeader(s, o[s]);
          n = function(t) {
            return function() {
              n && (n = i = a.onload = a.onerror = a.onabort = a.onreadystatechange = null, "abort" === t ? a.abort() : "error" === t ? "number" != typeof a.status ? r(0, "error") : r(a.status, a.statusText) : r(Oe[a.status] || a.status, a.statusText, "text" !== (a.responseType || "text") || "string" != typeof a.responseText ? {
                binary: a.response
              } : {
                text: a.responseText
              }, a.getAllResponseHeaders()))
            }
          }, a.onload = n(), i = a.onerror = n("error"), void 0 !== a.onabort ? a.onabort = i : a.onreadystatechange = function() {
            4 === a.readyState && t.setTimeout(function() {
              n && i()
            })
          }, n = n("abort");
          try {
            a.send(e.hasContent && e.data || null)
          } catch (t) {
            if (n) throw t
          }
        },
        abort: function() {
          n && n()
        }
      }
    }), at.ajaxPrefilter(function(t) {
      t.crossDomain && (t.contents.script = !1)
    }), at.ajaxSetup({
      accepts: {
        script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
      },
      contents: {
        script: /\b(?:java|ecma)script\b/
      },
      converters: {
        "text script": function(t) {
          return at.globalEval(t), t
        }
      }
    }), at.ajaxPrefilter("script", function(t) {
      void 0 === t.cache && (t.cache = !1), t.crossDomain && (t.type = "GET")
    }), at.ajaxTransport("script", function(t) {
      var e, n;
      if (t.crossDomain) return {
        send: function(i, o) {
          e = at("<script>").prop({
            charset: t.scriptCharset,
            src: t.url
          }).on("load error", n = function(t) {
            e.remove(), n = null, t && o("error" === t.type ? 404 : 200, t.type)
          }), K.head.appendChild(e[0])
        },
        abort: function() {
          n && n()
        }
      }
    });
    var Ne = [],
      je = /(=)\?(?=&|$)|\?\?/;
    at.ajaxSetup({
      jsonp: "callback",
      jsonpCallback: function() {
        var t = Ne.pop() || at.expando + "_" + ge++;
        return this[t] = !0, t
      }
    }), at.ajaxPrefilter("json jsonp", function(e, n, i) {
      var o, r, s, a = !1 !== e.jsonp && (je.test(e.url) ? "url" : "string" == typeof e.data && 0 === (e.contentType || "").indexOf("application/x-www-form-urlencoded") && je.test(e.data) && "data");
      if (a || "jsonp" === e.dataTypes[0]) return o = e.jsonpCallback = at.isFunction(e.jsonpCallback) ? e.jsonpCallback() : e.jsonpCallback, a ? e[a] = e[a].replace(je, "$1" + o) : !1 !== e.jsonp && (e.url += (ve.test(e.url) ? "&" : "?") + e.jsonp + "=" + o), e.converters["script json"] = function() {
        return s || at.error(o + " was not called"), s[0]
      }, e.dataTypes[0] = "json", r = t[o], t[o] = function() {
        s = arguments
      }, i.always(function() {
        void 0 === r ? at(t).removeProp(o) : t[o] = r, e[o] && (e.jsonpCallback = n.jsonpCallback, Ne.push(o)), s && at.isFunction(r) && r(s[0]), s = r = void 0
      }), "script"
    }), st.createHTMLDocument = function() {
      var t = K.implementation.createHTMLDocument("").body;
      return t.innerHTML = "<form></form><form></form>", 2 === t.childNodes.length
    }(), at.parseHTML = function(t, e, n) {
      return "string" != typeof t ? [] : ("boolean" == typeof e && (n = e, e = !1), e || (st.createHTMLDocument ? ((i = (e = K.implementation.createHTMLDocument("")).createElement("base")).href = K.location.href, e.head.appendChild(i)) : e = K), r = !n && [], (o = gt.exec(t)) ? [e.createElement(o[1])] : (o = v([t], e, r), r && r.length && at(r).remove(), at.merge([], o.childNodes)));
      var i, o, r
    }, at.fn.load = function(t, e, n) {
      var i, o, r, s = this,
        a = t.indexOf(" ");
      return a > -1 && (i = B(t.slice(a)), t = t.slice(0, a)), at.isFunction(e) ? (n = e, e = void 0) : e && "object" == typeof e && (o = "POST"), s.length > 0 && at.ajax({
        url: t,
        type: o || "GET",
        dataType: "html",
        data: e
      }).done(function(t) {
        r = arguments, s.html(i ? at("<div>").append(at.parseHTML(t)).find(i) : t)
      }).always(n && function(t, e) {
        s.each(function() {
          n.apply(this, r || [t.responseText, e, t])
        })
      }), this
    }, at.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function(t, e) {
      at.fn[e] = function(t) {
        return this.on(e, t)
      }
    }), at.expr.pseudos.animated = function(t) {
      return at.grep(at.timers, function(e) {
        return t === e.elem
      }).length
    }, at.offset = {
      setOffset: function(t, e, n) {
        var i, o, r, s, a, l, u = at.css(t, "position"),
          c = at(t),
          d = {};
        "static" === u && (t.style.position = "relative"), a = c.offset(), r = at.css(t, "top"), l = at.css(t, "left"), ("absolute" === u || "fixed" === u) && (r + l).indexOf("auto") > -1 ? (s = (i = c.position()).top, o = i.left) : (s = parseFloat(r) || 0, o = parseFloat(l) || 0), at.isFunction(e) && (e = e.call(t, n, at.extend({}, a))), null != e.top && (d.top = e.top - a.top + s), null != e.left && (d.left = e.left - a.left + o), "using" in e ? e.using.call(t, d) : c.css(d)
      }
    }, at.fn.extend({
      offset: function(t) {
        if (arguments.length) return void 0 === t ? this : this.each(function(e) {
          at.offset.setOffset(this, t, e)
        });
        var e, n, i, o, r = this[0];
        return r ? r.getClientRects().length ? (i = r.getBoundingClientRect()).width || i.height ? (n = Q(o = r.ownerDocument), e = o.documentElement, {
          top: i.top + n.pageYOffset - e.clientTop,
          left: i.left + n.pageXOffset - e.clientLeft
        }) : i : {
          top: 0,
          left: 0
        } : void 0
      },
      position: function() {
        if (this[0]) {
          var t, e, n = this[0],
            i = {
              top: 0,
              left: 0
            };
          return "fixed" === at.css(n, "position") ? e = n.getBoundingClientRect() : (t = this.offsetParent(), e = this.offset(), at.nodeName(t[0], "html") || (i = t.offset()), i = {
            top: i.top + at.css(t[0], "borderTopWidth", !0),
            left: i.left + at.css(t[0], "borderLeftWidth", !0)
          }), {
            top: e.top - i.top - at.css(n, "marginTop", !0),
            left: e.left - i.left - at.css(n, "marginLeft", !0)
          }
        }
      },
      offsetParent: function() {
        return this.map(function() {
          for (var t = this.offsetParent; t && "static" === at.css(t, "position");) t = t.offsetParent;
          return t || Ht
        })
      }
    }), at.each({
      scrollLeft: "pageXOffset",
      scrollTop: "pageYOffset"
    }, function(t, e) {
      var n = "pageYOffset" === e;
      at.fn[t] = function(i) {
        return Tt(this, function(t, i, o) {
          var r = Q(t);
          return void 0 === o ? r ? r[e] : t[i] : void(r ? r.scrollTo(n ? r.pageXOffset : o, n ? o : r.pageYOffset) : t[i] = o)
        }, t, i, arguments.length)
      }
    }), at.each(["top", "left"], function(t, e) {
      at.cssHooks[e] = D(st.pixelPosition, function(t, n) {
        if (n) return n = A(t, e), Xt.test(n) ? at(t).position()[e] + "px" : n
      })
    }), at.each({
      Height: "height",
      Width: "width"
    }, function(t, e) {
      at.each({
        padding: "inner" + t,
        content: e,
        "": "outer" + t
      }, function(n, i) {
        at.fn[i] = function(o, r) {
          var s = arguments.length && (n || "boolean" != typeof o),
            a = n || (!0 === o || !0 === r ? "margin" : "border");
          return Tt(this, function(e, n, o) {
            var r;
            return at.isWindow(e) ? 0 === i.indexOf("outer") ? e["inner" + t] : e.document.documentElement["client" + t] : 9 === e.nodeType ? (r = e.documentElement, Math.max(e.body["scroll" + t], r["scroll" + t], e.body["offset" + t], r["offset" + t], r["client" + t])) : void 0 === o ? at.css(e, n, a) : at.style(e, n, o, a)
          }, e, s ? o : void 0, s)
        }
      })
    }), at.fn.extend({
      bind: function(t, e, n) {
        return this.on(t, null, e, n)
      },
      unbind: function(t, e) {
        return this.off(t, null, e)
      },
      delegate: function(t, e, n, i) {
        return this.on(e, t, n, i)
      },
      undelegate: function(t, e, n) {
        return 1 === arguments.length ? this.off(t, "**") : this.off(e, t || "**", n)
      }
    }), at.parseJSON = JSON.parse, "function" == typeof define && define.amd && define("jquery", [], function() {
      return at
    });
    var ze = t.jQuery,
      Me = t.$;
    return at.noConflict = function(e) {
      return t.$ === at && (t.$ = Me), e && t.jQuery === at && (t.jQuery = ze), at
    }, e || (t.jQuery = t.$ = at), at
  }), function(t, e) {
    "object" == typeof exports && "undefined" != typeof module ? module.exports = e() : "function" == typeof define && define.amd ? define(e) : t.Popper = e()
  }(this, function() {
    "use strict";

    function t(t) {
      return t && "[object Function]" === {}.toString.call(t)
    }

    function e(t, e) {
      if (1 !== t.nodeType) return [];
      var n = window.getComputedStyle(t, null);
      return e ? n[e] : n
    }

    function n(t) {
      return "HTML" === t.nodeName ? t : t.parentNode || t.host
    }

    function i(t) {
      if (!t || -1 !== ["HTML", "BODY", "#document"].indexOf(t.nodeName)) return window.document.body;
      var o = e(t),
        r = o.overflow,
        s = o.overflowX,
        a = o.overflowY;
      return /(auto|scroll)/.test(r + a + s) ? t : i(n(t))
    }

    function o(t) {
      var n = t && t.offsetParent,
        i = n && n.nodeName;
      return i && "BODY" !== i && "HTML" !== i ? -1 !== ["TD", "TABLE"].indexOf(n.nodeName) && "static" === e(n, "position") ? o(n) : n : window.document.documentElement
    }

    function r(t) {
      return null === t.parentNode ? t : r(t.parentNode)
    }

    function s(t, e) {
      if (!(t && t.nodeType && e && e.nodeType)) return window.document.documentElement;
      var n = t.compareDocumentPosition(e) & Node.DOCUMENT_POSITION_FOLLOWING,
        i = n ? t : e,
        a = n ? e : t,
        l = document.createRange();
      l.setStart(i, 0), l.setEnd(a, 0);
      var u = l.commonAncestorContainer;
      if (t !== u && e !== u || i.contains(a)) return function(t) {
        var e = t.nodeName;
        return "BODY" !== e && ("HTML" === e || o(t.firstElementChild) === t)
      }(u) ? u : o(u);
      var c = r(t);
      return c.host ? s(c.host, e) : s(t, r(e).host)
    }

    function a(t) {
      var e = "top" === (1 < arguments.length && void 0 !== arguments[1] ? arguments[1] : "top") ? "scrollTop" : "scrollLeft",
        n = t.nodeName;
      if ("BODY" === n || "HTML" === n) {
        var i = window.document.documentElement;
        return (window.document.scrollingElement || i)[e]
      }
      return t[e]
    }

    function l(t, e) {
      var n = 2 < arguments.length && void 0 !== arguments[2] && arguments[2],
        i = a(e, "top"),
        o = a(e, "left"),
        r = n ? -1 : 1;
      return t.top += i * r, t.bottom += i * r, t.left += o * r, t.right += o * r, t
    }

    function u(t, e) {
      var n = "x" === e ? "Left" : "Top",
        i = "Left" == n ? "Right" : "Bottom";
      return +t["border" + n + "Width"].split("px")[0] + +t["border" + i + "Width"].split("px")[0]
    }

    function c(t, e, n, i) {
      return M(e["offset" + t], n["client" + t], n["offset" + t], V() ? n["offset" + t] + i["margin" + ("Height" === t ? "Top" : "Left")] + i["margin" + ("Height" === t ? "Bottom" : "Right")] : 0)
    }

    function d() {
      var t = window.document.body,
        e = window.document.documentElement,
        n = V() && window.getComputedStyle(e);
      return {
        height: c("Height", t, e, n),
        width: c("Width", t, e, n)
      }
    }

    function f(t) {
      return K({}, t, {
        right: t.left + t.width,
        bottom: t.top + t.height
      })
    }

    function h(t) {
      var n = {};
      if (V()) try {
        n = t.getBoundingClientRect();
        var i = a(t, "top"),
          o = a(t, "left");
        n.top += i, n.left += o, n.bottom += i, n.right += o
      } catch (t) {} else n = t.getBoundingClientRect();
      var r = {
          left: n.left,
          top: n.top,
          width: n.right - n.left,
          height: n.bottom - n.top
        },
        s = "HTML" === t.nodeName ? d() : {},
        l = s.width || t.clientWidth || r.right - r.left,
        c = s.height || t.clientHeight || r.bottom - r.top,
        h = t.offsetWidth - l,
        p = t.offsetHeight - c;
      if (h || p) {
        var m = e(t);
        h -= u(m, "x"), p -= u(m, "y"), r.width -= h, r.height -= p
      }
      return f(r)
    }

    function p(t, n) {
      var o = V(),
        r = "HTML" === n.nodeName,
        s = h(t),
        a = h(n),
        u = i(t),
        c = e(n),
        d = +c.borderTopWidth.split("px")[0],
        p = +c.borderLeftWidth.split("px")[0],
        m = f({
          top: s.top - a.top - d,
          left: s.left - a.left - p,
          width: s.width,
          height: s.height
        });
      if (m.marginTop = 0, m.marginLeft = 0, !o && r) {
        var g = +c.marginTop.split("px")[0],
          v = +c.marginLeft.split("px")[0];
        m.top -= d - g, m.bottom -= d - g, m.left -= p - v, m.right -= p - v, m.marginTop = g, m.marginLeft = v
      }
      return (o ? n.contains(u) : n === u && "BODY" !== u.nodeName) && (m = l(m, n)), m
    }

    function m(t) {
      var e = window.document.documentElement,
        n = p(t, e),
        i = M(e.clientWidth, window.innerWidth || 0),
        o = M(e.clientHeight, window.innerHeight || 0),
        r = a(e),
        s = a(e, "left");
      return f({
        top: r - n.top + n.marginTop,
        left: s - n.left + n.marginLeft,
        width: i,
        height: o
      })
    }

    function g(t) {
      var i = t.nodeName;
      return "BODY" !== i && "HTML" !== i && ("fixed" === e(t, "position") || g(n(t)))
    }

    function v(t, e, o, r) {
      var a = {
          top: 0,
          left: 0
        },
        l = s(t, e);
      if ("viewport" === r) a = m(l);
      else {
        var u;
        "scrollParent" === r ? "BODY" === (u = i(n(t))).nodeName && (u = window.document.documentElement) : u = "window" === r ? window.document.documentElement : r;
        var c = p(u, l);
        if ("HTML" !== u.nodeName || g(l)) a = c;
        else {
          var f = d(),
            h = f.height,
            v = f.width;
          a.top += c.top - c.marginTop, a.bottom = h + c.top, a.left += c.left - c.marginLeft, a.right = v + c.left
        }
      }
      return a.left += o, a.top += o, a.right -= o, a.bottom -= o, a
    }

    function y(t) {
      return t.width * t.height
    }

    function b(t, e, n, i, o) {
      var r = 5 < arguments.length && void 0 !== arguments[5] ? arguments[5] : 0;
      if (-1 === t.indexOf("auto")) return t;
      var s = v(n, i, r, o),
        a = {
          top: {
            width: s.width,
            height: e.top - s.top
          },
          right: {
            width: s.right - e.right,
            height: s.height
          },
          bottom: {
            width: s.width,
            height: s.bottom - e.bottom
          },
          left: {
            width: e.left - s.left,
            height: s.height
          }
        },
        l = Object.keys(a).map(function(t) {
          return K({
            key: t
          }, a[t], {
            area: y(a[t])
          })
        }).sort(function(t, e) {
          return e.area - t.area
        }),
        u = l.filter(function(t) {
          var e = t.width,
            i = t.height;
          return e >= n.clientWidth && i >= n.clientHeight
        }),
        c = 0 < u.length ? u[0].key : l[0].key,
        d = t.split("-")[1];
      return c + (d ? "-" + d : "")
    }

    function _(t, e, n) {
      return p(n, s(e, n))
    }

    function w(t) {
      var e = window.getComputedStyle(t),
        n = parseFloat(e.marginTop) + parseFloat(e.marginBottom),
        i = parseFloat(e.marginLeft) + parseFloat(e.marginRight);
      return {
        width: t.offsetWidth + i,
        height: t.offsetHeight + n
      }
    }

    function x(t) {
      var e = {
        left: "right",
        right: "left",
        bottom: "top",
        top: "bottom"
      };
      return t.replace(/left|right|bottom|top/g, function(t) {
        return e[t]
      })
    }

    function C(t, e, n) {
      n = n.split("-")[0];
      var i = w(t),
        o = {
          width: i.width,
          height: i.height
        },
        r = -1 !== ["right", "left"].indexOf(n),
        s = r ? "top" : "left",
        a = r ? "left" : "top",
        l = r ? "height" : "width",
        u = r ? "width" : "height";
      return o[s] = e[s] + e[l] / 2 - i[l] / 2, o[a] = n === a ? e[a] - i[u] : e[x(a)], o
    }

    function E(t, e) {
      return Array.prototype.find ? t.find(e) : t.filter(e)[0]
    }

    function T(e, n, i) {
      return (void 0 === i ? e : e.slice(0, function(t, e, n) {
        if (Array.prototype.findIndex) return t.findIndex(function(t) {
          return t[e] === n
        });
        var i = E(t, function(t) {
          return t[e] === n
        });
        return t.indexOf(i)
      }(e, "name", i))).forEach(function(e) {
        e.function && console.warn("`modifier.function` is deprecated, use `modifier.fn`!");
        var i = e.function || e.fn;
        e.enabled && t(i) && (n.offsets.popper = f(n.offsets.popper), n.offsets.reference = f(n.offsets.reference), n = i(n, e))
      }), n
    }

    function S(t, e) {
      return t.some(function(t) {
        var n = t.name;
        return t.enabled && n === e
      })
    }

    function k(t) {
      for (var e = [!1, "ms", "Webkit", "Moz", "O"], n = t.charAt(0).toUpperCase() + t.slice(1), i = 0; i < e.length - 1; i++) {
        var o = e[i],
          r = o ? "" + o + n : t;
        if (void 0 !== window.document.body.style[r]) return r
      }
      return null
    }

    function I(t, e, n, o) {
      n.updateBound = o, window.addEventListener("resize", n.updateBound, {
        passive: !0
      });
      var r = i(t);
      return function t(e, n, o, r) {
        var s = "BODY" === e.nodeName,
          a = s ? window : e;
        a.addEventListener(n, o, {
          passive: !0
        }), s || t(i(a.parentNode), n, o, r), r.push(a)
      }(r, "scroll", n.updateBound, n.scrollParents), n.scrollElement = r, n.eventsEnabled = !0, n
    }

    function A() {
      var t;
      this.state.eventsEnabled && (window.cancelAnimationFrame(this.scheduleUpdate), this.state = (this.reference, t = this.state, window.removeEventListener("resize", t.updateBound), t.scrollParents.forEach(function(e) {
        e.removeEventListener("scroll", t.updateBound)
      }), t.updateBound = null, t.scrollParents = [], t.scrollElement = null, t.eventsEnabled = !1, t))
    }

    function D(t) {
      return "" !== t && !isNaN(parseFloat(t)) && isFinite(t)
    }

    function L(t, e) {
      Object.keys(e).forEach(function(n) {
        var i = ""; - 1 !== ["width", "height", "top", "right", "bottom", "left"].indexOf(n) && D(e[n]) && (i = "px"), t.style[n] = e[n] + i
      })
    }

    function O(t, e, n) {
      var i = E(t, function(t) {
          return t.name === e
        }),
        o = !!i && t.some(function(t) {
          return t.name === n && t.enabled && t.order < i.order
        });
      if (!o) {
        var r = "`" + e + "`";
        console.warn("`" + n + "` modifier is required by " + r + " modifier in order to work, be sure to include it before " + r + "!")
      }
      return o
    }

    function P(t) {
      var e = 1 < arguments.length && void 0 !== arguments[1] && arguments[1],
        n = Z.indexOf(t),
        i = Z.slice(n + 1).concat(Z.slice(0, n));
      return e ? i.reverse() : i
    }

    function N(t, e, n, i) {
      var o = [0, 0],
        r = -1 !== ["right", "left"].indexOf(i),
        s = t.split(/(\+|\-)/).map(function(t) {
          return t.trim()
        }),
        a = s.indexOf(E(s, function(t) {
          return -1 !== t.search(/,|\s/)
        }));
      s[a] && -1 === s[a].indexOf(",") && console.warn("Offsets separated by white space(s) are deprecated, use a comma (,) instead.");
      var l = /\s*,\s*|\s+/,
        u = -1 === a ? [s] : [s.slice(0, a).concat([s[a].split(l)[0]]), [s[a].split(l)[1]].concat(s.slice(a + 1))];
      return (u = u.map(function(t, i) {
        var o = (1 === i ? !r : r) ? "height" : "width",
          s = !1;
        return t.reduce(function(t, e) {
          return "" === t[t.length - 1] && -1 !== ["+", "-"].indexOf(e) ? (t[t.length - 1] = e, s = !0, t) : s ? (t[t.length - 1] += e, s = !1, t) : t.concat(e)
        }, []).map(function(t) {
          return function(t, e, n, i) {
            var o = t.match(/((?:\-|\+)?\d*\.?\d*)(.*)/),
              r = +o[1],
              s = o[2];
            if (!r) return t;
            if (0 === s.indexOf("%")) {
              var a;
              switch (s) {
                case "%p":
                  a = n;
                  break;
                case "%":
                case "%r":
                default:
                  a = i
              }
              return f(a)[e] / 100 * r
            }
            return "vh" === s || "vw" === s ? ("vh" === s ? M(document.documentElement.clientHeight, window.innerHeight || 0) : M(document.documentElement.clientWidth, window.innerWidth || 0)) / 100 * r : r
          }(t, o, e, n)
        })
      })).forEach(function(t, e) {
        t.forEach(function(n, i) {
          D(n) && (o[e] += n * ("-" === t[i - 1] ? -1 : 1))
        })
      }), o
    }
    for (var j = Math.min, z = Math.floor, M = Math.max, $ = ["native code", "[object MutationObserverConstructor]"], R = "undefined" != typeof window, F = ["Edge", "Trident", "Firefox"], B = 0, H = 0; H < F.length; H += 1)
      if (R && 0 <= navigator.userAgent.indexOf(F[H])) {
        B = 1;
        break
      } var W, q = R && function(t) {
        return $.some(function(e) {
          return -1 < (t || "").toString().indexOf(e)
        })
      }(window.MutationObserver) ? function(t) {
        var e = !1,
          n = 0,
          i = document.createElement("span");
        return new MutationObserver(function() {
            t(), e = !1
          }).observe(i, {
            attributes: !0
          }),
          function() {
            e || (e = !0, i.setAttribute("x-index", n), ++n)
          }
      } : function(t) {
        var e = !1;
        return function() {
          e || (e = !0, setTimeout(function() {
            e = !1, t()
          }, B))
        }
      },
      V = function() {
        return null == W && (W = -1 !== navigator.appVersion.indexOf("MSIE 10")), W
      },
      U = function(t, e) {
        if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function")
      },
      Q = function() {
        function t(t, e) {
          for (var n, i = 0; i < e.length; i++)(n = e[i]).enumerable = n.enumerable || !1, n.configurable = !0, "value" in n && (n.writable = !0), Object.defineProperty(t, n.key, n)
        }
        return function(e, n, i) {
          return n && t(e.prototype, n), i && t(e, i), e
        }
      }(),
      G = function(t, e, n) {
        return e in t ? Object.defineProperty(t, e, {
          value: n,
          enumerable: !0,
          configurable: !0,
          writable: !0
        }) : t[e] = n, t
      },
      K = Object.assign || function(t) {
        for (var e, n = 1; n < arguments.length; n++)
          for (var i in e = arguments[n]) Object.prototype.hasOwnProperty.call(e, i) && (t[i] = e[i]);
        return t
      },
      Y = ["auto-start", "auto", "auto-end", "top-start", "top", "top-end", "right-start", "right", "right-end", "bottom-end", "bottom", "bottom-start", "left-end", "left", "left-start"],
      Z = Y.slice(3),
      X = "flip",
      J = "clockwise",
      tt = "counterclockwise",
      et = function() {
        function e(n, i) {
          var o = this,
            r = 2 < arguments.length && void 0 !== arguments[2] ? arguments[2] : {};
          U(this, e), this.scheduleUpdate = function() {
            return requestAnimationFrame(o.update)
          }, this.update = q(this.update.bind(this)), this.options = K({}, e.Defaults, r), this.state = {
            isDestroyed: !1,
            isCreated: !1,
            scrollParents: []
          }, this.reference = n.jquery ? n[0] : n, this.popper = i.jquery ? i[0] : i, this.options.modifiers = {}, Object.keys(K({}, e.Defaults.modifiers, r.modifiers)).forEach(function(t) {
            o.options.modifiers[t] = K({}, e.Defaults.modifiers[t] || {}, r.modifiers ? r.modifiers[t] : {})
          }), this.modifiers = Object.keys(this.options.modifiers).map(function(t) {
            return K({
              name: t
            }, o.options.modifiers[t])
          }).sort(function(t, e) {
            return t.order - e.order
          }), this.modifiers.forEach(function(e) {
            e.enabled && t(e.onLoad) && e.onLoad(o.reference, o.popper, o.options, e, o.state)
          }), this.update();
          var s = this.options.eventsEnabled;
          s && this.enableEventListeners(), this.state.eventsEnabled = s
        }
        return Q(e, [{
          key: "update",
          value: function() {
            return function() {
              if (!this.state.isDestroyed) {
                var t = {
                  instance: this,
                  styles: {},
                  attributes: {},
                  flipped: !1,
                  offsets: {}
                };
                t.offsets.reference = _(this.state, this.popper, this.reference), t.placement = b(this.options.placement, t.offsets.reference, this.popper, this.reference, this.options.modifiers.flip.boundariesElement, this.options.modifiers.flip.padding), t.originalPlacement = t.placement, t.offsets.popper = C(this.popper, t.offsets.reference, t.placement), t.offsets.popper.position = "absolute", t = T(this.modifiers, t), this.state.isCreated ? this.options.onUpdate(t) : (this.state.isCreated = !0, this.options.onCreate(t))
              }
            }.call(this)
          }
        }, {
          key: "destroy",
          value: function() {
            return function() {
              return this.state.isDestroyed = !0, S(this.modifiers, "applyStyle") && (this.popper.removeAttribute("x-placement"), this.popper.style.left = "", this.popper.style.position = "", this.popper.style.top = "", this.popper.style[k("transform")] = ""), this.disableEventListeners(), this.options.removeOnDestroy && this.popper.parentNode.removeChild(this.popper), this
            }.call(this)
          }
        }, {
          key: "enableEventListeners",
          value: function() {
            return function() {
              this.state.eventsEnabled || (this.state = I(this.reference, this.options, this.state, this.scheduleUpdate))
            }.call(this)
          }
        }, {
          key: "disableEventListeners",
          value: function() {
            return A.call(this)
          }
        }]), e
      }();
    return et.Utils = ("undefined" == typeof window ? global : window).PopperUtils, et.placements = Y, et.Defaults = {
      placement: "bottom",
      eventsEnabled: !0,
      removeOnDestroy: !1,
      onCreate: function() {},
      onUpdate: function() {},
      modifiers: {
        shift: {
          order: 100,
          enabled: !0,
          fn: function(t) {
            var e = t.placement,
              n = e.split("-")[0],
              i = e.split("-")[1];
            if (i) {
              var o = t.offsets,
                r = o.reference,
                s = o.popper,
                a = -1 !== ["bottom", "top"].indexOf(n),
                l = a ? "left" : "top",
                u = a ? "width" : "height",
                c = {
                  start: G({}, l, r[l]),
                  end: G({}, l, r[l] + r[u] - s[u])
                };
              t.offsets.popper = K({}, s, c[i])
            }
            return t
          }
        },
        offset: {
          order: 200,
          enabled: !0,
          fn: function(t, e) {
            var n, i = e.offset,
              o = t.placement,
              r = t.offsets,
              s = r.popper,
              a = r.reference,
              l = o.split("-")[0];
            return n = D(+i) ? [+i, 0] : N(i, s, a, l), "left" === l ? (s.top += n[0], s.left -= n[1]) : "right" === l ? (s.top += n[0], s.left += n[1]) : "top" === l ? (s.left += n[0], s.top -= n[1]) : "bottom" === l && (s.left += n[0], s.top += n[1]), t.popper = s, t
          },
          offset: 0
        },
        preventOverflow: {
          order: 300,
          enabled: !0,
          fn: function(t, e) {
            var n = e.boundariesElement || o(t.instance.popper);
            t.instance.reference === n && (n = o(n));
            var i = v(t.instance.popper, t.instance.reference, e.padding, n);
            e.boundaries = i;
            var r = e.priority,
              s = t.offsets.popper,
              a = {
                primary: function(t) {
                  var n = s[t];
                  return s[t] < i[t] && !e.escapeWithReference && (n = M(s[t], i[t])), G({}, t, n)
                },
                secondary: function(t) {
                  var n = "right" === t ? "left" : "top",
                    o = s[n];
                  return s[t] > i[t] && !e.escapeWithReference && (o = j(s[n], i[t] - ("right" === t ? s.width : s.height))), G({}, n, o)
                }
              };
            return r.forEach(function(t) {
              var e = -1 === ["left", "top"].indexOf(t) ? "secondary" : "primary";
              s = K({}, s, a[e](t))
            }), t.offsets.popper = s, t
          },
          priority: ["left", "right", "top", "bottom"],
          padding: 5,
          boundariesElement: "scrollParent"
        },
        keepTogether: {
          order: 400,
          enabled: !0,
          fn: function(t) {
            var e = t.offsets,
              n = e.popper,
              i = e.reference,
              o = t.placement.split("-")[0],
              r = z,
              s = -1 !== ["top", "bottom"].indexOf(o),
              a = s ? "right" : "bottom",
              l = s ? "left" : "top",
              u = s ? "width" : "height";
            return n[a] < r(i[l]) && (t.offsets.popper[l] = r(i[l]) - n[u]), n[l] > r(i[a]) && (t.offsets.popper[l] = r(i[a])), t
          }
        },
        arrow: {
          order: 500,
          enabled: !0,
          fn: function(t, e) {
            if (!O(t.instance.modifiers, "arrow", "keepTogether")) return t;
            var n = e.element;
            if ("string" == typeof n) {
              if (!(n = t.instance.popper.querySelector(n))) return t
            } else if (!t.instance.popper.contains(n)) return console.warn("WARNING: `arrow.element` must be child of its popper element!"), t;
            var i = t.placement.split("-")[0],
              o = t.offsets,
              r = o.popper,
              s = o.reference,
              a = -1 !== ["left", "right"].indexOf(i),
              l = a ? "height" : "width",
              u = a ? "top" : "left",
              c = a ? "left" : "top",
              d = a ? "bottom" : "right",
              h = w(n)[l];
            s[d] - h < r[u] && (t.offsets.popper[u] -= r[u] - (s[d] - h)), s[u] + h > r[d] && (t.offsets.popper[u] += s[u] + h - r[d]);
            var p = s[u] + s[l] / 2 - h / 2 - f(t.offsets.popper)[u];
            return p = M(j(r[l] - h, p), 0), t.arrowElement = n, t.offsets.arrow = {}, t.offsets.arrow[u] = Math.round(p), t.offsets.arrow[c] = "", t
          },
          element: "[x-arrow]"
        },
        flip: {
          order: 600,
          enabled: !0,
          fn: function(t, e) {
            if (S(t.instance.modifiers, "inner")) return t;
            if (t.flipped && t.placement === t.originalPlacement) return t;
            var n = v(t.instance.popper, t.instance.reference, e.padding, e.boundariesElement),
              i = t.placement.split("-")[0],
              o = x(i),
              r = t.placement.split("-")[1] || "",
              s = [];
            switch (e.behavior) {
              case X:
                s = [i, o];
                break;
              case J:
                s = P(i);
                break;
              case tt:
                s = P(i, !0);
                break;
              default:
                s = e.behavior
            }
            return s.forEach(function(a, l) {
              if (i !== a || s.length === l + 1) return t;
              i = t.placement.split("-")[0], o = x(i);
              var u = t.offsets.popper,
                c = t.offsets.reference,
                d = z,
                f = "left" === i && d(u.right) > d(c.left) || "right" === i && d(u.left) < d(c.right) || "top" === i && d(u.bottom) > d(c.top) || "bottom" === i && d(u.top) < d(c.bottom),
                h = d(u.left) < d(n.left),
                p = d(u.right) > d(n.right),
                m = d(u.top) < d(n.top),
                g = d(u.bottom) > d(n.bottom),
                v = "left" === i && h || "right" === i && p || "top" === i && m || "bottom" === i && g,
                y = -1 !== ["top", "bottom"].indexOf(i),
                b = !!e.flipVariations && (y && "start" === r && h || y && "end" === r && p || !y && "start" === r && m || !y && "end" === r && g);
              (f || v || b) && (t.flipped = !0, (f || v) && (i = s[l + 1]), b && (r = function(t) {
                return "end" === t ? "start" : "start" === t ? "end" : t
              }(r)), t.placement = i + (r ? "-" + r : ""), t.offsets.popper = K({}, t.offsets.popper, C(t.instance.popper, t.offsets.reference, t.placement)), t = T(t.instance.modifiers, t, "flip"))
            }), t
          },
          behavior: "flip",
          padding: 5,
          boundariesElement: "viewport"
        },
        inner: {
          order: 700,
          enabled: !1,
          fn: function(t) {
            var e = t.placement,
              n = e.split("-")[0],
              i = t.offsets,
              o = i.popper,
              r = i.reference,
              s = -1 !== ["left", "right"].indexOf(n),
              a = -1 === ["top", "left"].indexOf(n);
            return o[s ? "left" : "top"] = r[e] - (a ? o[s ? "width" : "height"] : 0), t.placement = x(e), t.offsets.popper = f(o), t
          }
        },
        hide: {
          order: 800,
          enabled: !0,
          fn: function(t) {
            if (!O(t.instance.modifiers, "hide", "preventOverflow")) return t;
            var e = t.offsets.reference,
              n = E(t.instance.modifiers, function(t) {
                return "preventOverflow" === t.name
              }).boundaries;
            if (e.bottom < n.top || e.left > n.right || e.top > n.bottom || e.right < n.left) {
              if (!0 === t.hide) return t;
              t.hide = !0, t.attributes["x-out-of-boundaries"] = ""
            } else {
              if (!1 === t.hide) return t;
              t.hide = !1, t.attributes["x-out-of-boundaries"] = !1
            }
            return t
          }
        },
        computeStyle: {
          order: 850,
          enabled: !0,
          fn: function(t, e) {
            var n = e.x,
              i = e.y,
              r = t.offsets.popper,
              s = E(t.instance.modifiers, function(t) {
                return "applyStyle" === t.name
              }).gpuAcceleration;
            void 0 !== s && console.warn("WARNING: `gpuAcceleration` option moved to `computeStyle` modifier and will not be supported in future versions of Popper.js!");
            var a, l, u = void 0 === s ? e.gpuAcceleration : s,
              c = h(o(t.instance.popper)),
              d = {
                position: r.position
              },
              f = {
                left: z(r.left),
                top: z(r.top),
                bottom: z(r.bottom),
                right: z(r.right)
              },
              p = "bottom" === n ? "top" : "bottom",
              m = "right" === i ? "left" : "right",
              g = k("transform");
            if (l = "bottom" == p ? -c.height + f.bottom : f.top, a = "right" == m ? -c.width + f.right : f.left, u && g) d[g] = "translate3d(" + a + "px, " + l + "px, 0)", d[p] = 0, d[m] = 0, d.willChange = "transform";
            else {
              var v = "bottom" == p ? -1 : 1,
                y = "right" == m ? -1 : 1;
              d[p] = l * v, d[m] = a * y, d.willChange = p + ", " + m
            }
            var b = {
              "x-placement": t.placement
            };
            return t.attributes = K({}, b, t.attributes), t.styles = K({}, d, t.styles), t
          },
          gpuAcceleration: !0,
          x: "bottom",
          y: "right"
        },
        applyStyle: {
          order: 900,
          enabled: !0,
          fn: function(t) {
            return L(t.instance.popper, t.styles),
              function(t, e) {
                Object.keys(e).forEach(function(n) {
                  !1 === e[n] ? t.removeAttribute(n) : t.setAttribute(n, e[n])
                })
              }(t.instance.popper, t.attributes), t.offsets.arrow && L(t.arrowElement, t.offsets.arrow), t
          },
          onLoad: function(t, e, n, i, o) {
            var r = _(0, e, t),
              s = b(n.placement, r, e, t, n.modifiers.flip.boundariesElement, n.modifiers.flip.padding);
            return e.setAttribute("x-placement", s), L(e, {
              position: "absolute"
            }), n
          },
          gpuAcceleration: void 0
        }
      }
    }, et
  }), function(t, e) {
    "function" == typeof define && define.amd ? define(e) : "object" == typeof exports ? module.exports = e() : t.PhotoSwipe = e()
  }(this, function() {
    "use strict";
    return function(t, e, n, i) {
      var o = {
        features: null,
        bind: function(t, e, n, i) {
          var o = (i ? "remove" : "add") + "EventListener";
          e = e.split(" ");
          for (var r = 0; r < e.length; r++) e[r] && t[o](e[r], n, !1)
        },
        isArray: function(t) {
          return t instanceof Array
        },
        createEl: function(t, e) {
          var n = document.createElement(e || "div");
          return t && (n.className = t), n
        },
        getScrollY: function() {
          var t = window.pageYOffset;
          return void 0 !== t ? t : document.documentElement.scrollTop
        },
        unbind: function(t, e, n) {
          o.bind(t, e, n, !0)
        },
        removeClass: function(t, e) {
          var n = new RegExp("(\\s|^)" + e + "(\\s|$)");
          t.className = t.className.replace(n, " ").replace(/^\s\s*/, "").replace(/\s\s*$/, "")
        },
        addClass: function(t, e) {
          o.hasClass(t, e) || (t.className += (t.className ? " " : "") + e)
        },
        hasClass: function(t, e) {
          return t.className && new RegExp("(^|\\s)" + e + "(\\s|$)").test(t.className)
        },
        getChildByClass: function(t, e) {
          for (var n = t.firstChild; n;) {
            if (o.hasClass(n, e)) return n;
            n = n.nextSibling
          }
        },
        arraySearch: function(t, e, n) {
          for (var i = t.length; i--;)
            if (t[i][n] === e) return i;
          return -1
        },
        extend: function(t, e, n) {
          for (var i in e)
            if (e.hasOwnProperty(i)) {
              if (n && t.hasOwnProperty(i)) continue;
              t[i] = e[i]
            }
        },
        easing: {
          sine: {
            out: function(t) {
              return Math.sin(t * (Math.PI / 2))
            },
            inOut: function(t) {
              return -(Math.cos(Math.PI * t) - 1) / 2
            }
          },
          cubic: {
            out: function(t) {
              return --t * t * t + 1
            }
          }
        },
        detectFeatures: function() {
          if (o.features) return o.features;
          var t = o.createEl().style,
            e = "",
            n = {};
          if (n.oldIE = document.all && !document.addEventListener, n.touch = "ontouchstart" in window, window.requestAnimationFrame && (n.raf = window.requestAnimationFrame, n.caf = window.cancelAnimationFrame), n.pointerEvent = navigator.pointerEnabled || navigator.msPointerEnabled, !n.pointerEvent) {
            var i = navigator.userAgent;
            if (/iP(hone|od)/.test(navigator.platform)) {
              var r = navigator.appVersion.match(/OS (\d+)_(\d+)_?(\d+)?/);
              r && r.length > 0 && (r = parseInt(r[1], 10)) >= 1 && 8 > r && (n.isOldIOSPhone = !0)
            }
            var s = i.match(/Android\s([0-9\.]*)/),
              a = s ? s[1] : 0;
            (a = parseFloat(a)) >= 1 && (4.4 > a && (n.isOldAndroid = !0), n.androidVersion = a), n.isMobileOpera = /opera mini|opera mobi/i.test(i)
          }
          for (var l, u, c = ["transform", "perspective", "animationName"], d = ["", "webkit", "Moz", "ms", "O"], f = 0; 4 > f; f++) {
            e = d[f];
            for (var h = 0; 3 > h; h++) l = c[h], u = e + (e ? l.charAt(0).toUpperCase() + l.slice(1) : l), !n[l] && u in t && (n[l] = u);
            e && !n.raf && (e = e.toLowerCase(), n.raf = window[e + "RequestAnimationFrame"], n.raf && (n.caf = window[e + "CancelAnimationFrame"] || window[e + "CancelRequestAnimationFrame"]))
          }
          if (!n.raf) {
            var p = 0;
            n.raf = function(t) {
              var e = (new Date).getTime(),
                n = Math.max(0, 16 - (e - p)),
                i = window.setTimeout(function() {
                  t(e + n)
                }, n);
              return p = e + n, i
            }, n.caf = function(t) {
              clearTimeout(t)
            }
          }
          return n.svg = !!document.createElementNS && !!document.createElementNS("http://www.w3.org/2000/svg", "svg").createSVGRect, o.features = n, n
        }
      };
      o.detectFeatures(), o.features.oldIE && (o.bind = function(t, e, n, i) {
        e = e.split(" ");
        for (var o, r = (i ? "detach" : "attach") + "Event", s = function() {
            n.handleEvent.call(n)
          }, a = 0; a < e.length; a++)
          if (o = e[a])
            if ("object" == typeof n && n.handleEvent) {
              if (i) {
                if (!n["oldIE" + o]) return !1
              } else n["oldIE" + o] = s;
              t[r]("on" + o, n["oldIE" + o])
            } else t[r]("on" + o, n)
      });
      var r = this,
        s = {
          allowPanToNext: !0,
          spacing: .12,
          bgOpacity: 1,
          mouseUsed: !1,
          loop: !0,
          pinchToClose: !0,
          closeOnScroll: !0,
          closeOnVerticalDrag: !0,
          verticalDragRange: .75,
          hideAnimationDuration: 333,
          showAnimationDuration: 333,
          showHideOpacity: !1,
          focus: !0,
          escKey: !0,
          arrowKeys: !0,
          mainScrollEndFriction: .35,
          panEndFriction: .35,
          isClickableElement: function(t) {
            return "A" === t.tagName
          },
          getDoubleTapZoom: function(t, e) {
            return t ? 1 : e.initialZoomLevel < .7 ? 1 : 1.33
          },
          maxSpreadZoom: 1.33,
          modal: !0,
          scaleMode: "fit"
        };
      o.extend(s, i);
      var a, l, u, c, d, f, h, p, m, g, v, y, b, _, w, x, C, E, T, S, k, I, A, D, L, O, P, N, j, z, M, $, R, F, B, H, W, q, V, U, Q, G, K, Y, Z, X, J, tt, et, nt, it, ot, rt, st, at, lt = {
          x: 0,
          y: 0
        },
        ut = {
          x: 0,
          y: 0
        },
        ct = {
          x: 0,
          y: 0
        },
        dt = {},
        ft = 0,
        ht = {},
        pt = {
          x: 0,
          y: 0
        },
        mt = 0,
        gt = !0,
        vt = [],
        yt = {},
        bt = !1,
        _t = function(t, e) {
          o.extend(r, e.publicMethods), vt.push(t)
        },
        wt = function(t) {
          var e = Fe();
          return t > e - 1 ? t - e : 0 > t ? e + t : t
        },
        xt = {},
        Ct = function(t, e) {
          return xt[t] || (xt[t] = []), xt[t].push(e)
        },
        Et = function(t) {
          var e = xt[t];
          if (e) {
            var n = Array.prototype.slice.call(arguments);
            n.shift();
            for (var i = 0; i < e.length; i++) e[i].apply(r, n)
          }
        },
        Tt = function() {
          return (new Date).getTime()
        },
        St = function(t) {
          rt = t, r.bg.style.opacity = t * s.bgOpacity
        },
        kt = function(t, e, n, i, o) {
          (!bt || o && o !== r.currItem) && (i /= o ? o.fitRatio : r.currItem.fitRatio), t[I] = y + e + "px, " + n + "px" + b + " scale(" + i + ")"
        },
        It = function(t) {
          tt && (t && (g > r.currItem.fitRatio ? bt || (Ke(r.currItem, !1, !0), bt = !0) : bt && (Ke(r.currItem), bt = !1)), kt(tt, ct.x, ct.y, g))
        },
        At = function(t) {
          t.container && kt(t.container.style, t.initialPosition.x, t.initialPosition.y, t.initialZoomLevel, t)
        },
        Dt = function(t, e) {
          e[I] = y + t + "px, 0px" + b
        },
        Lt = function(t, e) {
          if (!s.loop && e) {
            var n = c + (pt.x * ft - t) / pt.x,
              i = Math.round(t - ue.x);
            (0 > n && i > 0 || n >= Fe() - 1 && 0 > i) && (t = ue.x + i * s.mainScrollEndFriction)
          }
          ue.x = t, Dt(t, d)
        },
        Ot = function(t, e) {
          var n = ce[t] - ht[t];
          return ut[t] + lt[t] + n - n * (e / v)
        },
        Pt = function(t, e) {
          t.x = e.x, t.y = e.y, e.id && (t.id = e.id)
        },
        Nt = function(t) {
          t.x = Math.round(t.x), t.y = Math.round(t.y)
        },
        jt = null,
        zt = function() {
          jt && (o.unbind(document, "mousemove", zt), o.addClass(t, "pswp--has_mouse"), s.mouseUsed = !0, Et("mouseUsed")), jt = setTimeout(function() {
            jt = null
          }, 100)
        },
        Mt = function(t, e) {
          var n = Ve(r.currItem, dt, t);
          return e && (J = n), n
        },
        $t = function(t) {
          return t || (t = r.currItem), t.initialZoomLevel
        },
        Rt = function(t) {
          return t || (t = r.currItem), t.w > 0 ? s.maxSpreadZoom : 1
        },
        Ft = function(t, e, n, i) {
          return i === r.currItem.initialZoomLevel ? (n[t] = r.currItem.initialPosition[t], !0) : (n[t] = Ot(t, i), n[t] > e.min[t] ? (n[t] = e.min[t], !0) : n[t] < e.max[t] && (n[t] = e.max[t], !0))
        },
        Bt = function(t) {
          var e = "";
          s.escKey && 27 === t.keyCode ? e = "close" : s.arrowKeys && (37 === t.keyCode ? e = "prev" : 39 === t.keyCode && (e = "next")), e && (t.ctrlKey || t.altKey || t.shiftKey || t.metaKey || (t.preventDefault ? t.preventDefault() : t.returnValue = !1, r[e]()))
        },
        Ht = function(t) {
          t && (Q || U || et || H) && (t.preventDefault(), t.stopPropagation())
        },
        Wt = function() {
          r.setScrollOffset(0, o.getScrollY())
        },
        qt = {},
        Vt = 0,
        Ut = function(t) {
          qt[t] && (qt[t].raf && O(qt[t].raf), Vt--, delete qt[t])
        },
        Qt = function(t) {
          qt[t] && Ut(t), qt[t] || (Vt++, qt[t] = {})
        },
        Gt = function() {
          for (var t in qt) qt.hasOwnProperty(t) && Ut(t)
        },
        Kt = function(t, e, n, i, o, r, s) {
          var a, l = Tt();
          Qt(t);
          var u = function() {
            if (qt[t]) {
              if ((a = Tt() - l) >= i) return Ut(t), r(n), void(s && s());
              r((n - e) * o(a / i) + e), qt[t].raf = L(u)
            }
          };
          u()
        },
        Yt = {
          shout: Et,
          listen: Ct,
          viewportSize: dt,
          options: s,
          isMainScrollAnimating: function() {
            return et
          },
          getZoomLevel: function() {
            return g
          },
          getCurrentIndex: function() {
            return c
          },
          isDragging: function() {
            return q
          },
          isZooming: function() {
            return Z
          },
          setScrollOffset: function(t, e) {
            ht.x = t, z = ht.y = e, Et("updateScrollOffset", ht)
          },
          applyZoomPan: function(t, e, n, i) {
            ct.x = e, ct.y = n, g = t, It(i)
          },
          init: function() {
            if (!a && !l) {
              var n;
              r.framework = o, r.template = t, r.bg = o.getChildByClass(t, "pswp__bg"), P = t.className, a = !0, M = o.detectFeatures(), L = M.raf, O = M.caf, I = M.transform, j = M.oldIE, r.scrollWrap = o.getChildByClass(t, "pswp__scroll-wrap"), r.container = o.getChildByClass(r.scrollWrap, "pswp__container"), d = r.container.style, r.itemHolders = x = [{
                  el: r.container.children[0],
                  wrap: 0,
                  index: -1
                }, {
                  el: r.container.children[1],
                  wrap: 0,
                  index: -1
                }, {
                  el: r.container.children[2],
                  wrap: 0,
                  index: -1
                }], x[0].el.style.display = x[2].el.style.display = "none",
                function() {
                  if (I) {
                    var e = M.perspective && !D;
                    return y = "translate" + (e ? "3d(" : "("), void(b = M.perspective ? ", 0px)" : ")")
                  }
                  I = "left", o.addClass(t, "pswp--ie"), Dt = function(t, e) {
                    e.left = t + "px"
                  }, At = function(t) {
                    var e = t.fitRatio > 1 ? 1 : t.fitRatio,
                      n = t.container.style,
                      i = e * t.w,
                      o = e * t.h;
                    n.width = i + "px", n.height = o + "px", n.left = t.initialPosition.x + "px", n.top = t.initialPosition.y + "px"
                  }, It = function() {
                    if (tt) {
                      var t = tt,
                        e = r.currItem,
                        n = e.fitRatio > 1 ? 1 : e.fitRatio,
                        i = n * e.w,
                        o = n * e.h;
                      t.width = i + "px", t.height = o + "px", t.left = ct.x + "px", t.top = ct.y + "px"
                    }
                  }
                }(), m = {
                  resize: r.updateSize,
                  scroll: Wt,
                  keydown: Bt,
                  click: Ht
                };
              var i = M.isOldIOSPhone || M.isOldAndroid || M.isMobileOpera;
              for (M.animationName && M.transform && !i || (s.showAnimationDuration = s.hideAnimationDuration = 0), n = 0; n < vt.length; n++) r["init" + vt[n]]();
              e && (r.ui = new e(r, o)).init(), Et("firstUpdate"), c = c || s.index || 0, (isNaN(c) || 0 > c || c >= Fe()) && (c = 0), r.currItem = Re(c), (M.isOldIOSPhone || M.isOldAndroid) && (gt = !1), t.setAttribute("aria-hidden", "false"), s.modal && (gt ? t.style.position = "fixed" : (t.style.position = "absolute", t.style.top = o.getScrollY() + "px")), void 0 === z && (Et("initialLayout"), z = N = o.getScrollY());
              var u = "pswp--open ";
              for (s.mainClass && (u += s.mainClass + " "), s.showHideOpacity && (u += "pswp--animate_opacity "), u += D ? "pswp--touch" : "pswp--notouch", u += M.animationName ? " pswp--css_animation" : "", u += M.svg ? " pswp--svg" : "", o.addClass(t, u), r.updateSize(), f = -1, mt = null, n = 0; 3 > n; n++) Dt((n + f) * pt.x, x[n].el.style);
              j || o.bind(r.scrollWrap, p, r), Ct("initialZoomInEnd", function() {
                r.setContent(x[0], c - 1), r.setContent(x[2], c + 1), x[0].el.style.display = x[2].el.style.display = "block", s.focus && t.focus(), o.bind(document, "keydown", r), M.transform && o.bind(r.scrollWrap, "click", r), s.mouseUsed || o.bind(document, "mousemove", zt), o.bind(window, "resize scroll", r), Et("bindEvents")
              }), r.setContent(x[1], c), r.updateCurrItem(), Et("afterInit"), gt || (_ = setInterval(function() {
                Vt || q || Z || g !== r.currItem.initialZoomLevel || r.updateSize()
              }, 1e3)), o.addClass(t, "pswp--visible")
            }
          },
          close: function() {
            a && (a = !1, l = !0, Et("close"), o.unbind(window, "resize", r), o.unbind(window, "scroll", m.scroll), o.unbind(document, "keydown", r), o.unbind(document, "mousemove", zt), M.transform && o.unbind(r.scrollWrap, "click", r), q && o.unbind(window, h, r), Et("unbindEvents"), Be(r.currItem, null, !0, r.destroy))
          },
          destroy: function() {
            Et("destroy"), je && clearTimeout(je), t.setAttribute("aria-hidden", "true"), t.className = P, _ && clearInterval(_), o.unbind(r.scrollWrap, p, r), o.unbind(window, "scroll", r), he(), Gt(), xt = null
          },
          panTo: function(t, e, n) {
            n || (t > J.min.x ? t = J.min.x : t < J.max.x && (t = J.max.x), e > J.min.y ? e = J.min.y : e < J.max.y && (e = J.max.y)), ct.x = t, ct.y = e, It()
          },
          handleEvent: function(t) {
            t = t || window.event, m[t.type] && m[t.type](t)
          },
          goTo: function(t) {
            var e = (t = wt(t)) - c;
            mt = e, c = t, r.currItem = Re(c), ft -= e, Lt(pt.x * ft), Gt(), et = !1, r.updateCurrItem()
          },
          next: function() {
            r.goTo(c + 1)
          },
          prev: function() {
            r.goTo(c - 1)
          },
          updateCurrZoomItem: function(t) {
            if (t && Et("beforeChange", 0), x[1].el.children.length) {
              var e = x[1].el.children[0];
              tt = o.hasClass(e, "pswp__zoom-wrap") ? e.style : null
            } else tt = null;
            J = r.currItem.bounds, v = g = r.currItem.initialZoomLevel, ct.x = J.center.x, ct.y = J.center.y, t && Et("afterChange")
          },
          invalidateCurrItems: function() {
            w = !0;
            for (var t = 0; 3 > t; t++) x[t].item && (x[t].item.needsUpdate = !0)
          },
          updateCurrItem: function(t) {
            if (0 !== mt) {
              var e, n = Math.abs(mt);
              if (!(t && 2 > n)) {
                r.currItem = Re(c), bt = !1, Et("beforeChange", mt), n >= 3 && (f += mt + (mt > 0 ? -3 : 3), n = 3);
                for (var i = 0; n > i; i++) mt > 0 ? (e = x.shift(), x[2] = e, Dt((++f + 2) * pt.x, e.el.style), r.setContent(e, c - n + i + 1 + 1)) : (e = x.pop(), x.unshift(e), Dt(--f * pt.x, e.el.style), r.setContent(e, c + n - i - 1 - 1));
                if (tt && 1 === Math.abs(mt)) {
                  var o = Re(C);
                  o.initialZoomLevel !== g && (Ve(o, dt), Ke(o), At(o))
                }
                mt = 0, r.updateCurrZoomItem(), C = c, Et("afterChange")
              }
            }
          },
          updateSize: function(e) {
            if (!gt && s.modal) {
              var n = o.getScrollY();
              if (z !== n && (t.style.top = n + "px", z = n), !e && yt.x === window.innerWidth && yt.y === window.innerHeight) return;
              yt.x = window.innerWidth, yt.y = window.innerHeight, t.style.height = yt.y + "px"
            }
            if (dt.x = r.scrollWrap.clientWidth, dt.y = r.scrollWrap.clientHeight, Wt(), pt.x = dt.x + Math.round(dt.x * s.spacing), pt.y = dt.y, Lt(pt.x * ft), Et("beforeResize"), void 0 !== f) {
              for (var i, a, l, u = 0; 3 > u; u++) i = x[u], Dt((u + f) * pt.x, i.el.style), l = c + u - 1, s.loop && Fe() > 2 && (l = wt(l)), (a = Re(l)) && (w || a.needsUpdate || !a.bounds) ? (r.cleanSlide(a), r.setContent(i, l), 1 === u && (r.currItem = a, r.updateCurrZoomItem(!0)), a.needsUpdate = !1) : -1 === i.index && l >= 0 && r.setContent(i, l), a && a.container && (Ve(a, dt), Ke(a), At(a));
              w = !1
            }
            v = g = r.currItem.initialZoomLevel, (J = r.currItem.bounds) && (ct.x = J.center.x, ct.y = J.center.y, It(!0)), Et("resize")
          },
          zoomTo: function(t, e, n, i, r) {
            e && (v = g, ce.x = Math.abs(e.x) - ct.x, ce.y = Math.abs(e.y) - ct.y, Pt(ut, ct));
            var s = Mt(t, !1),
              a = {};
            Ft("x", s, a, t), Ft("y", s, a, t);
            var l = g,
              u = ct.x,
              c = ct.y;
            Nt(a);
            var d = function(e) {
              1 === e ? (g = t, ct.x = a.x, ct.y = a.y) : (g = (t - l) * e + l, ct.x = (a.x - u) * e + u, ct.y = (a.y - c) * e + c), r && r(e), It(1 === e)
            };
            n ? Kt("customZoomTo", 0, 1, n, i || o.easing.sine.inOut, d) : d(1)
          }
        },
        Zt = {},
        Xt = {},
        Jt = {},
        te = {},
        ee = {},
        ne = [],
        ie = {},
        oe = [],
        re = {},
        se = 0,
        ae = {
          x: 0,
          y: 0
        },
        le = 0,
        ue = {
          x: 0,
          y: 0
        },
        ce = {
          x: 0,
          y: 0
        },
        de = {
          x: 0,
          y: 0
        },
        fe = function(t, e) {
          return re.x = Math.abs(t.x - e.x), re.y = Math.abs(t.y - e.y), Math.sqrt(re.x * re.x + re.y * re.y)
        },
        he = function() {
          G && (O(G), G = null)
        },
        pe = function() {
          q && (G = L(pe), Ie())
        },
        me = function(t, e) {
          return !(!t || t === document) && !(t.getAttribute("class") && t.getAttribute("class").indexOf("pswp__scroll-wrap") > -1) && (e(t) ? t : me(t.parentNode, e))
        },
        ge = {},
        ve = function(t, e) {
          return ge.prevent = !me(t.target, s.isClickableElement), Et("preventDragEvent", t, e, ge), ge.prevent
        },
        ye = function(t, e) {
          return e.x = t.pageX, e.y = t.pageY, e.id = t.identifier, e
        },
        be = function(t, e, n) {
          n.x = .5 * (t.x + e.x), n.y = .5 * (t.y + e.y)
        },
        _e = function() {
          var t = ct.y - r.currItem.initialPosition.y;
          return 1 - Math.abs(t / (dt.y / 2))
        },
        we = {},
        xe = {},
        Ce = [],
        Ee = function(t) {
          for (; Ce.length > 0;) Ce.pop();
          return A ? (at = 0, ne.forEach(function(t) {
            0 === at ? Ce[0] = t : 1 === at && (Ce[1] = t), at++
          })) : t.type.indexOf("touch") > -1 ? t.touches && t.touches.length > 0 && (Ce[0] = ye(t.touches[0], we), t.touches.length > 1 && (Ce[1] = ye(t.touches[1], xe))) : (we.x = t.pageX, we.y = t.pageY, we.id = "", Ce[0] = we), Ce
        },
        Te = function(t, e) {
          var n, i, o, a, l = ct[t] + e[t],
            u = e[t] > 0,
            c = ue.x + e.x,
            d = ue.x - ie.x;
          return n = l > J.min[t] || l < J.max[t] ? s.panEndFriction : 1, l = ct[t] + e[t] * n, !s.allowPanToNext && g !== r.currItem.initialZoomLevel || (tt ? "h" !== nt || "x" !== t || U || (u ? (l > J.min[t] && (n = s.panEndFriction, J.min[t], i = J.min[t] - ut[t]), (0 >= i || 0 > d) && Fe() > 1 ? (a = c, 0 > d && c > ie.x && (a = ie.x)) : J.min.x !== J.max.x && (o = l)) : (l < J.max[t] && (n = s.panEndFriction, J.max[t], i = ut[t] - J.max[t]), (0 >= i || d > 0) && Fe() > 1 ? (a = c, d > 0 && c < ie.x && (a = ie.x)) : J.min.x !== J.max.x && (o = l))) : a = c, "x" !== t) ? void(et || K || g > r.currItem.fitRatio && (ct[t] += e[t] * n)) : (void 0 !== a && (Lt(a, !0), K = a !== ie.x), J.min.x !== J.max.x && (void 0 !== o ? ct.x = o : K || (ct.x += e.x * n)), void 0 !== a)
        },
        Se = function(t) {
          if (!("mousedown" === t.type && t.button > 0)) {
            if ($e) return void t.preventDefault();
            if (!W || "mousedown" !== t.type) {
              if (ve(t, !0) && t.preventDefault(), Et("pointerDown"), A) {
                var e = o.arraySearch(ne, t.pointerId, "id");
                0 > e && (e = ne.length), ne[e] = {
                  x: t.pageX,
                  y: t.pageY,
                  id: t.pointerId
                }
              }
              var n = Ee(t),
                i = n.length;
              Y = null, Gt(), q && 1 !== i || (q = it = !0, o.bind(window, h, r), B = st = ot = H = K = Q = V = U = !1, nt = null, Et("firstTouchStart", n), Pt(ut, ct), lt.x = lt.y = 0, Pt(te, n[0]), Pt(ee, te), ie.x = pt.x * ft, oe = [{
                x: te.x,
                y: te.y
              }], R = $ = Tt(), Mt(g, !0), he(), pe()), !Z && i > 1 && !et && !K && (v = g, U = !1, Z = V = !0, lt.y = lt.x = 0, Pt(ut, ct), Pt(Zt, n[0]), Pt(Xt, n[1]), be(Zt, Xt, de), ce.x = Math.abs(de.x) - ct.x, ce.y = Math.abs(de.y) - ct.y, X = fe(Zt, Xt))
            }
          }
        },
        ke = function(t) {
          if (t.preventDefault(), A) {
            var e = o.arraySearch(ne, t.pointerId, "id");
            if (e > -1) {
              var n = ne[e];
              n.x = t.pageX, n.y = t.pageY
            }
          }
          if (q) {
            var i = Ee(t);
            if (nt || Q || Z) Y = i;
            else if (ue.x !== pt.x * ft) nt = "h";
            else {
              var r = Math.abs(i[0].x - te.x) - Math.abs(i[0].y - te.y);
              Math.abs(r) >= 10 && (nt = r > 0 ? "h" : "v", Y = i)
            }
          }
        },
        Ie = function() {
          if (Y) {
            var t = Y.length;
            if (0 !== t)
              if (Pt(Zt, Y[0]), Jt.x = Zt.x - te.x, Jt.y = Zt.y - te.y, Z && t > 1) {
                if (te.x = Zt.x, te.y = Zt.y, !Jt.x && !Jt.y && function(t, e) {
                    return t.x === e.x && t.y === e.y
                  }(Y[1], Xt)) return;
                Pt(Xt, Y[1]), U || (U = !0, Et("zoomGestureStarted"));
                var e = fe(Zt, Xt),
                  n = Pe(e);
                n > r.currItem.initialZoomLevel + r.currItem.initialZoomLevel / 15 && (st = !0);
                var i = 1,
                  o = $t(),
                  a = Rt();
                if (o > n)
                  if (s.pinchToClose && !st && v <= r.currItem.initialZoomLevel) {
                    var l = 1 - (o - n) / (o / 1.2);
                    St(l), Et("onPinchClose", l), ot = !0
                  } else(i = (o - n) / o) > 1 && (i = 1), n = o - i * (o / 3);
                else n > a && ((i = (n - a) / (6 * o)) > 1 && (i = 1), n = a + i * o);
                0 > i && (i = 0), be(Zt, Xt, ae), lt.x += ae.x - de.x, lt.y += ae.y - de.y, Pt(de, ae), ct.x = Ot("x", n), ct.y = Ot("y", n), B = n > g, g = n, It()
              } else {
                if (!nt) return;
                if (it && (it = !1, Math.abs(Jt.x) >= 10 && (Jt.x -= Y[0].x - ee.x), Math.abs(Jt.y) >= 10 && (Jt.y -= Y[0].y - ee.y)), te.x = Zt.x, te.y = Zt.y, 0 === Jt.x && 0 === Jt.y) return;
                if ("v" === nt && s.closeOnVerticalDrag && "fit" === s.scaleMode && g === r.currItem.initialZoomLevel) {
                  lt.y += Jt.y, ct.y += Jt.y;
                  var u = _e();
                  return H = !0, Et("onVerticalDrag", u), St(u), void It()
                }(function(t, e, n) {
                  if (t - R > 50) {
                    var i = oe.length > 2 ? oe.shift() : {};
                    i.x = e, i.y = n, oe.push(i), R = t
                  }
                })(Tt(), Zt.x, Zt.y), Q = !0, J = r.currItem.bounds, Te("x", Jt) || (Te("y", Jt), Nt(ct), It())
              }
          }
        },
        Ae = function(t) {
          if (M.isOldAndroid) {
            if (W && "mouseup" === t.type) return;
            t.type.indexOf("touch") > -1 && (clearTimeout(W), W = setTimeout(function() {
              W = 0
            }, 600))
          }
          var e;
          if (Et("pointerUp"), ve(t, !1) && t.preventDefault(), A) {
            var n = o.arraySearch(ne, t.pointerId, "id");
            n > -1 && (e = ne.splice(n, 1)[0], navigator.pointerEnabled ? e.type = t.pointerType || "mouse" : (e.type = {
              4: "mouse",
              2: "touch",
              3: "pen"
            } [t.pointerType], e.type || (e.type = t.pointerType || "mouse")))
          }
          var i, a = Ee(t),
            l = a.length;
          if ("mouseup" === t.type && (l = 0), 2 === l) return Y = null, !0;
          1 === l && Pt(ee, a[0]), 0 !== l || nt || et || (e || ("mouseup" === t.type ? e = {
            x: t.pageX,
            y: t.pageY,
            type: "mouse"
          } : t.changedTouches && t.changedTouches[0] && (e = {
            x: t.changedTouches[0].pageX,
            y: t.changedTouches[0].pageY,
            type: "touch"
          })), Et("touchRelease", t, e));
          var u = -1;
          if (0 === l && (q = !1, o.unbind(window, h, r), he(), Z ? u = 0 : -1 !== le && (u = Tt() - le)), le = 1 === l ? Tt() : -1, i = -1 !== u && 150 > u ? "zoom" : "swipe", Z && 2 > l && (Z = !1, 1 === l && (i = "zoomPointerUp"), Et("zoomGestureEnded")), Y = null, Q || U || et || H)
            if (Gt(), F || (F = De()), F.calculateSwipeSpeed("x"), H)
              if (_e() < s.verticalDragRange) r.close();
              else {
                var c = ct.y,
                  d = rt;
                Kt("verticalDrag", 0, 1, 300, o.easing.cubic.out, function(t) {
                  ct.y = (r.currItem.initialPosition.y - c) * t + c, St((1 - d) * t + d), It()
                }), Et("onVerticalDrag", 1)
              }
          else {
            if ((K || et) && 0 === l) {
              if (Oe(i, F)) return;
              i = "zoomPointerUp"
            }
            if (!et) return "swipe" !== i ? void Ne() : void(!K && g > r.currItem.fitRatio && Le(F))
          }
        },
        De = function() {
          var t, e, n = {
            lastFlickOffset: {},
            lastFlickDist: {},
            lastFlickSpeed: {},
            slowDownRatio: {},
            slowDownRatioReverse: {},
            speedDecelerationRatio: {},
            speedDecelerationRatioAbs: {},
            distanceOffset: {},
            backAnimDestination: {},
            backAnimStarted: {},
            calculateSwipeSpeed: function(i) {
              oe.length > 1 ? (t = Tt() - R + 50, e = oe[oe.length - 2][i]) : (t = Tt() - $, e = ee[i]), n.lastFlickOffset[i] = te[i] - e, n.lastFlickDist[i] = Math.abs(n.lastFlickOffset[i]), n.lastFlickDist[i] > 20 ? n.lastFlickSpeed[i] = n.lastFlickOffset[i] / t : n.lastFlickSpeed[i] = 0, Math.abs(n.lastFlickSpeed[i]) < .1 && (n.lastFlickSpeed[i] = 0), n.slowDownRatio[i] = .95, n.slowDownRatioReverse[i] = 1 - n.slowDownRatio[i], n.speedDecelerationRatio[i] = 1
            },
            calculateOverBoundsAnimOffset: function(t, e) {
              n.backAnimStarted[t] || (ct[t] > J.min[t] ? n.backAnimDestination[t] = J.min[t] : ct[t] < J.max[t] && (n.backAnimDestination[t] = J.max[t]), void 0 !== n.backAnimDestination[t] && (n.slowDownRatio[t] = .7, n.slowDownRatioReverse[t] = 1 - n.slowDownRatio[t], n.speedDecelerationRatioAbs[t] < .05 && (n.lastFlickSpeed[t] = 0, n.backAnimStarted[t] = !0, Kt("bounceZoomPan" + t, ct[t], n.backAnimDestination[t], e || 300, o.easing.sine.out, function(e) {
                ct[t] = e, It()
              }))))
            },
            calculateAnimOffset: function(t) {
              n.backAnimStarted[t] || (n.speedDecelerationRatio[t] = n.speedDecelerationRatio[t] * (n.slowDownRatio[t] + n.slowDownRatioReverse[t] - n.slowDownRatioReverse[t] * n.timeDiff / 10), n.speedDecelerationRatioAbs[t] = Math.abs(n.lastFlickSpeed[t] * n.speedDecelerationRatio[t]), n.distanceOffset[t] = n.lastFlickSpeed[t] * n.speedDecelerationRatio[t] * n.timeDiff, ct[t] += n.distanceOffset[t])
            },
            panAnimLoop: function() {
              return qt.zoomPan && (qt.zoomPan.raf = L(n.panAnimLoop), n.now = Tt(), n.timeDiff = n.now - n.lastNow, n.lastNow = n.now, n.calculateAnimOffset("x"), n.calculateAnimOffset("y"), It(), n.calculateOverBoundsAnimOffset("x"), n.calculateOverBoundsAnimOffset("y"), n.speedDecelerationRatioAbs.x < .05 && n.speedDecelerationRatioAbs.y < .05) ? (ct.x = Math.round(ct.x), ct.y = Math.round(ct.y), It(), void Ut("zoomPan")) : void 0
            }
          };
          return n
        },
        Le = function(t) {
          return t.calculateSwipeSpeed("y"), J = r.currItem.bounds, t.backAnimDestination = {}, t.backAnimStarted = {}, Math.abs(t.lastFlickSpeed.x) <= .05 && Math.abs(t.lastFlickSpeed.y) <= .05 ? (t.speedDecelerationRatioAbs.x = t.speedDecelerationRatioAbs.y = 0, t.calculateOverBoundsAnimOffset("x"), t.calculateOverBoundsAnimOffset("y"), !0) : (Qt("zoomPan"), t.lastNow = Tt(), void t.panAnimLoop())
        },
        Oe = function(t, e) {
          var n, i, a;
          if (et || (se = c), "swipe" === t) {
            var l = te.x - ee.x,
              u = e.lastFlickDist.x < 10;
            l > 30 && (u || e.lastFlickOffset.x > 20) ? i = -1 : -30 > l && (u || e.lastFlickOffset.x < -20) && (i = 1)
          }
          i && (0 > (c += i) ? (c = s.loop ? Fe() - 1 : 0, a = !0) : c >= Fe() && (c = s.loop ? 0 : Fe() - 1, a = !0), (!a || s.loop) && (mt += i, ft -= i, n = !0));
          var d, f = pt.x * ft,
            h = Math.abs(f - ue.x);
          return n || f > ue.x == e.lastFlickSpeed.x > 0 ? (d = Math.abs(e.lastFlickSpeed.x) > 0 ? h / Math.abs(e.lastFlickSpeed.x) : 333, d = Math.min(d, 400), d = Math.max(d, 250)) : d = 333, se === c && (n = !1), et = !0, Et("mainScrollAnimStart"), Kt("mainScroll", ue.x, f, d, o.easing.cubic.out, Lt, function() {
            Gt(), et = !1, se = -1, (n || se !== c) && r.updateCurrItem(), Et("mainScrollAnimComplete")
          }), n && r.updateCurrItem(!0), n
        },
        Pe = function(t) {
          return 1 / X * t * v
        },
        Ne = function() {
          var t = g,
            e = $t(),
            n = Rt();
          e > g ? t = e : g > n && (t = n);
          var i, s = rt;
          return ot && !B && !st && e > g ? (r.close(), !0) : (ot && (i = function(t) {
            St((1 - s) * t + s)
          }), r.zoomTo(t, 0, 200, o.easing.cubic.out, i), !0)
        };
      _t("Gestures", {
        publicMethods: {
          initGestures: function() {
            var t = function(t, e, n, i, o) {
              E = t + e, T = t + n, S = t + i, k = o ? t + o : ""
            };
            (A = M.pointerEvent) && M.touch && (M.touch = !1), A ? navigator.pointerEnabled ? t("pointer", "down", "move", "up", "cancel") : t("MSPointer", "Down", "Move", "Up", "Cancel") : M.touch ? (t("touch", "start", "move", "end", "cancel"), D = !0) : t("mouse", "down", "move", "up"), h = T + " " + S + " " + k, p = E, A && !D && (D = navigator.maxTouchPoints > 1 || navigator.msMaxTouchPoints > 1), r.likelyTouchDevice = D, m[E] = Se, m[T] = ke, m[S] = Ae, k && (m[k] = m[S]), M.touch && (p += " mousedown", h += " mousemove mouseup", m.mousedown = m[E], m.mousemove = m[T], m.mouseup = m[S]), D || (s.allowPanToNext = !1)
          }
        }
      });
      var je, ze, Me, $e, Re, Fe, Be = function(e, n, i, a) {
          var l;
          je && clearTimeout(je), $e = !0, Me = !0, e.initialLayout ? (l = e.initialLayout, e.initialLayout = null) : l = s.getThumbBoundsFn && s.getThumbBoundsFn(c);
          var d = i ? s.hideAnimationDuration : s.showAnimationDuration,
            f = function() {
              Ut("initialZoom"), i ? (r.template.removeAttribute("style"), r.bg.removeAttribute("style")) : (St(1), n && (n.style.display = "block"), o.addClass(t, "pswp--animated-in"), Et("initialZoom" + (i ? "OutEnd" : "InEnd"))), a && a(), $e = !1
            };
          if (!d || !l || void 0 === l.x) return Et("initialZoom" + (i ? "Out" : "In")), g = e.initialZoomLevel, Pt(ct, e.initialPosition), It(), t.style.opacity = i ? 0 : 1, St(1), void(d ? setTimeout(function() {
            f()
          }, d) : f());
          ! function() {
            var n = u,
              a = !r.currItem.src || r.currItem.loadError || s.showHideOpacity;
            e.miniImg && (e.miniImg.style.webkitBackfaceVisibility = "hidden"), i || (g = l.w / e.w, ct.x = l.x, ct.y = l.y - N, r[a ? "template" : "bg"].style.opacity = .001, It()), Qt("initialZoom"), i && !n && o.removeClass(t, "pswp--animated-in"), a && (i ? o[(n ? "remove" : "add") + "Class"](t, "pswp--animate_opacity") : setTimeout(function() {
              o.addClass(t, "pswp--animate_opacity")
            }, 30)), je = setTimeout(function() {
              if (Et("initialZoom" + (i ? "Out" : "In")), i) {
                var r = l.w / e.w,
                  s = {
                    x: ct.x,
                    y: ct.y
                  },
                  u = g,
                  c = rt,
                  h = function(e) {
                    1 === e ? (g = r, ct.x = l.x, ct.y = l.y - z) : (g = (r - u) * e + u, ct.x = (l.x - s.x) * e + s.x, ct.y = (l.y - z - s.y) * e + s.y), It(), a ? t.style.opacity = 1 - e : St(c - e * c)
                  };
                n ? Kt("initialZoom", 0, 1, d, o.easing.cubic.out, h, f) : (h(1), je = setTimeout(f, d + 20))
              } else g = e.initialZoomLevel, Pt(ct, e.initialPosition), It(), St(1), a ? t.style.opacity = 1 : St(1), je = setTimeout(f, d + 20)
            }, i ? 25 : 90)
          }()
        },
        He = {},
        We = [],
        qe = {
          index: 0,
          errorMsg: '<div class="pswp__error-msg"><a href="%url%" target="_blank">The image</a> could not be loaded.</div>',
          forceProgressiveLoading: !1,
          preload: [1, 1],
          getNumItemsFn: function() {
            return ze.length
          }
        },
        Ve = function(t, e, n) {
          if (t.src && !t.loadError) {
            var i = !n;
            if (i && (t.vGap || (t.vGap = {
                top: 0,
                bottom: 0
              }), Et("parseVerticalMargin", t)), He.x = e.x, He.y = e.y - t.vGap.top - t.vGap.bottom, i) {
              var o = He.x / t.w,
                r = He.y / t.h;
              t.fitRatio = r > o ? o : r;
              var a = s.scaleMode;
              "orig" === a ? n = 1 : "fit" === a && (n = t.fitRatio), n > 1 && (n = 1), t.initialZoomLevel = n, t.bounds || (t.bounds = {
                center: {
                  x: 0,
                  y: 0
                },
                max: {
                  x: 0,
                  y: 0
                },
                min: {
                  x: 0,
                  y: 0
                }
              })
            }
            if (!n) return;
            return function(t, e, n) {
              var i = t.bounds;
              i.center.x = Math.round((He.x - e) / 2), i.center.y = Math.round((He.y - n) / 2) + t.vGap.top, i.max.x = e > He.x ? Math.round(He.x - e) : i.center.x, i.max.y = n > He.y ? Math.round(He.y - n) + t.vGap.top : i.center.y, i.min.x = e > He.x ? 0 : i.center.x, i.min.y = n > He.y ? t.vGap.top : i.center.y
            }(t, t.w * n, t.h * n), i && n === t.initialZoomLevel && (t.initialPosition = t.bounds.center), t.bounds
          }
          return t.w = t.h = 0, t.initialZoomLevel = t.fitRatio = 1, t.bounds = {
            center: {
              x: 0,
              y: 0
            },
            max: {
              x: 0,
              y: 0
            },
            min: {
              x: 0,
              y: 0
            }
          }, t.initialPosition = t.bounds.center, t.bounds
        },
        Ue = function(t, e, n, i, o, s) {
          e.loadError || i && (e.imageAppended = !0, Ke(e, i, e === r.currItem && bt), n.appendChild(i), s && setTimeout(function() {
            e && e.loaded && e.placeholder && (e.placeholder.style.display = "none", e.placeholder = null)
          }, 500))
        },
        Qe = function(t) {
          t.loading = !0, t.loaded = !1;
          var e = t.img = o.createEl("pswp__img", "img"),
            n = function() {
              t.loading = !1, t.loaded = !0, t.loadComplete ? t.loadComplete(t) : t.img = null, e.onload = e.onerror = null, e = null
            };
          return e.onload = n, e.onerror = function() {
            t.loadError = !0, n()
          }, e.src = t.src, e
        },
        Ge = function(t, e) {
          return t.src && t.loadError && t.container ? (e && (t.container.innerHTML = ""), t.container.innerHTML = s.errorMsg.replace("%url%", t.src), !0) : void 0
        },
        Ke = function(t, e, n) {
          if (t.src) {
            e || (e = t.container.lastChild);
            var i = n ? t.w : Math.round(t.w * t.fitRatio),
              o = n ? t.h : Math.round(t.h * t.fitRatio);
            t.placeholder && !t.loaded && (t.placeholder.style.width = i + "px", t.placeholder.style.height = o + "px"), e.style.width = i + "px", e.style.height = o + "px"
          }
        },
        Ye = function() {
          if (We.length) {
            for (var t, e = 0; e < We.length; e++)(t = We[e]).holder.index === t.index && Ue(t.index, t.item, t.baseDiv, t.img, 0, t.clearPlaceholder);
            We = []
          }
        };
      _t("Controller", {
        publicMethods: {
          lazyLoadItem: function(t) {
            t = wt(t);
            var e = Re(t);
            e && (!e.loaded && !e.loading || w) && (Et("gettingData", t, e), e.src && Qe(e))
          },
          initController: function() {
            o.extend(s, qe, !0), r.items = ze = n, Re = r.getItemAt, Fe = s.getNumItemsFn, s.loop, Fe() < 3 && (s.loop = !1), Ct("beforeChange", function(t) {
              var e, n = s.preload,
                i = null === t || t >= 0,
                o = Math.min(n[0], Fe()),
                a = Math.min(n[1], Fe());
              for (e = 1;
                (i ? a : o) >= e; e++) r.lazyLoadItem(c + e);
              for (e = 1;
                (i ? o : a) >= e; e++) r.lazyLoadItem(c - e)
            }), Ct("initialLayout", function() {
              r.currItem.initialLayout = s.getThumbBoundsFn && s.getThumbBoundsFn(c)
            }), Ct("mainScrollAnimComplete", Ye), Ct("initialZoomInEnd", Ye), Ct("destroy", function() {
              for (var t, e = 0; e < ze.length; e++)(t = ze[e]).container && (t.container = null), t.placeholder && (t.placeholder = null), t.img && (t.img = null), t.preloader && (t.preloader = null), t.loadError && (t.loaded = t.loadError = !1);
              We = null
            })
          },
          getItemAt: function(t) {
            return t >= 0 && void 0 !== ze[t] && ze[t]
          },
          allowProgressiveImg: function() {
            return s.forceProgressiveLoading || !D || s.mouseUsed || screen.width > 1200
          },
          setContent: function(t, e) {
            s.loop && (e = wt(e));
            var n = r.getItemAt(t.index);
            n && (n.container = null);
            var i, l = r.getItemAt(e);
            if (l) {
              Et("gettingData", e, l), t.index = e, t.item = l;
              var u = l.container = o.createEl("pswp__zoom-wrap");
              if (!l.src && l.html && (l.html.tagName ? u.appendChild(l.html) : u.innerHTML = l.html), Ge(l), Ve(l, dt), !l.src || l.loadError || l.loaded) l.src && !l.loadError && ((i = o.createEl("pswp__img", "img")).style.opacity = 1, i.src = l.src, Ke(l, i), Ue(0, l, u, i));
              else {
                if (l.loadComplete = function(n) {
                    if (a) {
                      if (t && t.index === e) {
                        if (Ge(n, !0)) return n.loadComplete = n.img = null, Ve(n, dt), At(n), void(t.index === c && r.updateCurrZoomItem());
                        n.imageAppended ? !$e && n.placeholder && (n.placeholder.style.display = "none", n.placeholder = null) : M.transform && (et || $e) ? We.push({
                          item: n,
                          baseDiv: u,
                          img: n.img,
                          index: e,
                          holder: t,
                          clearPlaceholder: !0
                        }) : Ue(0, n, u, n.img, 0, !0)
                      }
                      n.loadComplete = null, n.img = null, Et("imageLoadComplete", e, n)
                    }
                  }, o.features.transform) {
                  var d = "pswp__img pswp__img--placeholder";
                  d += l.msrc ? "" : " pswp__img--placeholder--blank";
                  var f = o.createEl(d, l.msrc ? "img" : "");
                  l.msrc && (f.src = l.msrc), Ke(l, f), u.appendChild(f), l.placeholder = f
                }
                l.loading || Qe(l), r.allowProgressiveImg() && (!Me && M.transform ? We.push({
                  item: l,
                  baseDiv: u,
                  img: l.img,
                  index: e,
                  holder: t
                }) : Ue(0, l, u, l.img, 0, !0))
              }
              Me || e !== c ? At(l) : (tt = u.style, Be(l, i || l.img)), t.el.innerHTML = "", t.el.appendChild(u)
            } else t.el.innerHTML = ""
          },
          cleanSlide: function(t) {
            t.img && (t.img.onload = t.img.onerror = null), t.loaded = t.loading = t.img = t.imageAppended = !1
          }
        }
      });
      var Ze, Xe, Je = {},
        tn = function(t, e, n) {
          var i = document.createEvent("CustomEvent"),
            o = {
              origEvent: t,
              target: t.target,
              releasePoint: e,
              pointerType: n || "touch"
            };
          i.initCustomEvent("pswpTap", !0, !0, o), t.target.dispatchEvent(i)
        };
      _t("Tap", {
        publicMethods: {
          initTap: function() {
            Ct("firstTouchStart", r.onTapStart), Ct("touchRelease", r.onTapRelease), Ct("destroy", function() {
              Je = {}, Ze = null
            })
          },
          onTapStart: function(t) {
            t.length > 1 && (clearTimeout(Ze), Ze = null)
          },
          onTapRelease: function(t, e) {
            if (e && !Q && !V && !Vt) {
              var n = e;
              if (Ze && (clearTimeout(Ze), Ze = null, function(t, e) {
                  return Math.abs(t.x - e.x) < 25 && Math.abs(t.y - e.y) < 25
                }(n, Je))) return void Et("doubleTap", n);
              if ("mouse" === e.type) return void tn(t, e, "mouse");
              if ("BUTTON" === t.target.tagName.toUpperCase() || o.hasClass(t.target, "pswp__single-tap")) return void tn(t, e);
              Pt(Je, n), Ze = setTimeout(function() {
                tn(t, e), Ze = null
              }, 300)
            }
          }
        }
      }), _t("DesktopZoom", {
        publicMethods: {
          initDesktopZoom: function() {
            j || (D ? Ct("mouseUsed", function() {
              r.setupDesktopZoom()
            }) : r.setupDesktopZoom(!0))
          },
          setupDesktopZoom: function(e) {
            Xe = {};
            var n = "wheel mousewheel DOMMouseScroll";
            Ct("bindEvents", function() {
              o.bind(t, n, r.handleMouseWheel)
            }), Ct("unbindEvents", function() {
              Xe && o.unbind(t, n, r.handleMouseWheel)
            }), r.mouseZoomedIn = !1;
            var i, s = function() {
                r.mouseZoomedIn && (o.removeClass(t, "pswp--zoomed-in"), r.mouseZoomedIn = !1), 1 > g ? o.addClass(t, "pswp--zoom-allowed") : o.removeClass(t, "pswp--zoom-allowed"), a()
              },
              a = function() {
                i && (o.removeClass(t, "pswp--dragging"), i = !1)
              };
            Ct("resize", s), Ct("afterChange", s), Ct("pointerDown", function() {
              r.mouseZoomedIn && (i = !0, o.addClass(t, "pswp--dragging"))
            }), Ct("pointerUp", a), e || s()
          },
          handleMouseWheel: function(t) {
            if (g <= r.currItem.fitRatio) return s.modal && (!s.closeOnScroll || Vt || q ? t.preventDefault() : I && Math.abs(t.deltaY) > 2 && (u = !0, r.close())), !0;
            if (t.stopPropagation(), Xe.x = 0, "deltaX" in t) 1 === t.deltaMode ? (Xe.x = 18 * t.deltaX, Xe.y = 18 * t.deltaY) : (Xe.x = t.deltaX, Xe.y = t.deltaY);
            else if ("wheelDelta" in t) t.wheelDeltaX && (Xe.x = -.16 * t.wheelDeltaX), t.wheelDeltaY ? Xe.y = -.16 * t.wheelDeltaY : Xe.y = -.16 * t.wheelDelta;
            else {
              if (!("detail" in t)) return;
              Xe.y = t.detail
            }
            Mt(g, !0);
            var e = ct.x - Xe.x,
              n = ct.y - Xe.y;
            (s.modal || e <= J.min.x && e >= J.max.x && n <= J.min.y && n >= J.max.y) && t.preventDefault(), r.panTo(e, n)
          },
          toggleDesktopZoom: function(e) {
            e = e || {
              x: dt.x / 2 + ht.x,
              y: dt.y / 2 + ht.y
            };
            var n = s.getDoubleTapZoom(!0, r.currItem),
              i = g === n;
            r.mouseZoomedIn = !i, r.zoomTo(i ? r.currItem.initialZoomLevel : n, e, 333), o[(i ? "remove" : "add") + "Class"](t, "pswp--zoomed-in")
          }
        }
      });
      var en, nn, on, rn, sn, an, ln, un, cn, dn, fn, hn, pn = {
          history: !0,
          galleryUID: 1
        },
        mn = function() {
          return fn.hash.substring(1)
        },
        gn = function() {
          en && clearTimeout(en), on && clearTimeout(on)
        },
        vn = function() {
          var t = mn(),
            e = {};
          if (t.length < 5) return e;
          var n, i = t.split("&");
          for (n = 0; n < i.length; n++)
            if (i[n]) {
              var o = i[n].split("=");
              o.length < 2 || (e[o[0]] = o[1])
            } if (s.galleryPIDs) {
            var r = e.pid;
            for (e.pid = 0, n = 0; n < ze.length; n++)
              if (ze[n].pid === r) {
                e.pid = n;
                break
              }
          } else e.pid = parseInt(e.pid, 10) - 1;
          return e.pid < 0 && (e.pid = 0), e
        },
        yn = function() {
          if (on && clearTimeout(on), Vt || q) on = setTimeout(yn, 500);
          else {
            rn ? clearTimeout(nn) : rn = !0;
            var t = c + 1,
              e = Re(c);
            e.hasOwnProperty("pid") && (t = e.pid);
            var n = ln + "&gid=" + s.galleryUID + "&pid=" + t;
            un || -1 === fn.hash.indexOf(n) && (dn = !0);
            var i = fn.href.split("#")[0] + "#" + n;
            hn ? "#" + n !== window.location.hash && history[un ? "replaceState" : "pushState"]("", document.title, i) : un ? fn.replace(i) : fn.hash = n, un = !0, nn = setTimeout(function() {
              rn = !1
            }, 60)
          }
        };
      _t("History", {
        publicMethods: {
          initHistory: function() {
            if (o.extend(s, pn, !0), s.history) {
              fn = window.location, dn = !1, cn = !1, un = !1, ln = mn(), hn = "pushState" in history, ln.indexOf("gid=") > -1 && (ln = (ln = ln.split("&gid=")[0]).split("?gid=")[0]), Ct("afterChange", r.updateURL), Ct("unbindEvents", function() {
                o.unbind(window, "hashchange", r.onHashChange)
              });
              var t = function() {
                an = !0, cn || (dn ? history.back() : ln ? fn.hash = ln : hn ? history.pushState("", document.title, fn.pathname + fn.search) : fn.hash = ""), gn()
              };
              Ct("unbindEvents", function() {
                u && t()
              }), Ct("destroy", function() {
                an || t()
              }), Ct("firstUpdate", function() {
                c = vn().pid
              });
              var e = ln.indexOf("pid=");
              e > -1 && "&" === (ln = ln.substring(0, e)).slice(-1) && (ln = ln.slice(0, -1)), setTimeout(function() {
                a && o.bind(window, "hashchange", r.onHashChange)
              }, 40)
            }
          },
          onHashChange: function() {
            return mn() === ln ? (cn = !0, void r.close()) : void(rn || (sn = !0, r.goTo(vn().pid), sn = !1))
          },
          updateURL: function() {
            gn(), sn || (un ? en = setTimeout(yn, 800) : yn())
          }
        }
      }), o.extend(r, Yt)
    }
  }), "undefined" == typeof jQuery) throw new Error("Bootstrap's JavaScript requires jQuery. jQuery must be included before Bootstrap's JavaScript.");
! function(t) {
  var e = jQuery.fn.jquery.split(" ")[0].split(".");
  if (e[0] < 2 && e[1] < 9 || 1 == e[0] && 9 == e[1] && e[2] < 1 || e[0] >= 4) throw new Error("Bootstrap's JavaScript requires at least jQuery v1.9.1 but less than v4.0.0")
}(),
function() {
  function t(t, e) {
    if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function")
  }
  var e = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function(t) {
      return typeof t
    } : function(t) {
      return t && "function" == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype ? "symbol" : typeof t
    },
    n = function() {
      function t(t, e) {
        for (var n = 0; n < e.length; n++) {
          var i = e[n];
          i.enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), Object.defineProperty(t, i.key, i)
        }
      }
      return function(e, n, i) {
        return n && t(e.prototype, n), i && t(e, i), e
      }
    }(),
    i = function(t) {
      function e(t) {
        return {}.toString.call(t).match(/\s([a-zA-Z]+)/)[1].toLowerCase()
      }

      function n(t) {
        return (t[0] || t).nodeType
      }
      var i = !1,
        o = {
          WebkitTransition: "webkitTransitionEnd",
          MozTransition: "transitionend",
          OTransition: "oTransitionEnd otransitionend",
          transition: "transitionend"
        },
        r = {
          TRANSITION_END: "bsTransitionEnd",
          getUID: function(t) {
            do {
              t += ~~(1e6 * Math.random())
            } while (document.getElementById(t));
            return t
          },
          getSelectorFromElement: function(e) {
            var n = e.getAttribute("data-target");
            n && "#" !== n || (n = e.getAttribute("href") || "");
            try {
              return t(n).length > 0 ? n : null
            } catch (t) {
              return null
            }
          },
          reflow: function(t) {
            return t.offsetHeight
          },
          triggerTransitionEnd: function(e) {
            t(e).trigger(i.end)
          },
          supportsTransitionEnd: function() {
            return Boolean(i)
          },
          typeCheckConfig: function(t, i, o) {
            for (var r in o)
              if (o.hasOwnProperty(r)) {
                var s = o[r],
                  a = i[r],
                  l = a && n(a) ? "element" : e(a);
                if (!new RegExp(s).test(l)) throw new Error(t.toUpperCase() + ': Option "' + r + '" provided type "' + l + '" but expected type "' + s + '".')
              }
          }
        };
      return i = function() {
        if (window.QUnit) return !1;
        var t = document.createElement("bootstrap");
        for (var e in o)
          if (void 0 !== t.style[e]) return {
            end: o[e]
          };
        return !1
      }(), t.fn.emulateTransitionEnd = function(e) {
        var n = this,
          i = !1;
        return t(this).one(r.TRANSITION_END, function() {
          i = !0
        }), setTimeout(function() {
          i || r.triggerTransitionEnd(n)
        }, e), this
      }, r.supportsTransitionEnd() && (t.event.special[r.TRANSITION_END] = {
        bindType: i.end,
        delegateType: i.end,
        handle: function(e) {
          if (t(e.target).is(this)) return e.handleObj.handler.apply(this, arguments)
        }
      }), r
    }(jQuery),
    o = (function(e) {
      var o = "alert",
        r = e.fn[o],
        s = "close.bs.alert",
        a = "closed.bs.alert",
        l = "click.bs.alert.data-api",
        u = "alert",
        c = "fade",
        d = "show",
        f = function() {
          function o(e) {
            t(this, o), this._element = e
          }
          return o.prototype.close = function(t) {
            t = t || this._element;
            var e = this._getRootElement(t);
            this._triggerCloseEvent(e).isDefaultPrevented() || this._removeElement(e)
          }, o.prototype.dispose = function() {
            e.removeData(this._element, "bs.alert"), this._element = null
          }, o.prototype._getRootElement = function(t) {
            var n = i.getSelectorFromElement(t),
              o = !1;
            return n && (o = e(n)[0]), o || (o = e(t).closest("." + u)[0]), o
          }, o.prototype._triggerCloseEvent = function(t) {
            var n = e.Event(s);
            return e(t).trigger(n), n
          }, o.prototype._removeElement = function(t) {
            var n = this;
            e(t).removeClass(d), i.supportsTransitionEnd() && e(t).hasClass(c) ? e(t).one(i.TRANSITION_END, function(e) {
              return n._destroyElement(t, e)
            }).emulateTransitionEnd(150) : this._destroyElement(t)
          }, o.prototype._destroyElement = function(t) {
            e(t).detach().trigger(a).remove()
          }, o._jQueryInterface = function(t) {
            return this.each(function() {
              var n = e(this),
                i = n.data("bs.alert");
              i || (i = new o(this), n.data("bs.alert", i)), "close" === t && i[t](this)
            })
          }, o._handleDismiss = function(t) {
            return function(e) {
              e && e.preventDefault(), t.close(this)
            }
          }, n(o, null, [{
            key: "VERSION",
            get: function() {
              return "4.0.0-beta"
            }
          }]), o
        }();
      e(document).on(l, '[data-dismiss="alert"]', f._handleDismiss(new f)), e.fn[o] = f._jQueryInterface, e.fn[o].Constructor = f, e.fn[o].noConflict = function() {
        return e.fn[o] = r, f._jQueryInterface
      }
    }(jQuery), function(e) {
      var i = "button",
        o = e.fn[i],
        r = "active",
        s = "btn",
        a = "focus",
        l = '[data-toggle^="button"]',
        u = '[data-toggle="buttons"]',
        c = "input",
        d = ".active",
        f = ".btn",
        h = "click.bs.button.data-api",
        p = "focus.bs.button.data-api blur.bs.button.data-api",
        m = function() {
          function i(e) {
            t(this, i), this._element = e
          }
          return i.prototype.toggle = function() {
            var t = !0,
              n = !0,
              i = e(this._element).closest(u)[0];
            if (i) {
              var o = e(this._element).find(c)[0];
              if (o) {
                if ("radio" === o.type)
                  if (o.checked && e(this._element).hasClass(r)) t = !1;
                  else {
                    var s = e(i).find(d)[0];
                    s && e(s).removeClass(r)
                  } if (t) {
                  if (o.hasAttribute("disabled") || i.hasAttribute("disabled") || o.classList.contains("disabled") || i.classList.contains("disabled")) return;
                  o.checked = !e(this._element).hasClass(r), e(o).trigger("change")
                }
                o.focus(), n = !1
              }
            }
            n && this._element.setAttribute("aria-pressed", !e(this._element).hasClass(r)), t && e(this._element).toggleClass(r)
          }, i.prototype.dispose = function() {
            e.removeData(this._element, "bs.button"), this._element = null
          }, i._jQueryInterface = function(t) {
            return this.each(function() {
              var n = e(this).data("bs.button");
              n || (n = new i(this), e(this).data("bs.button", n)), "toggle" === t && n[t]()
            })
          }, n(i, null, [{
            key: "VERSION",
            get: function() {
              return "4.0.0-beta"
            }
          }]), i
        }();
      e(document).on(h, l, function(t) {
        t.preventDefault();
        var n = t.target;
        e(n).hasClass(s) || (n = e(n).closest(f)), m._jQueryInterface.call(e(n), "toggle")
      }).on(p, l, function(t) {
        var n = e(t.target).closest(f)[0];
        e(n).toggleClass(a, /^focus(in)?$/.test(t.type))
      }), e.fn[i] = m._jQueryInterface, e.fn[i].Constructor = m, e.fn[i].noConflict = function() {
        return e.fn[i] = o, m._jQueryInterface
      }
    }(jQuery), function(o) {
      var r = "carousel",
        s = "bs.carousel",
        a = "." + s,
        l = o.fn[r],
        u = {
          interval: 5e3,
          keyboard: !0,
          slide: !1,
          pause: "hover",
          wrap: !0
        },
        c = {
          interval: "(number|boolean)",
          keyboard: "boolean",
          slide: "(boolean|string)",
          pause: "(string|boolean)",
          wrap: "boolean"
        },
        d = "next",
        f = "prev",
        h = "left",
        p = "right",
        m = {
          SLIDE: "slide" + a,
          SLID: "slid" + a,
          KEYDOWN: "keydown" + a,
          MOUSEENTER: "mouseenter" + a,
          MOUSELEAVE: "mouseleave" + a,
          TOUCHEND: "touchend" + a,
          LOAD_DATA_API: "load.bs.carousel.data-api",
          CLICK_DATA_API: "click.bs.carousel.data-api"
        },
        g = "carousel",
        v = "active",
        y = "slide",
        b = "carousel-item-right",
        _ = "carousel-item-left",
        w = "carousel-item-next",
        x = "carousel-item-prev",
        C = {
          ACTIVE: ".active",
          ACTIVE_ITEM: ".active.carousel-item",
          ITEM: ".carousel-item",
          NEXT_PREV: ".carousel-item-next, .carousel-item-prev",
          INDICATORS: ".carousel-indicators",
          DATA_SLIDE: "[data-slide], [data-slide-to]",
          DATA_RIDE: '[data-ride="carousel"]'
        },
        E = function() {
          function l(e, n) {
            t(this, l), this._items = null, this._interval = null, this._activeElement = null, this._isPaused = !1, this._isSliding = !1, this.touchTimeout = null, this._config = this._getConfig(n), this._element = o(e)[0], this._indicatorsElement = o(this._element).find(C.INDICATORS)[0], this._addEventListeners()
          }
          return l.prototype.next = function() {
            this._isSliding || this._slide(d)
          }, l.prototype.nextWhenVisible = function() {
            document.hidden || this.next()
          }, l.prototype.prev = function() {
            this._isSliding || this._slide(f)
          }, l.prototype.pause = function(t) {
            t || (this._isPaused = !0), o(this._element).find(C.NEXT_PREV)[0] && i.supportsTransitionEnd() && (i.triggerTransitionEnd(this._element), this.cycle(!0)), clearInterval(this._interval), this._interval = null
          }, l.prototype.cycle = function(t) {
            t || (this._isPaused = !1), this._interval && (clearInterval(this._interval), this._interval = null), this._config.interval && !this._isPaused && (this._interval = setInterval((document.visibilityState ? this.nextWhenVisible : this.next).bind(this), this._config.interval))
          }, l.prototype.to = function(t) {
            var e = this;
            this._activeElement = o(this._element).find(C.ACTIVE_ITEM)[0];
            var n = this._getItemIndex(this._activeElement);
            if (!(t > this._items.length - 1 || t < 0))
              if (this._isSliding) o(this._element).one(m.SLID, function() {
                return e.to(t)
              });
              else {
                if (n === t) return this.pause(), void this.cycle();
                var i = t > n ? d : f;
                this._slide(i, this._items[t])
              }
          }, l.prototype.dispose = function() {
            o(this._element).off(a), o.removeData(this._element, s), this._items = null, this._config = null, this._element = null, this._interval = null, this._isPaused = null, this._isSliding = null, this._activeElement = null, this._indicatorsElement = null
          }, l.prototype._getConfig = function(t) {
            return t = o.extend({}, u, t), i.typeCheckConfig(r, t, c), t
          }, l.prototype._addEventListeners = function() {
            var t = this;
            this._config.keyboard && o(this._element).on(m.KEYDOWN, function(e) {
              return t._keydown(e)
            }), "hover" === this._config.pause && (o(this._element).on(m.MOUSEENTER, function(e) {
              return t.pause(e)
            }).on(m.MOUSELEAVE, function(e) {
              return t.cycle(e)
            }), "ontouchstart" in document.documentElement && o(this._element).on(m.TOUCHEND, function() {
              t.pause(), t.touchTimeout && clearTimeout(t.touchTimeout), t.touchTimeout = setTimeout(function(e) {
                return t.cycle(e)
              }, 500 + t._config.interval)
            }))
          }, l.prototype._keydown = function(t) {
            if (!/input|textarea/i.test(t.target.tagName)) switch (t.which) {
              case 37:
                t.preventDefault(), this.prev();
                break;
              case 39:
                t.preventDefault(), this.next();
                break;
              default:
                return
            }
          }, l.prototype._getItemIndex = function(t) {
            return this._items = o.makeArray(o(t).parent().find(C.ITEM)), this._items.indexOf(t)
          }, l.prototype._getItemByDirection = function(t, e) {
            var n = t === d,
              i = t === f,
              o = this._getItemIndex(e),
              r = this._items.length - 1;
            if ((i && 0 === o || n && o === r) && !this._config.wrap) return e;
            var s = (o + (t === f ? -1 : 1)) % this._items.length;
            return -1 === s ? this._items[this._items.length - 1] : this._items[s]
          }, l.prototype._triggerSlideEvent = function(t, e) {
            var n = this._getItemIndex(t),
              i = this._getItemIndex(o(this._element).find(C.ACTIVE_ITEM)[0]),
              r = o.Event(m.SLIDE, {
                relatedTarget: t,
                direction: e,
                from: i,
                to: n
              });
            return o(this._element).trigger(r), r
          }, l.prototype._setActiveIndicatorElement = function(t) {
            if (this._indicatorsElement) {
              o(this._indicatorsElement).find(C.ACTIVE).removeClass(v);
              var e = this._indicatorsElement.children[this._getItemIndex(t)];
              e && o(e).addClass(v)
            }
          }, l.prototype._slide = function(t, e) {
            var n = this,
              r = o(this._element).find(C.ACTIVE_ITEM)[0],
              s = this._getItemIndex(r),
              a = e || r && this._getItemByDirection(t, r),
              l = this._getItemIndex(a),
              u = Boolean(this._interval),
              c = void 0,
              f = void 0,
              g = void 0;
            if (t === d ? (c = _, f = w, g = h) : (c = b, f = x, g = p), a && o(a).hasClass(v)) this._isSliding = !1;
            else if (!this._triggerSlideEvent(a, g).isDefaultPrevented() && r && a) {
              this._isSliding = !0, u && this.pause(), this._setActiveIndicatorElement(a);
              var E = o.Event(m.SLID, {
                relatedTarget: a,
                direction: g,
                from: s,
                to: l
              });
              i.supportsTransitionEnd() && o(this._element).hasClass(y) ? (o(a).addClass(f), i.reflow(a), o(r).addClass(c), o(a).addClass(c), o(r).one(i.TRANSITION_END, function() {
                o(a).removeClass(c + " " + f).addClass(v), o(r).removeClass(v + " " + f + " " + c), n._isSliding = !1, setTimeout(function() {
                  return o(n._element).trigger(E)
                }, 0)
              }).emulateTransitionEnd(600)) : (o(r).removeClass(v), o(a).addClass(v), this._isSliding = !1, o(this._element).trigger(E)), u && this.cycle()
            }
          }, l._jQueryInterface = function(t) {
            return this.each(function() {
              var n = o(this).data(s),
                i = o.extend({}, u, o(this).data());
              "object" === (void 0 === t ? "undefined" : e(t)) && o.extend(i, t);
              var r = "string" == typeof t ? t : i.slide;
              if (n || (n = new l(this, i), o(this).data(s, n)), "number" == typeof t) n.to(t);
              else if ("string" == typeof r) {
                if (void 0 === n[r]) throw new Error('No method named "' + r + '"');
                n[r]()
              } else i.interval && (n.pause(), n.cycle())
            })
          }, l._dataApiClickHandler = function(t) {
            var e = i.getSelectorFromElement(this);
            if (e) {
              var n = o(e)[0];
              if (n && o(n).hasClass(g)) {
                var r = o.extend({}, o(n).data(), o(this).data()),
                  a = this.getAttribute("data-slide-to");
                a && (r.interval = !1), l._jQueryInterface.call(o(n), r), a && o(n).data(s).to(a), t.preventDefault()
              }
            }
          }, n(l, null, [{
            key: "VERSION",
            get: function() {
              return "4.0.0-beta"
            }
          }, {
            key: "Default",
            get: function() {
              return u
            }
          }]), l
        }();
      o(document).on(m.CLICK_DATA_API, C.DATA_SLIDE, E._dataApiClickHandler), o(window).on(m.LOAD_DATA_API, function() {
        o(C.DATA_RIDE).each(function() {
          var t = o(this);
          E._jQueryInterface.call(t, t.data())
        })
      }), o.fn[r] = E._jQueryInterface, o.fn[r].Constructor = E, o.fn[r].noConflict = function() {
        return o.fn[r] = l, E._jQueryInterface
      }
    }(jQuery), function(o) {
      var r = "collapse",
        s = "bs.collapse",
        a = o.fn[r],
        l = {
          toggle: !0,
          parent: ""
        },
        u = {
          toggle: "boolean",
          parent: "string"
        },
        c = "show.bs.collapse",
        d = "shown.bs.collapse",
        f = "hide.bs.collapse",
        h = "hidden.bs.collapse",
        p = "click.bs.collapse.data-api",
        m = "show",
        g = "collapse",
        v = "collapsing",
        y = "collapsed",
        b = "width",
        _ = "height",
        w = {
          ACTIVES: ".show, .collapsing",
          DATA_TOGGLE: '[data-toggle="collapse"]'
        },
        x = function() {
          function a(e, n) {
            t(this, a), this._isTransitioning = !1, this._element = e, this._config = this._getConfig(n), this._triggerArray = o.makeArray(o('[data-toggle="collapse"][href="#' + e.id + '"],[data-toggle="collapse"][data-target="#' + e.id + '"]'));
            for (var r = o(w.DATA_TOGGLE), s = 0; s < r.length; s++) {
              var l = r[s],
                u = i.getSelectorFromElement(l);
              null !== u && o(u).filter(e).length > 0 && this._triggerArray.push(l)
            }
            this._parent = this._config.parent ? this._getParent() : null, this._config.parent || this._addAriaAndCollapsedClass(this._element, this._triggerArray), this._config.toggle && this.toggle()
          }
          return a.prototype.toggle = function() {
            o(this._element).hasClass(m) ? this.hide() : this.show()
          }, a.prototype.show = function() {
            var t = this;
            if (!this._isTransitioning && !o(this._element).hasClass(m)) {
              var e = void 0,
                n = void 0;
              if (this._parent && ((e = o.makeArray(o(this._parent).children().children(w.ACTIVES))).length || (e = null)), !(e && (n = o(e).data(s)) && n._isTransitioning)) {
                var r = o.Event(c);
                if (o(this._element).trigger(r), !r.isDefaultPrevented()) {
                  e && (a._jQueryInterface.call(o(e), "hide"), n || o(e).data(s, null));
                  var l = this._getDimension();
                  o(this._element).removeClass(g).addClass(v), this._element.style[l] = 0, this._triggerArray.length && o(this._triggerArray).removeClass(y).attr("aria-expanded", !0), this.setTransitioning(!0);
                  var u = function() {
                    o(t._element).removeClass(v).addClass(g).addClass(m), t._element.style[l] = "", t.setTransitioning(!1), o(t._element).trigger(d)
                  };
                  if (i.supportsTransitionEnd()) {
                    var f = "scroll" + (l[0].toUpperCase() + l.slice(1));
                    o(this._element).one(i.TRANSITION_END, u).emulateTransitionEnd(600), this._element.style[l] = this._element[f] + "px"
                  } else u()
                }
              }
            }
          }, a.prototype.hide = function() {
            var t = this;
            if (!this._isTransitioning && o(this._element).hasClass(m)) {
              var e = o.Event(f);
              if (o(this._element).trigger(e), !e.isDefaultPrevented()) {
                var n = this._getDimension();
                if (this._element.style[n] = this._element.getBoundingClientRect()[n] + "px", i.reflow(this._element), o(this._element).addClass(v).removeClass(g).removeClass(m), this._triggerArray.length)
                  for (var r = 0; r < this._triggerArray.length; r++) {
                    var s = this._triggerArray[r],
                      a = i.getSelectorFromElement(s);
                    null !== a && (o(a).hasClass(m) || o(s).addClass(y).attr("aria-expanded", !1))
                  }
                this.setTransitioning(!0);
                var l = function() {
                  t.setTransitioning(!1), o(t._element).removeClass(v).addClass(g).trigger(h)
                };
                this._element.style[n] = "", i.supportsTransitionEnd() ? o(this._element).one(i.TRANSITION_END, l).emulateTransitionEnd(600) : l()
              }
            }
          }, a.prototype.setTransitioning = function(t) {
            this._isTransitioning = t
          }, a.prototype.dispose = function() {
            o.removeData(this._element, s), this._config = null, this._parent = null, this._element = null, this._triggerArray = null, this._isTransitioning = null
          }, a.prototype._getConfig = function(t) {
            return (t = o.extend({}, l, t)).toggle = Boolean(t.toggle), i.typeCheckConfig(r, t, u), t
          }, a.prototype._getDimension = function() {
            return o(this._element).hasClass(b) ? b : _
          }, a.prototype._getParent = function() {
            var t = this,
              e = o(this._config.parent)[0],
              n = '[data-toggle="collapse"][data-parent="' + this._config.parent + '"]';
            return o(e).find(n).each(function(e, n) {
              t._addAriaAndCollapsedClass(a._getTargetFromElement(n), [n])
            }), e
          }, a.prototype._addAriaAndCollapsedClass = function(t, e) {
            if (t) {
              var n = o(t).hasClass(m);
              e.length && o(e).toggleClass(y, !n).attr("aria-expanded", n)
            }
          }, a._getTargetFromElement = function(t) {
            var e = i.getSelectorFromElement(t);
            return e ? o(e)[0] : null
          }, a._jQueryInterface = function(t) {
            return this.each(function() {
              var n = o(this),
                i = n.data(s),
                r = o.extend({}, l, n.data(), "object" === (void 0 === t ? "undefined" : e(t)) && t);
              if (!i && r.toggle && /show|hide/.test(t) && (r.toggle = !1), i || (i = new a(this, r), n.data(s, i)), "string" == typeof t) {
                if (void 0 === i[t]) throw new Error('No method named "' + t + '"');
                i[t]()
              }
            })
          }, n(a, null, [{
            key: "VERSION",
            get: function() {
              return "4.0.0-beta"
            }
          }, {
            key: "Default",
            get: function() {
              return l
            }
          }]), a
        }();
      o(document).on(p, w.DATA_TOGGLE, function(t) {
        /input|textarea/i.test(t.target.tagName) || t.preventDefault();
        var e = o(this),
          n = i.getSelectorFromElement(this);
        o(n).each(function() {
          var t = o(this),
            n = t.data(s) ? "toggle" : e.data();
          x._jQueryInterface.call(t, n)
        })
      }), o.fn[r] = x._jQueryInterface, o.fn[r].Constructor = x, o.fn[r].noConflict = function() {
        return o.fn[r] = a, x._jQueryInterface
      }
    }(jQuery), function(o) {
      if ("undefined" == typeof Popper) throw new Error("Bootstrap dropdown require Popper.js (https://popper.js.org)");
      var r = "dropdown",
        s = "bs.dropdown",
        a = "." + s,
        l = o.fn[r],
        u = new RegExp("38|40|27"),
        c = {
          HIDE: "hide" + a,
          HIDDEN: "hidden" + a,
          SHOW: "show" + a,
          SHOWN: "shown" + a,
          CLICK: "click" + a,
          CLICK_DATA_API: "click.bs.dropdown.data-api",
          KEYDOWN_DATA_API: "keydown.bs.dropdown.data-api",
          KEYUP_DATA_API: "keyup.bs.dropdown.data-api"
        },
        d = "disabled",
        f = "show",
        h = "dropup",
        p = "dropdown-menu-right",
        m = "dropdown-menu-left",
        g = '[data-toggle="dropdown"]',
        v = ".dropdown form",
        y = ".dropdown-menu",
        b = ".navbar-nav",
        _ = ".dropdown-menu .dropdown-item:not(.disabled)",
        w = {
          TOP: "top-start",
          TOPEND: "top-end",
          BOTTOM: "bottom-start",
          BOTTOMEND: "bottom-end"
        },
        x = {
          placement: w.BOTTOM,
          offset: 0,
          flip: !0
        },
        C = {
          placement: "string",
          offset: "(number|string)",
          flip: "boolean"
        },
        E = function() {
          function l(e, n) {
            t(this, l), this._element = e, this._popper = null, this._config = this._getConfig(n), this._menu = this._getMenuElement(), this._inNavbar = this._detectNavbar(), this._addEventListeners()
          }
          return l.prototype.toggle = function() {
            if (!this._element.disabled && !o(this._element).hasClass(d)) {
              var t = l._getParentFromElement(this._element),
                e = o(this._menu).hasClass(f);
              if (l._clearMenus(), !e) {
                var n = {
                    relatedTarget: this._element
                  },
                  i = o.Event(c.SHOW, n);
                if (o(t).trigger(i), !i.isDefaultPrevented()) {
                  var r = this._element;
                  o(t).hasClass(h) && (o(this._menu).hasClass(m) || o(this._menu).hasClass(p)) && (r = t), this._popper = new Popper(r, this._menu, this._getPopperConfig()), "ontouchstart" in document.documentElement && !o(t).closest(b).length && o("body").children().on("mouseover", null, o.noop), this._element.focus(), this._element.setAttribute("aria-expanded", !0), o(this._menu).toggleClass(f), o(t).toggleClass(f).trigger(o.Event(c.SHOWN, n))
                }
              }
            }
          }, l.prototype.dispose = function() {
            o.removeData(this._element, s), o(this._element).off(a), this._element = null, this._menu = null, null !== this._popper && this._popper.destroy(), this._popper = null
          }, l.prototype.update = function() {
            this._inNavbar = this._detectNavbar(), null !== this._popper && this._popper.scheduleUpdate()
          }, l.prototype._addEventListeners = function() {
            var t = this;
            o(this._element).on(c.CLICK, function(e) {
              e.preventDefault(), e.stopPropagation(), t.toggle()
            })
          }, l.prototype._getConfig = function(t) {
            var e = o(this._element).data();
            return void 0 !== e.placement && (e.placement = w[e.placement.toUpperCase()]), t = o.extend({}, this.constructor.Default, o(this._element).data(), t), i.typeCheckConfig(r, t, this.constructor.DefaultType), t
          }, l.prototype._getMenuElement = function() {
            if (!this._menu) {
              var t = l._getParentFromElement(this._element);
              this._menu = o(t).find(y)[0]
            }
            return this._menu
          }, l.prototype._getPlacement = function() {
            var t = o(this._element).parent(),
              e = this._config.placement;
            return t.hasClass(h) || this._config.placement === w.TOP ? (e = w.TOP, o(this._menu).hasClass(p) && (e = w.TOPEND)) : o(this._menu).hasClass(p) && (e = w.BOTTOMEND), e
          }, l.prototype._detectNavbar = function() {
            return o(this._element).closest(".navbar").length > 0
          }, l.prototype._getPopperConfig = function() {
            var t = {
              placement: this._getPlacement(),
              modifiers: {
                offset: {
                  offset: this._config.offset
                },
                flip: {
                  enabled: this._config.flip
                }
              }
            };
            return this._inNavbar && (t.modifiers.applyStyle = {
              enabled: !this._inNavbar
            }), t
          }, l._jQueryInterface = function(t) {
            return this.each(function() {
              var n = o(this).data(s),
                i = "object" === (void 0 === t ? "undefined" : e(t)) ? t : null;
              if (n || (n = new l(this, i), o(this).data(s, n)), "string" == typeof t) {
                if (void 0 === n[t]) throw new Error('No method named "' + t + '"');
                n[t]()
              }
            })
          }, l._clearMenus = function(t) {
            if (!t || 3 !== t.which && ("keyup" !== t.type || 9 === t.which))
              for (var e = o.makeArray(o(g)), n = 0; n < e.length; n++) {
                var i = l._getParentFromElement(e[n]),
                  r = o(e[n]).data(s),
                  a = {
                    relatedTarget: e[n]
                  };
                if (r) {
                  var u = r._menu;
                  if (o(i).hasClass(f) && !(t && ("click" === t.type && /input|textarea/i.test(t.target.tagName) || "keyup" === t.type && 9 === t.which) && o.contains(i, t.target))) {
                    var d = o.Event(c.HIDE, a);
                    o(i).trigger(d), d.isDefaultPrevented() || ("ontouchstart" in document.documentElement && o("body").children().off("mouseover", null, o.noop), e[n].setAttribute("aria-expanded", "false"), o(u).removeClass(f), o(i).removeClass(f).trigger(o.Event(c.HIDDEN, a)))
                  }
                }
              }
          }, l._getParentFromElement = function(t) {
            var e = void 0,
              n = i.getSelectorFromElement(t);
            return n && (e = o(n)[0]), e || t.parentNode
          }, l._dataApiKeydownHandler = function(t) {
            if (!(!u.test(t.which) || /button/i.test(t.target.tagName) && 32 === t.which || /input|textarea/i.test(t.target.tagName) || (t.preventDefault(), t.stopPropagation(), this.disabled || o(this).hasClass(d)))) {
              var e = l._getParentFromElement(this),
                n = o(e).hasClass(f);
              if ((n || 27 === t.which && 32 === t.which) && (!n || 27 !== t.which && 32 !== t.which)) {
                var i = o(e).find(_).get();
                if (i.length) {
                  var r = i.indexOf(t.target);
                  38 === t.which && r > 0 && r--, 40 === t.which && r < i.length - 1 && r++, r < 0 && (r = 0), i[r].focus()
                }
              } else {
                if (27 === t.which) {
                  var s = o(e).find(g)[0];
                  o(s).trigger("focus")
                }
                o(this).trigger("click")
              }
            }
          }, n(l, null, [{
            key: "VERSION",
            get: function() {
              return "4.0.0-beta"
            }
          }, {
            key: "Default",
            get: function() {
              return x
            }
          }, {
            key: "DefaultType",
            get: function() {
              return C
            }
          }]), l
        }();
      o(document).on(c.KEYDOWN_DATA_API, g, E._dataApiKeydownHandler).on(c.KEYDOWN_DATA_API, y, E._dataApiKeydownHandler).on(c.CLICK_DATA_API + " " + c.KEYUP_DATA_API, E._clearMenus).on(c.CLICK_DATA_API, g, function(t) {
        t.preventDefault(), t.stopPropagation(), E._jQueryInterface.call(o(this), "toggle")
      }).on(c.CLICK_DATA_API, v, function(t) {
        t.stopPropagation()
      }), o.fn[r] = E._jQueryInterface, o.fn[r].Constructor = E, o.fn[r].noConflict = function() {
        return o.fn[r] = l, E._jQueryInterface
      }
    }(jQuery), function(o) {
      var r = "modal",
        s = o.fn[r],
        a = {
          backdrop: !0,
          keyboard: !0,
          focus: !0,
          show: !0
        },
        l = {
          backdrop: "(boolean|string)",
          keyboard: "boolean",
          focus: "boolean",
          show: "boolean"
        },
        u = "hide.bs.modal",
        c = "hidden.bs.modal",
        d = "show.bs.modal",
        f = "shown.bs.modal",
        h = "focusin.bs.modal",
        p = "resize.bs.modal",
        m = "click.dismiss.bs.modal",
        g = "keydown.dismiss.bs.modal",
        v = "mouseup.dismiss.bs.modal",
        y = "mousedown.dismiss.bs.modal",
        b = "click.bs.modal.data-api",
        _ = "modal-scrollbar-measure",
        w = "modal-backdrop",
        x = "modal-open",
        C = "fade",
        E = "show",
        T = {
          DIALOG: ".modal-dialog",
          DATA_TOGGLE: '[data-toggle="modal"]',
          DATA_DISMISS: '[data-dismiss="modal"]',
          FIXED_CONTENT: ".fixed-top, .fixed-bottom, .is-fixed, .sticky-top",
          NAVBAR_TOGGLER: ".navbar-toggler"
        },
        S = function() {
          function s(e, n) {
            t(this, s), this._config = this._getConfig(n), this._element = e, this._dialog = o(e).find(T.DIALOG)[0], this._backdrop = null, this._isShown = !1, this._isBodyOverflowing = !1, this._ignoreBackdropClick = !1, this._originalBodyPadding = 0, this._scrollbarWidth = 0
          }
          return s.prototype.toggle = function(t) {
            return this._isShown ? this.hide() : this.show(t)
          }, s.prototype.show = function(t) {
            var e = this;
            if (!this._isTransitioning) {
              i.supportsTransitionEnd() && o(this._element).hasClass(C) && (this._isTransitioning = !0);
              var n = o.Event(d, {
                relatedTarget: t
              });
              o(this._element).trigger(n), this._isShown || n.isDefaultPrevented() || (this._isShown = !0, this._checkScrollbar(), this._setScrollbar(), o(document.body).addClass(x), this._setEscapeEvent(), this._setResizeEvent(), o(this._element).on(m, T.DATA_DISMISS, function(t) {
                return e.hide(t)
              }), o(this._dialog).on(y, function() {
                o(e._element).one(v, function(t) {
                  o(t.target).is(e._element) && (e._ignoreBackdropClick = !0)
                })
              }), this._showBackdrop(function() {
                return e._showElement(t)
              }))
            }
          }, s.prototype.hide = function(t) {
            var e = this;
            if (t && t.preventDefault(), !this._isTransitioning && this._isShown) {
              var n = i.supportsTransitionEnd() && o(this._element).hasClass(C);
              n && (this._isTransitioning = !0);
              var r = o.Event(u);
              o(this._element).trigger(r), this._isShown && !r.isDefaultPrevented() && (this._isShown = !1, this._setEscapeEvent(), this._setResizeEvent(), o(document).off(h), o(this._element).removeClass(E), o(this._element).off(m), o(this._dialog).off(y), n ? o(this._element).one(i.TRANSITION_END, function(t) {
                return e._hideModal(t)
              }).emulateTransitionEnd(300) : this._hideModal())
            }
          }, s.prototype.dispose = function() {
            o.removeData(this._element, "bs.modal"), o(window, document, this._element, this._backdrop).off(".bs.modal"), this._config = null, this._element = null, this._dialog = null, this._backdrop = null, this._isShown = null, this._isBodyOverflowing = null, this._ignoreBackdropClick = null, this._scrollbarWidth = null
          }, s.prototype.handleUpdate = function() {
            this._adjustDialog()
          }, s.prototype._getConfig = function(t) {
            return t = o.extend({}, a, t), i.typeCheckConfig(r, t, l), t
          }, s.prototype._showElement = function(t) {
            var e = this,
              n = i.supportsTransitionEnd() && o(this._element).hasClass(C);
            this._element.parentNode && this._element.parentNode.nodeType === Node.ELEMENT_NODE || document.body.appendChild(this._element), this._element.style.display = "block", this._element.removeAttribute("aria-hidden"), this._element.scrollTop = 0, n && i.reflow(this._element), o(this._element).addClass(E), this._config.focus && this._enforceFocus();
            var r = o.Event(f, {
                relatedTarget: t
              }),
              s = function() {
                e._config.focus && e._element.focus(), e._isTransitioning = !1, o(e._element).trigger(r)
              };
            n ? o(this._dialog).one(i.TRANSITION_END, s).emulateTransitionEnd(300) : s()
          }, s.prototype._enforceFocus = function() {
            var t = this;
            o(document).off(h).on(h, function(e) {
              document === e.target || t._element === e.target || o(t._element).has(e.target).length || t._element.focus()
            })
          }, s.prototype._setEscapeEvent = function() {
            var t = this;
            this._isShown && this._config.keyboard ? o(this._element).on(g, function(e) {
              27 === e.which && (e.preventDefault(), t.hide())
            }) : this._isShown || o(this._element).off(g)
          }, s.prototype._setResizeEvent = function() {
            var t = this;
            this._isShown ? o(window).on(p, function(e) {
              return t.handleUpdate(e)
            }) : o(window).off(p)
          }, s.prototype._hideModal = function() {
            var t = this;
            this._element.style.display = "none", this._element.setAttribute("aria-hidden", !0), this._isTransitioning = !1, this._showBackdrop(function() {
              o(document.body).removeClass(x), t._resetAdjustments(), t._resetScrollbar(), o(t._element).trigger(c)
            })
          }, s.prototype._removeBackdrop = function() {
            this._backdrop && (o(this._backdrop).remove(), this._backdrop = null)
          }, s.prototype._showBackdrop = function(t) {
            var e = this,
              n = o(this._element).hasClass(C) ? C : "";
            if (this._isShown && this._config.backdrop) {
              var r = i.supportsTransitionEnd() && n;
              if (this._backdrop = document.createElement("div"), this._backdrop.className = w, n && o(this._backdrop).addClass(n), o(this._backdrop).appendTo(document.body), o(this._element).on(m, function(t) {
                  e._ignoreBackdropClick ? e._ignoreBackdropClick = !1 : t.target === t.currentTarget && ("static" === e._config.backdrop ? e._element.focus() : e.hide())
                }), r && i.reflow(this._backdrop), o(this._backdrop).addClass(E), !t) return;
              if (!r) return void t();
              o(this._backdrop).one(i.TRANSITION_END, t).emulateTransitionEnd(150)
            } else if (!this._isShown && this._backdrop) {
              o(this._backdrop).removeClass(E);
              var s = function() {
                e._removeBackdrop(), t && t()
              };
              i.supportsTransitionEnd() && o(this._element).hasClass(C) ? o(this._backdrop).one(i.TRANSITION_END, s).emulateTransitionEnd(150) : s()
            } else t && t()
          }, s.prototype._adjustDialog = function() {
            var t = this._element.scrollHeight > document.documentElement.clientHeight;
            !this._isBodyOverflowing && t && (this._element.style.paddingLeft = this._scrollbarWidth + "px"), this._isBodyOverflowing && !t && (this._element.style.paddingRight = this._scrollbarWidth + "px")
          }, s.prototype._resetAdjustments = function() {
            this._element.style.paddingLeft = "", this._element.style.paddingRight = ""
          }, s.prototype._checkScrollbar = function() {
            this._isBodyOverflowing = document.body.clientWidth < window.innerWidth, this._scrollbarWidth = this._getScrollbarWidth()
          }, s.prototype._setScrollbar = function() {
            var t = this;
            if (this._isBodyOverflowing) {
              o(T.FIXED_CONTENT).each(function(e, n) {
                var i = o(n)[0].style.paddingRight,
                  r = o(n).css("padding-right");
                o(n).data("padding-right", i).css("padding-right", parseFloat(r) + t._scrollbarWidth + "px")
              }), o(T.NAVBAR_TOGGLER).each(function(e, n) {
                var i = o(n)[0].style.marginRight,
                  r = o(n).css("margin-right");
                o(n).data("margin-right", i).css("margin-right", parseFloat(r) + t._scrollbarWidth + "px")
              });
              var e = document.body.style.paddingRight,
                n = o("body").css("padding-right");
              o("body").data("padding-right", e).css("padding-right", parseFloat(n) + this._scrollbarWidth + "px")
            }
          }, s.prototype._resetScrollbar = function() {
            o(T.FIXED_CONTENT).each(function(t, e) {
              var n = o(e).data("padding-right");
              void 0 !== n && o(e).css("padding-right", n).removeData("padding-right")
            }), o(T.NAVBAR_TOGGLER).each(function(t, e) {
              var n = o(e).data("margin-right");
              void 0 !== n && o(e).css("margin-right", n).removeData("margin-right")
            });
            var t = o("body").data("padding-right");
            void 0 !== t && o("body").css("padding-right", t).removeData("padding-right")
          }, s.prototype._getScrollbarWidth = function() {
            var t = document.createElement("div");
            t.className = _, document.body.appendChild(t);
            var e = t.getBoundingClientRect().width - t.clientWidth;
            return document.body.removeChild(t), e
          }, s._jQueryInterface = function(t, n) {
            return this.each(function() {
              var i = o(this).data("bs.modal"),
                r = o.extend({}, s.Default, o(this).data(), "object" === (void 0 === t ? "undefined" : e(t)) && t);
              if (i || (i = new s(this, r), o(this).data("bs.modal", i)), "string" == typeof t) {
                if (void 0 === i[t]) throw new Error('No method named "' + t + '"');
                i[t](n)
              } else r.show && i.show(n)
            })
          }, n(s, null, [{
            key: "VERSION",
            get: function() {
              return "4.0.0-beta"
            }
          }, {
            key: "Default",
            get: function() {
              return a
            }
          }]), s
        }();
      o(document).on(b, T.DATA_TOGGLE, function(t) {
        var e = this,
          n = void 0,
          r = i.getSelectorFromElement(this);
        r && (n = o(r)[0]);
        var s = o(n).data("bs.modal") ? "toggle" : o.extend({}, o(n).data(), o(this).data());
        "A" !== this.tagName && "AREA" !== this.tagName || t.preventDefault();
        var a = o(n).one(d, function(t) {
          t.isDefaultPrevented() || a.one(c, function() {
            o(e).is(":visible") && e.focus()
          })
        });
        S._jQueryInterface.call(o(n), s, this)
      }), o.fn[r] = S._jQueryInterface, o.fn[r].Constructor = S, o.fn[r].noConflict = function() {
        return o.fn[r] = s, S._jQueryInterface
      }
    }(jQuery), function(o) {
      var r = "scrollspy",
        s = o.fn[r],
        a = {
          offset: 10,
          method: "auto",
          target: ""
        },
        l = {
          offset: "number",
          method: "string",
          target: "(string|element)"
        },
        u = {
          ACTIVATE: "activate.bs.scrollspy",
          SCROLL: "scroll.bs.scrollspy",
          LOAD_DATA_API: "load.bs.scrollspy.data-api"
        },
        c = "dropdown-item",
        d = "active",
        f = {
          DATA_SPY: '[data-spy="scroll"]',
          ACTIVE: ".active",
          NAV_LIST_GROUP: ".nav, .list-group",
          NAV_LINKS: ".nav-link",
          LIST_ITEMS: ".list-group-item",
          DROPDOWN: ".dropdown",
          DROPDOWN_ITEMS: ".dropdown-item",
          DROPDOWN_TOGGLE: ".dropdown-toggle"
        },
        h = "offset",
        p = "position",
        m = function() {
          function s(e, n) {
            var i = this;
            t(this, s), this._element = e, this._scrollElement = "BODY" === e.tagName ? window : e, this._config = this._getConfig(n), this._selector = this._config.target + " " + f.NAV_LINKS + "," + this._config.target + " " + f.LIST_ITEMS + "," + this._config.target + " " + f.DROPDOWN_ITEMS, this._offsets = [], this._targets = [], this._activeTarget = null, this._scrollHeight = 0, o(this._scrollElement).on(u.SCROLL, function(t) {
              return i._process(t)
            }), this.refresh(), this._process()
          }
          return s.prototype.refresh = function() {
            var t = this,
              e = this._scrollElement !== this._scrollElement.window ? p : h,
              n = "auto" === this._config.method ? e : this._config.method,
              r = n === p ? this._getScrollTop() : 0;
            this._offsets = [], this._targets = [], this._scrollHeight = this._getScrollHeight(), o.makeArray(o(this._selector)).map(function(t) {
              var e = void 0,
                s = i.getSelectorFromElement(t);
              if (s && (e = o(s)[0]), e) {
                var a = e.getBoundingClientRect();
                if (a.width || a.height) return [o(e)[n]().top + r, s]
              }
              return null
            }).filter(function(t) {
              return t
            }).sort(function(t, e) {
              return t[0] - e[0]
            }).forEach(function(e) {
              t._offsets.push(e[0]), t._targets.push(e[1])
            })
          }, s.prototype.dispose = function() {
            o.removeData(this._element, "bs.scrollspy"), o(this._scrollElement).off(".bs.scrollspy"), this._element = null, this._scrollElement = null, this._config = null, this._selector = null, this._offsets = null, this._targets = null, this._activeTarget = null, this._scrollHeight = null
          }, s.prototype._getConfig = function(t) {
            if ("string" != typeof(t = o.extend({}, a, t)).target) {
              var e = o(t.target).attr("id");
              e || (e = i.getUID(r), o(t.target).attr("id", e)), t.target = "#" + e
            }
            return i.typeCheckConfig(r, t, l), t
          }, s.prototype._getScrollTop = function() {
            return this._scrollElement === window ? this._scrollElement.pageYOffset : this._scrollElement.scrollTop
          }, s.prototype._getScrollHeight = function() {
            return this._scrollElement.scrollHeight || Math.max(document.body.scrollHeight, document.documentElement.scrollHeight)
          }, s.prototype._getOffsetHeight = function() {
            return this._scrollElement === window ? window.innerHeight : this._scrollElement.getBoundingClientRect().height
          }, s.prototype._process = function() {
            var t = this._getScrollTop() + this._config.offset,
              e = this._getScrollHeight(),
              n = this._config.offset + e - this._getOffsetHeight();
            if (this._scrollHeight !== e && this.refresh(), t >= n) {
              var i = this._targets[this._targets.length - 1];
              this._activeTarget !== i && this._activate(i)
            } else {
              if (this._activeTarget && t < this._offsets[0] && this._offsets[0] > 0) return this._activeTarget = null, void this._clear();
              for (var o = this._offsets.length; o--;) this._activeTarget !== this._targets[o] && t >= this._offsets[o] && (void 0 === this._offsets[o + 1] || t < this._offsets[o + 1]) && this._activate(this._targets[o])
            }
          }, s.prototype._activate = function(t) {
            this._activeTarget = t, this._clear();
            var e = this._selector.split(",");
            e = e.map(function(e) {
              return e + '[data-target="' + t + '"],' + e + '[href="' + t + '"]'
            });
            var n = o(e.join(","));
            n.hasClass(c) ? (n.closest(f.DROPDOWN).find(f.DROPDOWN_TOGGLE).addClass(d), n.addClass(d)) : (n.addClass(d), n.parents(f.NAV_LIST_GROUP).prev(f.NAV_LINKS + ", " + f.LIST_ITEMS).addClass(d)), o(this._scrollElement).trigger(u.ACTIVATE, {
              relatedTarget: t
            })
          }, s.prototype._clear = function() {
            o(this._selector).filter(f.ACTIVE).removeClass(d)
          }, s._jQueryInterface = function(t) {
            return this.each(function() {
              var n = o(this).data("bs.scrollspy"),
                i = "object" === (void 0 === t ? "undefined" : e(t)) && t;
              if (n || (n = new s(this, i), o(this).data("bs.scrollspy", n)), "string" == typeof t) {
                if (void 0 === n[t]) throw new Error('No method named "' + t + '"');
                n[t]()
              }
            })
          }, n(s, null, [{
            key: "VERSION",
            get: function() {
              return "4.0.0-beta"
            }
          }, {
            key: "Default",
            get: function() {
              return a
            }
          }]), s
        }();
      o(window).on(u.LOAD_DATA_API, function() {
        for (var t = o.makeArray(o(f.DATA_SPY)), e = t.length; e--;) {
          var n = o(t[e]);
          m._jQueryInterface.call(n, n.data())
        }
      }), o.fn[r] = m._jQueryInterface, o.fn[r].Constructor = m, o.fn[r].noConflict = function() {
        return o.fn[r] = s, m._jQueryInterface
      }
    }(jQuery), function(e) {
      var o = e.fn.tab,
        r = "hide.bs.tab",
        s = "hidden.bs.tab",
        a = "show.bs.tab",
        l = "shown.bs.tab",
        u = "click.bs.tab.data-api",
        c = "dropdown-menu",
        d = "active",
        f = "disabled",
        h = "fade",
        p = "show",
        m = ".dropdown",
        g = ".nav, .list-group",
        v = ".active",
        y = '[data-toggle="tab"], [data-toggle="pill"], [data-toggle="list"]',
        b = ".dropdown-toggle",
        _ = "> .dropdown-menu .active",
        w = function() {
          function o(e) {
            t(this, o), this._element = e
          }
          return o.prototype.show = function() {
            var t = this;
            if (!(this._element.parentNode && this._element.parentNode.nodeType === Node.ELEMENT_NODE && e(this._element).hasClass(d) || e(this._element).hasClass(f))) {
              var n = void 0,
                o = void 0,
                u = e(this._element).closest(g)[0],
                c = i.getSelectorFromElement(this._element);
              u && (o = (o = e.makeArray(e(u).find(v)))[o.length - 1]);
              var h = e.Event(r, {
                  relatedTarget: this._element
                }),
                p = e.Event(a, {
                  relatedTarget: o
                });
              if (o && e(o).trigger(h), e(this._element).trigger(p), !p.isDefaultPrevented() && !h.isDefaultPrevented()) {
                c && (n = e(c)[0]), this._activate(this._element, u);
                var m = function() {
                  var n = e.Event(s, {
                      relatedTarget: t._element
                    }),
                    i = e.Event(l, {
                      relatedTarget: o
                    });
                  e(o).trigger(n), e(t._element).trigger(i)
                };
                n ? this._activate(n, n.parentNode, m) : m()
              }
            }
          }, o.prototype.dispose = function() {
            e.removeData(this._element, "bs.tab"), this._element = null
          }, o.prototype._activate = function(t, n, o) {
            var r = this,
              s = e(n).find(v)[0],
              a = o && i.supportsTransitionEnd() && s && e(s).hasClass(h),
              l = function() {
                return r._transitionComplete(t, s, a, o)
              };
            s && a ? e(s).one(i.TRANSITION_END, l).emulateTransitionEnd(150) : l(), s && e(s).removeClass(p)
          }, o.prototype._transitionComplete = function(t, n, o, r) {
            if (n) {
              e(n).removeClass(d);
              var s = e(n.parentNode).find(_)[0];
              s && e(s).removeClass(d), n.setAttribute("aria-expanded", !1)
            }
            if (e(t).addClass(d), t.setAttribute("aria-expanded", !0), o ? (i.reflow(t), e(t).addClass(p)) : e(t).removeClass(h), t.parentNode && e(t.parentNode).hasClass(c)) {
              var a = e(t).closest(m)[0];
              a && e(a).find(b).addClass(d), t.setAttribute("aria-expanded", !0)
            }
            r && r()
          }, o._jQueryInterface = function(t) {
            return this.each(function() {
              var n = e(this),
                i = n.data("bs.tab");
              if (i || (i = new o(this), n.data("bs.tab", i)), "string" == typeof t) {
                if (void 0 === i[t]) throw new Error('No method named "' + t + '"');
                i[t]()
              }
            })
          }, n(o, null, [{
            key: "VERSION",
            get: function() {
              return "4.0.0-beta"
            }
          }]), o
        }();
      e(document).on(u, y, function(t) {
        t.preventDefault(), w._jQueryInterface.call(e(this), "show")
      }), e.fn.tab = w._jQueryInterface, e.fn.tab.Constructor = w, e.fn.tab.noConflict = function() {
        return e.fn.tab = o, w._jQueryInterface
      }
    }(jQuery), function(o) {
      if ("undefined" == typeof Popper) throw new Error("Bootstrap tooltips require Popper.js (https://popper.js.org)");
      var r = "tooltip",
        s = ".bs.tooltip",
        a = o.fn[r],
        l = new RegExp("(^|\\s)bs-tooltip\\S+", "g"),
        u = {
          animation: "boolean",
          template: "string",
          title: "(string|element|function)",
          trigger: "string",
          delay: "(number|object)",
          html: "boolean",
          selector: "(string|boolean)",
          placement: "(string|function)",
          offset: "(number|string)",
          container: "(string|element|boolean)",
          fallbackPlacement: "(string|array)"
        },
        c = {
          AUTO: "auto",
          TOP: "top",
          RIGHT: "right",
          BOTTOM: "bottom",
          LEFT: "left"
        },
        d = {
          animation: !0,
          template: '<div class="tooltip" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>',
          trigger: "hover focus",
          title: "",
          delay: 0,
          html: !1,
          selector: !1,
          placement: "top",
          offset: 0,
          container: !1,
          fallbackPlacement: "flip"
        },
        f = "show",
        h = "out",
        p = {
          HIDE: "hide" + s,
          HIDDEN: "hidden" + s,
          SHOW: "show" + s,
          SHOWN: "shown" + s,
          INSERTED: "inserted" + s,
          CLICK: "click" + s,
          FOCUSIN: "focusin" + s,
          FOCUSOUT: "focusout" + s,
          MOUSEENTER: "mouseenter" + s,
          MOUSELEAVE: "mouseleave" + s
        },
        m = "fade",
        g = "show",
        v = ".tooltip-inner",
        y = ".arrow",
        b = "hover",
        _ = "focus",
        w = "click",
        x = "manual",
        C = function() {
          function a(e, n) {
            t(this, a), this._isEnabled = !0, this._timeout = 0, this._hoverState = "", this._activeTrigger = {}, this._popper = null, this.element = e, this.config = this._getConfig(n), this.tip = null, this._setListeners()
          }
          return a.prototype.enable = function() {
            this._isEnabled = !0
          }, a.prototype.disable = function() {
            this._isEnabled = !1
          }, a.prototype.toggleEnabled = function() {
            this._isEnabled = !this._isEnabled
          }, a.prototype.toggle = function(t) {
            if (t) {
              var e = this.constructor.DATA_KEY,
                n = o(t.currentTarget).data(e);
              n || (n = new this.constructor(t.currentTarget, this._getDelegateConfig()), o(t.currentTarget).data(e, n)), n._activeTrigger.click = !n._activeTrigger.click, n._isWithActiveTrigger() ? n._enter(null, n) : n._leave(null, n)
            } else {
              if (o(this.getTipElement()).hasClass(g)) return void this._leave(null, this);
              this._enter(null, this)
            }
          }, a.prototype.dispose = function() {
            clearTimeout(this._timeout), o.removeData(this.element, this.constructor.DATA_KEY), o(this.element).off(this.constructor.EVENT_KEY), o(this.element).closest(".modal").off("hide.bs.modal"), this.tip && o(this.tip).remove(), this._isEnabled = null, this._timeout = null, this._hoverState = null, this._activeTrigger = null, null !== this._popper && this._popper.destroy(), this._popper = null, this.element = null, this.config = null, this.tip = null
          }, a.prototype.show = function() {
            var t = this;
            if ("none" === o(this.element).css("display")) throw new Error("Please use show on visible elements");
            var e = o.Event(this.constructor.Event.SHOW);
            if (this.isWithContent() && this._isEnabled) {
              o(this.element).trigger(e);
              var n = o.contains(this.element.ownerDocument.documentElement, this.element);
              if (e.isDefaultPrevented() || !n) return;
              var r = this.getTipElement(),
                s = i.getUID(this.constructor.NAME);
              r.setAttribute("id", s), this.element.setAttribute("aria-describedby", s), this.setContent(), this.config.animation && o(r).addClass(m);
              var l = "function" == typeof this.config.placement ? this.config.placement.call(this, r, this.element) : this.config.placement,
                u = this._getAttachment(l);
              this.addAttachmentClass(u);
              var c = !1 === this.config.container ? document.body : o(this.config.container);
              o(r).data(this.constructor.DATA_KEY, this), o.contains(this.element.ownerDocument.documentElement, this.tip) || o(r).appendTo(c), o(this.element).trigger(this.constructor.Event.INSERTED), this._popper = new Popper(this.element, r, {
                placement: u,
                modifiers: {
                  offset: {
                    offset: this.config.offset
                  },
                  flip: {
                    behavior: this.config.fallbackPlacement
                  },
                  arrow: {
                    element: y
                  }
                },
                onCreate: function(e) {
                  e.originalPlacement !== e.placement && t._handlePopperPlacementChange(e)
                },
                onUpdate: function(e) {
                  t._handlePopperPlacementChange(e)
                }
              }), o(r).addClass(g), "ontouchstart" in document.documentElement && o("body").children().on("mouseover", null, o.noop);
              var d = function() {
                t.config.animation && t._fixTransition();
                var e = t._hoverState;
                t._hoverState = null, o(t.element).trigger(t.constructor.Event.SHOWN), e === h && t._leave(null, t)
              };
              i.supportsTransitionEnd() && o(this.tip).hasClass(m) ? o(this.tip).one(i.TRANSITION_END, d).emulateTransitionEnd(a._TRANSITION_DURATION) : d()
            }
          }, a.prototype.hide = function(t) {
            var e = this,
              n = this.getTipElement(),
              r = o.Event(this.constructor.Event.HIDE),
              s = function() {
                e._hoverState !== f && n.parentNode && n.parentNode.removeChild(n), e._cleanTipClass(), e.element.removeAttribute("aria-describedby"), o(e.element).trigger(e.constructor.Event.HIDDEN), null !== e._popper && e._popper.destroy(), t && t()
              };
            o(this.element).trigger(r), r.isDefaultPrevented() || (o(n).removeClass(g), "ontouchstart" in document.documentElement && o("body").children().off("mouseover", null, o.noop), this._activeTrigger[w] = !1, this._activeTrigger[_] = !1, this._activeTrigger[b] = !1, i.supportsTransitionEnd() && o(this.tip).hasClass(m) ? o(n).one(i.TRANSITION_END, s).emulateTransitionEnd(150) : s(), this._hoverState = "")
          }, a.prototype.update = function() {
            null !== this._popper && this._popper.scheduleUpdate()
          }, a.prototype.isWithContent = function() {
            return Boolean(this.getTitle())
          }, a.prototype.addAttachmentClass = function(t) {
            o(this.getTipElement()).addClass("bs-tooltip-" + t)
          }, a.prototype.getTipElement = function() {
            return this.tip = this.tip || o(this.config.template)[0]
          }, a.prototype.setContent = function() {
            var t = o(this.getTipElement());
            this.setElementContent(t.find(v), this.getTitle()), t.removeClass(m + " " + g)
          }, a.prototype.setElementContent = function(t, n) {
            var i = this.config.html;
            "object" === (void 0 === n ? "undefined" : e(n)) && (n.nodeType || n.jquery) ? i ? o(n).parent().is(t) || t.empty().append(n) : t.text(o(n).text()): t[i ? "html" : "text"](n)
          }, a.prototype.getTitle = function() {
            var t = this.element.getAttribute("data-original-title");
            return t || (t = "function" == typeof this.config.title ? this.config.title.call(this.element) : this.config.title), t
          }, a.prototype._getAttachment = function(t) {
            return c[t.toUpperCase()]
          }, a.prototype._setListeners = function() {
            var t = this;
            this.config.trigger.split(" ").forEach(function(e) {
              if ("click" === e) o(t.element).on(t.constructor.Event.CLICK, t.config.selector, function(e) {
                return t.toggle(e)
              });
              else if (e !== x) {
                var n = e === b ? t.constructor.Event.MOUSEENTER : t.constructor.Event.FOCUSIN,
                  i = e === b ? t.constructor.Event.MOUSELEAVE : t.constructor.Event.FOCUSOUT;
                o(t.element).on(n, t.config.selector, function(e) {
                  return t._enter(e)
                }).on(i, t.config.selector, function(e) {
                  return t._leave(e)
                })
              }
              o(t.element).closest(".modal").on("hide.bs.modal", function() {
                return t.hide()
              })
            }), this.config.selector ? this.config = o.extend({}, this.config, {
              trigger: "manual",
              selector: ""
            }) : this._fixTitle()
          }, a.prototype._fixTitle = function() {
            var t = e(this.element.getAttribute("data-original-title"));
            (this.element.getAttribute("title") || "string" !== t) && (this.element.setAttribute("data-original-title", this.element.getAttribute("title") || ""), this.element.setAttribute("title", ""))
          }, a.prototype._enter = function(t, e) {
            var n = this.constructor.DATA_KEY;
            (e = e || o(t.currentTarget).data(n)) || (e = new this.constructor(t.currentTarget, this._getDelegateConfig()), o(t.currentTarget).data(n, e)), t && (e._activeTrigger["focusin" === t.type ? _ : b] = !0), o(e.getTipElement()).hasClass(g) || e._hoverState === f ? e._hoverState = f : (clearTimeout(e._timeout), e._hoverState = f, e.config.delay && e.config.delay.show ? e._timeout = setTimeout(function() {
              e._hoverState === f && e.show()
            }, e.config.delay.show) : e.show())
          }, a.prototype._leave = function(t, e) {
            var n = this.constructor.DATA_KEY;
            (e = e || o(t.currentTarget).data(n)) || (e = new this.constructor(t.currentTarget, this._getDelegateConfig()), o(t.currentTarget).data(n, e)), t && (e._activeTrigger["focusout" === t.type ? _ : b] = !1), e._isWithActiveTrigger() || (clearTimeout(e._timeout), e._hoverState = h, e.config.delay && e.config.delay.hide ? e._timeout = setTimeout(function() {
              e._hoverState === h && e.hide()
            }, e.config.delay.hide) : e.hide())
          }, a.prototype._isWithActiveTrigger = function() {
            for (var t in this._activeTrigger)
              if (this._activeTrigger[t]) return !0;
            return !1
          }, a.prototype._getConfig = function(t) {
            return (t = o.extend({}, this.constructor.Default, o(this.element).data(), t)).delay && "number" == typeof t.delay && (t.delay = {
              show: t.delay,
              hide: t.delay
            }), t.title && "number" == typeof t.title && (t.title = t.title.toString()), t.content && "number" == typeof t.content && (t.content = t.content.toString()), i.typeCheckConfig(r, t, this.constructor.DefaultType), t
          }, a.prototype._getDelegateConfig = function() {
            var t = {};
            if (this.config)
              for (var e in this.config) this.constructor.Default[e] !== this.config[e] && (t[e] = this.config[e]);
            return t
          }, a.prototype._cleanTipClass = function() {
            var t = o(this.getTipElement()),
              e = t.attr("class").match(l);
            null !== e && e.length > 0 && t.removeClass(e.join(""))
          }, a.prototype._handlePopperPlacementChange = function(t) {
            this._cleanTipClass(), this.addAttachmentClass(this._getAttachment(t.placement))
          }, a.prototype._fixTransition = function() {
            var t = this.getTipElement(),
              e = this.config.animation;
            null === t.getAttribute("x-placement") && (o(t).removeClass(m), this.config.animation = !1, this.hide(), this.show(), this.config.animation = e)
          }, a._jQueryInterface = function(t) {
            return this.each(function() {
              var n = o(this).data("bs.tooltip"),
                i = "object" === (void 0 === t ? "undefined" : e(t)) && t;
              if ((n || !/dispose|hide/.test(t)) && (n || (n = new a(this, i), o(this).data("bs.tooltip", n)), "string" == typeof t)) {
                if (void 0 === n[t]) throw new Error('No method named "' + t + '"');
                n[t]()
              }
            })
          }, n(a, null, [{
            key: "VERSION",
            get: function() {
              return "4.0.0-beta"
            }
          }, {
            key: "Default",
            get: function() {
              return d
            }
          }, {
            key: "NAME",
            get: function() {
              return r
            }
          }, {
            key: "DATA_KEY",
            get: function() {
              return "bs.tooltip"
            }
          }, {
            key: "Event",
            get: function() {
              return p
            }
          }, {
            key: "EVENT_KEY",
            get: function() {
              return s
            }
          }, {
            key: "DefaultType",
            get: function() {
              return u
            }
          }]), a
        }();
      return o.fn[r] = C._jQueryInterface, o.fn[r].Constructor = C, o.fn[r].noConflict = function() {
        return o.fn[r] = a, C._jQueryInterface
      }, C
    }(jQuery));
  ! function(i) {
    var r = "popover",
      s = ".bs.popover",
      a = i.fn[r],
      l = new RegExp("(^|\\s)bs-popover\\S+", "g"),
      u = i.extend({}, o.Default, {
        placement: "right",
        trigger: "click",
        content: "",
        template: '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body"></div></div>'
      }),
      c = i.extend({}, o.DefaultType, {
        content: "(string|element|function)"
      }),
      d = "fade",
      f = "show",
      h = ".popover-header",
      p = ".popover-body",
      m = {
        HIDE: "hide" + s,
        HIDDEN: "hidden" + s,
        SHOW: "show" + s,
        SHOWN: "shown" + s,
        INSERTED: "inserted" + s,
        CLICK: "click" + s,
        FOCUSIN: "focusin" + s,
        FOCUSOUT: "focusout" + s,
        MOUSEENTER: "mouseenter" + s,
        MOUSELEAVE: "mouseleave" + s
      },
      g = function(o) {
        function a() {
          return t(this, a),
            function(t, e) {
              if (!t) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
              return !e || "object" != typeof e && "function" != typeof e ? t : e
            }(this, o.apply(this, arguments))
        }
        return function(t, e) {
          if ("function" != typeof e && null !== e) throw new TypeError("Super expression must either be null or a function, not " + typeof e);
          t.prototype = Object.create(e && e.prototype, {
            constructor: {
              value: t,
              enumerable: !1,
              writable: !0,
              configurable: !0
            }
          }), e && (Object.setPrototypeOf ? Object.setPrototypeOf(t, e) : t.__proto__ = e)
        }(a, o), a.prototype.isWithContent = function() {
          return this.getTitle() || this._getContent()
        }, a.prototype.addAttachmentClass = function(t) {
          i(this.getTipElement()).addClass("bs-popover-" + t)
        }, a.prototype.getTipElement = function() {
          return this.tip = this.tip || i(this.config.template)[0]
        }, a.prototype.setContent = function() {
          var t = i(this.getTipElement());
          this.setElementContent(t.find(h), this.getTitle()), this.setElementContent(t.find(p), this._getContent()), t.removeClass(d + " " + f)
        }, a.prototype._getContent = function() {
          return this.element.getAttribute("data-content") || ("function" == typeof this.config.content ? this.config.content.call(this.element) : this.config.content)
        }, a.prototype._cleanTipClass = function() {
          var t = i(this.getTipElement()),
            e = t.attr("class").match(l);
          null !== e && e.length > 0 && t.removeClass(e.join(""))
        }, a._jQueryInterface = function(t) {
          return this.each(function() {
            var n = i(this).data("bs.popover"),
              o = "object" === (void 0 === t ? "undefined" : e(t)) ? t : null;
            if ((n || !/destroy|hide/.test(t)) && (n || (n = new a(this, o), i(this).data("bs.popover", n)), "string" == typeof t)) {
              if (void 0 === n[t]) throw new Error('No method named "' + t + '"');
              n[t]()
            }
          })
        }, n(a, null, [{
          key: "VERSION",
          get: function() {
            return "4.0.0-beta"
          }
        }, {
          key: "Default",
          get: function() {
            return u
          }
        }, {
          key: "NAME",
          get: function() {
            return r
          }
        }, {
          key: "DATA_KEY",
          get: function() {
            return "bs.popover"
          }
        }, {
          key: "Event",
          get: function() {
            return m
          }
        }, {
          key: "EVENT_KEY",
          get: function() {
            return s
          }
        }, {
          key: "DefaultType",
          get: function() {
            return c
          }
        }]), a
      }(o);
    i.fn[r] = g._jQueryInterface, i.fn[r].Constructor = g, i.fn[r].noConflict = function() {
      return i.fn[r] = a, g._jQueryInterface
    }
  }(jQuery)
}(),
function(t) {
  t.fn.downCount = function(e, n) {
    var i = t.extend({
      date: null,
      offset: null,
      daySingular: "day",
      dayPlural: "days",
      hourSingular: "hour",
      hourPlural: "hours",
      minuteSingular: "minute",
      minutePlural: "minutes",
      secondSingular: "second",
      secondPlural: "seconds"
    }, e);
    i.date || t.error("Date is not defined.");
    var o = this,
      r = function() {
        var t = new Date,
          e = t.getTime() + 6e4 * t.getTimezoneOffset();
        return new Date(e + 36e5 * i.offset)
      },
      s = setInterval(function() {
        var t = i.date - r();
        if (t < 0) return clearInterval(s), void(n && "function" == typeof n && n());
        var e = 36e5,
          a = Math.floor(t / 864e5),
          l = Math.floor(t % 864e5 / e),
          u = Math.floor(t % e / 6e4),
          c = Math.floor(t % 6e4 / 1e3);
        a = String(a).length >= 2 ? a : "0" + a, l = String(l).length >= 2 ? l : "0" + l, u = String(u).length >= 2 ? u : "0" + u, c = String(c).length >= 2 ? c : "0" + c;
        var d = "01" === a ? i.daySingular : i.dayPlural,
          f = "01" === l ? i.hourSingular : i.hourPlural,
          h = "01" === u ? i.minuteSingular : i.minutePlural,
          p = "01" === c ? i.secondSingular : i.secondPlural;
        o.find(".days").text(a), o.find(".hours").text(l), o.find(".minutes").text(u), o.find(".seconds").text(c), o.find(".days_ref").text(d), o.find(".hours_ref").text(f), o.find(".minutes_ref").text(h), o.find(".seconds_ref").text(p)
      }, 1e3);
    return s
  }
}(jQuery),
function(t, e) {
  function n(t) {
    return "object" == typeof t
  }

  function i(t) {
    return "string" == typeof t
  }

  function o(t) {
    return "number" == typeof t
  }

  function r(t) {
    return t === e
  }

  function s(t, e) {
    return r(t) ? "gmap3_" + (e ? L + 1 : ++L) : t
  }

  function a(e, n, i, o, r) {
    function s(n, o) {
      n && t.each(n, function(t, n) {
        var s = e,
          a = n;
        P(n) && (s = n[0], a = n[1]), o(i, t, function(t) {
          a.apply(s, [r || i, t, l])
        })
      })
    }
    var a = n.td || {},
      l = {
        id: o,
        data: a.data,
        tag: a.tag
      };
    s(a.events, D.event.addListener), s(a.onces, D.event.addListenerOnce)
  }

  function l(t) {
    var e, n = [];
    for (e in t) t.hasOwnProperty(e) && n.push(e);
    return n
  }

  function u(t, e) {
    var n, i = arguments;
    for (n = 2; n < i.length; n++)
      if (e in i[n] && i[n].hasOwnProperty(e)) return void(t[e] = i[n][e])
  }

  function c(e, n) {
    var i, o, r = ["data", "tag", "id", "events", "onces"],
      s = {};
    if (e.td)
      for (i in e.td) e.td.hasOwnProperty(i) && "options" !== i && "values" !== i && (s[i] = e.td[i]);
    for (o = 0; o < r.length; o++) u(s, r[o], n, e.td);
    return s.options = t.extend({}, e.opts || {}, n.options || {}), s
  }

  function d() {
    if (A.verbose) {
      var t, e = [];
      if (window.console && O(console.error)) {
        for (t = 0; t < arguments.length; t++) e.push(arguments[t]);
        console.error.apply(console, e)
      } else {
        for (e = "", t = 0; t < arguments.length; t++) e += arguments[t].toString() + " ";
        alert(e)
      }
    }
  }

  function f(t) {
    return (o(t) || i(t)) && "" !== t && !isNaN(t)
  }

  function h(t) {
    var e, i = [];
    if (!r(t))
      if (n(t))
        if (o(t.length)) i = t;
        else
          for (e in t) i.push(t[e]);
    else i.push(t);
    return i
  }

  function p(e) {
    return e ? O(e) ? e : (e = h(e), function(i) {
      var o;
      if (r(i)) return !1;
      if (n(i)) {
        for (o = 0; o < i.length; o++)
          if (t.inArray(i[o], e) >= 0) return !0;
        return !1
      }
      return t.inArray(i, e) >= 0
    }) : void 0
  }

  function m(t, e, n) {
    var o = e ? t : null;
    return !t || i(t) ? o : t.latLng ? m(t.latLng) : t instanceof D.LatLng ? t : f(t.lat) ? new D.LatLng(t.lat, t.lng) : !n && P(t) && f(t[0]) && f(t[1]) ? new D.LatLng(t[0], t[1]) : o
  }

  function g(t) {
    var e, n;
    return !t || t instanceof D.LatLngBounds ? t || null : (P(t) ? 2 === t.length ? (e = m(t[0]), n = m(t[1])) : 4 === t.length && (e = m([t[0], t[1]]), n = m([t[2], t[3]])) : "ne" in t && "sw" in t ? (e = m(t.ne), n = m(t.sw)) : "n" in t && "e" in t && "s" in t && "w" in t && (e = m([t.n, t.e]), n = m([t.s, t.w])), e && n ? new D.LatLngBounds(n, e) : null)
  }

  function v(t, e, n, o, r) {
    var s = !!n && m(o.td, !1, !0),
      a = s ? {
        latLng: s
      } : !!o.td.address && (i(o.td.address) ? {
        address: o.td.address
      } : o.td.address),
      l = !!a && j.get(a),
      u = this;
    a ? (r = r || 0, l ? (o.latLng = l.results[0].geometry.location, o.results = l.results, o.status = l.status, e.apply(t, [o])) : (a.location && (a.location = m(a.location)), a.bounds && (a.bounds = g(a.bounds)), (N.geocoder || (N.geocoder = new D.Geocoder), N.geocoder).geocode(a, function(i, s) {
      s === D.GeocoderStatus.OK ? (j.store(a, {
        results: i,
        status: s
      }), o.latLng = i[0].geometry.location, o.results = i, o.status = s, e.apply(t, [o])) : s === D.GeocoderStatus.OVER_QUERY_LIMIT && r < A.queryLimit.attempt ? setTimeout(function() {
        v.apply(u, [t, e, n, o, r + 1])
      }, A.queryLimit.delay + Math.floor(Math.random() * A.queryLimit.random)) : (d("geocode failed", s, a), o.latLng = o.results = !1, o.status = s, e.apply(t, [o]))
    }))) : (o.latLng = m(o.td, !1, !0), e.apply(t, [o]))
  }

  function y(e, n, i, o) {
    var r = this,
      s = -1;
    ! function a() {
      do {
        s++
      } while (s < e.length && !("address" in e[s]));
      return s >= e.length ? void i.apply(n, [o]) : void v(r, function(n) {
        delete n.td, t.extend(e[s], n), a.apply(r, [])
      }, !0, {
        td: e[s]
      })
    }()
  }

  function b(t, e, n) {
    var i = !1;
    navigator && navigator.geolocation ? navigator.geolocation.getCurrentPosition(function(o) {
      i || (i = !0, n.latLng = new D.LatLng(o.coords.latitude, o.coords.longitude), e.apply(t, [n]))
    }, function() {
      i || (i = !0, n.latLng = !1, e.apply(t, [n]))
    }, n.opts.getCurrentPosition) : (n.latLng = !1, e.apply(t, [n]))
  }

  function _(t) {
    var e, i = !1;
    if (n(t) && t.hasOwnProperty("get")) {
      for (e in t)
        if ("get" !== e) return !1;
      i = !t.get.hasOwnProperty("callback")
    }
    return i
  }

  function w() {
    var t = [],
      e = this;
    e.empty = function() {
      return !t.length
    }, e.add = function(e) {
      t.push(e)
    }, e.get = function() {
      return !!t.length && t[0]
    }, e.ack = function() {
      t.shift()
    }
  }

  function x() {
    function e(t) {
      return {
        id: t.id,
        name: t.name,
        object: t.obj,
        tag: t.tag,
        data: t.data
      }
    }

    function n(t) {
      O(t.setMap) && t.setMap(null), O(t.remove) && t.remove(), O(t.free) && t.free(), t = null
    }
    var i = {},
      o = {},
      a = this;
    a.add = function(t, e, n, r) {
      var l = t.td || {},
        u = s(l.id);
      return i[e] || (i[e] = []), u in o && a.clearById(u), o[u] = {
        obj: n,
        sub: r,
        name: e,
        id: u,
        tag: l.tag,
        data: l.data
      }, i[e].push(u), u
    }, a.getById = function(t, n, i) {
      var r = !1;
      return t in o && (r = n ? o[t].sub : i ? e(o[t]) : o[t].obj), r
    }, a.get = function(t, n, r, s) {
      var a, l, u = p(r);
      if (!i[t] || !i[t].length) return null;
      for (a = i[t].length; a;)
        if (a--, (l = i[t][n ? a : i[t].length - a - 1]) && o[l]) {
          if (u && !u(o[l].tag)) continue;
          return s ? e(o[l]) : o[l].obj
        } return null
    }, a.all = function(t, n, s) {
      var a = [],
        l = p(n),
        u = function(t) {
          var n, r;
          for (n = 0; n < i[t].length; n++)
            if ((r = i[t][n]) && o[r]) {
              if (l && !l(o[r].tag)) continue;
              a.push(s ? e(o[r]) : o[r].obj)
            }
        };
      if (t in i) u(t);
      else if (r(t))
        for (t in i) u(t);
      return a
    }, a.rm = function(t, e, n) {
      var r, s;
      if (!i[t]) return !1;
      if (e)
        if (n)
          for (r = i[t].length - 1; r >= 0 && (s = i[t][r], !e(o[s].tag)); r--);
        else
          for (r = 0; r < i[t].length && (s = i[t][r], !e(o[s].tag)); r++);
      else r = n ? i[t].length - 1 : 0;
      return r in i[t] && a.clearById(i[t][r], r)
    }, a.clearById = function(t, e) {
      if (t in o) {
        var s, a = o[t].name;
        for (s = 0; r(e) && s < i[a].length; s++) t === i[a][s] && (e = s);
        return n(o[t].obj), o[t].sub && n(o[t].sub), delete o[t], i[a].splice(e, 1), !0
      }
      return !1
    }, a.objGetById = function(t) {
      var e, n;
      if (i.clusterer)
        for (n in i.clusterer)
          if (!1 !== (e = o[i.clusterer[n]].obj.getById(t))) return e;
      return !1
    }, a.objClearById = function(t) {
      var e;
      if (i.clusterer)
        for (e in i.clusterer)
          if (o[i.clusterer[e]].obj.clearById(t)) return !0;
      return null
    }, a.clear = function(t, e, n, o) {
      var r, s, l, u = p(o);
      if (t && t.length) t = h(t);
      else
        for (r in t = [], i) t.push(r);
      for (s = 0; s < t.length; s++)
        if (l = t[s], e) a.rm(l, u, !0);
        else if (n) a.rm(l, u, !1);
      else
        for (; a.rm(l, u, !1););
    }, a.objClear = function(e, n, r, s) {
      var a;
      if (i.clusterer && (t.inArray("marker", e) >= 0 || !e.length))
        for (a in i.clusterer) o[i.clusterer[a]].obj.clear(n, r, s)
    }
  }

  function C(e, n, o) {
    function r() {
      var t;
      for (t in o)
        if (o.hasOwnProperty(t) && !a.hasOwnProperty(t)) return t
    }
    var s, a = {},
      l = this,
      u = {
        latLng: {
          map: !1,
          marker: !1,
          infowindow: !1,
          circle: !1,
          overlay: !1,
          getlatlng: !1,
          getmaxzoom: !1,
          getelevation: !1,
          streetviewpanorama: !1,
          getaddress: !0
        },
        geoloc: {
          getgeoloc: !0
        }
      };
    i(o) && (o = function(t) {
      var e = {};
      return e[t] = {}, e
    }(o)), l.run = function() {
      for (var i, l; i = r();) {
        if (O(e[i])) return s = i, l = t.extend(!0, {}, A[i] || {}, o[i].options || {}), void(i in u.latLng ? o[i].values ? y(o[i].values, e, e[i], {
          td: o[i],
          opts: l,
          session: a
        }) : v(e, e[i], u.latLng[i], {
          td: o[i],
          opts: l,
          session: a
        }) : i in u.geoloc ? b(e, e[i], {
          td: o[i],
          opts: l,
          session: a
        }) : e[i].apply(e, [{
          td: o[i],
          opts: l,
          session: a
        }]));
        a[i] = null
      }
      n.apply(e, [o, a])
    }, l.ack = function(t) {
      a[s] = t, l.run.apply(l, [])
    }
  }

  function E() {
    return N.es || (N.es = new D.ElevationService), N.es
  }

  function T(e, i, o) {
    function r(t) {
      I[t] || (delete L[t].options.map, I[t] = new A.classes.Marker(L[t].options), a(e, {
        td: L[t]
      }, I[t], L[t].id))
    }

    function l(t) {
      n(T[t]) ? (O(T[t].obj.setMap) && T[t].obj.setMap(null), O(T[t].obj.remove) && T[t].obj.remove(), O(T[t].shadow.remove) && T[t].obj.remove(), O(T[t].shadow.setMap) && T[t].shadow.setMap(null), delete T[t].obj, delete T[t].shadow) : I[t] && I[t].setMap(null), delete T[t]
    }

    function u() {
      var t = function() {
        var t, e, n, i, o, r, s, a, l = Math.cos,
          u = Math.sin,
          c = arguments;
        return c[0] instanceof D.LatLng ? (t = c[0].lat(), n = c[0].lng(), c[1] instanceof D.LatLng ? (e = c[1].lat(), i = c[1].lng()) : (e = c[1], i = c[2])) : (t = c[0], n = c[1], c[2] instanceof D.LatLng ? (e = c[2].lat(), i = c[2].lng()) : (e = c[2], i = c[3])), o = Math.PI * t / 180, r = Math.PI * n / 180, s = Math.PI * e / 180, a = Math.PI * i / 180, 6371e3 * Math.acos(Math.min(l(o) * l(s) * l(r) * l(a) + l(o) * u(r) * l(s) * u(a) + u(o) * u(s), 1))
      }(i.getCenter(), i.getBounds().getNorthEast());
      return new D.Circle({
        center: i.getCenter(),
        radius: 1.25 * t
      }).getBounds()
    }

    function c() {
      clearTimeout(h), h = setTimeout(f, 25)
    }

    function d(t) {
      var e = m.fromLatLngToDivPixel(t),
        n = m.fromDivPixelToLatLng(new D.Point(e.x + o.radius, e.y - o.radius)),
        i = m.fromDivPixelToLatLng(new D.Point(e.x - o.radius, e.y + o.radius));
      return new D.LatLngBounds(i, n)
    }

    function f() {
      if (!y && !_ && w) {
        var e, n, r, s, a, c, f, h, p, m, C, E = !1,
          S = [],
          k = {},
          I = i.getZoom(),
          A = "maxZoom" in o && I > o.maxZoom,
          O = function() {
            var t, e = {};
            for (t in T) e[t] = !0;
            return e
          }();
        for (b = !1, I > 3 && (E = (a = u()).getSouthWest().lng() < a.getNorthEast().lng()), e = 0; e < L.length; e++) !L[e] || E && !a.contains(L[e].options.position) || g && !g(P[e]) || S.push(e);
        for (;;) {
          for (e = 0; k[e] && e < S.length;) e++;
          if (e === S.length) break;
          if (s = [], x && !A) {
            C = 10;
            do {
              for (h = s, s = [], C--, f = h.length ? a.getCenter() : L[S[e]].options.position, a = d(f), n = e; n < S.length; n++) k[n] || a.contains(L[S[n]].options.position) && s.push(n)
            } while (h.length < s.length && s.length > 1 && C)
          } else
            for (n = e; n < S.length; n++)
              if (!k[n]) {
                s.push(n);
                break
              } for (c = {
              indexes: [],
              ref: []
            }, p = m = 0, r = 0; r < s.length; r++) k[s[r]] = !0, c.indexes.push(S[s[r]]), c.ref.push(S[s[r]]), p += L[S[s[r]]].options.position.lat(), m += L[S[s[r]]].options.position.lng();
          p /= s.length, m /= s.length, c.latLng = new D.LatLng(p, m), c.ref = c.ref.join("-"), c.ref in O ? delete O[c.ref] : (1 === s.length && (T[c.ref] = !0), v(c))
        }
        t.each(O, function(t) {
          l(t)
        }), _ = !1
      }
    }
    var h, m, g, v, y = !1,
      b = !1,
      _ = !1,
      w = !1,
      x = !0,
      C = this,
      E = [],
      T = {},
      S = {},
      k = {},
      I = [],
      L = [],
      P = [],
      N = function(t) {
        function e() {
          var t = this;
          return t.onAdd = function() {}, t.onRemove = function() {}, t.draw = function() {}, A.classes.OverlayView.apply(t, [])
        }
        e.prototype = A.classes.OverlayView.prototype;
        var n = new e;
        return n.setMap(t), n
      }(i, o.radius);
    (function t() {
      return (m = N.getProjection()) ? (w = !0, E.push(D.event.addListener(i, "zoom_changed", c)), E.push(D.event.addListener(i, "bounds_changed", c)), void f()) : void setTimeout(function() {
        t.apply(C, [])
      }, 25)
    })(), C.getById = function(t) {
      return t in S && (r(S[t]), I[S[t]])
    }, C.rm = function(t) {
      var e = S[t];
      I[e] && I[e].setMap(null), delete I[e], I[e] = !1, delete L[e], L[e] = !1, delete P[e], P[e] = !1, delete S[t], delete k[e], b = !0
    }, C.clearById = function(t) {
      return t in S ? (C.rm(t), !0) : void 0
    }, C.clear = function(t, e, n) {
      var i, o, r, s, a, l = [],
        u = p(n);
      for (t ? (i = L.length - 1, o = -1, r = -1) : (i = 0, o = L.length, r = 1), s = i; s !== o && (!L[s] || u && !u(L[s].tag) || (l.push(k[s]), !e && !t)); s += r);
      for (a = 0; a < l.length; a++) C.rm(l[a])
    }, C.add = function(t, e) {
      t.id = s(t.id), C.clearById(t.id), S[t.id] = I.length, k[I.length] = t.id, I.push(null), L.push(t), P.push(e), b = !0
    }, C.addMarker = function(t, n) {
      (n = n || {}).id = s(n.id), C.clearById(n.id), n.options || (n.options = {}), n.options.position = t.getPosition(), a(e, {
        td: n
      }, t, n.id), S[n.id] = I.length, k[I.length] = n.id, I.push(t), L.push(n), P.push(n.data || {}), b = !0
    }, C.td = function(t) {
      return L[t]
    }, C.value = function(t) {
      return P[t]
    }, C.marker = function(t) {
      return t in I && (r(t), I[t])
    }, C.markerIsSet = function(t) {
      return Boolean(I[t])
    }, C.setMarker = function(t, e) {
      I[t] = e
    }, C.store = function(t, e, n) {
      T[t.ref] = {
        obj: e,
        shadow: n
      }
    }, C.free = function() {
      var e;
      for (e = 0; e < E.length; e++) D.event.removeListener(E[e]);
      E = [], t.each(T, function(t) {
        l(t)
      }), T = {}, t.each(L, function(t) {
        L[t] = null
      }), L = [], t.each(I, function(t) {
        I[t] && (I[t].setMap(null), delete I[t])
      }), I = [], t.each(P, function(t) {
        delete P[t]
      }), P = [], S = {}, k = {}
    }, C.filter = function(t) {
      g = t, f()
    }, C.enable = function(t) {
      x !== t && (x = t, f())
    }, C.display = function(t) {
      v = t
    }, C.error = function(t) {
      t
    }, C.beginUpdate = function() {
      y = !0
    }, C.endUpdate = function() {
      y = !1, b && f()
    }, C.autofit = function(t) {
      var e;
      for (e = 0; e < L.length; e++) L[e] && t.extend(L[e].options.position)
    }
  }

  function S(t, e) {
    var n = this;
    n.id = function() {
      return t
    }, n.filter = function(t) {
      e.filter(t)
    }, n.enable = function() {
      e.enable(!0)
    }, n.disable = function() {
      e.enable(!1)
    }, n.add = function(t, n, i) {
      i || e.beginUpdate(), e.addMarker(t, n), i || e.endUpdate()
    }, n.getById = function(t) {
      return e.getById(t)
    }, n.clearById = function(t, n) {
      var i;
      return n || e.beginUpdate(), i = e.clearById(t), n || e.endUpdate(), i
    }, n.clear = function(t, n, i, o) {
      o || e.beginUpdate(), e.clear(t, n, i), o || e.endUpdate()
    }
  }

  function k(e, n, i, o) {
    var r = this,
      s = [];
    A.classes.OverlayView.call(r), r.setMap(e), r.onAdd = function() {
      var e = r.getPanes();
      n.pane in e && t(e[n.pane]).append(o), t.each("dblclick click mouseover mousemove mouseout mouseup mousedown".split(" "), function(e, n) {
        s.push(D.event.addDomListener(o[0], n, function(e) {
          t.Event(e).stopPropagation(), D.event.trigger(r, n, [e]), r.draw()
        }))
      }), s.push(D.event.addDomListener(o[0], "contextmenu", function(e) {
        t.Event(e).stopPropagation(), D.event.trigger(r, "rightclick", [e]), r.draw()
      }))
    }, r.getPosition = function() {
      return i
    }, r.setPosition = function(t) {
      i = t, r.draw()
    }, r.draw = function() {
      var t = r.getProjection().fromLatLngToDivPixel(i);
      o.css("left", t.x + n.offset.x + "px").css("top", t.y + n.offset.y + "px")
    }, r.onRemove = function() {
      var t;
      for (t = 0; t < s.length; t++) D.event.removeListener(s[t]);
      o.remove()
    }, r.hide = function() {
      o.hide()
    }, r.show = function() {
      o.show()
    }, r.toggle = function() {
      o && (o.is(":visible") ? r.show() : r.hide())
    }, r.toggleDOM = function() {
      r.setMap(r.getMap() ? null : e)
    }, r.getDOMElement = function() {
      return o[0]
    }
  }

  function I(o) {
    function l() {
      !_ && (_ = L.get()) && _.run()
    }

    function u() {
      _ = null, L.ack(), l.call(I)
    }

    function f(t) {
      var e, n = t.td.callback;
      n && (e = Array.prototype.slice.call(arguments, 1), O(n) ? n.apply(o, e) : P(n) && O(n[1]) && n[1].apply(n[0], e))
    }

    function p(t, e, n) {
      n && a(o, t, e, n), f(t, e), _.ack(e)
    }

    function v(e, n) {
      var i = (n = n || {}).td && n.td.options ? n.td.options : 0;
      z ? i && (i.center && (i.center = m(i.center)), z.setOptions(i)) : ((i = n.opts || t.extend(!0, {}, A.map, i || {})).center = e || m(i.center), z = new A.classes.Map(o.get(0), i))
    }

    function y(n) {
      var i, r, s = new T(o, z, n),
        l = {},
        u = {},
        c = [],
        f = /^[0-9]+$/;
      for (r in n) f.test(r) ? (c.push(1 * r), u[r] = n[r], u[r].width = u[r].width || 0, u[r].height = u[r].height || 0) : l[r] = n[r];
      return c.sort(function(t, e) {
        return t > e
      }), i = l.calculator ? function(e) {
        var n = [];
        return t.each(e, function(t, e) {
          n.push(s.value(e))
        }), l.calculator.apply(o, [n])
      } : function(t) {
        return t.length
      }, s.error(function() {
        d.apply(I, arguments)
      }), s.display(function(r) {
        var d, f, h, p, g, v, y = i(r.indexes);
        if (n.force || y > 1)
          for (d = 0; d < c.length; d++) c[d] <= y && (f = u[c[d]]);
        f ? (g = f.offset || [-f.width / 2, -f.height / 2], (h = t.extend({}, l)).options = t.extend({
          pane: "overlayLayer",
          content: f.content ? f.content.replace("CLUSTER_COUNT", y) : "",
          offset: {
            x: ("x" in g ? g.x : g[0]) || 0,
            y: ("y" in g ? g.y : g[1]) || 0
          }
        }, l.options || {}), p = I.overlay({
          td: h,
          opts: h.options,
          latLng: m(r)
        }, !0), h.options.pane = "floatShadow", h.options.content = t(document.createElement("div")).width(f.width + "px").height(f.height + "px").css({
          cursor: "pointer"
        }), v = I.overlay({
          td: h,
          opts: h.options,
          latLng: m(r)
        }, !0), l.data = {
          latLng: m(r),
          markers: []
        }, t.each(r.indexes, function(t, e) {
          l.data.markers.push(s.value(e)), s.markerIsSet(e) && s.marker(e).setMap(null)
        }), a(o, {
          td: l
        }, v, e, {
          main: p,
          shadow: v
        }), s.store(r, p, v)) : t.each(r.indexes, function(t, e) {
          s.marker(e).setMap(z)
        })
      }), s
    }

    function b(e, n, i) {
      var r = [],
        s = "values" in e.td;
      return s || (e.td.values = [{
        options: e.opts
      }]), e.td.values.length ? (v(), t.each(e.td.values, function(t, s) {
        var l, u, d, f, h = c(e, s);
        if (h.options[i])
          if (h.options[i][0][0] && P(h.options[i][0][0]))
            for (u = 0; u < h.options[i].length; u++)
              for (d = 0; d < h.options[i][u].length; d++) h.options[i][u][d] = m(h.options[i][u][d]);
          else
            for (u = 0; u < h.options[i].length; u++) h.options[i][u] = m(h.options[i][u]);
        h.options.map = z, f = new D[n](h.options), r.push(f), l = j.add({
          td: h
        }, n.toLowerCase(), f), a(o, {
          td: h
        }, f, l)
      }), void p(e, s ? r : r[0])) : void p(e, !1)
    }
    var _, I = this,
      L = new w,
      j = new x,
      z = null;
    I._plan = function(t) {
      var e;
      for (e = 0; e < t.length; e++) L.add(new C(I, u, t[e]));
      l()
    }, I.map = function(t) {
      v(t.latLng, t), a(o, t, z), p(t, z)
    }, I.destroy = function(t) {
      j.clear(), o.empty(), z && (z = null), p(t, !0)
    }, I.overlay = function(e, n) {
      var i = [],
        r = "values" in e.td;
      return r || (e.td.values = [{
        latLng: e.latLng,
        options: e.opts
      }]), e.td.values.length ? (k.__initialised || (k.prototype = new A.classes.OverlayView, k.__initialised = !0), t.each(e.td.values, function(r, s) {
        var l, u, d = c(e, s),
          f = t(document.createElement("div")).css({
            border: "none",
            borderWidth: 0,
            position: "absolute"
          });
        f.append(d.options.content), u = new k(z, d.options, m(d) || m(s), f), i.push(u), f = null, n || (l = j.add(e, "overlay", u), a(o, {
          td: d
        }, u, l))
      }), n ? i[0] : void p(e, r ? i : i[0])) : void p(e, !1)
    }, I.marker = function(e) {
      var n, i, r, l = "values" in e.td,
        u = !z;
      return l || (e.opts.position = e.latLng || m(e.opts.position), e.td.values = [{
        options: e.opts
      }]), e.td.values.length ? (u && v(), e.td.cluster && !z.getBounds() ? void D.event.addListenerOnce(z, "bounds_changed", function() {
        I.marker.apply(I, [e])
      }) : void(e.td.cluster ? (e.td.cluster instanceof S ? (i = e.td.cluster, r = j.getById(i.id(), !0)) : (r = y(e.td.cluster), i = new S(s(e.td.id, !0), r), j.add(e, "clusterer", i, r)), r.beginUpdate(), t.each(e.td.values, function(t, n) {
        var i = c(e, n);
        i.options.position = m(i.options.position ? i.options.position : n), i.options.position && (i.options.map = z, u && (z.setCenter(i.options.position), u = !1), r.add(i, n))
      }), r.endUpdate(), p(e, i)) : (n = [], t.each(e.td.values, function(t, i) {
        var r, s, l = c(e, i);
        l.options.position = m(l.options.position ? l.options.position : i), l.options.position && (l.options.map = z, u && (z.setCenter(l.options.position), u = !1), s = new A.classes.Marker(l.options), n.push(s), r = j.add({
          td: l
        }, "marker", s), a(o, {
          td: l
        }, s, r))
      }), p(e, l ? n : n[0])))) : void p(e, !1)
    }, I.getroute = function(t) {
      t.opts.origin = m(t.opts.origin, !0), t.opts.destination = m(t.opts.destination, !0), (N.ds || (N.ds = new D.DirectionsService), N.ds).route(t.opts, function(e, n) {
        f(t, n === D.DirectionsStatus.OK && e, n), _.ack()
      })
    }, I.getdistance = function(t) {
      var e;
      for (t.opts.origins = h(t.opts.origins), e = 0; e < t.opts.origins.length; e++) t.opts.origins[e] = m(t.opts.origins[e], !0);
      for (t.opts.destinations = h(t.opts.destinations), e = 0; e < t.opts.destinations.length; e++) t.opts.destinations[e] = m(t.opts.destinations[e], !0);
      (N.dms || (N.dms = new D.DistanceMatrixService), N.dms).getDistanceMatrix(t.opts, function(e, n) {
        f(t, n === D.DistanceMatrixStatus.OK && e, n), _.ack()
      })
    }, I.infowindow = function(n) {
      var i = [],
        s = "values" in n.td;
      s || (n.latLng && (n.opts.position = n.latLng), n.td.values = [{
        options: n.opts
      }]), t.each(n.td.values, function(t, l) {
        var u, d, f = c(n, l);
        f.options.position = m(f.options.position ? f.options.position : l.latLng), z || v(f.options.position), (d = new A.classes.InfoWindow(f.options)) && (r(f.open) || f.open) && (s ? d.open(z, f.anchor || e) : d.open(z, f.anchor || (n.latLng ? e : n.session.marker ? n.session.marker : e))), i.push(d), u = j.add({
          td: f
        }, "infowindow", d), a(o, {
          td: f
        }, d, u)
      }), p(n, s ? i : i[0])
    }, I.circle = function(e) {
      var n = [],
        i = "values" in e.td;
      return i || (e.opts.center = e.latLng || m(e.opts.center), e.td.values = [{
        options: e.opts
      }]), e.td.values.length ? (t.each(e.td.values, function(t, i) {
        var r, s, l = c(e, i);
        l.options.center = m(l.options.center ? l.options.center : i), z || v(l.options.center), l.options.map = z, s = new A.classes.Circle(l.options), n.push(s), r = j.add({
          td: l
        }, "circle", s), a(o, {
          td: l
        }, s, r)
      }), void p(e, i ? n : n[0])) : void p(e, !1)
    }, I.getaddress = function(t) {
      f(t, t.results, t.status), _.ack()
    }, I.getlatlng = function(t) {
      f(t, t.results, t.status), _.ack()
    }, I.getmaxzoom = function(t) {
      (N.mzs || (N.mzs = new D.MaxZoomService), N.mzs).getMaxZoomAtLatLng(t.latLng, function(e) {
        f(t, e.status === D.MaxZoomStatus.OK && e.zoom, status), _.ack()
      })
    }, I.getelevation = function(t) {
      var e, n = [],
        i = function(e, n) {
          f(t, n === D.ElevationStatus.OK && e, n), _.ack()
        };
      if (t.latLng) n.push(t.latLng);
      else
        for (n = h(t.td.locations || []), e = 0; e < n.length; e++) n[e] = m(n[e]);
      if (n.length) E().getElevationForLocations({
        locations: n
      }, i);
      else {
        if (t.td.path && t.td.path.length)
          for (e = 0; e < t.td.path.length; e++) n.push(m(t.td.path[e]));
        n.length ? E().getElevationAlongPath({
          path: n,
          samples: t.td.samples
        }, i) : _.ack()
      }
    }, I.defaults = function(e) {
      t.each(e.td, function(e, i) {
        A[e] = n(A[e]) ? t.extend({}, A[e], i) : i
      }), _.ack(!0)
    }, I.rectangle = function(e) {
      var n = [],
        i = "values" in e.td;
      return i || (e.td.values = [{
        options: e.opts
      }]), e.td.values.length ? (t.each(e.td.values, function(t, i) {
        var r, s, l = c(e, i);
        l.options.bounds = g(l.options.bounds ? l.options.bounds : i), z || v(l.options.bounds.getCenter()), l.options.map = z, s = new A.classes.Rectangle(l.options), n.push(s), r = j.add({
          td: l
        }, "rectangle", s), a(o, {
          td: l
        }, s, r)
      }), void p(e, i ? n : n[0])) : void p(e, !1)
    }, I.polyline = function(t) {
      b(t, "Polyline", "path")
    }, I.polygon = function(t) {
      b(t, "Polygon", "paths")
    }, I.trafficlayer = function(t) {
      v();
      var e = j.get("trafficlayer");
      e || ((e = new A.classes.TrafficLayer).setMap(z), j.add(t, "trafficlayer", e)), p(t, e)
    }, I.bicyclinglayer = function(t) {
      v();
      var e = j.get("bicyclinglayer");
      e || ((e = new A.classes.BicyclingLayer).setMap(z), j.add(t, "bicyclinglayer", e)), p(t, e)
    }, I.groundoverlay = function(t) {
      t.opts.bounds = g(t.opts.bounds), t.opts.bounds && v(t.opts.bounds.getCenter());
      var e = new A.classes.GroundOverlay(t.opts.url, t.opts.bounds, t.opts.opts);
      e.setMap(z), p(t, e, j.add(t, "groundoverlay", e))
    }, I.streetviewpanorama = function(e) {
      e.opts.opts || (e.opts.opts = {}), e.latLng ? e.opts.opts.position = e.latLng : e.opts.opts.position && (e.opts.opts.position = m(e.opts.opts.position)), e.td.divId ? e.opts.container = document.getElementById(e.td.divId) : e.opts.container && (e.opts.container = t(e.opts.container).get(0));
      var n = new A.classes.StreetViewPanorama(e.opts.container, e.opts.opts);
      n && z.setStreetView(n), p(e, n, j.add(e, "streetviewpanorama", n))
    }, I.kmllayer = function(e) {
      var n = [],
        i = "values" in e.td;
      return i || (e.td.values = [{
        options: e.opts
      }]), e.td.values.length ? (t.each(e.td.values, function(t, i) {
        var r, s, l, u = c(e, i);
        z || v(), l = u.options, u.options.opts && (l = u.options.opts, u.options.url && (l.url = u.options.url)), l.map = z, s = function(t) {
          var e, n = D.version.split(".");
          for (t = t.split("."), e = 0; e < n.length; e++) n[e] = parseInt(n[e], 10);
          for (e = 0; e < t.length; e++) {
            if (t[e] = parseInt(t[e], 10), !n.hasOwnProperty(e)) return !1;
            if (n[e] < t[e]) return !1
          }
          return !0
        }("3.10") ? new A.classes.KmlLayer(l) : new A.classes.KmlLayer(l.url, l), n.push(s), r = j.add({
          td: u
        }, "kmllayer", s), a(o, {
          td: u
        }, s, r)
      }), void p(e, i ? n : n[0])) : void p(e, !1)
    }, I.panel = function(e) {
      v();
      var n, i = 0,
        s = 0,
        a = t(document.createElement("div"));
      a.css({
        position: "absolute",
        zIndex: 1e3,
        visibility: "hidden"
      }), e.opts.content && (n = t(e.opts.content), a.append(n), o.first().prepend(a), r(e.opts.left) ? r(e.opts.right) ? e.opts.center && (i = (o.width() - n.width()) / 2) : i = o.width() - n.width() - e.opts.right : i = e.opts.left, r(e.opts.top) ? r(e.opts.bottom) ? e.opts.middle && (s = (o.height() - n.height()) / 2) : s = o.height() - n.height() - e.opts.bottom : s = e.opts.top, a.css({
        top: s,
        left: i,
        visibility: "visible"
      })), p(e, a, j.add(e, "panel", a)), a = null
    }, I.directionsrenderer = function(e) {
      e.opts.map = z;
      var n = new D.DirectionsRenderer(e.opts);
      e.td.divId ? n.setPanel(document.getElementById(e.td.divId)) : e.td.container && n.setPanel(t(e.td.container).get(0)), p(e, n, j.add(e, "directionsrenderer", n))
    }, I.getgeoloc = function(t) {
      p(t, t.latLng)
    }, I.styledmaptype = function(t) {
      v();
      var e = new A.classes.StyledMapType(t.td.styles, t.opts);
      z.mapTypes.set(t.td.id, e), p(t, e)
    }, I.imagemaptype = function(t) {
      v();
      var e = new A.classes.ImageMapType(t.opts);
      z.mapTypes.set(t.td.id, e), p(t, e)
    }, I.autofit = function(e) {
      var n = new D.LatLngBounds;
      t.each(j.all(), function(t, e) {
        e.getPosition ? n.extend(e.getPosition()) : e.getBounds ? (n.extend(e.getBounds().getNorthEast()), n.extend(e.getBounds().getSouthWest())) : e.getPaths ? e.getPaths().forEach(function(t) {
          t.forEach(function(t) {
            n.extend(t)
          })
        }) : e.getPath ? e.getPath().forEach(function(t) {
          n.extend(t)
        }) : e.getCenter ? n.extend(e.getCenter()) : e instanceof S && ((e = j.getById(e.id(), !0)) && e.autofit(n))
      }), n.isEmpty() || z.getBounds() && z.getBounds().equals(n) || ("maxZoom" in e.td && D.event.addListenerOnce(z, "bounds_changed", function() {
        this.getZoom() > e.td.maxZoom && this.setZoom(e.td.maxZoom)
      }), z.fitBounds(n)), p(e, !0)
    }, I.clear = function(e) {
      if (i(e.td)) {
        if (j.clearById(e.td) || j.objClearById(e.td)) return void p(e, !0);
        e.td = {
          name: e.td
        }
      }
      e.td.id ? t.each(h(e.td.id), function(t, e) {
        j.clearById(e) || j.objClearById(e)
      }) : (j.clear(h(e.td.name), e.td.last, e.td.first, e.td.tag), j.objClear(h(e.td.name), e.td.last, e.td.first, e.td.tag)), p(e, !0)
    }, I.get = function(n, o, r) {
      var s, a, l = o ? n : n.td;
      return o || (r = l.full), i(l) ? !1 === (a = j.getById(l, !1, r) || j.objGetById(l)) && (s = l, l = {}) : s = l.name, "map" === s && (a = z), a || (a = [], l.id ? (t.each(h(l.id), function(t, e) {
        a.push(j.getById(e, !1, r) || j.objGetById(e))
      }), P(l.id) || (a = a[0])) : (t.each(s ? h(s) : [e], function(e, n) {
        var i;
        l.first ? (i = j.get(n, !1, l.tag, r)) && a.push(i) : l.all ? t.each(j.all(n, l.tag, r), function(t, e) {
          a.push(e)
        }) : (i = j.get(n, !0, l.tag, r)) && a.push(i)
      }), l.all || P(s) || (a = a[0]))), a = P(a) || !l.all ? a : [a], o ? a : void p(n, a)
    }, I.exec = function(e) {
      t.each(h(e.td.func), function(n, i) {
        t.each(I.get(e.td, !0, !e.td.hasOwnProperty("full") || e.td.full), function(t, e) {
          i.call(o, e)
        })
      }), p(e, !0)
    }, I.trigger = function(e) {
      if (i(e.td)) D.event.trigger(z, e.td);
      else {
        var n = [z, e.td.eventName];
        e.td.var_args && t.each(e.td.var_args, function(t, e) {
          n.push(e)
        }), D.event.trigger.apply(D.event, n)
      }
      f(e), _.ack()
    }
  }
  var A, D, L = 0,
    O = t.isFunction,
    P = t.isArray,
    N = {},
    j = new function() {
      var t = [];
      this.get = function(e) {
        if (t.length) {
          var i, o, r, s, a, u = l(e);
          for (i = 0; i < t.length; i++) {
            for (s = t[i], a = u.length === s.keys.length, o = 0; o < u.length && a; o++)(a = (r = u[o]) in s.request) && (a = n(e[r]) && "equals" in e[r] && O(e[r]) ? e[r].equals(s.request[r]) : e[r] === s.request[r]);
            if (a) return s.results
          }
        }
      }, this.store = function(e, n) {
        t.push({
          request: e,
          keys: l(e),
          results: n
        })
      }
    };
  t.fn.gmap3 = function() {
    var e, n = [],
      i = [];
    for (function() {
        var e;
        D = google.maps, A || (A = {
          verbose: !1,
          queryLimit: {
            attempt: 5,
            delay: 250,
            random: 250
          },
          classes: (e = {}, t.each("Map Marker InfoWindow Circle Rectangle OverlayView StreetViewPanorama KmlLayer TrafficLayer BicyclingLayer GroundOverlay StyledMapType ImageMapType".split(" "), function(t, n) {
            e[n] = D[n]
          }), e),
          map: {
            mapTypeId: D.MapTypeId.ROADMAP,
            center: [46.578498, 2.457275],
            zoom: 2
          },
          overlay: {
            pane: "floatPane",
            content: "",
            offset: {
              x: 0,
              y: 0
            }
          },
          geoloc: {
            getCurrentPosition: {
              maximumAge: 6e4,
              timeout: 5e3
            }
          }
        })
      }(), e = 0; e < arguments.length; e++) arguments[e] && n.push(arguments[e]);
    return n.length || n.push("map"), t.each(this, function() {
      var e = t(this),
        o = e.data("gmap3");
      !1, o || (o = new I(e), e.data("gmap3", o)), 1 !== n.length || "get" !== n[0] && !_(n[0]) ? o._plan(n) : i.push("get" === n[0] ? o.get("map", !0) : o.get(n[0].get, !0, n[0].get.full))
    }), i.length ? 1 === i.length ? i[0] : i : this
  }
}(jQuery),
function(t, e) {
  "function" == typeof define && define.amd ? define("ev-emitter/ev-emitter", e) : "object" == typeof module && module.exports ? module.exports = e() : t.EvEmitter = e()
}("undefined" != typeof window ? window : this, function() {
  function t() {}
  var e = t.prototype;
  return e.on = function(t, e) {
    if (t && e) {
      var n = this._events = this._events || {},
        i = n[t] = n[t] || [];
      return -1 == i.indexOf(e) && i.push(e), this
    }
  }, e.once = function(t, e) {
    if (t && e) {
      this.on(t, e);
      var n = this._onceEvents = this._onceEvents || {};
      return (n[t] = n[t] || {})[e] = !0, this
    }
  }, e.off = function(t, e) {
    var n = this._events && this._events[t];
    if (n && n.length) {
      var i = n.indexOf(e);
      return -1 != i && n.splice(i, 1), this
    }
  }, e.emitEvent = function(t, e) {
    var n = this._events && this._events[t];
    if (n && n.length) {
      var i = 0,
        o = n[i];
      e = e || [];
      for (var r = this._onceEvents && this._onceEvents[t]; o;) {
        var s = r && r[o];
        s && (this.off(t, o), delete r[o]), o.apply(this, e), o = n[i += s ? 0 : 1]
      }
      return this
    }
  }, t
}),
function(t, e) {
  "use strict";
  "function" == typeof define && define.amd ? define(["ev-emitter/ev-emitter"], function(n) {
    return e(t, n)
  }) : "object" == typeof module && module.exports ? module.exports = e(t, require("ev-emitter")) : t.imagesLoaded = e(t, t.EvEmitter)
}(window, function(t, e) {
  function n(t, e) {
    for (var n in e) t[n] = e[n];
    return t
  }

  function i(t, e, o) {
    return this instanceof i ? ("string" == typeof t && (t = document.querySelectorAll(t)), this.elements = function(t) {
      var e = [];
      if (Array.isArray(t)) e = t;
      else if ("number" == typeof t.length)
        for (var n = 0; n < t.length; n++) e.push(t[n]);
      else e.push(t);
      return e
    }(t), this.options = n({}, this.options), "function" == typeof e ? o = e : n(this.options, e), o && this.on("always", o), this.getImages(), s && (this.jqDeferred = new s.Deferred), void setTimeout(function() {
      this.check()
    }.bind(this))) : new i(t, e, o)
  }

  function o(t) {
    this.img = t
  }

  function r(t, e) {
    this.url = t, this.element = e, this.img = new Image
  }
  var s = t.jQuery,
    a = t.console;
  i.prototype = Object.create(e.prototype), i.prototype.options = {}, i.prototype.getImages = function() {
    this.images = [], this.elements.forEach(this.addElementImages, this)
  }, i.prototype.addElementImages = function(t) {
    "IMG" == t.nodeName && this.addImage(t), !0 === this.options.background && this.addElementBackgroundImages(t);
    var e = t.nodeType;
    if (e && l[e]) {
      for (var n = t.querySelectorAll("img"), i = 0; i < n.length; i++) {
        var o = n[i];
        this.addImage(o)
      }
      if ("string" == typeof this.options.background) {
        var r = t.querySelectorAll(this.options.background);
        for (i = 0; i < r.length; i++) {
          var s = r[i];
          this.addElementBackgroundImages(s)
        }
      }
    }
  };
  var l = {
    1: !0,
    9: !0,
    11: !0
  };
  return i.prototype.addElementBackgroundImages = function(t) {
    var e = getComputedStyle(t);
    if (e)
      for (var n = /url\((['"])?(.*?)\1\)/gi, i = n.exec(e.backgroundImage); null !== i;) {
        var o = i && i[2];
        o && this.addBackground(o, t), i = n.exec(e.backgroundImage)
      }
  }, i.prototype.addImage = function(t) {
    var e = new o(t);
    this.images.push(e)
  }, i.prototype.addBackground = function(t, e) {
    var n = new r(t, e);
    this.images.push(n)
  }, i.prototype.check = function() {
    function t(t, n, i) {
      setTimeout(function() {
        e.progress(t, n, i)
      })
    }
    var e = this;
    return this.progressedCount = 0, this.hasAnyBroken = !1, this.images.length ? void this.images.forEach(function(e) {
      e.once("progress", t), e.check()
    }) : void this.complete()
  }, i.prototype.progress = function(t, e, n) {
    this.progressedCount++, this.hasAnyBroken = this.hasAnyBroken || !t.isLoaded, this.emitEvent("progress", [this, t, e]), this.jqDeferred && this.jqDeferred.notify && this.jqDeferred.notify(this, t), this.progressedCount == this.images.length && this.complete(), this.options.debug && a && a.log("progress: " + n, t, e)
  }, i.prototype.complete = function() {
    var t = this.hasAnyBroken ? "fail" : "done";
    if (this.isComplete = !0, this.emitEvent(t, [this]), this.emitEvent("always", [this]), this.jqDeferred) {
      var e = this.hasAnyBroken ? "reject" : "resolve";
      this.jqDeferred[e](this)
    }
  }, o.prototype = Object.create(e.prototype), o.prototype.check = function() {
    return this.getIsImageComplete() ? void this.confirm(0 !== this.img.naturalWidth, "naturalWidth") : (this.proxyImage = new Image, this.proxyImage.addEventListener("load", this), this.proxyImage.addEventListener("error", this), this.img.addEventListener("load", this), this.img.addEventListener("error", this), void(this.proxyImage.src = this.img.src))
  }, o.prototype.getIsImageComplete = function() {
    return this.img.complete && void 0 !== this.img.naturalWidth
  }, o.prototype.confirm = function(t, e) {
    this.isLoaded = t, this.emitEvent("progress", [this, this.img, e])
  }, o.prototype.handleEvent = function(t) {
    var e = "on" + t.type;
    this[e] && this[e](t)
  }, o.prototype.onload = function() {
    this.confirm(!0, "onload"), this.unbindEvents()
  }, o.prototype.onerror = function() {
    this.confirm(!1, "onerror"), this.unbindEvents()
  }, o.prototype.unbindEvents = function() {
    this.proxyImage.removeEventListener("load", this), this.proxyImage.removeEventListener("error", this), this.img.removeEventListener("load", this), this.img.removeEventListener("error", this)
  }, r.prototype = Object.create(o.prototype), r.prototype.check = function() {
    this.img.addEventListener("load", this), this.img.addEventListener("error", this), this.img.src = this.url, this.getIsImageComplete() && (this.confirm(0 !== this.img.naturalWidth, "naturalWidth"), this.unbindEvents())
  }, r.prototype.unbindEvents = function() {
    this.img.removeEventListener("load", this), this.img.removeEventListener("error", this)
  }, r.prototype.confirm = function(t, e) {
    this.isLoaded = t, this.emitEvent("progress", [this, this.element, e])
  }, i.makeJQueryPlugin = function(e) {
    (e = e || t.jQuery) && ((s = e).fn.imagesLoaded = function(t, e) {
      return new i(this, t, e).jqDeferred.promise(s(this))
    })
  }, i.makeJQueryPlugin(), i
}),
function(t, e) {
  "function" == typeof define && define.amd ? define("jquery-bridget/jquery-bridget", ["jquery"], function(n) {
    return e(t, n)
  }) : "object" == typeof module && module.exports ? module.exports = e(t, require("jquery")) : t.jQueryBridget = e(t, t.jQuery)
}(window, function(t, e) {
  "use strict";

  function n(n, r, a) {
    (a = a || e || t.jQuery) && (r.prototype.option || (r.prototype.option = function(t) {
      a.isPlainObject(t) && (this.options = a.extend(!0, this.options, t))
    }), a.fn[n] = function(t) {
      return "string" == typeof t ? function(t, e, i) {
        var o, r = "$()." + n + '("' + e + '")';
        return t.each(function(t, l) {
          var u = a.data(l, n);
          if (u) {
            var c = u[e];
            if (c && "_" != e.charAt(0)) {
              var d = c.apply(u, i);
              o = void 0 === o ? d : o
            } else s(r + " is not a valid method")
          } else s(n + " not initialized. Cannot call methods, i.e. " + r)
        }), void 0 !== o ? o : t
      }(this, t, o.call(arguments, 1)) : (function(t, e) {
        t.each(function(t, i) {
          var o = a.data(i, n);
          o ? (o.option(e), o._init()) : (o = new r(i, e), a.data(i, n, o))
        })
      }(this, t), this)
    }, i(a))
  }

  function i(t) {
    !t || t && t.bridget || (t.bridget = n)
  }
  var o = Array.prototype.slice,
    r = t.console,
    s = void 0 === r ? function() {} : function(t) {
      r.error(t)
    };
  return i(e || t.jQuery), n
}),
function(t, e) {
  "function" == typeof define && define.amd ? define("ev-emitter/ev-emitter", e) : "object" == typeof module && module.exports ? module.exports = e() : t.EvEmitter = e()
}("undefined" != typeof window ? window : this, function() {
  function t() {}
  var e = t.prototype;
  return e.on = function(t, e) {
    if (t && e) {
      var n = this._events = this._events || {},
        i = n[t] = n[t] || [];
      return -1 == i.indexOf(e) && i.push(e), this
    }
  }, e.once = function(t, e) {
    if (t && e) {
      this.on(t, e);
      var n = this._onceEvents = this._onceEvents || {};
      return (n[t] = n[t] || {})[e] = !0, this
    }
  }, e.off = function(t, e) {
    var n = this._events && this._events[t];
    if (n && n.length) {
      var i = n.indexOf(e);
      return -1 != i && n.splice(i, 1), this
    }
  }, e.emitEvent = function(t, e) {
    var n = this._events && this._events[t];
    if (n && n.length) {
      var i = 0,
        o = n[i];
      e = e || [];
      for (var r = this._onceEvents && this._onceEvents[t]; o;) {
        var s = r && r[o];
        s && (this.off(t, o), delete r[o]), o.apply(this, e), o = n[i += s ? 0 : 1]
      }
      return this
    }
  }, t
}),
function(t, e) {
  "use strict";
  "function" == typeof define && define.amd ? define("get-size/get-size", [], function() {
    return e()
  }) : "object" == typeof module && module.exports ? module.exports = e() : t.getSize = e()
}(window, function() {
  "use strict";

  function t(t) {
    var e = parseFloat(t);
    return -1 == t.indexOf("%") && !isNaN(e) && e
  }

  function e(t) {
    var e = getComputedStyle(t);
    return e || r("Style returned " + e + ". Are you running this code in a hidden iframe on Firefox? See http://bit.ly/getsizebug1"), e
  }

  function n() {
    if (!l) {
      l = !0;
      var n = document.createElement("div");
      n.style.width = "200px", n.style.padding = "1px 2px 3px 4px", n.style.borderStyle = "solid", n.style.borderWidth = "1px 2px 3px 4px", n.style.boxSizing = "border-box";
      var r = document.body || document.documentElement;
      r.appendChild(n);
      var s = e(n);
      i.isBoxSizeOuter = o = 200 == t(s.width), r.removeChild(n)
    }
  }

  function i(i) {
    if (n(), "string" == typeof i && (i = document.querySelector(i)), i && "object" == typeof i && i.nodeType) {
      var r = e(i);
      if ("none" == r.display) return function() {
        for (var t = {
            width: 0,
            height: 0,
            innerWidth: 0,
            innerHeight: 0,
            outerWidth: 0,
            outerHeight: 0
          }, e = 0; e < a; e++) t[s[e]] = 0;
        return t
      }();
      var l = {};
      l.width = i.offsetWidth, l.height = i.offsetHeight;
      for (var u = l.isBorderBox = "border-box" == r.boxSizing, c = 0; c < a; c++) {
        var d = s[c],
          f = r[d],
          h = parseFloat(f);
        l[d] = isNaN(h) ? 0 : h
      }
      var p = l.paddingLeft + l.paddingRight,
        m = l.paddingTop + l.paddingBottom,
        g = l.marginLeft + l.marginRight,
        v = l.marginTop + l.marginBottom,
        y = l.borderLeftWidth + l.borderRightWidth,
        b = l.borderTopWidth + l.borderBottomWidth,
        _ = u && o,
        w = t(r.width);
      !1 !== w && (l.width = w + (_ ? 0 : p + y));
      var x = t(r.height);
      return !1 !== x && (l.height = x + (_ ? 0 : m + b)), l.innerWidth = l.width - (p + y), l.innerHeight = l.height - (m + b), l.outerWidth = l.width + g, l.outerHeight = l.height + v, l
    }
  }
  var o, r = "undefined" == typeof console ? function() {} : function(t) {
      console.error(t)
    },
    s = ["paddingLeft", "paddingRight", "paddingTop", "paddingBottom", "marginLeft", "marginRight", "marginTop", "marginBottom", "borderLeftWidth", "borderRightWidth", "borderTopWidth", "borderBottomWidth"],
    a = s.length,
    l = !1;
  return i
}),
function(t, e) {
  "use strict";
  "function" == typeof define && define.amd ? define("desandro-matches-selector/matches-selector", e) : "object" == typeof module && module.exports ? module.exports = e() : t.matchesSelector = e()
}(window, function() {
  "use strict";
  var t = function() {
    var t = window.Element.prototype;
    if (t.matches) return "matches";
    if (t.matchesSelector) return "matchesSelector";
    for (var e = ["webkit", "moz", "ms", "o"], n = 0; n < e.length; n++) {
      var i = e[n] + "MatchesSelector";
      if (t[i]) return i
    }
  }();
  return function(e, n) {
    return e[t](n)
  }
}),
function(t, e) {
  "function" == typeof define && define.amd ? define("fizzy-ui-utils/utils", ["desandro-matches-selector/matches-selector"], function(n) {
    return e(t, n)
  }) : "object" == typeof module && module.exports ? module.exports = e(t, require("desandro-matches-selector")) : t.fizzyUIUtils = e(t, t.matchesSelector)
}(window, function(t, e) {
  var n = {
      extend: function(t, e) {
        for (var n in e) t[n] = e[n];
        return t
      },
      modulo: function(t, e) {
        return (t % e + e) % e
      },
      makeArray: function(t) {
        var e = [];
        if (Array.isArray(t)) e = t;
        else if (t && "object" == typeof t && "number" == typeof t.length)
          for (var n = 0; n < t.length; n++) e.push(t[n]);
        else e.push(t);
        return e
      },
      removeFrom: function(t, e) {
        var n = t.indexOf(e); - 1 != n && t.splice(n, 1)
      },
      getParent: function(t, n) {
        for (; t != document.body;)
          if (t = t.parentNode, e(t, n)) return t
      },
      getQueryElement: function(t) {
        return "string" == typeof t ? document.querySelector(t) : t
      },
      handleEvent: function(t) {
        var e = "on" + t.type;
        this[e] && this[e](t)
      },
      filterFindElements: function(t, i) {
        t = n.makeArray(t);
        var o = [];
        return t.forEach(function(t) {
          if (t instanceof HTMLElement) {
            if (!i) return void o.push(t);
            e(t, i) && o.push(t);
            for (var n = t.querySelectorAll(i), r = 0; r < n.length; r++) o.push(n[r])
          }
        }), o
      },
      debounceMethod: function(t, e, n) {
        var i = t.prototype[e],
          o = e + "Timeout";
        t.prototype[e] = function() {
          var t = this[o];
          t && clearTimeout(t);
          var e = arguments,
            r = this;
          this[o] = setTimeout(function() {
            i.apply(r, e), delete r[o]
          }, n || 100)
        }
      },
      docReady: function(t) {
        var e = document.readyState;
        "complete" == e || "interactive" == e ? setTimeout(t) : document.addEventListener("DOMContentLoaded", t)
      },
      toDashed: function(t) {
        return t.replace(/(.)([A-Z])/g, function(t, e, n) {
          return e + "-" + n
        }).toLowerCase()
      }
    },
    i = t.console;
  return n.htmlInit = function(e, o) {
    n.docReady(function() {
      var r = n.toDashed(o),
        s = "data-" + r,
        a = document.querySelectorAll("[" + s + "]"),
        l = document.querySelectorAll(".js-" + r),
        u = n.makeArray(a).concat(n.makeArray(l)),
        c = s + "-options",
        d = t.jQuery;
      u.forEach(function(t) {
        var n, r = t.getAttribute(s) || t.getAttribute(c);
        try {
          n = r && JSON.parse(r)
        } catch (e) {
          return void(i && i.error("Error parsing " + s + " on " + t.className + ": " + e))
        }
        var a = new e(t, n);
        d && d.data(t, o, a)
      })
    })
  }, n
}),
function(t, e) {
  "function" == typeof define && define.amd ? define("outlayer/item", ["ev-emitter/ev-emitter", "get-size/get-size"], e) : "object" == typeof module && module.exports ? module.exports = e(require("ev-emitter"), require("get-size")) : (t.Outlayer = {}, t.Outlayer.Item = e(t.EvEmitter, t.getSize))
}(window, function(t, e) {
  "use strict";

  function n(t, e) {
    t && (this.element = t, this.layout = e, this.position = {
      x: 0,
      y: 0
    }, this._create())
  }
  var i = document.documentElement.style,
    o = "string" == typeof i.transition ? "transition" : "WebkitTransition",
    r = "string" == typeof i.transform ? "transform" : "WebkitTransform",
    s = {
      WebkitTransition: "webkitTransitionEnd",
      transition: "transitionend"
    } [o],
    a = {
      transform: r,
      transition: o,
      transitionDuration: o + "Duration",
      transitionProperty: o + "Property",
      transitionDelay: o + "Delay"
    },
    l = n.prototype = Object.create(t.prototype);
  l.constructor = n, l._create = function() {
    this._transn = {
      ingProperties: {},
      clean: {},
      onEnd: {}
    }, this.css({
      position: "absolute"
    })
  }, l.handleEvent = function(t) {
    var e = "on" + t.type;
    this[e] && this[e](t)
  }, l.getSize = function() {
    this.size = e(this.element)
  }, l.css = function(t) {
    var e = this.element.style;
    for (var n in t) {
      e[a[n] || n] = t[n]
    }
  }, l.getPosition = function() {
    var t = getComputedStyle(this.element),
      e = this.layout._getOption("originLeft"),
      n = this.layout._getOption("originTop"),
      i = t[e ? "left" : "right"],
      o = t[n ? "top" : "bottom"],
      r = this.layout.size,
      s = -1 != i.indexOf("%") ? parseFloat(i) / 100 * r.width : parseInt(i, 10),
      a = -1 != o.indexOf("%") ? parseFloat(o) / 100 * r.height : parseInt(o, 10);
    s = isNaN(s) ? 0 : s, a = isNaN(a) ? 0 : a, s -= e ? r.paddingLeft : r.paddingRight, a -= n ? r.paddingTop : r.paddingBottom, this.position.x = s, this.position.y = a
  }, l.layoutPosition = function() {
    var t = this.layout.size,
      e = {},
      n = this.layout._getOption("originLeft"),
      i = this.layout._getOption("originTop"),
      o = n ? "paddingLeft" : "paddingRight",
      r = n ? "left" : "right",
      s = n ? "right" : "left",
      a = this.position.x + t[o];
    e[r] = this.getXValue(a), e[s] = "";
    var l = i ? "paddingTop" : "paddingBottom",
      u = i ? "top" : "bottom",
      c = i ? "bottom" : "top",
      d = this.position.y + t[l];
    e[u] = this.getYValue(d), e[c] = "", this.css(e), this.emitEvent("layout", [this])
  }, l.getXValue = function(t) {
    var e = this.layout._getOption("horizontal");
    return this.layout.options.percentPosition && !e ? t / this.layout.size.width * 100 + "%" : t + "px"
  }, l.getYValue = function(t) {
    var e = this.layout._getOption("horizontal");
    return this.layout.options.percentPosition && e ? t / this.layout.size.height * 100 + "%" : t + "px"
  }, l._transitionTo = function(t, e) {
    this.getPosition();
    var n = this.position.x,
      i = this.position.y,
      o = parseInt(t, 10),
      r = parseInt(e, 10),
      s = o === this.position.x && r === this.position.y;
    if (this.setPosition(t, e), !s || this.isTransitioning) {
      var a = t - n,
        l = e - i,
        u = {};
      u.transform = this.getTranslate(a, l), this.transition({
        to: u,
        onTransitionEnd: {
          transform: this.layoutPosition
        },
        isCleaning: !0
      })
    } else this.layoutPosition()
  }, l.getTranslate = function(t, e) {
    return "translate3d(" + (t = this.layout._getOption("originLeft") ? t : -t) + "px, " + (e = this.layout._getOption("originTop") ? e : -e) + "px, 0)"
  }, l.goTo = function(t, e) {
    this.setPosition(t, e), this.layoutPosition()
  }, l.moveTo = l._transitionTo, l.setPosition = function(t, e) {
    this.position.x = parseInt(t, 10), this.position.y = parseInt(e, 10)
  }, l._nonTransition = function(t) {
    for (var e in this.css(t.to), t.isCleaning && this._removeStyles(t.to), t.onTransitionEnd) t.onTransitionEnd[e].call(this)
  }, l.transition = function(t) {
    if (parseFloat(this.layout.options.transitionDuration)) {
      var e = this._transn;
      for (var n in t.onTransitionEnd) e.onEnd[n] = t.onTransitionEnd[n];
      for (n in t.to) e.ingProperties[n] = !0, t.isCleaning && (e.clean[n] = !0);
      if (t.from) {
        this.css(t.from);
        this.element.offsetHeight;
        null
      }
      this.enableTransition(t.to), this.css(t.to), this.isTransitioning = !0
    } else this._nonTransition(t)
  };
  var u = "opacity," + function(t) {
    return t.replace(/([A-Z])/g, function(t) {
      return "-" + t.toLowerCase()
    })
  }(r);
  l.enableTransition = function() {
    if (!this.isTransitioning) {
      var t = this.layout.options.transitionDuration;
      t = "number" == typeof t ? t + "ms" : t, this.css({
        transitionProperty: u,
        transitionDuration: t,
        transitionDelay: this.staggerDelay || 0
      }), this.element.addEventListener(s, this, !1)
    }
  }, l.onwebkitTransitionEnd = function(t) {
    this.ontransitionend(t)
  }, l.onotransitionend = function(t) {
    this.ontransitionend(t)
  };
  var c = {
    "-webkit-transform": "transform"
  };
  l.ontransitionend = function(t) {
    if (t.target === this.element) {
      var e = this._transn,
        n = c[t.propertyName] || t.propertyName;
      if (delete e.ingProperties[n], function(t) {
          for (var e in t) return !1;
          return !0
        }(e.ingProperties) && this.disableTransition(), n in e.clean && (this.element.style[t.propertyName] = "", delete e.clean[n]), n in e.onEnd) e.onEnd[n].call(this), delete e.onEnd[n];
      this.emitEvent("transitionEnd", [this])
    }
  }, l.disableTransition = function() {
    this.removeTransitionStyles(), this.element.removeEventListener(s, this, !1), this.isTransitioning = !1
  }, l._removeStyles = function(t) {
    var e = {};
    for (var n in t) e[n] = "";
    this.css(e)
  };
  var d = {
    transitionProperty: "",
    transitionDuration: "",
    transitionDelay: ""
  };
  return l.removeTransitionStyles = function() {
    this.css(d)
  }, l.stagger = function(t) {
    t = isNaN(t) ? 0 : t, this.staggerDelay = t + "ms"
  }, l.removeElem = function() {
    this.element.parentNode.removeChild(this.element), this.css({
      display: ""
    }), this.emitEvent("remove", [this])
  }, l.remove = function() {
    return o && parseFloat(this.layout.options.transitionDuration) ? (this.once("transitionEnd", function() {
      this.removeElem()
    }), void this.hide()) : void this.removeElem()
  }, l.reveal = function() {
    delete this.isHidden, this.css({
      display: ""
    });
    var t = this.layout.options,
      e = {};
    e[this.getHideRevealTransitionEndProperty("visibleStyle")] = this.onRevealTransitionEnd, this.transition({
      from: t.hiddenStyle,
      to: t.visibleStyle,
      isCleaning: !0,
      onTransitionEnd: e
    })
  }, l.onRevealTransitionEnd = function() {
    this.isHidden || this.emitEvent("reveal")
  }, l.getHideRevealTransitionEndProperty = function(t) {
    var e = this.layout.options[t];
    if (e.opacity) return "opacity";
    for (var n in e) return n
  }, l.hide = function() {
    this.isHidden = !0, this.css({
      display: ""
    });
    var t = this.layout.options,
      e = {};
    e[this.getHideRevealTransitionEndProperty("hiddenStyle")] = this.onHideTransitionEnd, this.transition({
      from: t.visibleStyle,
      to: t.hiddenStyle,
      isCleaning: !0,
      onTransitionEnd: e
    })
  }, l.onHideTransitionEnd = function() {
    this.isHidden && (this.css({
      display: "none"
    }), this.emitEvent("hide"))
  }, l.destroy = function() {
    this.css({
      position: "",
      left: "",
      right: "",
      top: "",
      bottom: "",
      transition: "",
      transform: ""
    })
  }, n
}),
function(t, e) {
  "use strict";
  "function" == typeof define && define.amd ? define("outlayer/outlayer", ["ev-emitter/ev-emitter", "get-size/get-size", "fizzy-ui-utils/utils", "./item"], function(n, i, o, r) {
    return e(t, n, i, o, r)
  }) : "object" == typeof module && module.exports ? module.exports = e(t, require("ev-emitter"), require("get-size"), require("fizzy-ui-utils"), require("./item")) : t.Outlayer = e(t, t.EvEmitter, t.getSize, t.fizzyUIUtils, t.Outlayer.Item)
}(window, function(t, e, n, i, o) {
  "use strict";

  function r(t, e) {
    var n = i.getQueryElement(t);
    if (n) {
      this.element = n, l && (this.$element = l(this.element)), this.options = i.extend({}, this.constructor.defaults), this.option(e);
      var o = ++c;
      this.element.outlayerGUID = o, d[o] = this, this._create(), this._getOption("initLayout") && this.layout()
    } else a && a.error("Bad element for " + this.constructor.namespace + ": " + (n || t))
  }

  function s(t) {
    function e() {
      t.apply(this, arguments)
    }
    return e.prototype = Object.create(t.prototype), e.prototype.constructor = e, e
  }
  var a = t.console,
    l = t.jQuery,
    u = function() {},
    c = 0,
    d = {};
  r.namespace = "outlayer", r.Item = o, r.defaults = {
    containerStyle: {
      position: "relative"
    },
    initLayout: !0,
    originLeft: !0,
    originTop: !0,
    resize: !0,
    resizeContainer: !0,
    transitionDuration: "0.4s",
    hiddenStyle: {
      opacity: 0,
      transform: "scale(0.001)"
    },
    visibleStyle: {
      opacity: 1,
      transform: "scale(1)"
    }
  };
  var f = r.prototype;
  i.extend(f, e.prototype), f.option = function(t) {
    i.extend(this.options, t)
  }, f._getOption = function(t) {
    var e = this.constructor.compatOptions[t];
    return e && void 0 !== this.options[e] ? this.options[e] : this.options[t]
  }, r.compatOptions = {
    initLayout: "isInitLayout",
    horizontal: "isHorizontal",
    layoutInstant: "isLayoutInstant",
    originLeft: "isOriginLeft",
    originTop: "isOriginTop",
    resize: "isResizeBound",
    resizeContainer: "isResizingContainer"
  }, f._create = function() {
    this.reloadItems(), this.stamps = [], this.stamp(this.options.stamp), i.extend(this.element.style, this.options.containerStyle), this._getOption("resize") && this.bindResize()
  }, f.reloadItems = function() {
    this.items = this._itemize(this.element.children)
  }, f._itemize = function(t) {
    for (var e = this._filterFindItemElements(t), n = this.constructor.Item, i = [], o = 0; o < e.length; o++) {
      var r = new n(e[o], this);
      i.push(r)
    }
    return i
  }, f._filterFindItemElements = function(t) {
    return i.filterFindElements(t, this.options.itemSelector)
  }, f.getItemElements = function() {
    return this.items.map(function(t) {
      return t.element
    })
  }, f.layout = function() {
    this._resetLayout(), this._manageStamps();
    var t = this._getOption("layoutInstant"),
      e = void 0 !== t ? t : !this._isLayoutInited;
    this.layoutItems(this.items, e), this._isLayoutInited = !0
  }, f._init = f.layout, f._resetLayout = function() {
    this.getSize()
  }, f.getSize = function() {
    this.size = n(this.element)
  }, f._getMeasurement = function(t, e) {
    var i, o = this.options[t];
    o ? ("string" == typeof o ? i = this.element.querySelector(o) : o instanceof HTMLElement && (i = o), this[t] = i ? n(i)[e] : o) : this[t] = 0
  }, f.layoutItems = function(t, e) {
    t = this._getItemsForLayout(t), this._layoutItems(t, e), this._postLayout()
  }, f._getItemsForLayout = function(t) {
    return t.filter(function(t) {
      return !t.isIgnored
    })
  }, f._layoutItems = function(t, e) {
    if (this._emitCompleteOnItems("layout", t), t && t.length) {
      var n = [];
      t.forEach(function(t) {
        var i = this._getItemLayoutPosition(t);
        i.item = t, i.isInstant = e || t.isLayoutInstant, n.push(i)
      }, this), this._processLayoutQueue(n)
    }
  }, f._getItemLayoutPosition = function() {
    return {
      x: 0,
      y: 0
    }
  }, f._processLayoutQueue = function(t) {
    this.updateStagger(), t.forEach(function(t, e) {
      this._positionItem(t.item, t.x, t.y, t.isInstant, e)
    }, this)
  }, f.updateStagger = function() {
    var t = this.options.stagger;
    return null == t ? void(this.stagger = 0) : (this.stagger = function(t) {
      if ("number" == typeof t) return t;
      var e = t.match(/(^\d*\.?\d*)(\w*)/),
        n = e && e[1],
        i = e && e[2];
      return n.length ? (n = parseFloat(n)) * (h[i] || 1) : 0
    }(t), this.stagger)
  }, f._positionItem = function(t, e, n, i, o) {
    i ? t.goTo(e, n) : (t.stagger(o * this.stagger), t.moveTo(e, n))
  }, f._postLayout = function() {
    this.resizeContainer()
  }, f.resizeContainer = function() {
    if (this._getOption("resizeContainer")) {
      var t = this._getContainerSize();
      t && (this._setContainerMeasure(t.width, !0), this._setContainerMeasure(t.height, !1))
    }
  }, f._getContainerSize = u, f._setContainerMeasure = function(t, e) {
    if (void 0 !== t) {
      var n = this.size;
      n.isBorderBox && (t += e ? n.paddingLeft + n.paddingRight + n.borderLeftWidth + n.borderRightWidth : n.paddingBottom + n.paddingTop + n.borderTopWidth + n.borderBottomWidth), t = Math.max(t, 0), this.element.style[e ? "width" : "height"] = t + "px"
    }
  }, f._emitCompleteOnItems = function(t, e) {
    function n() {
      o.dispatchEvent(t + "Complete", null, [e])
    }

    function i() {
      ++s == r && n()
    }
    var o = this,
      r = e.length;
    if (e && r) {
      var s = 0;
      e.forEach(function(e) {
        e.once(t, i)
      })
    } else n()
  }, f.dispatchEvent = function(t, e, n) {
    var i = e ? [e].concat(n) : n;
    if (this.emitEvent(t, i), l)
      if (this.$element = this.$element || l(this.element), e) {
        var o = l.Event(e);
        o.type = t, this.$element.trigger(o, n)
      } else this.$element.trigger(t, n)
  }, f.ignore = function(t) {
    var e = this.getItem(t);
    e && (e.isIgnored = !0)
  }, f.unignore = function(t) {
    var e = this.getItem(t);
    e && delete e.isIgnored
  }, f.stamp = function(t) {
    (t = this._find(t)) && (this.stamps = this.stamps.concat(t), t.forEach(this.ignore, this))
  }, f.unstamp = function(t) {
    (t = this._find(t)) && t.forEach(function(t) {
      i.removeFrom(this.stamps, t), this.unignore(t)
    }, this)
  }, f._find = function(t) {
    if (t) return "string" == typeof t && (t = this.element.querySelectorAll(t)), i.makeArray(t)
  }, f._manageStamps = function() {
    this.stamps && this.stamps.length && (this._getBoundingRect(), this.stamps.forEach(this._manageStamp, this))
  }, f._getBoundingRect = function() {
    var t = this.element.getBoundingClientRect(),
      e = this.size;
    this._boundingRect = {
      left: t.left + e.paddingLeft + e.borderLeftWidth,
      top: t.top + e.paddingTop + e.borderTopWidth,
      right: t.right - (e.paddingRight + e.borderRightWidth),
      bottom: t.bottom - (e.paddingBottom + e.borderBottomWidth)
    }
  }, f._manageStamp = u, f._getElementOffset = function(t) {
    var e = t.getBoundingClientRect(),
      i = this._boundingRect,
      o = n(t);
    return {
      left: e.left - i.left - o.marginLeft,
      top: e.top - i.top - o.marginTop,
      right: i.right - e.right - o.marginRight,
      bottom: i.bottom - e.bottom - o.marginBottom
    }
  }, f.handleEvent = i.handleEvent, f.bindResize = function() {
    t.addEventListener("resize", this), this.isResizeBound = !0
  }, f.unbindResize = function() {
    t.removeEventListener("resize", this), this.isResizeBound = !1
  }, f.onresize = function() {
    this.resize()
  }, i.debounceMethod(r, "onresize", 100), f.resize = function() {
    this.isResizeBound && this.needsResizeLayout() && this.layout()
  }, f.needsResizeLayout = function() {
    var t = n(this.element);
    return this.size && t && t.innerWidth !== this.size.innerWidth
  }, f.addItems = function(t) {
    var e = this._itemize(t);
    return e.length && (this.items = this.items.concat(e)), e
  }, f.appended = function(t) {
    var e = this.addItems(t);
    e.length && (this.layoutItems(e, !0), this.reveal(e))
  }, f.prepended = function(t) {
    var e = this._itemize(t);
    if (e.length) {
      var n = this.items.slice(0);
      this.items = e.concat(n), this._resetLayout(), this._manageStamps(), this.layoutItems(e, !0), this.reveal(e), this.layoutItems(n)
    }
  }, f.reveal = function(t) {
    if (this._emitCompleteOnItems("reveal", t), t && t.length) {
      var e = this.updateStagger();
      t.forEach(function(t, n) {
        t.stagger(n * e), t.reveal()
      })
    }
  }, f.hide = function(t) {
    if (this._emitCompleteOnItems("hide", t), t && t.length) {
      var e = this.updateStagger();
      t.forEach(function(t, n) {
        t.stagger(n * e), t.hide()
      })
    }
  }, f.revealItemElements = function(t) {
    var e = this.getItems(t);
    this.reveal(e)
  }, f.hideItemElements = function(t) {
    var e = this.getItems(t);
    this.hide(e)
  }, f.getItem = function(t) {
    for (var e = 0; e < this.items.length; e++) {
      var n = this.items[e];
      if (n.element == t) return n
    }
  }, f.getItems = function(t) {
    t = i.makeArray(t);
    var e = [];
    return t.forEach(function(t) {
      var n = this.getItem(t);
      n && e.push(n)
    }, this), e
  }, f.remove = function(t) {
    var e = this.getItems(t);
    this._emitCompleteOnItems("remove", e), e && e.length && e.forEach(function(t) {
      t.remove(), i.removeFrom(this.items, t)
    }, this)
  }, f.destroy = function() {
    var t = this.element.style;
    t.height = "", t.position = "", t.width = "", this.items.forEach(function(t) {
      t.destroy()
    }), this.unbindResize();
    var e = this.element.outlayerGUID;
    delete d[e], delete this.element.outlayerGUID, l && l.removeData(this.element, this.constructor.namespace)
  }, r.data = function(t) {
    var e = (t = i.getQueryElement(t)) && t.outlayerGUID;
    return e && d[e]
  }, r.create = function(t, e) {
    var n = s(r);
    return n.defaults = i.extend({}, r.defaults), i.extend(n.defaults, e), n.compatOptions = i.extend({}, r.compatOptions), n.namespace = t, n.data = r.data, n.Item = s(o), i.htmlInit(n, t), l && l.bridget && l.bridget(t, n), n
  };
  var h = {
    ms: 1,
    s: 1e3
  };
  return r.Item = o, r
}),
function(t, e) {
  "function" == typeof define && define.amd ? define("isotope/js/item", ["outlayer/outlayer"], e) : "object" == typeof module && module.exports ? module.exports = e(require("outlayer")) : (t.Isotope = t.Isotope || {}, t.Isotope.Item = e(t.Outlayer))
}(window, function(t) {
  "use strict";

  function e() {
    t.Item.apply(this, arguments)
  }
  var n = e.prototype = Object.create(t.Item.prototype),
    i = n._create;
  n._create = function() {
    this.id = this.layout.itemGUID++, i.call(this), this.sortData = {}
  }, n.updateSortData = function() {
    if (!this.isIgnored) {
      this.sortData.id = this.id, this.sortData["original-order"] = this.id, this.sortData.random = Math.random();
      var t = this.layout.options.getSortData,
        e = this.layout._sorters;
      for (var n in t) {
        var i = e[n];
        this.sortData[n] = i(this.element, this)
      }
    }
  };
  var o = n.destroy;
  return n.destroy = function() {
    o.apply(this, arguments), this.css({
      display: ""
    })
  }, e
}),
function(t, e) {
  "function" == typeof define && define.amd ? define("isotope/js/layout-mode", ["get-size/get-size", "outlayer/outlayer"], e) : "object" == typeof module && module.exports ? module.exports = e(require("get-size"), require("outlayer")) : (t.Isotope = t.Isotope || {}, t.Isotope.LayoutMode = e(t.getSize, t.Outlayer))
}(window, function(t, e) {
  "use strict";

  function n(t) {
    this.isotope = t, t && (this.options = t.options[this.namespace], this.element = t.element, this.items = t.filteredItems, this.size = t.size)
  }
  var i = n.prototype;
  return ["_resetLayout", "_getItemLayoutPosition", "_manageStamp", "_getContainerSize", "_getElementOffset", "needsResizeLayout", "_getOption"].forEach(function(t) {
    i[t] = function() {
      return e.prototype[t].apply(this.isotope, arguments)
    }
  }), i.needsVerticalResizeLayout = function() {
    var e = t(this.isotope.element);
    return this.isotope.size && e && e.innerHeight != this.isotope.size.innerHeight
  }, i._getMeasurement = function() {
    this.isotope._getMeasurement.apply(this, arguments)
  }, i.getColumnWidth = function() {
    this.getSegmentSize("column", "Width")
  }, i.getRowHeight = function() {
    this.getSegmentSize("row", "Height")
  }, i.getSegmentSize = function(t, e) {
    var n = t + e,
      i = "outer" + e;
    if (this._getMeasurement(n, i), !this[n]) {
      var o = this.getFirstItemSize();
      this[n] = o && o[i] || this.isotope.size["inner" + e]
    }
  }, i.getFirstItemSize = function() {
    var e = this.isotope.filteredItems[0];
    return e && e.element && t(e.element)
  }, i.layout = function() {
    this.isotope.layout.apply(this.isotope, arguments)
  }, i.getSize = function() {
    this.isotope.getSize(), this.size = this.isotope.size
  }, n.modes = {}, n.create = function(t, e) {
    function o() {
      n.apply(this, arguments)
    }
    return o.prototype = Object.create(i), o.prototype.constructor = o, e && (o.options = e), o.prototype.namespace = t, n.modes[t] = o, o
  }, n
}),
function(t, e) {
  "function" == typeof define && define.amd ? define("masonry/masonry", ["outlayer/outlayer", "get-size/get-size"], e) : "object" == typeof module && module.exports ? module.exports = e(require("outlayer"), require("get-size")) : t.Masonry = e(t.Outlayer, t.getSize)
}(window, function(t, e) {
  var n = t.create("masonry");
  return n.compatOptions.fitWidth = "isFitWidth", n.prototype._resetLayout = function() {
    this.getSize(), this._getMeasurement("columnWidth", "outerWidth"), this._getMeasurement("gutter", "outerWidth"), this.measureColumns(), this.colYs = [];
    for (var t = 0; t < this.cols; t++) this.colYs.push(0);
    this.maxY = 0
  }, n.prototype.measureColumns = function() {
    if (this.getContainerWidth(), !this.columnWidth) {
      var t = this.items[0],
        n = t && t.element;
      this.columnWidth = n && e(n).outerWidth || this.containerWidth
    }
    var i = this.columnWidth += this.gutter,
      o = this.containerWidth + this.gutter,
      r = o / i,
      s = i - o % i;
    r = Math[s && s < 1 ? "round" : "floor"](r), this.cols = Math.max(r, 1)
  }, n.prototype.getContainerWidth = function() {
    var t = this._getOption("fitWidth") ? this.element.parentNode : this.element,
      n = e(t);
    this.containerWidth = n && n.innerWidth
  }, n.prototype._getItemLayoutPosition = function(t) {
    t.getSize();
    var e = t.size.outerWidth % this.columnWidth,
      n = Math[e && e < 1 ? "round" : "ceil"](t.size.outerWidth / this.columnWidth);
    n = Math.min(n, this.cols);
    for (var i = this._getColGroup(n), o = Math.min.apply(Math, i), r = i.indexOf(o), s = {
        x: this.columnWidth * r,
        y: o
      }, a = o + t.size.outerHeight, l = this.cols + 1 - i.length, u = 0; u < l; u++) this.colYs[r + u] = a;
    return s
  }, n.prototype._getColGroup = function(t) {
    if (t < 2) return this.colYs;
    for (var e = [], n = this.cols + 1 - t, i = 0; i < n; i++) {
      var o = this.colYs.slice(i, i + t);
      e[i] = Math.max.apply(Math, o)
    }
    return e
  }, n.prototype._manageStamp = function(t) {
    var n = e(t),
      i = this._getElementOffset(t),
      o = this._getOption("originLeft") ? i.left : i.right,
      r = o + n.outerWidth,
      s = Math.floor(o / this.columnWidth);
    s = Math.max(0, s);
    var a = Math.floor(r / this.columnWidth);
    a -= r % this.columnWidth ? 0 : 1, a = Math.min(this.cols - 1, a);
    for (var l = (this._getOption("originTop") ? i.top : i.bottom) + n.outerHeight, u = s; u <= a; u++) this.colYs[u] = Math.max(l, this.colYs[u])
  }, n.prototype._getContainerSize = function() {
    this.maxY = Math.max.apply(Math, this.colYs);
    var t = {
      height: this.maxY
    };
    return this._getOption("fitWidth") && (t.width = this._getContainerFitWidth()), t
  }, n.prototype._getContainerFitWidth = function() {
    for (var t = 0, e = this.cols; --e && 0 === this.colYs[e];) t++;
    return (this.cols - t) * this.columnWidth - this.gutter
  }, n.prototype.needsResizeLayout = function() {
    var t = this.containerWidth;
    return this.getContainerWidth(), t != this.containerWidth
  }, n
}),
function(t, e) {
  "function" == typeof define && define.amd ? define("isotope/js/layout-modes/masonry", ["../layout-mode", "masonry/masonry"], e) : "object" == typeof module && module.exports ? module.exports = e(require("../layout-mode"), require("masonry-layout")) : e(t.Isotope.LayoutMode, t.Masonry)
}(window, function(t, e) {
  "use strict";
  var n = t.create("masonry"),
    i = n.prototype,
    o = {
      _getElementOffset: !0,
      layout: !0,
      _getMeasurement: !0
    };
  for (var r in e.prototype) o[r] || (i[r] = e.prototype[r]);
  var s = i.measureColumns;
  i.measureColumns = function() {
    this.items = this.isotope.filteredItems, s.call(this)
  };
  var a = i._getOption;
  return i._getOption = function(t) {
    return "fitWidth" == t ? void 0 !== this.options.isFitWidth ? this.options.isFitWidth : this.options.fitWidth : a.apply(this.isotope, arguments)
  }, n
}),
function(t, e) {
  "function" == typeof define && define.amd ? define("isotope/js/layout-modes/fit-rows", ["../layout-mode"], e) : "object" == typeof exports ? module.exports = e(require("../layout-mode")) : e(t.Isotope.LayoutMode)
}(window, function(t) {
  "use strict";
  var e = t.create("fitRows"),
    n = e.prototype;
  return n._resetLayout = function() {
    this.x = 0, this.y = 0, this.maxY = 0, this._getMeasurement("gutter", "outerWidth")
  }, n._getItemLayoutPosition = function(t) {
    t.getSize();
    var e = t.size.outerWidth + this.gutter,
      n = this.isotope.size.innerWidth + this.gutter;
    0 !== this.x && e + this.x > n && (this.x = 0, this.y = this.maxY);
    var i = {
      x: this.x,
      y: this.y
    };
    return this.maxY = Math.max(this.maxY, this.y + t.size.outerHeight), this.x += e, i
  }, n._getContainerSize = function() {
    return {
      height: this.maxY
    }
  }, e
}),
function(t, e) {
  "function" == typeof define && define.amd ? define("isotope/js/layout-modes/vertical", ["../layout-mode"], e) : "object" == typeof module && module.exports ? module.exports = e(require("../layout-mode")) : e(t.Isotope.LayoutMode)
}(window, function(t) {
  "use strict";
  var e = t.create("vertical", {
      horizontalAlignment: 0
    }),
    n = e.prototype;
  return n._resetLayout = function() {
    this.y = 0
  }, n._getItemLayoutPosition = function(t) {
    t.getSize();
    var e = (this.isotope.size.innerWidth - t.size.outerWidth) * this.options.horizontalAlignment,
      n = this.y;
    return this.y += t.size.outerHeight, {
      x: e,
      y: n
    }
  }, n._getContainerSize = function() {
    return {
      height: this.y
    }
  }, e
}),
function(t, e) {
  "function" == typeof define && define.amd ? define(["outlayer/outlayer", "get-size/get-size", "desandro-matches-selector/matches-selector", "fizzy-ui-utils/utils", "isotope/js/item", "isotope/js/layout-mode", "isotope/js/layout-modes/masonry", "isotope/js/layout-modes/fit-rows", "isotope/js/layout-modes/vertical"], function(n, i, o, r, s, a) {
    return e(t, n, i, o, r, s, a)
  }) : "object" == typeof module && module.exports ? module.exports = e(t, require("outlayer"), require("get-size"), require("desandro-matches-selector"), require("fizzy-ui-utils"), require("isotope/js/item"), require("isotope/js/layout-mode"), require("isotope/js/layout-modes/masonry"), require("isotope/js/layout-modes/fit-rows"), require("isotope/js/layout-modes/vertical")) : t.Isotope = e(t, t.Outlayer, t.getSize, t.matchesSelector, t.fizzyUIUtils, t.Isotope.Item, t.Isotope.LayoutMode)
}(window, function(t, e, n, i, o, r, s) {
  var a = t.jQuery,
    l = String.prototype.trim ? function(t) {
      return t.trim()
    } : function(t) {
      return t.replace(/^\s+|\s+$/g, "")
    },
    u = e.create("isotope", {
      layoutMode: "masonry",
      isJQueryFiltering: !0,
      sortAscending: !0
    });
  u.Item = r, u.LayoutMode = s;
  var c = u.prototype;
  c._create = function() {
    for (var t in this.itemGUID = 0, this._sorters = {}, this._getSorters(), e.prototype._create.call(this), this.modes = {}, this.filteredItems = this.items, this.sortHistory = ["original-order"], s.modes) this._initLayoutMode(t)
  }, c.reloadItems = function() {
    this.itemGUID = 0, e.prototype.reloadItems.call(this)
  }, c._itemize = function() {
    for (var t = e.prototype._itemize.apply(this, arguments), n = 0; n < t.length; n++) {
      t[n].id = this.itemGUID++
    }
    return this._updateItemsSortData(t), t
  }, c._initLayoutMode = function(t) {
    var e = s.modes[t],
      n = this.options[t] || {};
    this.options[t] = e.options ? o.extend(e.options, n) : n, this.modes[t] = new e(this)
  }, c.layout = function() {
    return !this._isLayoutInited && this._getOption("initLayout") ? void this.arrange() : void this._layout()
  }, c._layout = function() {
    var t = this._getIsInstant();
    this._resetLayout(), this._manageStamps(), this.layoutItems(this.filteredItems, t), this._isLayoutInited = !0
  }, c.arrange = function(t) {
    this.option(t), this._getIsInstant();
    var e = this._filter(this.items);
    this.filteredItems = e.matches, this._bindArrangeComplete(), this._isInstant ? this._noTransition(this._hideReveal, [e]) : this._hideReveal(e), this._sort(), this._layout()
  }, c._init = c.arrange, c._hideReveal = function(t) {
    this.reveal(t.needReveal), this.hide(t.needHide)
  }, c._getIsInstant = function() {
    var t = this._getOption("layoutInstant"),
      e = void 0 !== t ? t : !this._isLayoutInited;
    return this._isInstant = e, e
  }, c._bindArrangeComplete = function() {
    function t() {
      e && n && i && o.dispatchEvent("arrangeComplete", null, [o.filteredItems])
    }
    var e, n, i, o = this;
    this.once("layoutComplete", function() {
      e = !0, t()
    }), this.once("hideComplete", function() {
      n = !0, t()
    }), this.once("revealComplete", function() {
      i = !0, t()
    })
  }, c._filter = function(t) {
    var e = this.options.filter;
    e = e || "*";
    for (var n = [], i = [], o = [], r = this._getFilterTest(e), s = 0; s < t.length; s++) {
      var a = t[s];
      if (!a.isIgnored) {
        var l = r(a);
        l && n.push(a), l && a.isHidden ? i.push(a) : l || a.isHidden || o.push(a)
      }
    }
    return {
      matches: n,
      needReveal: i,
      needHide: o
    }
  }, c._getFilterTest = function(t) {
    return a && this.options.isJQueryFiltering ? function(e) {
      return a(e.element).is(t)
    } : "function" == typeof t ? function(e) {
      return t(e.element)
    } : function(e) {
      return i(e.element, t)
    }
  }, c.updateSortData = function(t) {
    var e;
    t ? (t = o.makeArray(t), e = this.getItems(t)) : e = this.items, this._getSorters(), this._updateItemsSortData(e)
  }, c._getSorters = function() {
    var t = this.options.getSortData;
    for (var e in t) {
      var n = t[e];
      this._sorters[e] = d(n)
    }
  }, c._updateItemsSortData = function(t) {
    for (var e = t && t.length, n = 0; e && n < e; n++) {
      t[n].updateSortData()
    }
  };
  var d = function() {
    return function(t) {
      if ("string" != typeof t) return t;
      var e = l(t).split(" "),
        n = e[0],
        i = n.match(/^\[(.+)\]$/),
        o = function(t, e) {
          return t ? function(e) {
            return e.getAttribute(t)
          } : function(t) {
            var n = t.querySelector(e);
            return n && n.textContent
          }
        }(i && i[1], n),
        r = u.sortDataParsers[e[1]];
      return r ? function(t) {
        return t && r(o(t))
      } : function(t) {
        return t && o(t)
      }
    }
  }();
  u.sortDataParsers = {
    parseInt: function(t) {
      return parseInt(t, 10)
    },
    parseFloat: function(t) {
      return parseFloat(t)
    }
  }, c._sort = function() {
    if (this.options.sortBy) {
      var t = o.makeArray(this.options.sortBy);
      this._getIsSameSortBy(t) || (this.sortHistory = t.concat(this.sortHistory));
      var e = function(t, e) {
        return function(n, i) {
          for (var o = 0; o < t.length; o++) {
            var r = t[o],
              s = n.sortData[r],
              a = i.sortData[r];
            if (s > a || s < a) return (s > a ? 1 : -1) * ((void 0 !== e[r] ? e[r] : e) ? 1 : -1)
          }
          return 0
        }
      }(this.sortHistory, this.options.sortAscending);
      this.filteredItems.sort(e)
    }
  }, c._getIsSameSortBy = function(t) {
    for (var e = 0; e < t.length; e++)
      if (t[e] != this.sortHistory[e]) return !1;
    return !0
  }, c._mode = function() {
    var t = this.options.layoutMode,
      e = this.modes[t];
    if (!e) throw new Error("No layout mode: " + t);
    return e.options = this.options[t], e
  }, c._resetLayout = function() {
    e.prototype._resetLayout.call(this), this._mode()._resetLayout()
  }, c._getItemLayoutPosition = function(t) {
    return this._mode()._getItemLayoutPosition(t)
  }, c._manageStamp = function(t) {
    this._mode()._manageStamp(t)
  }, c._getContainerSize = function() {
    return this._mode()._getContainerSize()
  }, c.needsResizeLayout = function() {
    return this._mode().needsResizeLayout()
  }, c.appended = function(t) {
    var e = this.addItems(t);
    if (e.length) {
      var n = this._filterRevealAdded(e);
      this.filteredItems = this.filteredItems.concat(n)
    }
  }, c.prepended = function(t) {
    var e = this._itemize(t);
    if (e.length) {
      this._resetLayout(), this._manageStamps();
      var n = this._filterRevealAdded(e);
      this.layoutItems(this.filteredItems), this.filteredItems = n.concat(this.filteredItems), this.items = e.concat(this.items)
    }
  }, c._filterRevealAdded = function(t) {
    var e = this._filter(t);
    return this.hide(e.needHide), this.reveal(e.matches), this.layoutItems(e.matches, !0), e.matches
  }, c.insert = function(t) {
    var e = this.addItems(t);
    if (e.length) {
      var n, i, o = e.length;
      for (n = 0; n < o; n++) i = e[n], this.element.appendChild(i.element);
      var r = this._filter(e).matches;
      for (n = 0; n < o; n++) e[n].isLayoutInstant = !0;
      for (this.arrange(), n = 0; n < o; n++) delete e[n].isLayoutInstant;
      this.reveal(r)
    }
  };
  var f = c.remove;
  return c.remove = function(t) {
    t = o.makeArray(t);
    var e = this.getItems(t);
    f.call(this, t);
    for (var n = e && e.length, i = 0; n && i < n; i++) {
      var r = e[i];
      o.removeFrom(this.filteredItems, r)
    }
  }, c.shuffle = function() {
    for (var t = 0; t < this.items.length; t++) {
      this.items[t].sortData.random = Math.random()
    }
    this.options.sortBy = "random", this._sort(), this._layout()
  }, c._noTransition = function(t, e) {
    var n = this.options.transitionDuration;
    this.options.transitionDuration = 0;
    var i = t.apply(this, e);
    return this.options.transitionDuration = n, i
  }, c.getFilteredItemElements = function() {
    return this.filteredItems.map(function(t) {
      return t.element
    })
  }, u
}),
function(t, e) {
  "function" == typeof define && define.amd ? define([], e(t)) : "object" == typeof exports ? module.exports = e(t) : t.iziToast = e(t)
}("undefined" != typeof global ? global : this.window || this.global, function(t) {
  "use strict";
  var e = {},
    n = "iziToast",
    i = (document.querySelector("body"), !!/Mobi/.test(navigator.userAgent)),
    o = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor),
    r = "undefined" != typeof InstallTrigger,
    s = "ontouchstart" in document.documentElement,
    a = ["bottomRight", "bottomLeft", "bottomCenter", "topRight", "topLeft", "topCenter", "center"],
    l = 568,
    u = {},
    c = {
      id: "",
      class: "",
      title: "",
      titleColor: "",
      titleSize: "",
      titleLineHeight: "",
      message: "",
      messageColor: "",
      messageSize: "",
      messageLineHeight: "",
      backgroundColor: "",
      color: "",
      icon: "",
      iconText: "",
      iconColor: "",
      image: "",
      imageWidth: 50,
      maxWidth: null,
      zindex: null,
      layout: 1,
      balloon: !1,
      close: !0,
      rtl: !1,
      position: "bottomRight",
      target: "",
      targetFirst: !0,
      timeout: 5e3,
      drag: !0,
      pauseOnHover: !0,
      resetOnHover: !1,
      progressBar: !0,
      progressBarColor: "",
      animateInside: !0,
      buttons: {},
      transitionIn: "fadeInUp",
      transitionOut: "fadeOut",
      transitionInMobile: "fadeInUp",
      transitionOutMobile: "fadeOutDown",
      onOpen: function() {},
      onClose: function() {}
    };
  if ("remove" in Element.prototype || (Element.prototype.remove = function() {
      this.parentNode && this.parentNode.removeChild(this)
    }), "function" != typeof window.CustomEvent) {
    var d = function(t, e) {
      e = e || {
        bubbles: !1,
        cancelable: !1,
        detail: void 0
      };
      var n = document.createEvent("CustomEvent");
      return n.initCustomEvent(t, e.bubbles, e.cancelable, e.detail), n
    };
    d.prototype = window.Event.prototype, window.CustomEvent = d
  }
  var f = function(t, e, n) {
      if ("[object Object]" === Object.prototype.toString.call(t))
        for (var i in t) Object.prototype.hasOwnProperty.call(t, i) && e.call(n, t[i], i, t);
      else if (t)
        for (var o = 0, r = t.length; r > o; o++) e.call(n, t[o], o, t)
    },
    h = function(t, e) {
      var n = {};
      return f(t, function(e, i) {
        n[i] = t[i]
      }), f(e, function(t, i) {
        n[i] = e[i]
      }), n
    },
    p = function(t) {
      var e = document.createDocumentFragment(),
        n = document.createElement("div");
      for (n.innerHTML = t; n.firstChild;) e.appendChild(n.firstChild);
      return e
    },
    m = {
      move: function(t, e, n, i) {
        var s, a = 180;
        t.style.transform = "translateX(" + i + "px)", i > 0 ? .3 > (s = (a - i) / a) && e.hide(h(n, {
          transitionOut: "fadeOutRight",
          transitionOutMobile: "fadeOutRight"
        }), t, "drag") : .3 > (s = (a + i) / a) && e.hide(h(n, {
          transitionOut: "fadeOutLeft",
          transitionOutMobile: "fadeOutLeft"
        }), t, "drag"), t.style.opacity = s, .3 > s && ((o || r) && (t.style.left = i + "px"), t.parentNode.style.opacity = .3, this.stopMoving(t, null))
      },
      startMoving: function(t, e, n, i) {
        i = i || window.event;
        var o = s ? i.touches[0].clientX : i.clientX,
          r = t.style.transform.replace("px)", ""),
          a = o - (r = r.replace("translateX(", ""));
        t.classList.remove(n.transitionIn), t.classList.remove(n.transitionInMobile), t.style.transition = "", s ? document.ontouchmove = function(i) {
          i.preventDefault();
          var o = (i = i || window.event).touches[0].clientX - a;
          m.move(t, e, n, o)
        } : document.onmousemove = function(i) {
          i.preventDefault();
          var o = (i = i || window.event).clientX - a;
          m.move(t, e, n, o)
        }
      },
      stopMoving: function(t, e) {
        s ? document.ontouchmove = function() {} : document.onmousemove = function() {}, t.style.transition = "transform 0.4s ease, opacity 0.4s ease", t.style.opacity = "", t.style.transform = "", window.setTimeout(function() {
          t.style.transition = ""
        }, 400)
      }
    },
    g = function(t, e, i) {
      var o = t.querySelector("." + n + "-progressbar div"),
        r = null,
        s = {
          Paused: !1,
          Reseted: !1,
          Closed: !1
        },
        a = {
          hideEta: null,
          maxHideTime: null,
          currentTime: (new Date).getTime(),
          updateProgress: function() {
            if (s.Paused = !!t.classList.contains(n + "-paused"), s.Reseted = !!t.classList.contains(n + "-reseted"), s.Closed = !!t.classList.contains(n + "-closed"), s.Reseted && (clearTimeout(r), o.style.width = "100%", g(t, e, i), t.classList.remove(n + "-reseted")), s.Closed && (clearTimeout(r), t.classList.remove(n + "-closed")), !s.Paused && !s.Reseted && !s.Closed) {
              a.currentTime = a.currentTime + 10;
              var l = (a.hideEta - a.currentTime) / a.maxHideTime * 100;
              o.style.width = l + "%", (Math.round(l) < 0 || "object" != typeof t) && (clearTimeout(r), i.apply())
            }
          }
        };
      e.timeout > 0 && (a.maxHideTime = parseFloat(e.timeout), a.hideEta = (new Date).getTime() + a.maxHideTime, r = setInterval(a.updateProgress, 10))
    };
  return e.destroy = function() {
    f(document.querySelectorAll("." + n + "-wrapper"), function(t, e) {
      t.remove()
    }), f(document.querySelectorAll("." + n), function(t, e) {
      t.remove()
    }), document.removeEventListener(n + "-open", {}, !1), document.removeEventListener(n + "-close", {}, !1), u = {}
  }, e.settings = function(t) {
    e.destroy(), u = t, c = h(c, t || {})
  }, f({
    info: {
      color: "blue",
      icon: "ico-info"
    },
    success: {
      color: "green",
      icon: "ico-check"
    },
    warning: {
      color: "yellow",
      icon: "ico-warning"
    },
    error: {
      color: "red",
      icon: "ico-error"
    }
  }, function(t, n) {
    e[n] = function(e) {
      var n = h(u, e || {});
      n = h(t, n || {}), this.show(n)
    }
  }), e.hide = function(t, e, o) {
    var r = h(c, t || {});
    o = o || !1, "object" != typeof e && (e = document.querySelector(e)), e.classList.add(n + "-closed"), (r.transitionIn || r.transitionInMobile) && (e.classList.remove(r.transitionIn), e.classList.remove(r.transitionInMobile)), i || window.innerWidth <= l ? r.transitionOutMobile && e.classList.add(r.transitionOutMobile) : r.transitionOut && e.classList.add(r.transitionOut);
    var s = e.parentNode.offsetHeight;
    e.parentNode.style.height = s + "px", e.style.pointerEvents = "none", (!i || window.innerWidth > l) && (e.parentNode.style.transitionDelay = "0.2s"), setTimeout(function() {
      e.parentNode.style.height = "0px", e.parentNode.style.overflow = "", window.setTimeout(function() {
        e.parentNode.remove()
      }, 1e3)
    }, 200);
    try {
      r.closedBy = o;
      var a = new CustomEvent(n + "-close", {
        detail: r,
        bubles: !0,
        cancelable: !0
      });
      document.dispatchEvent(a)
    } catch (t) {
      console.warn(t)
    }
    void 0 !== r.onClose && r.onClose.apply(null, [r, e, o])
  }, e.show = function(t) {
    var e = this,
      o = h(u, t || {});
    o = h(c, o);
    var r = {
      toast: document.createElement("div"),
      toastBody: document.createElement("div"),
      toastCapsule: document.createElement("div"),
      icon: document.createElement("i"),
      cover: document.createElement("div"),
      strong: document.createElement("strong"),
      p: document.createElement("p"),
      progressBar: document.createElement("div"),
      progressBarDiv: document.createElement("div"),
      buttonClose: document.createElement("button"),
      buttons: document.createElement("div"),
      wrapper: null
    };
    r.toastBody.appendChild(r.strong), r.toastBody.appendChild(r.p), r.toast.appendChild(r.toastBody), r.toastCapsule.appendChild(r.toast),
      function() {
        if (r.toast.classList.add(n), r.toastCapsule.classList.add(n + "-capsule"), r.toastBody.classList.add(n + "-body"), i || window.innerWidth <= l ? o.transitionInMobile && r.toast.classList.add(o.transitionInMobile) : o.transitionIn && r.toast.classList.add(o.transitionIn), o.class) {
          var t = o.class.split(" ");
          f(t, function(t, e) {
            r.toast.classList.add(t)
          })
        }
        o.id && (r.toast.id = o.id), o.rtl && r.toast.classList.add(n + "-rtl"), o.layout > 1 && r.toast.classList.add(n + "-layout" + o.layout), o.balloon && r.toast.classList.add(n + "-balloon"), o.maxWidth && (isNaN(o.maxWidth) ? r.toast.style.maxWidth = o.maxWidth : r.toast.style.maxWidth = o.maxWidth + "px"), o.color && (function(t) {
          return "#" == t.substring(0, 1) || "rgb" == t.substring(0, 3) || "hsl" == t.substring(0, 3)
        }(o.color) ? r.toast.style.background = o.color : r.toast.classList.add(n + "-color-" + o.color)), o.backgroundColor && (r.toast.style.background = o.backgroundColor, o.balloon && (r.toast.style.borderColor = o.backgroundColor))
      }(), o.image && (r.cover.classList.add(n + "-cover"), r.cover.style.width = o.imageWidth + "px", r.cover.style.backgroundImage = "url(" + o.image + ")", o.rtl ? r.toastBody.style.marginRight = o.imageWidth + 10 + "px" : r.toastBody.style.marginLeft = o.imageWidth + 10 + "px", r.toast.appendChild(r.cover)), o.close ? (r.buttonClose.classList.add(n + "-close"), r.buttonClose.addEventListener("click", function(t) {
        t.target, e.hide(o, r.toast, "button")
      }), r.toast.appendChild(r.buttonClose)) : o.rtl ? r.toast.style.paddingLeft = "30px" : r.toast.style.paddingRight = "30px", o.progressBar ? (r.progressBar.classList.add(n + "-progressbar"), r.progressBarDiv.style.background = o.progressBarColor, r.progressBar.appendChild(r.progressBarDiv), r.toast.appendChild(r.progressBar), setTimeout(function() {
        g(r.toast, o, function() {
          e.hide(o, r.toast)
        })
      }, 300)) : !1 === o.progressBar && o.timeout > 0 && setTimeout(function() {
        e.hide(o, r.toast)
      }, o.timeout), o.icon && (r.icon.setAttribute("class", n + "-icon " + o.icon), o.iconText && r.icon.appendChild(document.createTextNode(o.iconText)), o.rtl ? r.toastBody.style.paddingRight = "33px" : r.toastBody.style.paddingLeft = "33px", o.iconColor && (r.icon.style.color = o.iconColor), r.toastBody.appendChild(r.icon)), o.titleColor && (r.strong.style.color = o.titleColor), o.titleSize && (isNaN(o.titleSize) ? r.strong.style.fontSize = o.titleSize : r.strong.style.fontSize = o.titleSize + "px"), o.titleLineHeight && (isNaN(o.titleSize) ? r.strong.style.lineHeight = o.titleLineHeight : r.strong.style.lineHeight = o.titleLineHeight + "px"), r.strong.appendChild(p(o.title)), o.messageColor && (r.p.style.color = o.messageColor), o.messageSize && (isNaN(o.titleSize) ? r.p.style.fontSize = o.messageSize : r.p.style.fontSize = o.messageSize + "px"), o.messageLineHeight && (isNaN(o.titleSize) ? r.p.style.lineHeight = o.messageLineHeight : r.p.style.lineHeight = o.messageLineHeight + "px"), r.p.appendChild(p(o.message)),
      function() {
        if (o.buttons.length > 0) {
          r.buttons.classList.add(n + "-buttons"), o.rtl ? r.p.style.marginLeft = "15px" : r.p.style.marginRight = "15px";
          var t = 0;
          f(o.buttons, function(n, i) {
            r.buttons.appendChild(p(n[0])), r.buttons.childNodes[t].addEventListener("click", function(t) {
              return t.preventDefault(), new(0, n[1])(e, r.toast)
            }), t++
          }), r.toastBody.appendChild(r.buttons)
        }
      }(), r.toastCapsule.style.visibility = "hidden", setTimeout(function() {
        var t = r.toast.offsetHeight,
          e = r.toast.currentStyle || window.getComputedStyle(r.toast),
          n = e.marginTop;
        n = n.split("px"), n = parseInt(n[0]);
        var i = e.marginBottom;
        i = i.split("px"), i = parseInt(i[0]), r.toastCapsule.style.visibility = "", r.toastCapsule.style.height = t + i + n + "px", setTimeout(function() {
          r.toastCapsule.style.height = "auto", o.target && (r.toastCapsule.style.overflow = "visible")
        }, 1e3)
      }, 100),
      function() {
        var t = o.position;
        if (o.target) r.wrapper = document.querySelector(o.target), r.wrapper.classList.add(n + "-target"), o.targetFirst ? r.wrapper.insertBefore(r.toastCapsule, r.wrapper.firstChild) : r.wrapper.appendChild(r.toastCapsule);
        else {
          if (-1 == a.indexOf(o.position)) return void console.warn("[" + n + "] Incorrect position.\nIt can be › " + a);
          t = i || window.innerWidth <= l ? "bottomLeft" == o.position || "bottomRight" == o.position || "bottomCenter" == o.position ? n + "-wrapper-bottomCenter" : "topLeft" == o.position || "topRight" == o.position || "topCenter" == o.position ? n + "-wrapper-topCenter" : n + "-wrapper-center" : n + "-wrapper-" + t, r.wrapper = document.querySelector("." + n + "-wrapper." + t), r.wrapper || (r.wrapper = document.createElement("div"), r.wrapper.classList.add(n + "-wrapper"), r.wrapper.classList.add(t), document.body.appendChild(r.wrapper)), "topLeft" == o.position || "topCenter" == o.position || "topRight" == o.position ? r.wrapper.insertBefore(r.toastCapsule, r.wrapper.firstChild) : r.wrapper.appendChild(r.toastCapsule)
        }
        isNaN(o.zindex) ? console.warn("[" + n + "] Invalid zIndex.") : r.wrapper.style.zIndex = o.zindex
      }(),
      function() {
        if (o.animateInside) {
          r.toast.classList.add(n + "-animateInside");
          var t = 200,
            e = 100,
            i = 300;
          if ("bounceInLeft" == o.transitionIn && (t = 400, e = 200, i = 400), window.setTimeout(function() {
              r.strong.classList.add("slideIn")
            }, t), window.setTimeout(function() {
              r.p.classList.add("slideIn")
            }, e), o.icon && window.setTimeout(function() {
              r.icon.classList.add("revealIn")
            }, i), o.buttons.length > 0 && r.buttons) {
            var s = 150;
            f(r.buttons.childNodes, function(t, e) {
              window.setTimeout(function() {
                t.classList.add("revealIn")
              }, s), s += s
            })
          }
        }
      }(), o.onOpen.apply(null, [o, r.toast]);
    try {
      var d = new CustomEvent(n + "-open", {
        detail: o,
        bubles: !0,
        cancelable: !0
      });
      document.dispatchEvent(d)
    } catch (t) {
      console.warn(t)
    }
    o.pauseOnHover && (r.toast.addEventListener("mouseenter", function(t) {
      this.classList.add(n + "-paused")
    }), r.toast.addEventListener("mouseleave", function(t) {
      this.classList.remove(n + "-paused")
    })), o.resetOnHover && (r.toast.addEventListener("mouseenter", function(t) {
      this.classList.add(n + "-reseted")
    }), r.toast.addEventListener("mouseleave", function(t) {
      this.classList.remove(n + "-reseted")
    })), o.drag && (s ? (r.toast.addEventListener("touchstart", function(t) {
      m.startMoving(this, e, o, t)
    }, !1), r.toast.addEventListener("touchend", function(t) {
      m.stopMoving(this, t)
    }, !1)) : (r.toast.addEventListener("mousedown", function(t) {
      t.preventDefault(), m.startMoving(this, e, o, t)
    }, !1), r.toast.addEventListener("mouseup", function(t) {
      t.preventDefault(), m.stopMoving(this, t)
    }, !1)))
  }, e
}),
function(t, e, n) {
  function i(t, e) {
    return typeof t === e
  }

  function o() {
    return "function" != typeof e.createElement ? e.createElement(arguments[0]) : _ ? e.createElementNS.call(e, "http://www.w3.org/2000/svg", arguments[0]) : e.createElement.apply(e, arguments)
  }

  function r(t, e) {
    return !!~("" + t).indexOf(e)
  }

  function s(t) {
    return t.replace(/([a-z])-([a-z])/g, function(t, e, n) {
      return e + n.toUpperCase()
    }).replace(/^-/, "")
  }

  function a(t, n, i, r) {
    var s, a, l, u, c = "modernizr",
      d = o("div"),
      f = function() {
        var t = e.body;
        return t || ((t = o(_ ? "svg" : "body")).fake = !0), t
      }();
    if (parseInt(i, 10))
      for (; i--;)(l = o("div")).id = r ? r[i] : c + (i + 1), d.appendChild(l);
    return (s = o("style")).type = "text/css", s.id = "s" + c, (f.fake ? f : d).appendChild(s), f.appendChild(d), s.styleSheet ? s.styleSheet.cssText = t : s.appendChild(e.createTextNode(t)), d.id = c, f.fake && (f.style.background = "", f.style.overflow = "hidden", u = b.style.overflow, b.style.overflow = "hidden", b.appendChild(f)), a = n(d, t), f.fake ? (f.parentNode.removeChild(f), b.style.overflow = u, b.offsetHeight) : d.parentNode.removeChild(d), !!a
  }

  function l(t, e) {
    return function() {
      return t.apply(e, arguments)
    }
  }

  function u(t) {
    return t.replace(/([A-Z])/g, function(t, e) {
      return "-" + e.toLowerCase()
    }).replace(/^ms-/, "-ms-")
  }

  function c(e, i) {
    var o = e.length;
    if ("CSS" in t && "supports" in t.CSS) {
      for (; o--;)
        if (t.CSS.supports(u(e[o]), i)) return !0;
      return !1
    }
    if ("CSSSupportsRule" in t) {
      for (var r = []; o--;) r.push("(" + u(e[o]) + ":" + i + ")");
      return a("@supports (" + (r = r.join(" or ")) + ") { #modernizr { position: absolute; } }", function(t) {
        return "absolute" == getComputedStyle(t, null).position
      })
    }
    return n
  }

  function d(t, e, a, l) {
    function u() {
      f && (delete I.style, delete I.modElem)
    }
    if (l = !i(l, "undefined") && l, !i(a, "undefined")) {
      var d = c(t, a);
      if (!i(d, "undefined")) return d
    }
    for (var f, h, p, m, g, v = ["modernizr", "tspan", "samp"]; !I.style && v.length;) f = !0, I.modElem = o(v.shift()), I.style = I.modElem.style;
    for (p = t.length, h = 0; p > h; h++)
      if (m = t[h], g = I.style[m], r(m, "-") && (m = s(m)), I.style[m] !== n) {
        if (l || i(a, "undefined")) return u(), "pfx" != e || m;
        try {
          I.style[m] = a
        } catch (t) {}
        if (I.style[m] != g) return u(), "pfx" != e || m
      } return u(), !1
  }

  function f(t, e, n, o, r) {
    var s = t.charAt(0).toUpperCase() + t.slice(1),
      a = (t + " " + T.join(s + " ") + s).split(" ");
    return i(e, "string") || i(e, "undefined") ? d(a, e, o, r) : function(t, e, n) {
      var o;
      for (var r in t)
        if (t[r] in e) return !1 === n ? t[r] : i(o = e[t[r]], "function") ? l(o, n || e) : o;
      return !1
    }(a = (t + " " + x.join(s + " ") + s).split(" "), e, n)
  }

  function h(t, e, i) {
    return f(t, n, n, e, i)
  }
  var p = [],
    m = [],
    g = {
      _version: "3.3.1",
      _config: {
        classPrefix: "",
        enableClasses: !0,
        enableJSClass: !0,
        usePrefixes: !0
      },
      _q: [],
      on: function(t, e) {
        var n = this;
        setTimeout(function() {
          e(n[t])
        }, 0)
      },
      addTest: function(t, e, n) {
        m.push({
          name: t,
          fn: e,
          options: n
        })
      },
      addAsyncTest: function(t) {
        m.push({
          name: null,
          fn: t
        })
      }
    },
    v = function() {};
  v.prototype = g, (v = new v).addTest("svg", !!e.createElementNS && !!e.createElementNS("http://www.w3.org/2000/svg", "svg").createSVGRect);
  var y = g._config.usePrefixes ? " -webkit- -moz- -o- -ms- ".split(" ") : ["", ""];
  g._prefixes = y;
  var b = e.documentElement,
    _ = "svg" === b.nodeName.toLowerCase(),
    w = "Moz O ms Webkit",
    x = g._config.usePrefixes ? w.toLowerCase().split(" ") : [];
  g._domPrefixes = x;
  var C = "CSS" in t && "supports" in t.CSS,
    E = "supportsCSS" in t;
  v.addTest("supports", C || E), v.addTest("placeholder", "placeholder" in o("input") && "placeholder" in o("textarea"));
  var T = g._config.usePrefixes ? w.split(" ") : [];
  g._cssomPrefixes = T;
  var S = g.testStyles = a;
  v.addTest("touchevents", function() {
    var n;
    if ("ontouchstart" in t || t.DocumentTouch && e instanceof DocumentTouch) n = !0;
    else {
      var i = ["@media (", y.join("touch-enabled),("), "heartz", ")", "{#modernizr{top:9px;position:absolute}}"].join("");
      S(i, function(t) {
        n = 9 === t.offsetTop
      })
    }
    return n
  });
  var k = {
    elem: o("modernizr")
  };
  v._q.push(function() {
    delete k.elem
  });
  var I = {
    style: k.elem.style
  };
  v._q.unshift(function() {
      delete I.style
    }), g.testProp = function(t, e, i) {
      return d([t], n, e, i)
    }, g.testAllProps = f, g.testAllProps = h, v.addTest("csstransforms", function() {
      return -1 === navigator.userAgent.indexOf("Android 2.") && h("transform", "scale(1)", !0)
    }), v.addTest("csstransforms3d", function() {
      var t = !!h("perspective", "1px", !0),
        e = v._config.usePrefixes;
      if (t && (!e || "webkitPerspective" in b.style)) {
        var n;
        v.supports ? n = "@supports (perspective: 1px)" : (n = "@media (transform-3d)", e && (n += ",(-webkit-transform-3d)")), S("#modernizr{width:0;height:0}" + (n += "{#modernizr{width:7px;height:18px;margin:0;padding:0;border:0}}"), function(e) {
          t = 7 === e.offsetWidth && 18 === e.offsetHeight
        })
      }
      return t
    }), v.addTest("flexbox", h("flexBasis", "1px", !0)),
    function() {
      var t, e, n, o, r, s;
      for (var a in m)
        if (m.hasOwnProperty(a)) {
          if (t = [], (e = m[a]).name && (t.push(e.name.toLowerCase()), e.options && e.options.aliases && e.options.aliases.length))
            for (n = 0; n < e.options.aliases.length; n++) t.push(e.options.aliases[n].toLowerCase());
          for (o = i(e.fn, "function") ? e.fn() : e.fn, r = 0; r < t.length; r++) 1 === (s = t[r].split(".")).length ? v[s[0]] = o : (!v[s[0]] || v[s[0]] instanceof Boolean || (v[s[0]] = new Boolean(v[s[0]])), v[s[0]][s[1]] = o), p.push((o ? "" : "no-") + s.join("-"))
        }
    }(),
    function(t) {
      var e = b.className,
        n = v._config.classPrefix || "";
      if (_ && (e = e.baseVal), v._config.enableJSClass) {
        var i = new RegExp("(^|\\s)" + n + "no-js(\\s|$)");
        e = e.replace(i, "$1" + n + "js$2")
      }
      v._config.enableClasses && (e += " " + n + t.join(" " + n), _ ? b.className.baseVal = e : b.className = e)
    }(p), delete g.addTest, delete g.addAsyncTest;
  for (var A = 0; A < v._q.length; A++) v._q[A]();
  t.Modernizr = v
}(window, document),
function(t) {
  "function" == typeof define && define.amd ? define([], t) : "object" == typeof exports ? module.exports = t() : window.noUiSlider = t()
}(function() {
  "use strict";

  function t(t) {
    var e = t.getBoundingClientRect(),
      n = t.ownerDocument.documentElement,
      i = a();
    return /webkit.*Chrome.*Mobile/i.test(navigator.userAgent) && (i.x = 0), {
      top: e.top + i.y - n.clientTop,
      left: e.left + i.x - n.clientLeft
    }
  }

  function e(t) {
    return "number" == typeof t && !isNaN(t) && isFinite(t)
  }

  function n(t, e, n) {
    r(t, e), setTimeout(function() {
      s(t, e)
    }, n)
  }

  function i(t) {
    return Math.max(Math.min(t, 100), 0)
  }

  function o(t) {
    return Array.isArray(t) ? t : [t]
  }

  function r(t, e) {
    t.classList ? t.classList.add(e) : t.className += " " + e
  }

  function s(t, e) {
    t.classList ? t.classList.remove(e) : t.className = t.className.replace(new RegExp("(^|\\b)" + e.split(" ").join("|") + "(\\b|$)", "gi"), " ")
  }

  function a() {
    var t = void 0 !== window.pageXOffset,
      e = "CSS1Compat" === (document.compatMode || "");
    return {
      x: t ? window.pageXOffset : e ? document.documentElement.scrollLeft : document.body.scrollLeft,
      y: t ? window.pageYOffset : e ? document.documentElement.scrollTop : document.body.scrollTop
    }
  }

  function l(t, e) {
    return 100 / (e - t)
  }

  function u(t, e) {
    return 100 * e / (t[1] - t[0])
  }

  function c(t, e) {
    for (var n = 1; t >= e[n];) n += 1;
    return n
  }

  function d(t, e, n) {
    if (n >= t.slice(-1)[0]) return 100;
    var i, o, r, s, a = c(n, t);
    return i = t[a - 1], o = t[a], r = e[a - 1], s = e[a], r + function(t, e) {
      return u(t, t[0] < 0 ? e + Math.abs(t[0]) : e - t[0])
    }([i, o], n) / l(r, s)
  }

  function f(t, e, n, i) {
    if (100 === i) return i;
    var o, r, s = c(i, t);
    return n ? i - (o = t[s - 1]) > ((r = t[s]) - o) / 2 ? r : o : e[s - 1] ? t[s - 1] + function(t, e) {
      return Math.round(t / e) * e
    }(i - t[s - 1], e[s - 1]) : i
  }

  function h(t, n, i) {
    var o;
    if ("number" == typeof n && (n = [n]), "[object Array]" !== Object.prototype.toString.call(n)) throw new Error("noUiSlider: 'range' contains invalid value.");
    if (!e(o = "min" === t ? 0 : "max" === t ? 100 : parseFloat(t)) || !e(n[0])) throw new Error("noUiSlider: 'range' value isn't numeric.");
    i.xPct.push(o), i.xVal.push(n[0]), o ? i.xSteps.push(!isNaN(n[1]) && n[1]) : isNaN(n[1]) || (i.xSteps[0] = n[1])
  }

  function p(t, e, n) {
    return !e || void(n.xSteps[t] = u([n.xVal[t], n.xVal[t + 1]], e) / l(n.xPct[t], n.xPct[t + 1]))
  }

  function m(t, e, n, i) {
    this.xPct = [], this.xVal = [], this.xSteps = [i || !1], this.xNumSteps = [!1], this.snap = e, this.direction = n;
    var o, r = [];
    for (o in t) t.hasOwnProperty(o) && r.push([t[o], o]);
    for (r.length && "object" == typeof r[0][0] ? r.sort(function(t, e) {
        return t[0][0] - e[0][0]
      }) : r.sort(function(t, e) {
        return t[0] - e[0]
      }), o = 0; o < r.length; o++) h(r[o][1], r[o][0], this);
    for (this.xNumSteps = this.xSteps.slice(0), o = 0; o < this.xNumSteps.length; o++) p(o, this.xNumSteps[o], this)
  }

  function g(t, n) {
    if (!e(n)) throw new Error("noUiSlider: 'step' is not numeric.");
    t.singleStep = n
  }

  function v(t, e) {
    if ("object" != typeof e || Array.isArray(e)) throw new Error("noUiSlider: 'range' is not an object.");
    if (void 0 === e.min || void 0 === e.max) throw new Error("noUiSlider: Missing 'min' or 'max' in 'range'.");
    if (e.min === e.max) throw new Error("noUiSlider: 'range' 'min' and 'max' cannot be equal.");
    t.spectrum = new m(e, t.snap, t.dir, t.singleStep)
  }

  function y(t, e) {
    if (e = o(e), !Array.isArray(e) || !e.length || e.length > 2) throw new Error("noUiSlider: 'start' option is incorrect.");
    t.handles = e.length, t.start = e
  }

  function b(t, e) {
    if (t.snap = e, "boolean" != typeof e) throw new Error("noUiSlider: 'snap' option must be a boolean.")
  }

  function _(t, e) {
    if (t.animate = e, "boolean" != typeof e) throw new Error("noUiSlider: 'animate' option must be a boolean.")
  }

  function w(t, e) {
    if (t.animationDuration = e, "number" != typeof e) throw new Error("noUiSlider: 'animationDuration' option must be a number.")
  }

  function x(t, e) {
    if ("lower" === e && 1 === t.handles) t.connect = 1;
    else if ("upper" === e && 1 === t.handles) t.connect = 2;
    else if (!0 === e && 2 === t.handles) t.connect = 3;
    else {
      if (!1 !== e) throw new Error("noUiSlider: 'connect' option doesn't match handle count.");
      t.connect = 0
    }
  }

  function C(t, e) {
    switch (e) {
      case "horizontal":
        t.ort = 0;
        break;
      case "vertical":
        t.ort = 1;
        break;
      default:
        throw new Error("noUiSlider: 'orientation' option is invalid.")
    }
  }

  function E(t, n) {
    if (!e(n)) throw new Error("noUiSlider: 'margin' option must be numeric.");
    if (0 !== n && (t.margin = t.spectrum.getMargin(n), !t.margin)) throw new Error("noUiSlider: 'margin' option is only supported on linear sliders.")
  }

  function T(t, n) {
    if (!e(n)) throw new Error("noUiSlider: 'limit' option must be numeric.");
    if (t.limit = t.spectrum.getMargin(n), !t.limit) throw new Error("noUiSlider: 'limit' option is only supported on linear sliders.")
  }

  function S(t, e) {
    switch (e) {
      case "ltr":
        t.dir = 0;
        break;
      case "rtl":
        t.dir = 1, t.connect = [0, 2, 1, 3][t.connect];
        break;
      default:
        throw new Error("noUiSlider: 'direction' option was not recognized.")
    }
  }

  function k(t, e) {
    if ("string" != typeof e) throw new Error("noUiSlider: 'behaviour' must be a string containing options.");
    var n = e.indexOf("tap") >= 0,
      i = e.indexOf("drag") >= 0,
      o = e.indexOf("fixed") >= 0,
      r = e.indexOf("snap") >= 0,
      s = e.indexOf("hover") >= 0;
    if (i && !t.connect) throw new Error("noUiSlider: 'drag' behaviour must be used with 'connect': true.");
    t.events = {
      tap: n || r,
      drag: i,
      fixed: o,
      snap: r,
      hover: s
    }
  }

  function I(t, e) {
    var n;
    if (!1 !== e)
      if (!0 === e)
        for (t.tooltips = [], n = 0; n < t.handles; n++) t.tooltips.push(!0);
      else {
        if (t.tooltips = o(e), t.tooltips.length !== t.handles) throw new Error("noUiSlider: must pass a formatter for all handles.");
        t.tooltips.forEach(function(t) {
          if ("boolean" != typeof t && ("object" != typeof t || "function" != typeof t.to)) throw new Error("noUiSlider: 'tooltips' must be passed a formatter or 'false'.")
        })
      }
  }

  function A(t, e) {
    if (t.format = e, "function" == typeof e.to && "function" == typeof e.from) return !0;
    throw new Error("noUiSlider: 'format' requires 'to' and 'from' methods.")
  }

  function D(t, e) {
    if (void 0 !== e && "string" != typeof e && !1 !== e) throw new Error("noUiSlider: 'cssPrefix' must be a string or `false`.");
    t.cssPrefix = e
  }

  function L(t, e) {
    if (void 0 !== e && "object" != typeof e) throw new Error("noUiSlider: 'cssClasses' must be an object.");
    if ("string" == typeof t.cssPrefix)
      for (var n in t.cssClasses = {}, e) e.hasOwnProperty(n) && (t.cssClasses[n] = t.cssPrefix + e[n]);
    else t.cssClasses = e
  }

  function O(t) {
    var e, n = {
      margin: 0,
      limit: 0,
      animate: !0,
      animationDuration: 300,
      format: N
    };
    e = {
      step: {
        r: !1,
        t: g
      },
      start: {
        r: !0,
        t: y
      },
      connect: {
        r: !0,
        t: x
      },
      direction: {
        r: !0,
        t: S
      },
      snap: {
        r: !1,
        t: b
      },
      animate: {
        r: !1,
        t: _
      },
      animationDuration: {
        r: !1,
        t: w
      },
      range: {
        r: !0,
        t: v
      },
      orientation: {
        r: !1,
        t: C
      },
      margin: {
        r: !1,
        t: E
      },
      limit: {
        r: !1,
        t: T
      },
      behaviour: {
        r: !0,
        t: k
      },
      format: {
        r: !1,
        t: A
      },
      tooltips: {
        r: !1,
        t: I
      },
      cssPrefix: {
        r: !1,
        t: D
      },
      cssClasses: {
        r: !1,
        t: L
      }
    };
    var i = {
      connect: !1,
      direction: "ltr",
      behaviour: "tap",
      orientation: "horizontal",
      cssPrefix: "noUi-",
      cssClasses: {
        target: "target",
        base: "base",
        origin: "origin",
        handle: "handle",
        handleLower: "handle-lower",
        handleUpper: "handle-upper",
        horizontal: "horizontal",
        vertical: "vertical",
        background: "background",
        connect: "connect",
        ltr: "ltr",
        rtl: "rtl",
        draggable: "draggable",
        drag: "state-drag",
        tap: "state-tap",
        active: "active",
        stacking: "stacking",
        tooltip: "tooltip",
        pips: "pips",
        pipsHorizontal: "pips-horizontal",
        pipsVertical: "pips-vertical",
        marker: "marker",
        markerHorizontal: "marker-horizontal",
        markerVertical: "marker-vertical",
        markerNormal: "marker-normal",
        markerLarge: "marker-large",
        markerSub: "marker-sub",
        value: "value",
        valueHorizontal: "value-horizontal",
        valueVertical: "value-vertical",
        valueNormal: "value-normal",
        valueLarge: "value-large",
        valueSub: "value-sub"
      }
    };
    return Object.keys(e).forEach(function(o) {
      if (void 0 === t[o] && void 0 === i[o]) {
        if (e[o].r) throw new Error("noUiSlider: '" + o + "' is required.");
        return !0
      }
      e[o].t(n, void 0 === t[o] ? i[o] : t[o])
    }), n.pips = t.pips, n.style = n.ort ? "top" : "left", n
  }

  function P(e, l, u) {
    function c(t, e, n) {
      var o = t + e[0],
        r = t + e[1];
      return n ? (0 > o && (r += Math.abs(o)), r > 100 && (o -= r - 100), [i(o), i(r)]) : [o, r]
    }

    function d(t, e) {
      var n = document.createElement("div"),
        i = document.createElement("div"),
        o = [l.cssClasses.handleLower, l.cssClasses.handleUpper];
      return t && o.reverse(), r(i, l.cssClasses.handle), r(i, o[e]), r(n, l.cssClasses.origin), n.appendChild(i), n
    }

    function f(t, e) {
      if (!l.tooltips[e]) return !1;
      var n = document.createElement("div");
      return n.className = l.cssClasses.tooltip, t.firstChild.appendChild(n)
    }

    function h(t, e, n) {
      function i(t, e, n) {
        return 'class="' + function(t, e) {
          var n = e === l.cssClasses.value,
            i = n ? u : c;
          return e + " " + (n ? d : f)[l.ort] + " " + i[t]
        }(n[1], e) + '" style="' + l.style + ": " + t + '%"'
      }

      function o(t, o) {
        z.direction && (t = 100 - t), o[1] = o[1] && e ? e(o[0], o[1]) : o[1], a += "<div " + i(t, l.cssClasses.marker, o) + "></div>", o[1] && (a += "<div " + i(t, l.cssClasses.value, o) + ">" + n.to(o[0]) + "</div>")
      }
      var s = document.createElement("div"),
        a = "",
        u = [l.cssClasses.valueNormal, l.cssClasses.valueLarge, l.cssClasses.valueSub],
        c = [l.cssClasses.markerNormal, l.cssClasses.markerLarge, l.cssClasses.markerSub],
        d = [l.cssClasses.valueHorizontal, l.cssClasses.valueVertical],
        f = [l.cssClasses.markerHorizontal, l.cssClasses.markerVertical];
      return r(s, l.cssClasses.pips), r(s, 0 === l.ort ? l.cssClasses.pipsHorizontal : l.cssClasses.pipsVertical), Object.keys(t).forEach(function(e) {
        o(e, t[e])
      }), s.innerHTML = a, s
    }

    function p(t) {
      var e = t.mode,
        n = t.density || 1,
        i = t.filter || !1,
        o = function(t, e, n) {
          if ("range" === t || "steps" === t) return z.xVal;
          if ("count" === t) {
            var i, o = 100 / (e - 1),
              r = 0;
            for (e = [];
              (i = r++ * o) <= 100;) e.push(i);
            t = "positions"
          }
          return "positions" === t ? e.map(function(t) {
            return z.fromStepping(n ? z.getStep(t) : t)
          }) : "values" === t ? n ? e.map(function(t) {
            return z.fromStepping(z.getStep(z.toStepping(t)))
          }) : e : void 0
        }(e, t.values || !1, t.stepped || !1),
        r = function(t, e, n) {
          function i(t, e) {
            return (t + e).toFixed(7) / 1
          }
          var o = z.direction,
            r = {},
            s = z.xVal[0],
            a = z.xVal[z.xVal.length - 1],
            l = !1,
            u = !1,
            c = 0;
          return z.direction = 0, (n = function(t) {
            return t.filter(function(t) {
              return !this[t] && (this[t] = !0)
            }, {})
          }(n.slice().sort(function(t, e) {
            return t - e
          })))[0] !== s && (n.unshift(s), l = !0), n[n.length - 1] !== a && (n.push(a), u = !0), n.forEach(function(o, s) {
            var a, d, f, h, p, m, g, v, y, b = o,
              _ = n[s + 1];
            if ("steps" === e && (a = z.xNumSteps[s]), a || (a = _ - b), !1 !== b && void 0 !== _)
              for (d = b; _ >= d; d = i(d, a)) {
                for (g = (p = (h = z.toStepping(d)) - c) / t, y = p / (v = Math.round(g)), f = 1; v >= f; f += 1) r[(c + f * y).toFixed(5)] = ["x", 0];
                m = n.indexOf(d) > -1 ? 1 : "steps" === e ? 2 : 0, !s && l && (m = 0), d === _ && u || (r[h.toFixed(5)] = [d, m]), c = h
              }
          }), z.direction = o, r
        }(n, e, o),
        s = t.format || {
          to: Math.round
        };
      return N.appendChild(h(r, i, s))
    }

    function m() {
      var t = A.getBoundingClientRect(),
        e = "offset" + ["Width", "Height"][l.ort];
      return 0 === l.ort ? t.width || A[e] : t.height || A[e]
    }

    function g(t, e, n) {
      var i;
      for (i = 0; i < l.handles; i++)
        if (-1 === j[i]) return;
      void 0 !== e && 1 !== l.handles && (e = Math.abs(e - l.dir)), Object.keys($).forEach(function(i) {
        var r = i.split(".")[0];
        t === r && $[i].forEach(function(t) {
          t.call(L, o(k()), e, o(v(Array.prototype.slice.call(M))), n || !1, j)
        })
      })
    }

    function v(t) {
      return 1 === t.length ? t[0] : l.dir ? t.reverse() : t
    }

    function y(t, e, n, i) {
      var o = function(e) {
          return !N.hasAttribute("disabled") && ! function(t, e) {
            return t.classList ? t.classList.contains(e) : new RegExp("\\b" + e + "\\b").test(t.className)
          }(N, l.cssClasses.tap) && (e = function(t, e) {
            t.preventDefault();
            var n, i, o = 0 === t.type.indexOf("touch"),
              r = 0 === t.type.indexOf("mouse"),
              s = 0 === t.type.indexOf("pointer"),
              l = t;
            return 0 === t.type.indexOf("MSPointer") && (s = !0), o && (n = t.changedTouches[0].pageX, i = t.changedTouches[0].pageY), e = e || a(), (r || s) && (n = t.clientX + e.x, i = t.clientY + e.y), l.pageOffset = e, l.points = [n, i], l.cursor = r || s, l
          }(e, i.pageOffset), !(t === P.start && void 0 !== e.buttons && e.buttons > 1) && (!i.hover || !e.buttons) && (e.calcPoint = e.points[l.ort], void n(e, i)))
        },
        r = [];
      return t.split(" ").forEach(function(t) {
        e.addEventListener(t, o, !1), r.push([t, o])
      }), r
    }

    function b(t, e) {
      if (-1 === navigator.appVersion.indexOf("MSIE 9") && 0 === t.buttons && 0 !== e.buttonsProperty) return _(t, e);
      var n, i, o = e.handles || D,
        r = !1,
        s = 100 * (t.calcPoint - e.start) / e.baseSize,
        a = o[0] === D[0] ? 0 : 1;
      if (n = c(s, e.positions, o.length > 1), r = T(o[0], n[a], 1 === o.length), o.length > 1) {
        if (r = T(o[1], n[a ? 0 : 1], !1) || r)
          for (i = 0; i < e.handles.length; i++) g("slide", i)
      } else r && g("slide", a)
    }

    function _(t, e) {
      var n = A.querySelector("." + l.cssClasses.active),
        i = e.handles[0] === D[0] ? 0 : 1;
      null !== n && s(n, l.cssClasses.active), t.cursor && (document.body.style.cursor = "", document.body.removeEventListener("selectstart", document.body.noUiListener));
      var o = document.documentElement;
      o.noUiListeners.forEach(function(t) {
        o.removeEventListener(t[0], t[1])
      }), s(N, l.cssClasses.drag), g("set", i), g("change", i), void 0 !== e.handleNumber && g("end", e.handleNumber)
    }

    function w(t, e) {
      "mouseout" === t.type && "HTML" === t.target.nodeName && null === t.relatedTarget && _(t, e)
    }

    function x(t, e) {
      var n = document.documentElement;
      if (1 === e.handles.length) {
        if (e.handles[0].hasAttribute("disabled")) return !1;
        r(e.handles[0].children[0], l.cssClasses.active)
      }
      t.preventDefault(), t.stopPropagation();
      var i = y(P.move, n, b, {
          start: t.calcPoint,
          baseSize: m(),
          pageOffset: t.pageOffset,
          handles: e.handles,
          handleNumber: e.handleNumber,
          buttonsProperty: t.buttons,
          positions: [j[0], j[D.length - 1]]
        }),
        o = y(P.end, n, _, {
          handles: e.handles,
          handleNumber: e.handleNumber
        }),
        s = y("mouseout", n, w, {
          handles: e.handles,
          handleNumber: e.handleNumber
        });
      if (n.noUiListeners = i.concat(o, s), t.cursor) {
        document.body.style.cursor = getComputedStyle(t.target).cursor, D.length > 1 && r(N, l.cssClasses.drag);
        var a = function() {
          return !1
        };
        document.body.noUiListener = a, document.body.addEventListener("selectstart", a, !1)
      }
      void 0 !== e.handleNumber && g("start", e.handleNumber)
    }

    function C(e) {
      var i, o, r = e.calcPoint,
        s = 0;
      return e.stopPropagation(), D.forEach(function(e) {
        s += t(e)[l.style]
      }), i = s / 2 > r || 1 === D.length ? 0 : 1, D[i].hasAttribute("disabled") && (i = i ? 0 : 1), o = 100 * (r -= t(A)[l.style]) / m(), l.events.snap || n(N, l.cssClasses.tap, l.animationDuration), !D[i].hasAttribute("disabled") && (T(D[i], o), g("slide", i, !0), g("set", i, !0), g("change", i, !0), void(l.events.snap && x(e, {
        handles: [D[i]]
      })))
    }

    function E(e) {
      var n = e.calcPoint - t(A)[l.style],
        i = z.getStep(100 * n / m()),
        o = z.fromStepping(i);
      Object.keys($).forEach(function(t) {
        "hover" === t.split(".")[0] && $[t].forEach(function(t) {
          t.call(L, o)
        })
      })
    }

    function T(t, e, n) {
      var o = t !== D[0] ? 1 : 0,
        a = j[0] + l.margin,
        u = j[1] - l.margin,
        c = j[0] + l.limit,
        d = j[1] - l.limit;
      return D.length > 1 && (e = o ? Math.max(e, a) : Math.min(e, u)), !1 !== n && l.limit && D.length > 1 && (e = o ? Math.min(e, c) : Math.max(e, d)), (e = i(e = z.getStep(e))) !== j[o] && (window.requestAnimationFrame ? window.requestAnimationFrame(function() {
        t.style[l.style] = e + "%"
      }) : t.style[l.style] = e + "%", t.previousSibling || (s(t, l.cssClasses.stacking), e > 50 && r(t, l.cssClasses.stacking)), j[o] = e, M[o] = z.fromStepping(e), g("update", o), !0)
    }

    function S(t, e) {
      var i, r, s = o(t);
      for (e = void 0 === e || !!e, l.dir && l.handles > 1 && s.reverse(), l.animate && -1 !== j[0] && n(N, l.cssClasses.tap, l.animationDuration), i = D.length > 1 ? 3 : 1, 1 === s.length && (i = 1), function(t, e) {
          var n, i, o;
          for (l.limit && (t += 1), n = 0; t > n; n += 1) null !== (o = e[i = n % 2]) && !1 !== o && ("number" == typeof o && (o = String(o)), (!1 === (o = l.format.from(o)) || isNaN(o) || !1 === T(D[i], z.toStepping(o), n === 3 - l.dir)) && g("update", i))
        }(i, s), r = 0; r < D.length; r++) null !== s[r] && e && g("set", r)
    }

    function k() {
      var t, e = [];
      for (t = 0; t < l.handles; t += 1) e[t] = l.format.to(M[t]);
      return v(e)
    }

    function I(t, e) {
      $[t] = $[t] || [], $[t].push(e), "update" === t.split(".")[0] && D.forEach(function(t, e) {
        g("update", e)
      })
    }
    var A, D, L, P = window.navigator.pointerEnabled ? {
        start: "pointerdown",
        move: "pointermove",
        end: "pointerup"
      } : window.navigator.msPointerEnabled ? {
        start: "MSPointerDown",
        move: "MSPointerMove",
        end: "MSPointerUp"
      } : {
        start: "mousedown touchstart",
        move: "mousemove touchmove",
        end: "mouseup touchend"
      },
      N = e,
      j = [-1, -1],
      z = l.spectrum,
      M = [],
      $ = {};
    if (N.noUiSlider) throw new Error("Slider was already initialized.");
    return A = function(t, e, n) {
        r(n, l.cssClasses.target), r(n, 0 === t ? l.cssClasses.ltr : l.cssClasses.rtl), r(n, 0 === e ? l.cssClasses.horizontal : l.cssClasses.vertical);
        var i = document.createElement("div");
        return r(i, l.cssClasses.base), n.appendChild(i), i
      }(l.dir, l.ort, N), D = function(t, e, n) {
        var i, o = [];
        for (i = 0; t > i; i += 1) o.push(n.appendChild(d(e, i)));
        return o
      }(l.handles, l.dir, A),
      function(t, e, n) {
        switch (t) {
          case 1:
            r(e, l.cssClasses.connect), r(n[0], l.cssClasses.background);
            break;
          case 3:
            r(n[1], l.cssClasses.background);
          case 2:
            r(n[0], l.cssClasses.connect);
          case 0:
            r(e, l.cssClasses.background)
        }
      }(l.connect, N, D), l.pips && p(l.pips), l.tooltips && function() {
        l.dir && l.tooltips.reverse();
        var t = D.map(f);
        l.dir && (t.reverse(), l.tooltips.reverse()), I("update", function(e, n, i) {
          t[n] && (t[n].innerHTML = !0 === l.tooltips[n] ? e[n] : l.tooltips[n].to(i[n]))
        })
      }(), L = {
        destroy: function() {
          for (var t in l.cssClasses) l.cssClasses.hasOwnProperty(t) && s(N, l.cssClasses[t]);
          for (; N.firstChild;) N.removeChild(N.firstChild);
          delete N.noUiSlider
        },
        steps: function() {
          return v(j.map(function(t, e) {
            var n = z.getApplicableStep(t),
              i = function(t) {
                var e = t.split(".");
                return e.length > 1 ? e[1].length : 0
              }(String(n[2])),
              o = M[e],
              r = 100 === t ? null : n[2],
              s = Number((o - n[2]).toFixed(i));
            return [0 === t ? null : s >= n[1] ? n[2] : n[0] || !1, r]
          }))
        },
        on: I,
        off: function(t) {
          var e = t && t.split(".")[0],
            n = e && t.substring(e.length);
          Object.keys($).forEach(function(t) {
            var i = t.split(".")[0],
              o = t.substring(i.length);
            e && e !== i || n && n !== o || delete $[t]
          })
        },
        get: k,
        set: S,
        updateOptions: function(t, e) {
          var n = k(),
            i = O({
              start: [0, 0],
              margin: t.margin,
              limit: t.limit,
              step: void 0 === t.step ? l.singleStep : t.step,
              range: t.range,
              animate: t.animate,
              snap: void 0 === t.snap ? l.snap : t.snap
            });
          ["margin", "limit", "range", "animate"].forEach(function(e) {
            void 0 !== t[e] && (l[e] = t[e])
          }), i.spectrum.direction = z.direction, z = i.spectrum, j = [-1, -1], S(t.start || n, e)
        },
        options: u,
        target: N,
        pips: p
      },
      function(t) {
        if (t.fixed || D.forEach(function(t, e) {
            y(P.start, t.children[0], x, {
              handles: [t],
              handleNumber: e
            })
          }), t.tap && y(P.start, A, C, {
            handles: D
          }), t.hover && y(P.move, A, E, {
            hover: !0
          }), t.drag) {
          var e = [A.querySelector("." + l.cssClasses.connect)];
          r(e[0], l.cssClasses.draggable), t.fixed && e.push(D[e[0] === D[0] ? 1 : 0].children[0]), e.forEach(function(t) {
            y(P.start, t, x, {
              handles: D
            })
          })
        }
      }(l.events), L
  }
  m.prototype.getMargin = function(t) {
    return 2 === this.xPct.length && u(this.xVal, t)
  }, m.prototype.toStepping = function(t) {
    return t = d(this.xVal, this.xPct, t), this.direction && (t = 100 - t), t
  }, m.prototype.fromStepping = function(t) {
    return this.direction && (t = 100 - t),
      function(t, e, n) {
        if (n >= 100) return t.slice(-1)[0];
        var i, o = c(n, e);
        return function(t, e) {
          return e * (t[1] - t[0]) / 100 + t[0]
        }([t[o - 1], t[o]], (n - (i = e[o - 1])) * l(i, e[o]))
      }(this.xVal, this.xPct, t)
  }, m.prototype.getStep = function(t) {
    return this.direction && (t = 100 - t), t = f(this.xPct, this.xSteps, this.snap, t), this.direction && (t = 100 - t), t
  }, m.prototype.getApplicableStep = function(t) {
    var e = c(t, this.xPct),
      n = 100 === t ? 2 : 1;
    return [this.xNumSteps[e - 2], this.xVal[e - n], this.xNumSteps[e - n]]
  }, m.prototype.convert = function(t) {
    return this.getStep(this.toStepping(t))
  };
  var N = {
    to: function(t) {
      return void 0 !== t && t.toFixed(2)
    },
    from: Number
  };
  return {
    create: function(t, e) {
      if (!t.nodeName) throw new Error("noUiSlider.create requires a single element.");
      var n = O(e),
        i = P(t, n, e);
      return i.set(n.start), t.noUiSlider = i, i
    }
  }
}),
function(t, e, n, i) {
  function o(e, n) {
    this.settings = null, this.options = t.extend({}, o.Defaults, n), this.$element = t(e), this._handlers = {}, this._plugins = {}, this._supress = {}, this._current = null, this._speed = null, this._coordinates = [], this._breakpoint = null, this._width = null, this._items = [], this._clones = [], this._mergers = [], this._widths = [], this._invalidated = {}, this._pipe = [], this._drag = {
      time: null,
      target: null,
      pointer: null,
      stage: {
        start: null,
        current: null
      },
      direction: null
    }, this._states = {
      current: {},
      tags: {
        initializing: ["busy"],
        animating: ["busy"],
        dragging: ["interacting"]
      }
    }, t.each(["onResize", "onThrottledResize"], t.proxy(function(e, n) {
      this._handlers[n] = t.proxy(this[n], this)
    }, this)), t.each(o.Plugins, t.proxy(function(t, e) {
      this._plugins[t.charAt(0).toLowerCase() + t.slice(1)] = new e(this)
    }, this)), t.each(o.Workers, t.proxy(function(e, n) {
      this._pipe.push({
        filter: n.filter,
        run: t.proxy(n.run, this)
      })
    }, this)), this.setup(), this.initialize()
  }
  o.Defaults = {
    items: 3,
    loop: !1,
    center: !1,
    rewind: !1,
    mouseDrag: !0,
    touchDrag: !0,
    pullDrag: !0,
    freeDrag: !1,
    margin: 0,
    stagePadding: 0,
    merge: !1,
    mergeFit: !0,
    autoWidth: !1,
    startPosition: 0,
    rtl: !1,
    smartSpeed: 250,
    fluidSpeed: !1,
    dragEndSpeed: !1,
    responsive: {},
    responsiveRefreshRate: 200,
    responsiveBaseElement: e,
    fallbackEasing: "swing",
    info: !1,
    nestedItemSelector: !1,
    itemElement: "div",
    stageElement: "div",
    refreshClass: "owl-refresh",
    loadedClass: "owl-loaded",
    loadingClass: "owl-loading",
    rtlClass: "owl-rtl",
    responsiveClass: "owl-responsive",
    dragClass: "owl-drag",
    itemClass: "owl-item",
    stageClass: "owl-stage",
    stageOuterClass: "owl-stage-outer",
    grabClass: "owl-grab"
  }, o.Width = {
    Default: "default",
    Inner: "inner",
    Outer: "outer"
  }, o.Type = {
    Event: "event",
    State: "state"
  }, o.Plugins = {}, o.Workers = [{
    filter: ["width", "settings"],
    run: function() {
      this._width = this.$element.width()
    }
  }, {
    filter: ["width", "items", "settings"],
    run: function(t) {
      t.current = this._items && this._items[this.relative(this._current)]
    }
  }, {
    filter: ["items", "settings"],
    run: function() {
      this.$stage.children(".cloned").remove()
    }
  }, {
    filter: ["width", "items", "settings"],
    run: function(t) {
      var e = this.settings.margin || "",
        n = !this.settings.autoWidth,
        i = this.settings.rtl,
        o = {
          width: "auto",
          "margin-left": i ? e : "",
          "margin-right": i ? "" : e
        };
      !n && this.$stage.children().css(o), t.css = o
    }
  }, {
    filter: ["width", "items", "settings"],
    run: function(t) {
      var e = (this.width() / this.settings.items).toFixed(3) - this.settings.margin,
        n = null,
        i = this._items.length,
        o = !this.settings.autoWidth,
        r = [];
      for (t.items = {
          merge: !1,
          width: e
        }; i--;) n = this._mergers[i], n = this.settings.mergeFit && Math.min(n, this.settings.items) || n, t.items.merge = n > 1 || t.items.merge, r[i] = o ? e * n : this._items[i].width();
      this._widths = r
    }
  }, {
    filter: ["items", "settings"],
    run: function() {
      var e = [],
        n = this._items,
        i = this.settings,
        o = Math.max(2 * i.items, 4),
        r = 2 * Math.ceil(n.length / 2),
        s = i.loop && n.length ? i.rewind ? o : Math.max(o, r) : 0,
        a = "",
        l = "";
      for (s /= 2; s--;) e.push(this.normalize(e.length / 2, !0)), a += n[e[e.length - 1]][0].outerHTML, e.push(this.normalize(n.length - 1 - (e.length - 1) / 2, !0)), l = n[e[e.length - 1]][0].outerHTML + l;
      this._clones = e, t(a).addClass("cloned").appendTo(this.$stage), t(l).addClass("cloned").prependTo(this.$stage)
    }
  }, {
    filter: ["width", "items", "settings"],
    run: function() {
      for (var t = this.settings.rtl ? 1 : -1, e = this._clones.length + this._items.length, n = -1, i = 0, o = 0, r = []; ++n < e;) i = r[n - 1] || 0, o = this._widths[this.relative(n)] + this.settings.margin, r.push(i + o * t);
      this._coordinates = r
    }
  }, {
    filter: ["width", "items", "settings"],
    run: function() {
      var t = this.settings.stagePadding,
        e = this._coordinates,
        n = {
          width: Math.ceil(Math.abs(e[e.length - 1])) + 2 * t,
          "padding-left": t || "",
          "padding-right": t || ""
        };
      this.$stage.css(n)
    }
  }, {
    filter: ["width", "items", "settings"],
    run: function(t) {
      var e = this._coordinates.length,
        n = !this.settings.autoWidth,
        i = this.$stage.children();
      if (n && t.items.merge)
        for (; e--;) t.css.width = this._widths[this.relative(e)], i.eq(e).css(t.css);
      else n && (t.css.width = t.items.width, i.css(t.css))
    }
  }, {
    filter: ["items"],
    run: function() {
      this._coordinates.length < 1 && this.$stage.removeAttr("style")
    }
  }, {
    filter: ["width", "items", "settings"],
    run: function(t) {
      t.current = t.current ? this.$stage.children().index(t.current) : 0, t.current = Math.max(this.minimum(), Math.min(this.maximum(), t.current)), this.reset(t.current)
    }
  }, {
    filter: ["position"],
    run: function() {
      this.animate(this.coordinates(this._current))
    }
  }, {
    filter: ["width", "position", "items", "settings"],
    run: function() {
      var t, e, n, i, o = this.settings.rtl ? 1 : -1,
        r = 2 * this.settings.stagePadding,
        s = this.coordinates(this.current()) + r,
        a = s + this.width() * o,
        l = [];
      for (n = 0, i = this._coordinates.length; n < i; n++) t = this._coordinates[n - 1] || 0, e = Math.abs(this._coordinates[n]) + r * o, (this.op(t, "<=", s) && this.op(t, ">", a) || this.op(e, "<", s) && this.op(e, ">", a)) && l.push(n);
      this.$stage.children(".active").removeClass("active"), this.$stage.children(":eq(" + l.join("), :eq(") + ")").addClass("active"), this.settings.center && (this.$stage.children(".center").removeClass("center"), this.$stage.children().eq(this.current()).addClass("center"))
    }
  }], o.prototype.initialize = function() {
    var e, n, o;
    (this.enter("initializing"), this.trigger("initialize"), this.$element.toggleClass(this.settings.rtlClass, this.settings.rtl), this.settings.autoWidth && !this.is("pre-loading")) && (e = this.$element.find("img"), n = this.settings.nestedItemSelector ? "." + this.settings.nestedItemSelector : i, o = this.$element.children(n).width(), e.length && o <= 0 && this.preloadAutoWidthImages(e));
    this.$element.addClass(this.options.loadingClass), this.$stage = t("<" + this.settings.stageElement + ' class="' + this.settings.stageClass + '"/>').wrap('<div class="' + this.settings.stageOuterClass + '"/>'), this.$element.append(this.$stage.parent()), this.replace(this.$element.children().not(this.$stage.parent())), this.$element.is(":visible") ? this.refresh() : this.invalidate("width"), this.$element.removeClass(this.options.loadingClass).addClass(this.options.loadedClass), this.registerEventHandlers(), this.leave("initializing"), this.trigger("initialized")
  }, o.prototype.setup = function() {
    var e = this.viewport(),
      n = this.options.responsive,
      i = -1,
      o = null;
    n ? (t.each(n, function(t) {
      t <= e && t > i && (i = Number(t))
    }), "function" == typeof(o = t.extend({}, this.options, n[i])).stagePadding && (o.stagePadding = o.stagePadding()), delete o.responsive, o.responsiveClass && this.$element.attr("class", this.$element.attr("class").replace(new RegExp("(" + this.options.responsiveClass + "-)\\S+\\s", "g"), "$1" + i))) : o = t.extend({}, this.options), this.trigger("change", {
      property: {
        name: "settings",
        value: o
      }
    }), this._breakpoint = i, this.settings = o, this.invalidate("settings"), this.trigger("changed", {
      property: {
        name: "settings",
        value: this.settings
      }
    })
  }, o.prototype.optionsLogic = function() {
    this.settings.autoWidth && (this.settings.stagePadding = !1, this.settings.merge = !1)
  }, o.prototype.prepare = function(e) {
    var n = this.trigger("prepare", {
      content: e
    });
    return n.data || (n.data = t("<" + this.settings.itemElement + "/>").addClass(this.options.itemClass).append(e)), this.trigger("prepared", {
      content: n.data
    }), n.data
  }, o.prototype.update = function() {
    for (var e = 0, n = this._pipe.length, i = t.proxy(function(t) {
        return this[t]
      }, this._invalidated), o = {}; e < n;)(this._invalidated.all || t.grep(this._pipe[e].filter, i).length > 0) && this._pipe[e].run(o), e++;
    this._invalidated = {}, !this.is("valid") && this.enter("valid")
  }, o.prototype.width = function(t) {
    switch (t = t || o.Width.Default) {
      case o.Width.Inner:
      case o.Width.Outer:
        return this._width;
      default:
        return this._width - 2 * this.settings.stagePadding + this.settings.margin
    }
  }, o.prototype.refresh = function() {
    this.enter("refreshing"), this.trigger("refresh"), this.setup(), this.optionsLogic(), this.$element.addClass(this.options.refreshClass), this.update(), this.$element.removeClass(this.options.refreshClass), this.leave("refreshing"), this.trigger("refreshed")
  }, o.prototype.onThrottledResize = function() {
    e.clearTimeout(this.resizeTimer), this.resizeTimer = e.setTimeout(this._handlers.onResize, this.settings.responsiveRefreshRate)
  }, o.prototype.onResize = function() {
    return !!this._items.length && this._width !== this.$element.width() && !!this.$element.is(":visible") && (this.enter("resizing"), this.trigger("resize").isDefaultPrevented() ? (this.leave("resizing"), !1) : (this.invalidate("width"), this.refresh(), this.leave("resizing"), void this.trigger("resized")))
  }, o.prototype.registerEventHandlers = function() {
    t.support.transition && this.$stage.on(t.support.transition.end + ".owl.core", t.proxy(this.onTransitionEnd, this)), !1 !== this.settings.responsive && this.on(e, "resize", this._handlers.onThrottledResize), this.settings.mouseDrag && (this.$element.addClass(this.options.dragClass), this.$stage.on("mousedown.owl.core", t.proxy(this.onDragStart, this)), this.$stage.on("dragstart.owl.core selectstart.owl.core", function() {
      return !1
    })), this.settings.touchDrag && (this.$stage.on("touchstart.owl.core", t.proxy(this.onDragStart, this)), this.$stage.on("touchcancel.owl.core", t.proxy(this.onDragEnd, this)))
  }, o.prototype.onDragStart = function(e) {
    var i = null;
    3 !== e.which && (t.support.transform ? i = {
      x: (i = this.$stage.css("transform").replace(/.*\(|\)| /g, "").split(","))[16 === i.length ? 12 : 4],
      y: i[16 === i.length ? 13 : 5]
    } : (i = this.$stage.position(), i = {
      x: this.settings.rtl ? i.left + this.$stage.width() - this.width() + this.settings.margin : i.left,
      y: i.top
    }), this.is("animating") && (t.support.transform ? this.animate(i.x) : this.$stage.stop(), this.invalidate("position")), this.$element.toggleClass(this.options.grabClass, "mousedown" === e.type), this.speed(0), this._drag.time = (new Date).getTime(), this._drag.target = t(e.target), this._drag.stage.start = i, this._drag.stage.current = i, this._drag.pointer = this.pointer(e), t(n).on("mouseup.owl.core touchend.owl.core", t.proxy(this.onDragEnd, this)), t(n).one("mousemove.owl.core touchmove.owl.core", t.proxy(function(e) {
      var i = this.difference(this._drag.pointer, this.pointer(e));
      t(n).on("mousemove.owl.core touchmove.owl.core", t.proxy(this.onDragMove, this)), Math.abs(i.x) < Math.abs(i.y) && this.is("valid") || (e.preventDefault(), this.enter("dragging"), this.trigger("drag"))
    }, this)))
  }, o.prototype.onDragMove = function(t) {
    var e = null,
      n = null,
      i = null,
      o = this.difference(this._drag.pointer, this.pointer(t)),
      r = this.difference(this._drag.stage.start, o);
    this.is("dragging") && (t.preventDefault(), this.settings.loop ? (e = this.coordinates(this.minimum()), n = this.coordinates(this.maximum() + 1) - e, r.x = ((r.x - e) % n + n) % n + e) : (e = this.settings.rtl ? this.coordinates(this.maximum()) : this.coordinates(this.minimum()), n = this.settings.rtl ? this.coordinates(this.minimum()) : this.coordinates(this.maximum()), i = this.settings.pullDrag ? -1 * o.x / 5 : 0, r.x = Math.max(Math.min(r.x, e + i), n + i)), this._drag.stage.current = r, this.animate(r.x))
  }, o.prototype.onDragEnd = function(e) {
    var i = this.difference(this._drag.pointer, this.pointer(e)),
      o = this._drag.stage.current,
      r = i.x > 0 ^ this.settings.rtl ? "left" : "right";
    t(n).off(".owl.core"), this.$element.removeClass(this.options.grabClass), (0 !== i.x && this.is("dragging") || !this.is("valid")) && (this.speed(this.settings.dragEndSpeed || this.settings.smartSpeed), this.current(this.closest(o.x, 0 !== i.x ? r : this._drag.direction)), this.invalidate("position"), this.update(), this._drag.direction = r, (Math.abs(i.x) > 3 || (new Date).getTime() - this._drag.time > 300) && this._drag.target.one("click.owl.core", function() {
      return !1
    })), this.is("dragging") && (this.leave("dragging"), this.trigger("dragged"))
  }, o.prototype.closest = function(e, n) {
    var i = -1,
      o = this.width(),
      r = this.coordinates();
    return this.settings.freeDrag || t.each(r, t.proxy(function(t, s) {
      return "left" === n && e > s - 30 && e < s + 30 ? i = t : "right" === n && e > s - o - 30 && e < s - o + 30 ? i = t + 1 : this.op(e, "<", s) && this.op(e, ">", r[t + 1] || s - o) && (i = "left" === n ? t + 1 : t), -1 === i
    }, this)), this.settings.loop || (this.op(e, ">", r[this.minimum()]) ? i = e = this.minimum() : this.op(e, "<", r[this.maximum()]) && (i = e = this.maximum())), i
  }, o.prototype.animate = function(e) {
    var n = this.speed() > 0;
    this.is("animating") && this.onTransitionEnd(), n && (this.enter("animating"), this.trigger("translate")), t.support.transform3d && t.support.transition ? this.$stage.css({
      transform: "translate3d(" + e + "px,0px,0px)",
      transition: this.speed() / 1e3 + "s"
    }) : n ? this.$stage.animate({
      left: e + "px"
    }, this.speed(), this.settings.fallbackEasing, t.proxy(this.onTransitionEnd, this)) : this.$stage.css({
      left: e + "px"
    })
  }, o.prototype.is = function(t) {
    return this._states.current[t] && this._states.current[t] > 0
  }, o.prototype.current = function(t) {
    if (t === i) return this._current;
    if (0 === this._items.length) return i;
    if (t = this.normalize(t), this._current !== t) {
      var e = this.trigger("change", {
        property: {
          name: "position",
          value: t
        }
      });
      e.data !== i && (t = this.normalize(e.data)), this._current = t, this.invalidate("position"), this.trigger("changed", {
        property: {
          name: "position",
          value: this._current
        }
      })
    }
    return this._current
  }, o.prototype.invalidate = function(e) {
    return "string" === t.type(e) && (this._invalidated[e] = !0, this.is("valid") && this.leave("valid")), t.map(this._invalidated, function(t, e) {
      return e
    })
  }, o.prototype.reset = function(t) {
    (t = this.normalize(t)) !== i && (this._speed = 0, this._current = t, this.suppress(["translate", "translated"]), this.animate(this.coordinates(t)), this.release(["translate", "translated"]))
  }, o.prototype.normalize = function(t, e) {
    var n = this._items.length,
      o = e ? 0 : this._clones.length;
    return !this.isNumeric(t) || n < 1 ? t = i : (t < 0 || t >= n + o) && (t = ((t - o / 2) % n + n) % n + o / 2), t
  }, o.prototype.relative = function(t) {
    return t -= this._clones.length / 2, this.normalize(t, !0)
  }, o.prototype.maximum = function(t) {
    var e, n, i, o = this.settings,
      r = this._coordinates.length;
    if (o.loop) r = this._clones.length / 2 + this._items.length - 1;
    else if (o.autoWidth || o.merge) {
      for (e = this._items.length, n = this._items[--e].width(), i = this.$element.width(); e-- && !((n += this._items[e].width() + this.settings.margin) > i););
      r = e + 1
    } else r = o.center ? this._items.length - 1 : this._items.length - o.items;
    return t && (r -= this._clones.length / 2), Math.max(r, 0)
  }, o.prototype.minimum = function(t) {
    return t ? 0 : this._clones.length / 2
  }, o.prototype.items = function(t) {
    return t === i ? this._items.slice() : (t = this.normalize(t, !0), this._items[t])
  }, o.prototype.mergers = function(t) {
    return t === i ? this._mergers.slice() : (t = this.normalize(t, !0), this._mergers[t])
  }, o.prototype.clones = function(e) {
    var n = this._clones.length / 2,
      o = n + this._items.length,
      r = function(t) {
        return t % 2 == 0 ? o + t / 2 : n - (t + 1) / 2
      };
    return e === i ? t.map(this._clones, function(t, e) {
      return r(e)
    }) : t.map(this._clones, function(t, n) {
      return t === e ? r(n) : null
    })
  }, o.prototype.speed = function(t) {
    return t !== i && (this._speed = t), this._speed
  }, o.prototype.coordinates = function(e) {
    var n, o = 1,
      r = e - 1;
    return e === i ? t.map(this._coordinates, t.proxy(function(t, e) {
      return this.coordinates(e)
    }, this)) : (this.settings.center ? (this.settings.rtl && (o = -1, r = e + 1), n = this._coordinates[e], n += (this.width() - n + (this._coordinates[r] || 0)) / 2 * o) : n = this._coordinates[r] || 0, n = Math.ceil(n))
  }, o.prototype.duration = function(t, e, n) {
    return 0 === n ? 0 : Math.min(Math.max(Math.abs(e - t), 1), 6) * Math.abs(n || this.settings.smartSpeed)
  }, o.prototype.to = function(t, e) {
    var n = this.current(),
      i = null,
      o = t - this.relative(n),
      r = (o > 0) - (o < 0),
      s = this._items.length,
      a = this.minimum(),
      l = this.maximum();
    this.settings.loop ? (!this.settings.rewind && Math.abs(o) > s / 2 && (o += -1 * r * s), (i = (((t = n + o) - a) % s + s) % s + a) !== t && i - o <= l && i - o > 0 && (n = i - o, t = i, this.reset(n))) : this.settings.rewind ? t = (t % (l += 1) + l) % l : t = Math.max(a, Math.min(l, t)), this.speed(this.duration(n, t, e)), this.current(t), this.$element.is(":visible") && this.update()
  }, o.prototype.next = function(t) {
    t = t || !1, this.to(this.relative(this.current()) + 1, t)
  }, o.prototype.prev = function(t) {
    t = t || !1, this.to(this.relative(this.current()) - 1, t)
  }, o.prototype.onTransitionEnd = function(t) {
    return (t === i || (t.stopPropagation(), (t.target || t.srcElement || t.originalTarget) === this.$stage.get(0))) && (this.leave("animating"), void this.trigger("translated"))
  }, o.prototype.viewport = function() {
    var i;
    return this.options.responsiveBaseElement !== e ? i = t(this.options.responsiveBaseElement).width() : e.innerWidth ? i = e.innerWidth : n.documentElement && n.documentElement.clientWidth ? i = n.documentElement.clientWidth : console.warn("Can not detect viewport width."), i
  }, o.prototype.replace = function(e) {
    this.$stage.empty(), this._items = [], e && (e = e instanceof jQuery ? e : t(e)), this.settings.nestedItemSelector && (e = e.find("." + this.settings.nestedItemSelector)), e.filter(function() {
      return 1 === this.nodeType
    }).each(t.proxy(function(t, e) {
      e = this.prepare(e), this.$stage.append(e), this._items.push(e), this._mergers.push(1 * e.find("[data-merge]").addBack("[data-merge]").attr("data-merge") || 1)
    }, this)), this.reset(this.isNumeric(this.settings.startPosition) ? this.settings.startPosition : 0), this.invalidate("items")
  }, o.prototype.add = function(e, n) {
    var o = this.relative(this._current);
    n = n === i ? this._items.length : this.normalize(n, !0), e = e instanceof jQuery ? e : t(e), this.trigger("add", {
      content: e,
      position: n
    }), e = this.prepare(e), 0 === this._items.length || n === this._items.length ? (0 === this._items.length && this.$stage.append(e), 0 !== this._items.length && this._items[n - 1].after(e), this._items.push(e), this._mergers.push(1 * e.find("[data-merge]").addBack("[data-merge]").attr("data-merge") || 1)) : (this._items[n].before(e), this._items.splice(n, 0, e), this._mergers.splice(n, 0, 1 * e.find("[data-merge]").addBack("[data-merge]").attr("data-merge") || 1)), this._items[o] && this.reset(this._items[o].index()), this.invalidate("items"), this.trigger("added", {
      content: e,
      position: n
    })
  }, o.prototype.remove = function(t) {
    (t = this.normalize(t, !0)) !== i && (this.trigger("remove", {
      content: this._items[t],
      position: t
    }), this._items[t].remove(), this._items.splice(t, 1), this._mergers.splice(t, 1), this.invalidate("items"), this.trigger("removed", {
      content: null,
      position: t
    }))
  }, o.prototype.preloadAutoWidthImages = function(e) {
    e.each(t.proxy(function(e, n) {
      this.enter("pre-loading"), n = t(n), t(new Image).one("load", t.proxy(function(t) {
        n.attr("src", t.target.src), n.css("opacity", 1), this.leave("pre-loading"), !this.is("pre-loading") && !this.is("initializing") && this.refresh()
      }, this)).attr("src", n.attr("src") || n.attr("data-src") || n.attr("data-src-retina"))
    }, this))
  }, o.prototype.destroy = function() {
    for (var i in this.$element.off(".owl.core"), this.$stage.off(".owl.core"), t(n).off(".owl.core"), !1 !== this.settings.responsive && (e.clearTimeout(this.resizeTimer), this.off(e, "resize", this._handlers.onThrottledResize)), this._plugins) this._plugins[i].destroy();
    this.$stage.children(".cloned").remove(), this.$stage.unwrap(), this.$stage.children().contents().unwrap(), this.$stage.children().unwrap(), this.$element.removeClass(this.options.refreshClass).removeClass(this.options.loadingClass).removeClass(this.options.loadedClass).removeClass(this.options.rtlClass).removeClass(this.options.dragClass).removeClass(this.options.grabClass).attr("class", this.$element.attr("class").replace(new RegExp(this.options.responsiveClass + "-\\S+\\s", "g"), "")).removeData("owl.carousel")
  }, o.prototype.op = function(t, e, n) {
    var i = this.settings.rtl;
    switch (e) {
      case "<":
        return i ? t > n : t < n;
      case ">":
        return i ? t < n : t > n;
      case ">=":
        return i ? t <= n : t >= n;
      case "<=":
        return i ? t >= n : t <= n
    }
  }, o.prototype.on = function(t, e, n, i) {
    t.addEventListener ? t.addEventListener(e, n, i) : t.attachEvent && t.attachEvent("on" + e, n)
  }, o.prototype.off = function(t, e, n, i) {
    t.removeEventListener ? t.removeEventListener(e, n, i) : t.detachEvent && t.detachEvent("on" + e, n)
  }, o.prototype.trigger = function(e, n, i, r, s) {
    var a = {
        item: {
          count: this._items.length,
          index: this.current()
        }
      },
      l = t.camelCase(t.grep(["on", e, i], function(t) {
        return t
      }).join("-").toLowerCase()),
      u = t.Event([e, "owl", i || "carousel"].join(".").toLowerCase(), t.extend({
        relatedTarget: this
      }, a, n));
    return this._supress[e] || (t.each(this._plugins, function(t, e) {
      e.onTrigger && e.onTrigger(u)
    }), this.register({
      type: o.Type.Event,
      name: e
    }), this.$element.trigger(u), this.settings && "function" == typeof this.settings[l] && this.settings[l].call(this, u)), u
  }, o.prototype.enter = function(e) {
    t.each([e].concat(this._states.tags[e] || []), t.proxy(function(t, e) {
      this._states.current[e] === i && (this._states.current[e] = 0), this._states.current[e]++
    }, this))
  }, o.prototype.leave = function(e) {
    t.each([e].concat(this._states.tags[e] || []), t.proxy(function(t, e) {
      this._states.current[e]--
    }, this))
  }, o.prototype.register = function(e) {
    if (e.type === o.Type.Event) {
      if (t.event.special[e.name] || (t.event.special[e.name] = {}), !t.event.special[e.name].owl) {
        var n = t.event.special[e.name]._default;
        t.event.special[e.name]._default = function(t) {
          return !n || !n.apply || t.namespace && -1 !== t.namespace.indexOf("owl") ? t.namespace && t.namespace.indexOf("owl") > -1 : n.apply(this, arguments)
        }, t.event.special[e.name].owl = !0
      }
    } else e.type === o.Type.State && (this._states.tags[e.name] ? this._states.tags[e.name] = this._states.tags[e.name].concat(e.tags) : this._states.tags[e.name] = e.tags, this._states.tags[e.name] = t.grep(this._states.tags[e.name], t.proxy(function(n, i) {
      return t.inArray(n, this._states.tags[e.name]) === i
    }, this)))
  }, o.prototype.suppress = function(e) {
    t.each(e, t.proxy(function(t, e) {
      this._supress[e] = !0
    }, this))
  }, o.prototype.release = function(e) {
    t.each(e, t.proxy(function(t, e) {
      delete this._supress[e]
    }, this))
  }, o.prototype.pointer = function(t) {
    var n = {
      x: null,
      y: null
    };
    return (t = (t = t.originalEvent || t || e.event).touches && t.touches.length ? t.touches[0] : t.changedTouches && t.changedTouches.length ? t.changedTouches[0] : t).pageX ? (n.x = t.pageX, n.y = t.pageY) : (n.x = t.clientX, n.y = t.clientY), n
  }, o.prototype.isNumeric = function(t) {
    return !isNaN(parseFloat(t))
  }, o.prototype.difference = function(t, e) {
    return {
      x: t.x - e.x,
      y: t.y - e.y
    }
  }, t.fn.owlCarousel = function(e) {
    var n = Array.prototype.slice.call(arguments, 1);
    return this.each(function() {
      var i = t(this),
        r = i.data("owl.carousel");
      r || (r = new o(this, "object" == typeof e && e), i.data("owl.carousel", r), t.each(["next", "prev", "to", "destroy", "refresh", "replace", "add", "remove"], function(e, n) {
        r.register({
          type: o.Type.Event,
          name: n
        }), r.$element.on(n + ".owl.carousel.core", t.proxy(function(t) {
          t.namespace && t.relatedTarget !== this && (this.suppress([n]), r[n].apply(this, [].slice.call(arguments, 1)), this.release([n]))
        }, r))
      })), "string" == typeof e && "_" !== e.charAt(0) && r[e].apply(r, n)
    })
  }, t.fn.owlCarousel.Constructor = o
}(window.Zepto || window.jQuery, window, document),
function(t, e, n, i) {
  var o = function(e) {
    this._core = e, this._interval = null, this._visible = null, this._handlers = {
      "initialized.owl.carousel": t.proxy(function(t) {
        t.namespace && this._core.settings.autoRefresh && this.watch()
      }, this)
    }, this._core.options = t.extend({}, o.Defaults, this._core.options), this._core.$element.on(this._handlers)
  };
  o.Defaults = {
    autoRefresh: !0,
    autoRefreshInterval: 500
  }, o.prototype.watch = function() {
    this._interval || (this._visible = this._core.$element.is(":visible"), this._interval = e.setInterval(t.proxy(this.refresh, this), this._core.settings.autoRefreshInterval))
  }, o.prototype.refresh = function() {
    this._core.$element.is(":visible") !== this._visible && (this._visible = !this._visible, this._core.$element.toggleClass("owl-hidden", !this._visible), this._visible && this._core.invalidate("width") && this._core.refresh())
  }, o.prototype.destroy = function() {
    var t, n;
    for (t in e.clearInterval(this._interval), this._handlers) this._core.$element.off(t, this._handlers[t]);
    for (n in Object.getOwnPropertyNames(this)) "function" != typeof this[n] && (this[n] = null)
  }, t.fn.owlCarousel.Constructor.Plugins.AutoRefresh = o
}(window.Zepto || window.jQuery, window, document),
function(t, e, n, i) {
  var o = function(e) {
    this._core = e, this._loaded = [], this._handlers = {
      "initialized.owl.carousel change.owl.carousel resized.owl.carousel": t.proxy(function(e) {
        if (e.namespace && this._core.settings && this._core.settings.lazyLoad && (e.property && "position" == e.property.name || "initialized" == e.type))
          for (var n = this._core.settings, i = n.center && Math.ceil(n.items / 2) || n.items, o = n.center && -1 * i || 0, r = (e.property && void 0 !== e.property.value ? e.property.value : this._core.current()) + o, s = this._core.clones().length, a = t.proxy(function(t, e) {
              this.load(e)
            }, this); o++ < i;) this.load(s / 2 + this._core.relative(r)), s && t.each(this._core.clones(this._core.relative(r)), a), r++
      }, this)
    }, this._core.options = t.extend({}, o.Defaults, this._core.options), this._core.$element.on(this._handlers)
  };
  o.Defaults = {
    lazyLoad: !1
  }, o.prototype.load = function(n) {
    var i = this._core.$stage.children().eq(n),
      o = i && i.find(".owl-lazy");
    !o || t.inArray(i.get(0), this._loaded) > -1 || (o.each(t.proxy(function(n, i) {
      var o, r = t(i),
        s = e.devicePixelRatio > 1 && r.attr("data-src-retina") || r.attr("data-src");
      this._core.trigger("load", {
        element: r,
        url: s
      }, "lazy"), r.is("img") ? r.one("load.owl.lazy", t.proxy(function() {
        r.css("opacity", 1), this._core.trigger("loaded", {
          element: r,
          url: s
        }, "lazy")
      }, this)).attr("src", s) : ((o = new Image).onload = t.proxy(function() {
        r.css({
          "background-image": 'url("' + s + '")',
          opacity: "1"
        }), this._core.trigger("loaded", {
          element: r,
          url: s
        }, "lazy")
      }, this), o.src = s)
    }, this)), this._loaded.push(i.get(0)))
  }, o.prototype.destroy = function() {
    var t, e;
    for (t in this.handlers) this._core.$element.off(t, this.handlers[t]);
    for (e in Object.getOwnPropertyNames(this)) "function" != typeof this[e] && (this[e] = null)
  }, t.fn.owlCarousel.Constructor.Plugins.Lazy = o
}(window.Zepto || window.jQuery, window, document),
function(t, e, n, i) {
  var o = function(e) {
    this._core = e, this._handlers = {
      "initialized.owl.carousel refreshed.owl.carousel": t.proxy(function(t) {
        t.namespace && this._core.settings.autoHeight && this.update()
      }, this),
      "changed.owl.carousel": t.proxy(function(t) {
        t.namespace && this._core.settings.autoHeight && "position" == t.property.name && this.update()
      }, this),
      "loaded.owl.lazy": t.proxy(function(t) {
        t.namespace && this._core.settings.autoHeight && t.element.closest("." + this._core.settings.itemClass).index() === this._core.current() && this.update()
      }, this)
    }, this._core.options = t.extend({}, o.Defaults, this._core.options), this._core.$element.on(this._handlers)
  };
  o.Defaults = {
    autoHeight: !1,
    autoHeightClass: "owl-height"
  }, o.prototype.update = function() {
    var e, n = this._core._current,
      i = n + this._core.settings.items,
      o = this._core.$stage.children().toArray().slice(n, i),
      r = [];
    t.each(o, function(e, n) {
      r.push(t(n).height())
    }), e = Math.max.apply(null, r), this._core.$stage.parent().height(e).addClass(this._core.settings.autoHeightClass)
  }, o.prototype.destroy = function() {
    var t, e;
    for (t in this._handlers) this._core.$element.off(t, this._handlers[t]);
    for (e in Object.getOwnPropertyNames(this)) "function" != typeof this[e] && (this[e] = null)
  }, t.fn.owlCarousel.Constructor.Plugins.AutoHeight = o
}(window.Zepto || window.jQuery, window, document),
function(t, e, n, i) {
  var o = function(e) {
    this._core = e, this._videos = {}, this._playing = null, this._handlers = {
      "initialized.owl.carousel": t.proxy(function(t) {
        t.namespace && this._core.register({
          type: "state",
          name: "playing",
          tags: ["interacting"]
        })
      }, this),
      "resize.owl.carousel": t.proxy(function(t) {
        t.namespace && this._core.settings.video && this.isInFullScreen() && t.preventDefault()
      }, this),
      "refreshed.owl.carousel": t.proxy(function(t) {
        t.namespace && this._core.is("resizing") && this._core.$stage.find(".cloned .owl-video-frame").remove()
      }, this),
      "changed.owl.carousel": t.proxy(function(t) {
        t.namespace && "position" === t.property.name && this._playing && this.stop()
      }, this),
      "prepared.owl.carousel": t.proxy(function(e) {
        if (e.namespace) {
          var n = t(e.content).find(".owl-video");
          n.length && (n.css("display", "none"), this.fetch(n, t(e.content)))
        }
      }, this)
    }, this._core.options = t.extend({}, o.Defaults, this._core.options), this._core.$element.on(this._handlers), this._core.$element.on("click.owl.video", ".owl-video-play-icon", t.proxy(function(t) {
      this.play(t)
    }, this))
  };
  o.Defaults = {
    video: !1,
    videoHeight: !1,
    videoWidth: !1
  }, o.prototype.fetch = function(t, e) {
    var n = t.attr("data-vimeo-id") ? "vimeo" : t.attr("data-vzaar-id") ? "vzaar" : "youtube",
      i = t.attr("data-vimeo-id") || t.attr("data-youtube-id") || t.attr("data-vzaar-id"),
      o = t.attr("data-width") || this._core.settings.videoWidth,
      r = t.attr("data-height") || this._core.settings.videoHeight,
      s = t.attr("href");
    if (!s) throw new Error("Missing video URL.");
    if ((i = s.match(/(http:|https:|)\/\/(player.|www.|app.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com)|vzaar\.com)\/(video\/|videos\/|embed\/|channels\/.+\/|groups\/.+\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(\&\S+)?/))[3].indexOf("youtu") > -1) n = "youtube";
    else if (i[3].indexOf("vimeo") > -1) n = "vimeo";
    else {
      if (!(i[3].indexOf("vzaar") > -1)) throw new Error("Video URL not supported.");
      n = "vzaar"
    }
    i = i[6], this._videos[s] = {
      type: n,
      id: i,
      width: o,
      height: r
    }, e.attr("data-video", s), this.thumbnail(t, this._videos[s])
  }, o.prototype.thumbnail = function(e, n) {
    var i, o, r = n.width && n.height ? 'style="width:' + n.width + "px;height:" + n.height + 'px;"' : "",
      s = e.find("img"),
      a = "src",
      l = "",
      u = this._core.settings,
      c = function(t) {
        '<div class="owl-video-play-icon"></div>',
        i = u.lazyLoad ? '<div class="owl-video-tn ' + l + '" ' + a + '="' + t + '"></div>' : '<div class="owl-video-tn" style="opacity:1;background-image:url(' + t + ')"></div>',
        e.after(i),
        e.after('<div class="owl-video-play-icon"></div>')
      };
    return e.wrap('<div class="owl-video-wrapper"' + r + "></div>"), this._core.settings.lazyLoad && (a = "data-src", l = "owl-lazy"), s.length ? (c(s.attr(a)), s.remove(), !1) : void("youtube" === n.type ? (o = "//img.youtube.com/vi/" + n.id + "/hqdefault.jpg", c(o)) : "vimeo" === n.type ? t.ajax({
      type: "GET",
      url: "//vimeo.com/api/v2/video/" + n.id + ".json",
      jsonp: "callback",
      dataType: "jsonp",
      success: function(t) {
        o = t[0].thumbnail_large, c(o)
      }
    }) : "vzaar" === n.type && t.ajax({
      type: "GET",
      url: "//vzaar.com/api/videos/" + n.id + ".json",
      jsonp: "callback",
      dataType: "jsonp",
      success: function(t) {
        o = t.framegrab_url, c(o)
      }
    }))
  }, o.prototype.stop = function() {
    this._core.trigger("stop", null, "video"), this._playing.find(".owl-video-frame").remove(), this._playing.removeClass("owl-video-playing"), this._playing = null, this._core.leave("playing"), this._core.trigger("stopped", null, "video")
  }, o.prototype.play = function(e) {
    var n, i = t(e.target).closest("." + this._core.settings.itemClass),
      o = this._videos[i.attr("data-video")],
      r = o.width || "100%",
      s = o.height || this._core.$stage.height();
    this._playing || (this._core.enter("playing"), this._core.trigger("play", null, "video"), i = this._core.items(this._core.relative(i.index())), this._core.reset(i.index()), "youtube" === o.type ? n = '<iframe width="' + r + '" height="' + s + '" src="//www.youtube.com/embed/' + o.id + "?autoplay=1&rel=0&v=" + o.id + '" frameborder="0" allowfullscreen></iframe>' : "vimeo" === o.type ? n = '<iframe src="//player.vimeo.com/video/' + o.id + '?autoplay=1" width="' + r + '" height="' + s + '" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>' : "vzaar" === o.type && (n = '<iframe frameborder="0"height="' + s + '"width="' + r + '" allowfullscreen mozallowfullscreen webkitAllowFullScreen src="//view.vzaar.com/' + o.id + '/player?autoplay=true"></iframe>'), t('<div class="owl-video-frame">' + n + "</div>").insertAfter(i.find(".owl-video")), this._playing = i.addClass("owl-video-playing"))
  }, o.prototype.isInFullScreen = function() {
    var e = n.fullscreenElement || n.mozFullScreenElement || n.webkitFullscreenElement;
    return e && t(e).parent().hasClass("owl-video-frame")
  }, o.prototype.destroy = function() {
    var t, e;
    for (t in this._core.$element.off("click.owl.video"), this._handlers) this._core.$element.off(t, this._handlers[t]);
    for (e in Object.getOwnPropertyNames(this)) "function" != typeof this[e] && (this[e] = null)
  }, t.fn.owlCarousel.Constructor.Plugins.Video = o
}(window.Zepto || window.jQuery, window, document),
function(t, e, n, i) {
  var o = function(e) {
    this.core = e, this.core.options = t.extend({}, o.Defaults, this.core.options), this.swapping = !0, this.previous = i, this.next = i, this.handlers = {
      "change.owl.carousel": t.proxy(function(t) {
        t.namespace && "position" == t.property.name && (this.previous = this.core.current(), this.next = t.property.value)
      }, this),
      "drag.owl.carousel dragged.owl.carousel translated.owl.carousel": t.proxy(function(t) {
        t.namespace && (this.swapping = "translated" == t.type)
      }, this),
      "translate.owl.carousel": t.proxy(function(t) {
        t.namespace && this.swapping && (this.core.options.animateOut || this.core.options.animateIn) && this.swap()
      }, this)
    }, this.core.$element.on(this.handlers)
  };
  o.Defaults = {
    animateOut: !1,
    animateIn: !1
  }, o.prototype.swap = function() {
    if (1 === this.core.settings.items && t.support.animation && t.support.transition) {
      this.core.speed(0);
      var e, n = t.proxy(this.clear, this),
        i = this.core.$stage.children().eq(this.previous),
        o = this.core.$stage.children().eq(this.next),
        r = this.core.settings.animateIn,
        s = this.core.settings.animateOut;
      this.core.current() !== this.previous && (s && (e = this.core.coordinates(this.previous) - this.core.coordinates(this.next), i.one(t.support.animation.end, n).css({
        left: e + "px"
      }).addClass("animated owl-animated-out").addClass(s)), r && o.one(t.support.animation.end, n).addClass("animated owl-animated-in").addClass(r))
    }
  }, o.prototype.clear = function(e) {
    t(e.target).css({
      left: ""
    }).removeClass("animated owl-animated-out owl-animated-in").removeClass(this.core.settings.animateIn).removeClass(this.core.settings.animateOut), this.core.onTransitionEnd()
  }, o.prototype.destroy = function() {
    var t, e;
    for (t in this.handlers) this.core.$element.off(t, this.handlers[t]);
    for (e in Object.getOwnPropertyNames(this)) "function" != typeof this[e] && (this[e] = null)
  }, t.fn.owlCarousel.Constructor.Plugins.Animate = o
}(window.Zepto || window.jQuery, window, document),
function(t, e, n, i) {
  var o = function(e) {
    this._core = e, this._timeout = null, this._paused = !1, this._handlers = {
      "changed.owl.carousel": t.proxy(function(t) {
        t.namespace && "settings" === t.property.name ? this._core.settings.autoplay ? this.play() : this.stop() : t.namespace && "position" === t.property.name && this._core.settings.autoplay && this._setAutoPlayInterval()
      }, this),
      "initialized.owl.carousel": t.proxy(function(t) {
        t.namespace && this._core.settings.autoplay && this.play()
      }, this),
      "play.owl.autoplay": t.proxy(function(t, e, n) {
        t.namespace && this.play(e, n)
      }, this),
      "stop.owl.autoplay": t.proxy(function(t) {
        t.namespace && this.stop()
      }, this),
      "mouseover.owl.autoplay": t.proxy(function() {
        this._core.settings.autoplayHoverPause && this._core.is("rotating") && this.pause()
      }, this),
      "mouseleave.owl.autoplay": t.proxy(function() {
        this._core.settings.autoplayHoverPause && this._core.is("rotating") && this.play()
      }, this),
      "touchstart.owl.core": t.proxy(function() {
        this._core.settings.autoplayHoverPause && this._core.is("rotating") && this.pause()
      }, this),
      "touchend.owl.core": t.proxy(function() {
        this._core.settings.autoplayHoverPause && this.play()
      }, this)
    }, this._core.$element.on(this._handlers), this._core.options = t.extend({}, o.Defaults, this._core.options)
  };
  o.Defaults = {
    autoplay: !1,
    autoplayTimeout: 5e3,
    autoplayHoverPause: !1,
    autoplaySpeed: !1
  }, o.prototype.play = function(t, e) {
    this._paused = !1, this._core.is("rotating") || (this._core.enter("rotating"), this._setAutoPlayInterval())
  }, o.prototype._getNextTimeout = function(i, o) {
    return this._timeout && e.clearTimeout(this._timeout), e.setTimeout(t.proxy(function() {
      this._paused || this._core.is("busy") || this._core.is("interacting") || n.hidden || this._core.next(o || this._core.settings.autoplaySpeed)
    }, this), i || this._core.settings.autoplayTimeout)
  }, o.prototype._setAutoPlayInterval = function() {
    this._timeout = this._getNextTimeout()
  }, o.prototype.stop = function() {
    this._core.is("rotating") && (e.clearTimeout(this._timeout), this._core.leave("rotating"))
  }, o.prototype.pause = function() {
    this._core.is("rotating") && (this._paused = !0)
  }, o.prototype.destroy = function() {
    var t, e;
    for (t in this.stop(), this._handlers) this._core.$element.off(t, this._handlers[t]);
    for (e in Object.getOwnPropertyNames(this)) "function" != typeof this[e] && (this[e] = null)
  }, t.fn.owlCarousel.Constructor.Plugins.autoplay = o
}(window.Zepto || window.jQuery, window, document),
function(t, e, n, i) {
  "use strict";
  var o = function(e) {
    this._core = e, this._initialized = !1, this._pages = [], this._controls = {}, this._templates = [], this.$element = this._core.$element, this._overrides = {
      next: this._core.next,
      prev: this._core.prev,
      to: this._core.to
    }, this._handlers = {
      "prepared.owl.carousel": t.proxy(function(e) {
        e.namespace && this._core.settings.dotsData && this._templates.push('<div class="' + this._core.settings.dotClass + '">' + t(e.content).find("[data-dot]").addBack("[data-dot]").attr("data-dot") + "</div>")
      }, this),
      "added.owl.carousel": t.proxy(function(t) {
        t.namespace && this._core.settings.dotsData && this._templates.splice(t.position, 0, this._templates.pop())
      }, this),
      "remove.owl.carousel": t.proxy(function(t) {
        t.namespace && this._core.settings.dotsData && this._templates.splice(t.position, 1)
      }, this),
      "changed.owl.carousel": t.proxy(function(t) {
        t.namespace && "position" == t.property.name && this.draw()
      }, this),
      "initialized.owl.carousel": t.proxy(function(t) {
        t.namespace && !this._initialized && (this._core.trigger("initialize", null, "navigation"), this.initialize(), this.update(), this.draw(), this._initialized = !0, this._core.trigger("initialized", null, "navigation"))
      }, this),
      "refreshed.owl.carousel": t.proxy(function(t) {
        t.namespace && this._initialized && (this._core.trigger("refresh", null, "navigation"), this.update(), this.draw(), this._core.trigger("refreshed", null, "navigation"))
      }, this)
    }, this._core.options = t.extend({}, o.Defaults, this._core.options), this.$element.on(this._handlers)
  };
  o.Defaults = {
    nav: !1,
    navText: ["prev", "next"],
    navSpeed: !1,
    navElement: "div",
    navContainer: !1,
    navContainerClass: "owl-nav",
    navClass: ["owl-prev", "owl-next"],
    slideBy: 1,
    dotClass: "owl-dot",
    dotsClass: "owl-dots",
    dots: !0,
    dotsEach: !1,
    dotsData: !1,
    dotsSpeed: !1,
    dotsContainer: !1
  }, o.prototype.initialize = function() {
    var e, n = this._core.settings;
    for (e in this._controls.$relative = (n.navContainer ? t(n.navContainer) : t("<div>").addClass(n.navContainerClass).appendTo(this.$element)).addClass("disabled"), this._controls.$previous = t("<" + n.navElement + ">").addClass(n.navClass[0]).html(n.navText[0]).prependTo(this._controls.$relative).on("click", t.proxy(function(t) {
        this.prev(n.navSpeed)
      }, this)), this._controls.$next = t("<" + n.navElement + ">").addClass(n.navClass[1]).html(n.navText[1]).appendTo(this._controls.$relative).on("click", t.proxy(function(t) {
        this.next(n.navSpeed)
      }, this)), n.dotsData || (this._templates = [t("<div>").addClass(n.dotClass).append(t("<span>")).prop("outerHTML")]), this._controls.$absolute = (n.dotsContainer ? t(n.dotsContainer) : t("<div>").addClass(n.dotsClass).appendTo(this.$element)).addClass("disabled"), this._controls.$absolute.on("click", "div", t.proxy(function(e) {
        var i = t(e.target).parent().is(this._controls.$absolute) ? t(e.target).index() : t(e.target).parent().index();
        e.preventDefault(), this.to(i, n.dotsSpeed)
      }, this)), this._overrides) this._core[e] = t.proxy(this[e], this)
  }, o.prototype.destroy = function() {
    var t, e, n, i;
    for (t in this._handlers) this.$element.off(t, this._handlers[t]);
    for (e in this._controls) this._controls[e].remove();
    for (i in this.overides) this._core[i] = this._overrides[i];
    for (n in Object.getOwnPropertyNames(this)) "function" != typeof this[n] && (this[n] = null)
  }, o.prototype.update = function() {
    var t, e, n = this._core.clones().length / 2,
      i = n + this._core.items().length,
      o = this._core.maximum(!0),
      r = this._core.settings,
      s = r.center || r.autoWidth || r.dotsData ? 1 : r.dotsEach || r.items;
    if ("page" !== r.slideBy && (r.slideBy = Math.min(r.slideBy, r.items)), r.dots || "page" == r.slideBy)
      for (this._pages = [], t = n, e = 0, 0; t < i; t++) {
        if (e >= s || 0 === e) {
          if (this._pages.push({
              start: Math.min(o, t - n),
              end: t - n + s - 1
            }), Math.min(o, t - n) === o) break;
          e = 0, 0
        }
        e += this._core.mergers(this._core.relative(t))
      }
  }, o.prototype.draw = function() {
    var e, n = this._core.settings,
      i = this._core.items().length <= n.items,
      o = this._core.relative(this._core.current()),
      r = n.loop || n.rewind;
    this._controls.$relative.toggleClass("disabled", !n.nav || i), n.nav && (this._controls.$previous.toggleClass("disabled", !r && o <= this._core.minimum(!0)), this._controls.$next.toggleClass("disabled", !r && o >= this._core.maximum(!0))), this._controls.$absolute.toggleClass("disabled", !n.dots || i), n.dots && (e = this._pages.length - this._controls.$absolute.children().length, n.dotsData && 0 !== e ? this._controls.$absolute.html(this._templates.join("")) : e > 0 ? this._controls.$absolute.append(new Array(e + 1).join(this._templates[0])) : e < 0 && this._controls.$absolute.children().slice(e).remove(), this._controls.$absolute.find(".active").removeClass("active"), this._controls.$absolute.children().eq(t.inArray(this.current(), this._pages)).addClass("active"))
  }, o.prototype.onTrigger = function(e) {
    var n = this._core.settings;
    e.page = {
      index: t.inArray(this.current(), this._pages),
      count: this._pages.length,
      size: n && (n.center || n.autoWidth || n.dotsData ? 1 : n.dotsEach || n.items)
    }
  }, o.prototype.current = function() {
    var e = this._core.relative(this._core.current());
    return t.grep(this._pages, t.proxy(function(t, n) {
      return t.start <= e && t.end >= e
    }, this)).pop()
  }, o.prototype.getPosition = function(e) {
    var n, i, o = this._core.settings;
    return "page" == o.slideBy ? (n = t.inArray(this.current(), this._pages), i = this._pages.length, e ? ++n : --n, n = this._pages[(n % i + i) % i].start) : (n = this._core.relative(this._core.current()), i = this._core.items().length, e ? n += o.slideBy : n -= o.slideBy), n
  }, o.prototype.next = function(e) {
    t.proxy(this._overrides.to, this._core)(this.getPosition(!0), e)
  }, o.prototype.prev = function(e) {
    t.proxy(this._overrides.to, this._core)(this.getPosition(!1), e)
  }, o.prototype.to = function(e, n, i) {
    var o;
    !i && this._pages.length ? (o = this._pages.length, t.proxy(this._overrides.to, this._core)(this._pages[(e % o + o) % o].start, n)) : t.proxy(this._overrides.to, this._core)(e, n)
  }, t.fn.owlCarousel.Constructor.Plugins.Navigation = o
}(window.Zepto || window.jQuery, window, document),
function(t, e, n, i) {
  "use strict";
  var o = function(n) {
    this._core = n, this._hashes = {}, this.$element = this._core.$element, this._handlers = {
      "initialized.owl.carousel": t.proxy(function(n) {
        n.namespace && "URLHash" === this._core.settings.startPosition && t(e).trigger("hashchange.owl.navigation")
      }, this),
      "prepared.owl.carousel": t.proxy(function(e) {
        if (e.namespace) {
          var n = t(e.content).find("[data-hash]").addBack("[data-hash]").attr("data-hash");
          if (!n) return;
          this._hashes[n] = e.content
        }
      }, this),
      "changed.owl.carousel": t.proxy(function(n) {
        if (n.namespace && "position" === n.property.name) {
          var i = this._core.items(this._core.relative(this._core.current())),
            o = t.map(this._hashes, function(t, e) {
              return t === i ? e : null
            }).join();
          if (!o || e.location.hash.slice(1) === o) return;
          e.location.hash = o
        }
      }, this)
    }, this._core.options = t.extend({}, o.Defaults, this._core.options), this.$element.on(this._handlers), t(e).on("hashchange.owl.navigation", t.proxy(function(t) {
      var n = e.location.hash.substring(1),
        i = this._core.$stage.children(),
        o = this._hashes[n] && i.index(this._hashes[n]);
      void 0 !== o && o !== this._core.current() && this._core.to(this._core.relative(o), !1, !0)
    }, this))
  };
  o.Defaults = {
    URLhashListener: !1
  }, o.prototype.destroy = function() {
    var n, i;
    for (n in t(e).off("hashchange.owl.navigation"), this._handlers) this._core.$element.off(n, this._handlers[n]);
    for (i in Object.getOwnPropertyNames(this)) "function" != typeof this[i] && (this[i] = null)
  }, t.fn.owlCarousel.Constructor.Plugins.Hash = o
}(window.Zepto || window.jQuery, window, document),
function(t, e, n, i) {
  function o(e, n) {
    var o = !1,
      r = e.charAt(0).toUpperCase() + e.slice(1);
    return t.each((e + " " + a.join(r + " ") + r).split(" "), function(t, e) {
      if (s[e] !== i) return o = !n || e, !1
    }), o
  }

  function r(t) {
    return o(t, !0)
  }
  var s = t("<support>").get(0).style,
    a = "Webkit Moz O ms".split(" "),
    l = {
      transition: {
        end: {
          WebkitTransition: "webkitTransitionEnd",
          MozTransition: "transitionend",
          OTransition: "oTransitionEnd",
          transition: "transitionend"
        }
      },
      animation: {
        end: {
          WebkitAnimation: "webkitAnimationEnd",
          MozAnimation: "animationend",
          OAnimation: "oAnimationEnd",
          animation: "animationend"
        }
      }
    },
    u = function() {
      return !!o("transform")
    },
    c = function() {
      return !!o("perspective")
    },
    d = function() {
      return !!o("animation")
    };
  (function() {
    return !!o("transition")
  })() && (t.support.transition = new String(r("transition")), t.support.transition.end = l.transition.end[t.support.transition]), d() && (t.support.animation = new String(r("animation")), t.support.animation.end = l.animation.end[t.support.animation]), u() && (t.support.transform = new String(r("transform")), t.support.transform3d = c())
}(window.Zepto || window.jQuery, window, document),
function(t, e) {
  "function" == typeof define && define.amd ? define(e) : "object" == typeof exports ? module.exports = e() : t.PhotoSwipeUI_Default = e()
}(this, function() {
  "use strict";
  return function(t, e) {
    var n, i, o, r, s, a, l, u, c, d, f, h, p, m, g, v, y, b, _ = this,
      w = !1,
      x = !0,
      C = !0,
      E = {
        barsSize: {
          top: 44,
          bottom: "auto"
        },
        closeElClasses: ["item", "caption", "zoom-wrap", "ui", "top-bar"],
        timeToIdle: 4e3,
        timeToIdleOutside: 1e3,
        loadingIndicatorDelay: 1e3,
        addCaptionHTMLFn: function(t, e) {
          return t.title ? (e.children[0].innerHTML = t.title, !0) : (e.children[0].innerHTML = "", !1)
        },
        closeEl: !0,
        captionEl: !0,
        fullscreenEl: !0,
        zoomEl: !0,
        shareEl: !0,
        counterEl: !0,
        arrowEl: !0,
        preloaderEl: !0,
        tapToClose: !1,
        tapToToggleControls: !0,
        clickToCloseNonZoomable: !0,
        shareButtons: [{
          id: "facebook",
          label: "Share on Facebook",
          url: "https://www.facebook.com/sharer/sharer.php?u={{url}}"
        }, {
          id: "twitter",
          label: "Tweet",
          url: "https://twitter.com/intent/tweet?text={{text}}&url={{url}}"
        }, {
          id: "pinterest",
          label: "Pin it",
          url: "http://www.pinterest.com/pin/create/button/?url={{url}}&media={{image_url}}&description={{text}}"
        }, {
          id: "download",
          label: "Download image",
          url: "{{raw_image_url}}",
          download: !0
        }],
        getImageURLForShare: function() {
          return t.currItem.src || ""
        },
        getPageURLForShare: function() {
          return window.location.href
        },
        getTextForShare: function() {
          return t.currItem.title || ""
        },
        indexIndicatorSep: " / ",
        fitControlsWidth: 1200
      },
      T = function(t) {
        if (v) return !0;
        t = t || window.event, g.timeToIdle && g.mouseUsed && !c && j();
        for (var n, i, o = (t.target || t.srcElement).getAttribute("class") || "", r = 0; r < R.length; r++)(n = R[r]).onTap && o.indexOf("pswp__" + n.name) > -1 && (n.onTap(), i = !0);
        if (i) {
          t.stopPropagation && t.stopPropagation(), v = !0;
          var s = e.features.isOldAndroid ? 600 : 30;
          setTimeout(function() {
            v = !1
          }, s)
        }
      },
      S = function() {
        return !t.likelyTouchDevice || g.mouseUsed || screen.width > g.fitControlsWidth
      },
      k = function(t, n, i) {
        e[(i ? "add" : "remove") + "Class"](t, "pswp__" + n)
      },
      I = function() {
        var t = 1 === g.getNumItemsFn();
        t !== m && (k(i, "ui--one-slide", t), m = t)
      },
      A = function() {
        k(l, "share-modal--hidden", C)
      },
      D = function() {
        return (C = !C) ? (e.removeClass(l, "pswp__share-modal--fade-in"), setTimeout(function() {
          C && A()
        }, 300)) : (A(), setTimeout(function() {
          C || e.addClass(l, "pswp__share-modal--fade-in")
        }, 30)), C || O(), !1
      },
      L = function(e) {
        var n = (e = e || window.event).target || e.srcElement;
        return t.shout("shareLinkClick", e, n), !(!n.href || !n.hasAttribute("download") && (window.open(n.href, "pswp_share", "scrollbars=yes,resizable=yes,toolbar=no,location=yes,width=550,height=420,top=100,left=" + (window.screen ? Math.round(screen.width / 2 - 275) : 100)), C || D(), 1))
      },
      O = function() {
        for (var t, e, n, i, o = "", r = 0; r < g.shareButtons.length; r++) t = g.shareButtons[r], e = g.getImageURLForShare(t), n = g.getPageURLForShare(t), i = g.getTextForShare(t), o += '<a href="' + t.url.replace("{{url}}", encodeURIComponent(n)).replace("{{image_url}}", encodeURIComponent(e)).replace("{{raw_image_url}}", e).replace("{{text}}", encodeURIComponent(i)) + '" target="_blank" class="pswp__share--' + t.id + '"' + (t.download ? "download" : "") + ">" + t.label + "</a>", g.parseShareButtonOut && (o = g.parseShareButtonOut(t, o));
        l.children[0].innerHTML = o, l.children[0].onclick = L
      },
      P = function(t) {
        for (var n = 0; n < g.closeElClasses.length; n++)
          if (e.hasClass(t, "pswp__" + g.closeElClasses[n])) return !0
      },
      N = 0,
      j = function() {
        clearTimeout(b), N = 0, c && _.setIdle(!1)
      },
      z = function(t) {
        var e = (t = t || window.event).relatedTarget || t.toElement;
        e && "HTML" !== e.nodeName || (clearTimeout(b), b = setTimeout(function() {
          _.setIdle(!0)
        }, g.timeToIdleOutside))
      },
      M = function(t) {
        h !== t && (k(f, "preloader--active", !t), h = t)
      },
      $ = function(t) {
        var n = t.vGap;
        if (S()) {
          var s = g.barsSize;
          if (g.captionEl && "auto" === s.bottom)
            if (r || ((r = e.createEl("pswp__caption pswp__caption--fake")).appendChild(e.createEl("pswp__caption__center")), i.insertBefore(r, o), e.addClass(i, "pswp__ui--fit")), g.addCaptionHTMLFn(t, r, !0)) {
              var a = r.clientHeight;
              n.bottom = parseInt(a, 10) || 44
            } else n.bottom = s.top;
          else n.bottom = "auto" === s.bottom ? 0 : s.bottom;
          n.top = s.top
        } else n.top = n.bottom = 0
      },
      R = [{
        name: "caption",
        option: "captionEl",
        onInit: function(t) {
          o = t
        }
      }, {
        name: "share-modal",
        option: "shareEl",
        onInit: function(t) {
          l = t
        },
        onTap: function() {
          D()
        }
      }, {
        name: "button--share",
        option: "shareEl",
        onInit: function(t) {
          a = t
        },
        onTap: function() {
          D()
        }
      }, {
        name: "button--zoom",
        option: "zoomEl",
        onTap: t.toggleDesktopZoom
      }, {
        name: "counter",
        option: "counterEl",
        onInit: function(t) {
          s = t
        }
      }, {
        name: "button--close",
        option: "closeEl",
        onTap: t.close
      }, {
        name: "button--arrow--left",
        option: "arrowEl",
        onTap: t.prev
      }, {
        name: "button--arrow--right",
        option: "arrowEl",
        onTap: t.next
      }, {
        name: "button--fs",
        option: "fullscreenEl",
        onTap: function() {
          n.isFullscreen() ? n.exit() : n.enter()
        }
      }, {
        name: "preloader",
        option: "preloaderEl",
        onInit: function(t) {
          f = t
        }
      }];
    _.init = function() {
      e.extend(t.options, E, !0), g = t.options, i = e.getChildByClass(t.scrollWrap, "pswp__ui"), d = t.listen,
        function() {
          var t;
          d("onVerticalDrag", function(t) {
            x && .95 > t ? _.hideControls() : !x && t >= .95 && _.showControls()
          }), d("onPinchClose", function(e) {
            x && .9 > e ? (_.hideControls(), t = !0) : t && !x && e > .9 && _.showControls()
          }), d("zoomGestureEnded", function() {
            (t = !1) && !x && _.showControls()
          })
        }(), d("beforeChange", _.update), d("doubleTap", function(e) {
          var n = t.currItem.initialZoomLevel;
          t.getZoomLevel() !== n ? t.zoomTo(n, e, 333) : t.zoomTo(g.getDoubleTapZoom(!1, t.currItem), e, 333)
        }), d("preventDragEvent", function(t, e, n) {
          var i = t.target || t.srcElement;
          i && i.getAttribute("class") && t.type.indexOf("mouse") > -1 && (i.getAttribute("class").indexOf("__caption") > 0 || /(SMALL|STRONG|EM)/i.test(i.tagName)) && (n.prevent = !1)
        }), d("bindEvents", function() {
          e.bind(i, "pswpTap click", T), e.bind(t.scrollWrap, "pswpTap", _.onGlobalTap), t.likelyTouchDevice || e.bind(t.scrollWrap, "mouseover", _.onMouseOver)
        }), d("unbindEvents", function() {
          C || D(), y && clearInterval(y), e.unbind(document, "mouseout", z), e.unbind(document, "mousemove", j), e.unbind(i, "pswpTap click", T), e.unbind(t.scrollWrap, "pswpTap", _.onGlobalTap), e.unbind(t.scrollWrap, "mouseover", _.onMouseOver), n && (e.unbind(document, n.eventK, _.updateFullscreen), n.isFullscreen() && (g.hideAnimationDuration = 0, n.exit()), n = null)
        }), d("destroy", function() {
          g.captionEl && (r && i.removeChild(r), e.removeClass(o, "pswp__caption--empty")), l && (l.children[0].onclick = null), e.removeClass(i, "pswp__ui--over-close"), e.addClass(i, "pswp__ui--hidden"), _.setIdle(!1)
        }), g.showAnimationDuration || e.removeClass(i, "pswp__ui--hidden"), d("initialZoomIn", function() {
          g.showAnimationDuration && e.removeClass(i, "pswp__ui--hidden")
        }), d("initialZoomOut", function() {
          e.addClass(i, "pswp__ui--hidden")
        }), d("parseVerticalMargin", $),
        function() {
          var t, n, o, r = function(i) {
            if (i)
              for (var r = i.length, s = 0; r > s; s++) {
                t = i[s], n = t.className;
                for (var a = 0; a < R.length; a++) o = R[a], n.indexOf("pswp__" + o.name) > -1 && (g[o.option] ? (e.removeClass(t, "pswp__element--disabled"), o.onInit && o.onInit(t)) : e.addClass(t, "pswp__element--disabled"))
              }
          };
          r(i.children);
          var s = e.getChildByClass(i, "pswp__top-bar");
          s && r(s.children)
        }(), g.shareEl && a && l && (C = !0), I(), g.timeToIdle && d("mouseUsed", function() {
          e.bind(document, "mousemove", j), e.bind(document, "mouseout", z), y = setInterval(function() {
            2 == ++N && _.setIdle(!0)
          }, g.timeToIdle / 2)
        }), g.fullscreenEl && !e.features.isOldAndroid && (n || (n = _.getFullscreenAPI()), n ? (e.bind(document, n.eventK, _.updateFullscreen), _.updateFullscreen(), e.addClass(t.template, "pswp--supports-fs")) : e.removeClass(t.template, "pswp--supports-fs")), g.preloaderEl && (M(!0), d("beforeChange", function() {
          clearTimeout(p), p = setTimeout(function() {
            t.currItem && t.currItem.loading ? (!t.allowProgressiveImg() || t.currItem.img && !t.currItem.img.naturalWidth) && M(!1) : M(!0)
          }, g.loadingIndicatorDelay)
        }), d("imageLoadComplete", function(e, n) {
          t.currItem === n && M(!0)
        }))
    }, _.setIdle = function(t) {
      c = t, k(i, "ui--idle", t)
    }, _.update = function() {
      x && t.currItem ? (_.updateIndexIndicator(), g.captionEl && (g.addCaptionHTMLFn(t.currItem, o), k(o, "caption--empty", !t.currItem.title)), w = !0) : w = !1, C || D(), I()
    }, _.updateFullscreen = function(i) {
      i && setTimeout(function() {
        t.setScrollOffset(0, e.getScrollY())
      }, 50), e[(n.isFullscreen() ? "add" : "remove") + "Class"](t.template, "pswp--fs")
    }, _.updateIndexIndicator = function() {
      g.counterEl && (s.innerHTML = t.getCurrentIndex() + 1 + g.indexIndicatorSep + g.getNumItemsFn())
    }, _.onGlobalTap = function(n) {
      var i = (n = n || window.event).target || n.srcElement;
      if (!v)
        if (n.detail && "mouse" === n.detail.pointerType) {
          if (P(i)) return void t.close();
          e.hasClass(i, "pswp__img") && (1 === t.getZoomLevel() && t.getZoomLevel() <= t.currItem.fitRatio ? g.clickToCloseNonZoomable && t.close() : t.toggleDesktopZoom(n.detail.releasePoint))
        } else if (g.tapToToggleControls && (x ? _.hideControls() : _.showControls()), g.tapToClose && (e.hasClass(i, "pswp__img") || P(i))) return void t.close()
    }, _.onMouseOver = function(t) {
      var e = (t = t || window.event).target || t.srcElement;
      k(i, "ui--over-close", P(e))
    }, _.hideControls = function() {
      e.addClass(i, "pswp__ui--hidden"), x = !1
    }, _.showControls = function() {
      x = !0, w || _.update(), e.removeClass(i, "pswp__ui--hidden")
    }, _.supportsFullscreen = function() {
      var t = document;
      return !!(t.exitFullscreen || t.mozCancelFullScreen || t.webkitExitFullscreen || t.msExitFullscreen)
    }, _.getFullscreenAPI = function() {
      var e, n = document.documentElement,
        i = "fullscreenchange";
      return n.requestFullscreen ? e = {
        enterK: "requestFullscreen",
        exitK: "exitFullscreen",
        elementK: "fullscreenElement",
        eventK: i
      } : n.mozRequestFullScreen ? e = {
        enterK: "mozRequestFullScreen",
        exitK: "mozCancelFullScreen",
        elementK: "mozFullScreenElement",
        eventK: "moz" + i
      } : n.webkitRequestFullscreen ? e = {
        enterK: "webkitRequestFullscreen",
        exitK: "webkitExitFullscreen",
        elementK: "webkitFullscreenElement",
        eventK: "webkit" + i
      } : n.msRequestFullscreen && (e = {
        enterK: "msRequestFullscreen",
        exitK: "msExitFullscreen",
        elementK: "msFullscreenElement",
        eventK: "MSFullscreenChange"
      }), e && (e.enter = function() {
        return u = g.closeOnScroll, g.closeOnScroll = !1, "webkitRequestFullscreen" !== this.enterK ? t.template[this.enterK]() : void t.template[this.enterK](Element.ALLOW_KEYBOARD_INPUT)
      }, e.exit = function() {
        return g.closeOnScroll = u, document[this.exitK]()
      }, e.isFullscreen = function() {
        return document[this.elementK]
      }), e
    }
  }
}),
function(t) {
  "use strict";

  function e(t) {
    var e = t.length,
      i = n.type(t);
    return "function" !== i && !n.isWindow(t) && (!(1 !== t.nodeType || !e) || "array" === i || 0 === e || "number" == typeof e && e > 0 && e - 1 in t)
  }
  if (!t.jQuery) {
    var n = function(t, e) {
      return new n.fn.init(t, e)
    };
    n.isWindow = function(t) {
      return t && t === t.window
    }, n.type = function(t) {
      return t ? "object" == typeof t || "function" == typeof t ? o[s.call(t)] || "object" : typeof t : t + ""
    }, n.isArray = Array.isArray || function(t) {
      return "array" === n.type(t)
    }, n.isPlainObject = function(t) {
      var e;
      if (!t || "object" !== n.type(t) || t.nodeType || n.isWindow(t)) return !1;
      try {
        if (t.constructor && !r.call(t, "constructor") && !r.call(t.constructor.prototype, "isPrototypeOf")) return !1
      } catch (t) {
        return !1
      }
      for (e in t);
      return void 0 === e || r.call(t, e)
    }, n.each = function(t, n, i) {
      var o = 0,
        r = t.length,
        s = e(t);
      if (i) {
        if (s)
          for (; o < r && !1 !== n.apply(t[o], i); o++);
        else
          for (o in t)
            if (t.hasOwnProperty(o) && !1 === n.apply(t[o], i)) break
      } else if (s)
        for (; o < r && !1 !== n.call(t[o], o, t[o]); o++);
      else
        for (o in t)
          if (t.hasOwnProperty(o) && !1 === n.call(t[o], o, t[o])) break;
      return t
    }, n.data = function(t, e, o) {
      if (void 0 === o) {
        var r = t[n.expando],
          s = r && i[r];
        if (void 0 === e) return s;
        if (s && e in s) return s[e]
      } else if (void 0 !== e) {
        var a = t[n.expando] || (t[n.expando] = ++n.uuid);
        return i[a] = i[a] || {}, i[a][e] = o, o
      }
    }, n.removeData = function(t, e) {
      var o = t[n.expando],
        r = o && i[o];
      r && (e ? n.each(e, function(t, e) {
        delete r[e]
      }) : delete i[o])
    }, n.extend = function() {
      var t, e, i, o, r, s, a = arguments[0] || {},
        l = 1,
        u = arguments.length,
        c = !1;
      for ("boolean" == typeof a && (c = a, a = arguments[l] || {}, l++), "object" != typeof a && "function" !== n.type(a) && (a = {}), l === u && (a = this, l--); l < u; l++)
        if (r = arguments[l])
          for (o in r) r.hasOwnProperty(o) && (t = a[o], a !== (i = r[o]) && (c && i && (n.isPlainObject(i) || (e = n.isArray(i))) ? (e ? (e = !1, s = t && n.isArray(t) ? t : []) : s = t && n.isPlainObject(t) ? t : {}, a[o] = n.extend(c, s, i)) : void 0 !== i && (a[o] = i)));
      return a
    }, n.queue = function(t, i, o) {
      if (t) {
        i = (i || "fx") + "queue";
        var r = n.data(t, i);
        return o ? (!r || n.isArray(o) ? r = n.data(t, i, function(t, n) {
          var i = n || [];
          return t && (e(Object(t)) ? function(t, e) {
            for (var n = +e.length, i = 0, o = t.length; i < n;) t[o++] = e[i++];
            if (n != n)
              for (; void 0 !== e[i];) t[o++] = e[i++];
            t.length = o
          }(i, "string" == typeof t ? [t] : t) : [].push.call(i, t)), i
        }(o)) : r.push(o), r) : r || []
      }
    }, n.dequeue = function(t, e) {
      n.each(t.nodeType ? [t] : t, function(t, i) {
        e = e || "fx";
        var o = n.queue(i, e),
          r = o.shift();
        "inprogress" === r && (r = o.shift()), r && ("fx" === e && o.unshift("inprogress"), r.call(i, function() {
          n.dequeue(i, e)
        }))
      })
    }, n.fn = n.prototype = {
      init: function(t) {
        if (t.nodeType) return this[0] = t, this;
        throw new Error("Not a DOM node.")
      },
      offset: function() {
        var e = this[0].getBoundingClientRect ? this[0].getBoundingClientRect() : {
          top: 0,
          left: 0
        };
        return {
          top: e.top + (t.pageYOffset || document.scrollTop || 0) - (document.clientTop || 0),
          left: e.left + (t.pageXOffset || document.scrollLeft || 0) - (document.clientLeft || 0)
        }
      },
      position: function() {
        var t = this[0],
          e = function(t) {
            for (var e = t.offsetParent; e && "html" !== e.nodeName.toLowerCase() && e.style && "static" === e.style.position;) e = e.offsetParent;
            return e || document
          }(t),
          i = this.offset(),
          o = /^(?:body|html)$/i.test(e.nodeName) ? {
            top: 0,
            left: 0
          } : n(e).offset();
        return i.top -= parseFloat(t.style.marginTop) || 0, i.left -= parseFloat(t.style.marginLeft) || 0, e.style && (o.top += parseFloat(e.style.borderTopWidth) || 0, o.left += parseFloat(e.style.borderLeftWidth) || 0), {
          top: i.top - o.top,
          left: i.left - o.left
        }
      }
    };
    var i = {};
    n.expando = "velocity" + (new Date).getTime(), n.uuid = 0;
    for (var o = {}, r = o.hasOwnProperty, s = o.toString, a = "Boolean Number String Function Array Date RegExp Object Error".split(" "), l = 0; l < a.length; l++) o["[object " + a[l] + "]"] = a[l].toLowerCase();
    n.fn.init.prototype = n.fn, t.Velocity = {
      Utilities: n
    }
  }
}(window),
function(t) {
  "use strict";
  "object" == typeof module && "object" == typeof module.exports ? module.exports = t() : "function" == typeof define && define.amd ? define(t) : t()
}(function() {
  "use strict";
  return function(t, e, n, i) {
    function o(t) {
      return v.isWrapped(t) ? t = g.call(t) : v.isNode(t) && (t = [t]), t
    }

    function r(t) {
      var e = f.data(t, "velocity");
      return null === e ? i : e
    }

    function s(t, e) {
      var n = r(t);
      n && n.delayTimer && !n.delayPaused && (n.delayRemaining = n.delay - e + n.delayBegin, n.delayPaused = !0, clearTimeout(n.delayTimer.setTimeout))
    }

    function a(t, e) {
      var n = r(t);
      n && n.delayTimer && n.delayPaused && (n.delayPaused = !1, n.delayTimer.setTimeout = setTimeout(n.delayTimer.next, n.delayRemaining))
    }

    function l(t, n, i, o) {
      function r(t, e) {
        return 1 - 3 * e + 3 * t
      }

      function s(t, e) {
        return 3 * e - 6 * t
      }

      function a(t) {
        return 3 * t
      }

      function l(t, e, n) {
        return ((r(e, n) * t + s(e, n)) * t + a(e)) * t
      }

      function u(t, e, n) {
        return 3 * r(e, n) * t * t + 2 * s(e, n) * t + a(e)
      }

      function c(e, n) {
        for (var o = 0; o < h; ++o) {
          var r = u(n, t, i);
          if (0 === r) return n;
          n -= (l(n, t, i) - e) / r
        }
        return n
      }

      function d(e, n, o) {
        var r, s, a = 0;
        do {
          (r = l(s = n + (o - n) / 2, t, i) - e) > 0 ? o = s : n = s
        } while (Math.abs(r) > m && ++a < g);
        return s
      }

      function f() {
        x = !0, t === n && i === o || function() {
          for (var e = 0; e < v; ++e) w[e] = l(e * y, t, i)
        }()
      }
      var h = 4,
        p = .001,
        m = 1e-7,
        g = 10,
        v = 11,
        y = 1 / (v - 1),
        b = "Float32Array" in e;
      if (4 !== arguments.length) return !1;
      for (var _ = 0; _ < 4; ++_)
        if ("number" != typeof arguments[_] || isNaN(arguments[_]) || !isFinite(arguments[_])) return !1;
      t = Math.min(t, 1), i = Math.min(i, 1), t = Math.max(t, 0), i = Math.max(i, 0);
      var w = b ? new Float32Array(v) : new Array(v),
        x = !1,
        C = function(e) {
          return x || f(), t === n && i === o ? e : 0 === e ? 0 : 1 === e ? 1 : l(function(e) {
            for (var n = 0, o = 1, r = v - 1; o !== r && w[o] <= e; ++o) n += y;
            var s = n + (e - w[--o]) / (w[o + 1] - w[o]) * y,
              a = u(s, t, i);
            return a >= p ? c(e, s) : 0 === a ? s : d(e, n, n + y)
          }(e), n, o)
        };
      C.getControlPoints = function() {
        return [{
          x: t,
          y: n
        }, {
          x: i,
          y: o
        }]
      };
      var E = "generateBezier(" + [t, n, i, o] + ")";
      return C.toString = function() {
        return E
      }, C
    }

    function u(t, e) {
      var n = t;
      return v.isString(t) ? w.Easings[t] || (n = !1) : n = v.isArray(t) && 1 === t.length ? function(t) {
        return function(e) {
          return Math.round(e * t) * (1 / t)
        }
      }.apply(null, t) : v.isArray(t) && 2 === t.length ? x.apply(null, t.concat([e])) : !(!v.isArray(t) || 4 !== t.length) && l.apply(null, t), !1 === n && (n = w.Easings[w.defaults.easing] ? w.defaults.easing : _), n
    }

    function c(t) {
      if (t) {
        var e = w.timestamp && !0 !== t ? t : m.now(),
          n = w.State.calls.length;
        n > 1e4 && (w.State.calls = function(t) {
          for (var e = -1, n = t ? t.length : 0, i = []; ++e < n;) {
            var o = t[e];
            o && i.push(o)
          }
          return i
        }(w.State.calls), n = w.State.calls.length);
        for (var o = 0; o < n; o++)
          if (w.State.calls[o]) {
            var s = w.State.calls[o],
              a = s[0],
              l = s[2],
              u = s[3],
              p = !!u,
              g = null,
              y = s[5],
              b = s[6];
            if (u || (u = w.State.calls[o][3] = e - 16), y) {
              if (!0 !== y.resume) continue;
              u = s[3] = Math.round(e - b - 16), s[5] = null
            }
            b = s[6] = e - u;
            for (var _ = Math.min(b / l.duration, 1), x = 0, E = a.length; x < E; x++) {
              var S = a[x],
                k = S.element;
              if (r(k)) {
                var I = !1;
                if (l.display !== i && null !== l.display && "none" !== l.display) {
                  if ("flex" === l.display) {
                    f.each(["-webkit-box", "-moz-box", "-ms-flexbox", "-webkit-flex"], function(t, e) {
                      C.setPropertyValue(k, "display", e)
                    })
                  }
                  C.setPropertyValue(k, "display", l.display)
                }
                for (var A in l.visibility !== i && "hidden" !== l.visibility && C.setPropertyValue(k, "visibility", l.visibility), S)
                  if (S.hasOwnProperty(A) && "element" !== A) {
                    var D, L = S[A],
                      O = v.isString(L.easing) ? w.Easings[L.easing] : L.easing;
                    if (v.isString(L.pattern)) {
                      var P = 1 === _ ? function(t, e, n) {
                        var i = L.endValue[e];
                        return n ? Math.round(i) : i
                      } : function(t, e, n) {
                        var i = L.startValue[e],
                          o = L.endValue[e] - i,
                          r = i + o * O(_, l, o);
                        return n ? Math.round(r) : r
                      };
                      D = L.pattern.replace(/{(\d+)(!)?}/g, P)
                    } else if (1 === _) D = L.endValue;
                    else {
                      var N = L.endValue - L.startValue;
                      D = L.startValue + N * O(_, l, N)
                    }
                    if (!p && D === L.currentValue) continue;
                    if (L.currentValue = D, "tween" === A) g = D;
                    else {
                      var j;
                      if (C.Hooks.registered[A]) {
                        j = C.Hooks.getRoot(A);
                        var z = r(k).rootPropertyValueCache[j];
                        z && (L.rootPropertyValue = z)
                      }
                      var M = C.setPropertyValue(k, A, L.currentValue + (h < 9 && 0 === parseFloat(D) ? "" : L.unitType), L.rootPropertyValue, L.scrollData);
                      C.Hooks.registered[A] && (C.Normalizations.registered[j] ? r(k).rootPropertyValueCache[j] = C.Normalizations.registered[j]("extract", null, M[1]) : r(k).rootPropertyValueCache[j] = M[1]), "transform" === M[0] && (I = !0)
                    }
                  } l.mobileHA && r(k).transformCache.translate3d === i && (r(k).transformCache.translate3d = "(0px, 0px, 0px)", I = !0), I && C.flushTransformCache(k)
              }
            }
            l.display !== i && "none" !== l.display && (w.State.calls[o][2].display = !1), l.visibility !== i && "hidden" !== l.visibility && (w.State.calls[o][2].visibility = !1), l.progress && l.progress.call(s[1], s[1], _, Math.max(0, u + l.duration - e), u, g), 1 === _ && d(o)
          }
      }
      w.State.isTicking && T(c)
    }

    function d(t, e) {
      if (!w.State.calls[t]) return !1;
      for (var n = w.State.calls[t][0], o = w.State.calls[t][1], s = w.State.calls[t][2], a = w.State.calls[t][4], l = !1, u = 0, c = n.length; u < c; u++) {
        var d = n[u].element;
        e || s.loop || ("none" === s.display && C.setPropertyValue(d, "display", s.display), "hidden" === s.visibility && C.setPropertyValue(d, "visibility", s.visibility));
        var h = r(d);
        if (!0 !== s.loop && (f.queue(d)[1] === i || !/\.velocityQueueEntryFlag/i.test(f.queue(d)[1])) && h) {
          h.isAnimating = !1, h.rootPropertyValueCache = {};
          var p = !1;
          f.each(C.Lists.transforms3D, function(t, e) {
            var n = /^scale/.test(e) ? 1 : 0,
              o = h.transformCache[e];
            h.transformCache[e] !== i && new RegExp("^\\(" + n + "[^.]").test(o) && (p = !0, delete h.transformCache[e])
          }), s.mobileHA && (p = !0, delete h.transformCache.translate3d), p && C.flushTransformCache(d), C.Values.removeClass(d, "velocity-animating")
        }
        if (!e && s.complete && !s.loop && u === c - 1) try {
          s.complete.call(o, o)
        } catch (t) {
          setTimeout(function() {
            throw t
          }, 1)
        }
        a && !0 !== s.loop && a(o), h && !0 === s.loop && !e && (f.each(h.tweensContainer, function(t, e) {
          if (/^rotate/.test(t) && (parseFloat(e.startValue) - parseFloat(e.endValue)) % 360 == 0) {
            var n = e.startValue;
            e.startValue = e.endValue, e.endValue = n
          }
          /^backgroundPosition/.test(t) && 100 === parseFloat(e.endValue) && "%" === e.unitType && (e.endValue = 0, e.startValue = 100)
        }), w(d, "reverse", {
          loop: !0,
          delay: s.delay
        })), !1 !== s.queue && f.dequeue(d, s.queue)
      }
      w.State.calls[t] = !1;
      for (var m = 0, g = w.State.calls.length; m < g; m++)
        if (!1 !== w.State.calls[m]) {
          l = !0;
          break
        }! 1 === l && (w.State.isTicking = !1, delete w.State.calls, w.State.calls = [])
    }
    var f, h = function() {
        if (n.documentMode) return n.documentMode;
        for (var t = 7; t > 4; t--) {
          var e = n.createElement("div");
          if (e.innerHTML = "\x3c!--[if IE " + t + "]><span></span><![endif]--\x3e", e.getElementsByTagName("span").length) return e = null, t
        }
        return i
      }(),
      p = function() {
        var t = 0;
        return e.webkitRequestAnimationFrame || e.mozRequestAnimationFrame || function(e) {
          var n, i = (new Date).getTime();
          return n = Math.max(0, 16 - (i - t)), t = i + n, setTimeout(function() {
            e(i + n)
          }, n)
        }
      }(),
      m = function() {
        var t = e.performance || {};
        if (!Object.prototype.hasOwnProperty.call(t, "now")) {
          var n = t.timing && t.timing.domComplete ? t.timing.domComplete : (new Date).getTime();
          t.now = function() {
            return (new Date).getTime() - n
          }
        }
        return t
      }(),
      g = function() {
        var t = Array.prototype.slice;
        try {
          t.call(n.documentElement)
        } catch (e) {
          t = function() {
            for (var t = this.length, e = []; --t > 0;) e[t] = this[t];
            return e
          }
        }
        return t
      }(),
      v = {
        isNumber: function(t) {
          return "number" == typeof t
        },
        isString: function(t) {
          return "string" == typeof t
        },
        isArray: Array.isArray || function(t) {
          return "[object Array]" === Object.prototype.toString.call(t)
        },
        isFunction: function(t) {
          return "[object Function]" === Object.prototype.toString.call(t)
        },
        isNode: function(t) {
          return t && t.nodeType
        },
        isWrapped: function(t) {
          return t && v.isNumber(t.length) && !v.isString(t) && !v.isFunction(t) && !v.isNode(t) && (0 === t.length || v.isNode(t[0]))
        },
        isSVG: function(t) {
          return e.SVGElement && t instanceof e.SVGElement
        },
        isEmptyObject: function(t) {
          for (var e in t)
            if (t.hasOwnProperty(e)) return !1;
          return !0
        }
      },
      y = !1;
    if (t.fn && t.fn.jquery ? (f = t, y = !0) : f = e.Velocity.Utilities, h <= 8 && !y) throw new Error("Velocity: IE8 and below require jQuery to be loaded before Velocity.");
    if (!(h <= 7)) {
      var b = 400,
        _ = "swing",
        w = {
          State: {
            isMobile: /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent),
            isAndroid: /Android/i.test(navigator.userAgent),
            isGingerbread: /Android 2\.3\.[3-7]/i.test(navigator.userAgent),
            isChrome: e.chrome,
            isFirefox: /Firefox/i.test(navigator.userAgent),
            prefixElement: n.createElement("div"),
            prefixMatches: {},
            scrollAnchor: null,
            scrollPropertyLeft: null,
            scrollPropertyTop: null,
            isTicking: !1,
            calls: [],
            delayedElements: {
              count: 0
            }
          },
          CSS: {},
          Utilities: f,
          Redirects: {},
          Easings: {},
          Promise: e.Promise,
          defaults: {
            queue: "",
            duration: b,
            easing: _,
            begin: i,
            complete: i,
            progress: i,
            display: i,
            visibility: i,
            loop: !1,
            delay: !1,
            mobileHA: !0,
            _cacheValues: !0,
            promiseRejectEmpty: !0
          },
          init: function(t) {
            f.data(t, "velocity", {
              isSVG: v.isSVG(t),
              isAnimating: !1,
              computedStyle: null,
              tweensContainer: null,
              rootPropertyValueCache: {},
              transformCache: {}
            })
          },
          hook: null,
          mock: !1,
          version: {
            major: 1,
            minor: 4,
            patch: 2
          },
          debug: !1,
          timestamp: !0,
          pauseAll: function(t) {
            var e = (new Date).getTime();
            f.each(w.State.calls, function(e, n) {
              if (n) {
                if (t !== i && (n[2].queue !== t || !1 === n[2].queue)) return !0;
                n[5] = {
                  resume: !1
                }
              }
            }), f.each(w.State.delayedElements, function(t, n) {
              n && s(n, e)
            })
          },
          resumeAll: function(t) {
            (new Date).getTime();
            f.each(w.State.calls, function(e, n) {
              if (n) {
                if (t !== i && (n[2].queue !== t || !1 === n[2].queue)) return !0;
                n[5] && (n[5].resume = !0)
              }
            }), f.each(w.State.delayedElements, function(t, e) {
              e && a(e)
            })
          }
        };
      e.pageYOffset !== i ? (w.State.scrollAnchor = e, w.State.scrollPropertyLeft = "pageXOffset", w.State.scrollPropertyTop = "pageYOffset") : (w.State.scrollAnchor = n.documentElement || n.body.parentNode || n.body, w.State.scrollPropertyLeft = "scrollLeft", w.State.scrollPropertyTop = "scrollTop");
      var x = function() {
        function t(t) {
          return -t.tension * t.x - t.friction * t.v
        }

        function e(e, n, i) {
          var o = {
            x: e.x + i.dx * n,
            v: e.v + i.dv * n,
            tension: e.tension,
            friction: e.friction
          };
          return {
            dx: o.v,
            dv: t(o)
          }
        }

        function n(n, i) {
          var o = {
              dx: n.v,
              dv: t(n)
            },
            r = e(n, .5 * i, o),
            s = e(n, .5 * i, r),
            a = e(n, i, s),
            l = 1 / 6 * (o.dx + 2 * (r.dx + s.dx) + a.dx),
            u = 1 / 6 * (o.dv + 2 * (r.dv + s.dv) + a.dv);
          return n.x = n.x + l * i, n.v = n.v + u * i, n
        }
        return function t(e, i, o) {
          var r, s, a, l = {
              x: -1,
              v: 0,
              tension: null,
              friction: null
            },
            u = [0],
            c = 0;
          for (e = parseFloat(e) || 500, i = parseFloat(i) || 20, o = o || null, l.tension = e, l.friction = i, (r = null !== o) ? s = (c = t(e, i)) / o * .016 : s = .016; a = n(a || l, s), u.push(1 + a.x), c += 16, Math.abs(a.x) > 1e-4 && Math.abs(a.v) > 1e-4;);
          return r ? function(t) {
            return u[t * (u.length - 1) | 0]
          } : c
        }
      }();
      w.Easings = {
        linear: function(t) {
          return t
        },
        swing: function(t) {
          return .5 - Math.cos(t * Math.PI) / 2
        },
        spring: function(t) {
          return 1 - Math.cos(4.5 * t * Math.PI) * Math.exp(6 * -t)
        }
      }, f.each([
        ["ease", [.25, .1, .25, 1]],
        ["ease-in", [.42, 0, 1, 1]],
        ["ease-out", [0, 0, .58, 1]],
        ["ease-in-out", [.42, 0, .58, 1]],
        ["easeInSine", [.47, 0, .745, .715]],
        ["easeOutSine", [.39, .575, .565, 1]],
        ["easeInOutSine", [.445, .05, .55, .95]],
        ["easeInQuad", [.55, .085, .68, .53]],
        ["easeOutQuad", [.25, .46, .45, .94]],
        ["easeInOutQuad", [.455, .03, .515, .955]],
        ["easeInCubic", [.55, .055, .675, .19]],
        ["easeOutCubic", [.215, .61, .355, 1]],
        ["easeInOutCubic", [.645, .045, .355, 1]],
        ["easeInQuart", [.895, .03, .685, .22]],
        ["easeOutQuart", [.165, .84, .44, 1]],
        ["easeInOutQuart", [.77, 0, .175, 1]],
        ["easeInQuint", [.755, .05, .855, .06]],
        ["easeOutQuint", [.23, 1, .32, 1]],
        ["easeInOutQuint", [.86, 0, .07, 1]],
        ["easeInExpo", [.95, .05, .795, .035]],
        ["easeOutExpo", [.19, 1, .22, 1]],
        ["easeInOutExpo", [1, 0, 0, 1]],
        ["easeInCirc", [.6, .04, .98, .335]],
        ["easeOutCirc", [.075, .82, .165, 1]],
        ["easeInOutCirc", [.785, .135, .15, .86]]
      ], function(t, e) {
        w.Easings[e[0]] = l.apply(null, e[1])
      });
      var C = w.CSS = {
        RegEx: {
          isHex: /^#([A-f\d]{3}){1,2}$/i,
          valueUnwrap: /^[A-z]+\((.*)\)$/i,
          wrappedValueAlreadyExtracted: /[0-9.]+ [0-9.]+ [0-9.]+( [0-9.]+)?/,
          valueSplit: /([A-z]+\(.+\))|(([A-z0-9#-.]+?)(?=\s|$))/gi
        },
        Lists: {
          colors: ["fill", "stroke", "stopColor", "color", "backgroundColor", "borderColor", "borderTopColor", "borderRightColor", "borderBottomColor", "borderLeftColor", "outlineColor"],
          transformsBase: ["translateX", "translateY", "scale", "scaleX", "scaleY", "skewX", "skewY", "rotateZ"],
          transforms3D: ["transformPerspective", "translateZ", "scaleZ", "rotateX", "rotateY"],
          units: ["%", "em", "ex", "ch", "rem", "vw", "vh", "vmin", "vmax", "cm", "mm", "Q", "in", "pc", "pt", "px", "deg", "grad", "rad", "turn", "s", "ms"],
          colorNames: {
            aliceblue: "240,248,255",
            antiquewhite: "250,235,215",
            aquamarine: "127,255,212",
            aqua: "0,255,255",
            azure: "240,255,255",
            beige: "245,245,220",
            bisque: "255,228,196",
            black: "0,0,0",
            blanchedalmond: "255,235,205",
            blueviolet: "138,43,226",
            blue: "0,0,255",
            brown: "165,42,42",
            burlywood: "222,184,135",
            cadetblue: "95,158,160",
            chartreuse: "127,255,0",
            chocolate: "210,105,30",
            coral: "255,127,80",
            cornflowerblue: "100,149,237",
            cornsilk: "255,248,220",
            crimson: "220,20,60",
            cyan: "0,255,255",
            darkblue: "0,0,139",
            darkcyan: "0,139,139",
            darkgoldenrod: "184,134,11",
            darkgray: "169,169,169",
            darkgrey: "169,169,169",
            darkgreen: "0,100,0",
            darkkhaki: "189,183,107",
            darkmagenta: "139,0,139",
            darkolivegreen: "85,107,47",
            darkorange: "255,140,0",
            darkorchid: "153,50,204",
            darkred: "139,0,0",
            darksalmon: "233,150,122",
            darkseagreen: "143,188,143",
            darkslateblue: "72,61,139",
            darkslategray: "47,79,79",
            darkturquoise: "0,206,209",
            darkviolet: "148,0,211",
            deeppink: "255,20,147",
            deepskyblue: "0,191,255",
            dimgray: "105,105,105",
            dimgrey: "105,105,105",
            dodgerblue: "30,144,255",
            firebrick: "178,34,34",
            floralwhite: "255,250,240",
            forestgreen: "34,139,34",
            fuchsia: "255,0,255",
            gainsboro: "220,220,220",
            ghostwhite: "248,248,255",
            gold: "255,215,0",
            goldenrod: "218,165,32",
            gray: "128,128,128",
            grey: "128,128,128",
            greenyellow: "173,255,47",
            green: "0,128,0",
            honeydew: "240,255,240",
            hotpink: "255,105,180",
            indianred: "205,92,92",
            indigo: "75,0,130",
            ivory: "255,255,240",
            khaki: "240,230,140",
            lavenderblush: "255,240,245",
            lavender: "230,230,250",
            lawngreen: "124,252,0",
            lemonchiffon: "255,250,205",
            lightblue: "173,216,230",
            lightcoral: "240,128,128",
            lightcyan: "224,255,255",
            lightgoldenrodyellow: "250,250,210",
            lightgray: "211,211,211",
            lightgrey: "211,211,211",
            lightgreen: "144,238,144",
            lightpink: "255,182,193",
            lightsalmon: "255,160,122",
            lightseagreen: "32,178,170",
            lightskyblue: "135,206,250",
            lightslategray: "119,136,153",
            lightsteelblue: "176,196,222",
            lightyellow: "255,255,224",
            limegreen: "50,205,50",
            lime: "0,255,0",
            linen: "250,240,230",
            magenta: "255,0,255",
            maroon: "128,0,0",
            mediumaquamarine: "102,205,170",
            mediumblue: "0,0,205",
            mediumorchid: "186,85,211",
            mediumpurple: "147,112,219",
            mediumseagreen: "60,179,113",
            mediumslateblue: "123,104,238",
            mediumspringgreen: "0,250,154",
            mediumturquoise: "72,209,204",
            mediumvioletred: "199,21,133",
            midnightblue: "25,25,112",
            mintcream: "245,255,250",
            mistyrose: "255,228,225",
            moccasin: "255,228,181",
            navajowhite: "255,222,173",
            navy: "0,0,128",
            oldlace: "253,245,230",
            olivedrab: "107,142,35",
            olive: "128,128,0",
            orangered: "255,69,0",
            orange: "255,165,0",
            orchid: "218,112,214",
            palegoldenrod: "238,232,170",
            palegreen: "152,251,152",
            paleturquoise: "175,238,238",
            palevioletred: "219,112,147",
            papayawhip: "255,239,213",
            peachpuff: "255,218,185",
            peru: "205,133,63",
            pink: "255,192,203",
            plum: "221,160,221",
            powderblue: "176,224,230",
            purple: "128,0,128",
            red: "255,0,0",
            rosybrown: "188,143,143",
            royalblue: "65,105,225",
            saddlebrown: "139,69,19",
            salmon: "250,128,114",
            sandybrown: "244,164,96",
            seagreen: "46,139,87",
            seashell: "255,245,238",
            sienna: "160,82,45",
            silver: "192,192,192",
            skyblue: "135,206,235",
            slateblue: "106,90,205",
            slategray: "112,128,144",
            snow: "255,250,250",
            springgreen: "0,255,127",
            steelblue: "70,130,180",
            tan: "210,180,140",
            teal: "0,128,128",
            thistle: "216,191,216",
            tomato: "255,99,71",
            turquoise: "64,224,208",
            violet: "238,130,238",
            wheat: "245,222,179",
            whitesmoke: "245,245,245",
            white: "255,255,255",
            yellowgreen: "154,205,50",
            yellow: "255,255,0"
          }
        },
        Hooks: {
          templates: {
            textShadow: ["Color X Y Blur", "black 0px 0px 0px"],
            boxShadow: ["Color X Y Blur Spread", "black 0px 0px 0px 0px"],
            clip: ["Top Right Bottom Left", "0px 0px 0px 0px"],
            backgroundPosition: ["X Y", "0% 0%"],
            transformOrigin: ["X Y Z", "50% 50% 0px"],
            perspectiveOrigin: ["X Y", "50% 50%"]
          },
          registered: {},
          register: function() {
            for (var t = 0; t < C.Lists.colors.length; t++) {
              var e = "color" === C.Lists.colors[t] ? "0 0 0 1" : "255 255 255 1";
              C.Hooks.templates[C.Lists.colors[t]] = ["Red Green Blue Alpha", e]
            }
            var n, i, o;
            if (h)
              for (n in C.Hooks.templates)
                if (C.Hooks.templates.hasOwnProperty(n)) {
                  o = (i = C.Hooks.templates[n])[0].split(" ");
                  var r = i[1].match(C.RegEx.valueSplit);
                  "Color" === o[0] && (o.push(o.shift()), r.push(r.shift()), C.Hooks.templates[n] = [o.join(" "), r.join(" ")])
                } for (n in C.Hooks.templates)
              if (C.Hooks.templates.hasOwnProperty(n))
                for (var s in o = (i = C.Hooks.templates[n])[0].split(" "))
                  if (o.hasOwnProperty(s)) {
                    var a = n + o[s],
                      l = s;
                    C.Hooks.registered[a] = [n, l]
                  }
          },
          getRoot: function(t) {
            var e = C.Hooks.registered[t];
            return e ? e[0] : t
          },
          getUnit: function(t, e) {
            var n = (t.substr(e || 0, 5).match(/^[a-z%]+/) || [])[0] || "";
            return n && C.Lists.units.indexOf(n) >= 0 ? n : ""
          },
          fixColors: function(t) {
            return t.replace(/(rgba?\(\s*)?(\b[a-z]+\b)/g, function(t, e, n) {
              return C.Lists.colorNames.hasOwnProperty(n) ? (e || "rgba(") + C.Lists.colorNames[n] + (e ? "" : ",1)") : e + n
            })
          },
          cleanRootPropertyValue: function(t, e) {
            return C.RegEx.valueUnwrap.test(e) && (e = e.match(C.RegEx.valueUnwrap)[1]), C.Values.isCSSNullValue(e) && (e = C.Hooks.templates[t][1]), e
          },
          extractValue: function(t, e) {
            var n = C.Hooks.registered[t];
            if (n) {
              var i = n[0],
                o = n[1];
              return (e = C.Hooks.cleanRootPropertyValue(i, e)).toString().match(C.RegEx.valueSplit)[o]
            }
            return e
          },
          injectValue: function(t, e, n) {
            var i = C.Hooks.registered[t];
            if (i) {
              var o, r = i[0],
                s = i[1];
              return (o = (n = C.Hooks.cleanRootPropertyValue(r, n)).toString().match(C.RegEx.valueSplit))[s] = e, o.join(" ")
            }
            return n
          }
        },
        Normalizations: {
          registered: {
            clip: function(t, e, n) {
              switch (t) {
                case "name":
                  return "clip";
                case "extract":
                  var i;
                  return C.RegEx.wrappedValueAlreadyExtracted.test(n) ? i = n : i = (i = n.toString().match(C.RegEx.valueUnwrap)) ? i[1].replace(/,(\s+)?/g, " ") : n, i;
                case "inject":
                  return "rect(" + n + ")"
              }
            },
            blur: function(t, e, n) {
              switch (t) {
                case "name":
                  return w.State.isFirefox ? "filter" : "-webkit-filter";
                case "extract":
                  var i = parseFloat(n);
                  if (!i && 0 !== i) {
                    var o = n.toString().match(/blur\(([0-9]+[A-z]+)\)/i);
                    i = o ? o[1] : 0
                  }
                  return i;
                case "inject":
                  return parseFloat(n) ? "blur(" + n + ")" : "none"
              }
            },
            opacity: function(t, e, n) {
              if (h <= 8) switch (t) {
                case "name":
                  return "filter";
                case "extract":
                  var i = n.toString().match(/alpha\(opacity=(.*)\)/i);
                  return i ? i[1] / 100 : 1;
                case "inject":
                  return e.style.zoom = 1, parseFloat(n) >= 1 ? "" : "alpha(opacity=" + parseInt(100 * parseFloat(n), 10) + ")"
              } else switch (t) {
                case "name":
                  return "opacity";
                case "extract":
                case "inject":
                  return n
              }
            }
          },
          register: function() {
            function t(t, e, n) {
              if ("border-box" === C.getPropertyValue(e, "boxSizing").toString().toLowerCase() === (n || !1)) {
                var i, o, r = 0,
                  s = "width" === t ? ["Left", "Right"] : ["Top", "Bottom"],
                  a = ["padding" + s[0], "padding" + s[1], "border" + s[0] + "Width", "border" + s[1] + "Width"];
                for (i = 0; i < a.length; i++) o = parseFloat(C.getPropertyValue(e, a[i])), isNaN(o) || (r += o);
                return n ? -r : r
              }
              return 0
            }

            function e(e, n) {
              return function(i, o, r) {
                switch (i) {
                  case "name":
                    return e;
                  case "extract":
                    return parseFloat(r) + t(e, o, n);
                  case "inject":
                    return parseFloat(r) - t(e, o, n) + "px"
                }
              }
            }
            h && !(h > 9) || w.State.isGingerbread || (C.Lists.transformsBase = C.Lists.transformsBase.concat(C.Lists.transforms3D));
            for (var n = 0; n < C.Lists.transformsBase.length; n++) ! function() {
              var t = C.Lists.transformsBase[n];
              C.Normalizations.registered[t] = function(e, n, o) {
                switch (e) {
                  case "name":
                    return "transform";
                  case "extract":
                    return r(n) === i || r(n).transformCache[t] === i ? /^scale/i.test(t) ? 1 : 0 : r(n).transformCache[t].replace(/[()]/g, "");
                  case "inject":
                    var s = !1;
                    switch (t.substr(0, t.length - 1)) {
                      case "translate":
                        s = !/(%|px|em|rem|vw|vh|\d)$/i.test(o);
                        break;
                      case "scal":
                      case "scale":
                        w.State.isAndroid && r(n).transformCache[t] === i && o < 1 && (o = 1), s = !/(\d)$/i.test(o);
                        break;
                      case "skew":
                        s = !/(deg|\d)$/i.test(o);
                        break;
                      case "rotate":
                        s = !/(deg|\d)$/i.test(o)
                    }
                    return s || (r(n).transformCache[t] = "(" + o + ")"), r(n).transformCache[t]
                }
              }
            }();
            for (var o = 0; o < C.Lists.colors.length; o++) ! function() {
              var t = C.Lists.colors[o];
              C.Normalizations.registered[t] = function(e, n, o) {
                switch (e) {
                  case "name":
                    return t;
                  case "extract":
                    var r;
                    if (C.RegEx.wrappedValueAlreadyExtracted.test(o)) r = o;
                    else {
                      var s, a = {
                        black: "rgb(0, 0, 0)",
                        blue: "rgb(0, 0, 255)",
                        gray: "rgb(128, 128, 128)",
                        green: "rgb(0, 128, 0)",
                        red: "rgb(255, 0, 0)",
                        white: "rgb(255, 255, 255)"
                      };
                      /^[A-z]+$/i.test(o) ? s = a[o] !== i ? a[o] : a.black : C.RegEx.isHex.test(o) ? s = "rgb(" + C.Values.hexToRgb(o).join(" ") + ")" : /^rgba?\(/i.test(o) || (s = a.black), r = (s || o).toString().match(C.RegEx.valueUnwrap)[1].replace(/,(\s+)?/g, " ")
                    }
                    return (!h || h > 8) && 3 === r.split(" ").length && (r += " 1"), r;
                  case "inject":
                    return /^rgb/.test(o) ? o : (h <= 8 ? 4 === o.split(" ").length && (o = o.split(/\s+/).slice(0, 3).join(" ")) : 3 === o.split(" ").length && (o += " 1"), (h <= 8 ? "rgb" : "rgba") + "(" + o.replace(/\s+/g, ",").replace(/\.(\d)+(?=,)/g, "") + ")")
                }
              }
            }();
            C.Normalizations.registered.innerWidth = e("width", !0), C.Normalizations.registered.innerHeight = e("height", !0), C.Normalizations.registered.outerWidth = e("width"), C.Normalizations.registered.outerHeight = e("height")
          }
        },
        Names: {
          camelCase: function(t) {
            return t.replace(/-(\w)/g, function(t, e) {
              return e.toUpperCase()
            })
          },
          SVGAttribute: function(t) {
            var e = "width|height|x|y|cx|cy|r|rx|ry|x1|x2|y1|y2";
            return (h || w.State.isAndroid && !w.State.isChrome) && (e += "|transform"), new RegExp("^(" + e + ")$", "i").test(t)
          },
          prefixCheck: function(t) {
            if (w.State.prefixMatches[t]) return [w.State.prefixMatches[t], !0];
            for (var e = ["", "Webkit", "Moz", "ms", "O"], n = 0, i = e.length; n < i; n++) {
              var o;
              if (o = 0 === n ? t : e[n] + t.replace(/^\w/, function(t) {
                  return t.toUpperCase()
                }), v.isString(w.State.prefixElement.style[o])) return w.State.prefixMatches[t] = o, [o, !0]
            }
            return [t, !1]
          }
        },
        Values: {
          hexToRgb: function(t) {
            var e;
            return t = t.replace(/^#?([a-f\d])([a-f\d])([a-f\d])$/i, function(t, e, n, i) {
              return e + e + n + n + i + i
            }), (e = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(t)) ? [parseInt(e[1], 16), parseInt(e[2], 16), parseInt(e[3], 16)] : [0, 0, 0]
          },
          isCSSNullValue: function(t) {
            return !t || /^(none|auto|transparent|(rgba\(0, ?0, ?0, ?0\)))$/i.test(t)
          },
          getUnitType: function(t) {
            return /^(rotate|skew)/i.test(t) ? "deg" : /(^(scale|scaleX|scaleY|scaleZ|alpha|flexGrow|flexHeight|zIndex|fontWeight)$)|((opacity|red|green|blue|alpha)$)/i.test(t) ? "" : "px"
          },
          getDisplayType: function(t) {
            var e = t && t.tagName.toString().toLowerCase();
            return /^(b|big|i|small|tt|abbr|acronym|cite|code|dfn|em|kbd|strong|samp|var|a|bdo|br|img|map|object|q|script|span|sub|sup|button|input|label|select|textarea)$/i.test(e) ? "inline" : /^(li)$/i.test(e) ? "list-item" : /^(tr)$/i.test(e) ? "table-row" : /^(table)$/i.test(e) ? "table" : /^(tbody)$/i.test(e) ? "table-row-group" : "block"
          },
          addClass: function(t, e) {
            if (t)
              if (t.classList) t.classList.add(e);
              else if (v.isString(t.className)) t.className += (t.className.length ? " " : "") + e;
            else {
              var n = t.getAttribute(h <= 7 ? "className" : "class") || "";
              t.setAttribute("class", n + (n ? " " : "") + e)
            }
          },
          removeClass: function(t, e) {
            if (t)
              if (t.classList) t.classList.remove(e);
              else if (v.isString(t.className)) t.className = t.className.toString().replace(new RegExp("(^|\\s)" + e.split(" ").join("|") + "(\\s|$)", "gi"), " ");
            else {
              var n = t.getAttribute(h <= 7 ? "className" : "class") || "";
              t.setAttribute("class", n.replace(new RegExp("(^|s)" + e.split(" ").join("|") + "(s|$)", "gi"), " "))
            }
          }
        },
        getPropertyValue: function(t, n, o, s) {
          function a(t, n) {
            var o = 0;
            if (h <= 8) o = f.css(t, n);
            else {
              var l = !1;
              /^(width|height)$/.test(n) && 0 === C.getPropertyValue(t, "display") && (l = !0, C.setPropertyValue(t, "display", C.Values.getDisplayType(t)));
              var u, c = function() {
                l && C.setPropertyValue(t, "display", "none")
              };
              if (!s) {
                if ("height" === n && "border-box" !== C.getPropertyValue(t, "boxSizing").toString().toLowerCase()) {
                  var d = t.offsetHeight - (parseFloat(C.getPropertyValue(t, "borderTopWidth")) || 0) - (parseFloat(C.getPropertyValue(t, "borderBottomWidth")) || 0) - (parseFloat(C.getPropertyValue(t, "paddingTop")) || 0) - (parseFloat(C.getPropertyValue(t, "paddingBottom")) || 0);
                  return c(), d
                }
                if ("width" === n && "border-box" !== C.getPropertyValue(t, "boxSizing").toString().toLowerCase()) {
                  var p = t.offsetWidth - (parseFloat(C.getPropertyValue(t, "borderLeftWidth")) || 0) - (parseFloat(C.getPropertyValue(t, "borderRightWidth")) || 0) - (parseFloat(C.getPropertyValue(t, "paddingLeft")) || 0) - (parseFloat(C.getPropertyValue(t, "paddingRight")) || 0);
                  return c(), p
                }
              }
              u = r(t) === i ? e.getComputedStyle(t, null) : r(t).computedStyle ? r(t).computedStyle : r(t).computedStyle = e.getComputedStyle(t, null), "borderColor" === n && (n = "borderTopColor"), "" !== (o = 9 === h && "filter" === n ? u.getPropertyValue(n) : u[n]) && null !== o || (o = t.style[n]), c()
            }
            if ("auto" === o && /^(top|right|bottom|left)$/i.test(n)) {
              var m = a(t, "position");
              ("fixed" === m || "absolute" === m && /top|left/i.test(n)) && (o = f(t).position()[n] + "px")
            }
            return o
          }
          var l;
          if (C.Hooks.registered[n]) {
            var u = n,
              c = C.Hooks.getRoot(u);
            o === i && (o = C.getPropertyValue(t, C.Names.prefixCheck(c)[0])), C.Normalizations.registered[c] && (o = C.Normalizations.registered[c]("extract", t, o)), l = C.Hooks.extractValue(u, o)
          } else if (C.Normalizations.registered[n]) {
            var d, p;
            "transform" !== (d = C.Normalizations.registered[n]("name", t)) && (p = a(t, C.Names.prefixCheck(d)[0]), C.Values.isCSSNullValue(p) && C.Hooks.templates[n] && (p = C.Hooks.templates[n][1])), l = C.Normalizations.registered[n]("extract", t, p)
          }
          if (!/^[\d-]/.test(l)) {
            var m = r(t);
            if (m && m.isSVG && C.Names.SVGAttribute(n))
              if (/^(height|width)$/i.test(n)) try {
                l = t.getBBox()[n]
              } catch (t) {
                l = 0
              } else l = t.getAttribute(n);
              else l = a(t, C.Names.prefixCheck(n)[0])
          }
          return C.Values.isCSSNullValue(l) && (l = 0), w.debug >= 2 && console.log("Get " + n + ": " + l), l
        },
        setPropertyValue: function(t, n, i, o, s) {
          var a = n;
          if ("scroll" === n) s.container ? s.container["scroll" + s.direction] = i : "Left" === s.direction ? e.scrollTo(i, s.alternateValue) : e.scrollTo(s.alternateValue, i);
          else if (C.Normalizations.registered[n] && "transform" === C.Normalizations.registered[n]("name", t)) C.Normalizations.registered[n]("inject", t, i), a = "transform", i = r(t).transformCache[n];
          else {
            if (C.Hooks.registered[n]) {
              var l = n,
                u = C.Hooks.getRoot(n);
              o = o || C.getPropertyValue(t, u), i = C.Hooks.injectValue(l, i, o), n = u
            }
            if (C.Normalizations.registered[n] && (i = C.Normalizations.registered[n]("inject", t, i), n = C.Normalizations.registered[n]("name", t)), a = C.Names.prefixCheck(n)[0], h <= 8) try {
              t.style[a] = i
            } catch (t) {
              w.debug && console.log("Browser does not support [" + i + "] for [" + a + "]")
            } else {
              var c = r(t);
              c && c.isSVG && C.Names.SVGAttribute(n) ? t.setAttribute(n, i) : t.style[a] = i
            }
            w.debug >= 2 && console.log("Set " + n + " (" + a + "): " + i)
          }
          return [a, i]
        },
        flushTransformCache: function(t) {
          var e = "",
            n = r(t);
          if ((h || w.State.isAndroid && !w.State.isChrome) && n && n.isSVG) {
            var i = function(e) {
                return parseFloat(C.getPropertyValue(t, e))
              },
              o = {
                translate: [i("translateX"), i("translateY")],
                skewX: [i("skewX")],
                skewY: [i("skewY")],
                scale: 1 !== i("scale") ? [i("scale"), i("scale")] : [i("scaleX"), i("scaleY")],
                rotate: [i("rotateZ"), 0, 0]
              };
            f.each(r(t).transformCache, function(t) {
              /^translate/i.test(t) ? t = "translate" : /^scale/i.test(t) ? t = "scale" : /^rotate/i.test(t) && (t = "rotate"), o[t] && (e += t + "(" + o[t].join(" ") + ") ", delete o[t])
            })
          } else {
            var s, a;
            f.each(r(t).transformCache, function(n) {
              return s = r(t).transformCache[n], "transformPerspective" === n ? (a = s, !0) : (9 === h && "rotateZ" === n && (n = "rotate"), void(e += n + s + " "))
            }), a && (e = "perspective" + a + " " + e)
          }
          C.setPropertyValue(t, "transform", e)
        }
      };
      C.Hooks.register(), C.Normalizations.register(), w.hook = function(t, e, n) {
        var s;
        return t = o(t), f.each(t, function(t, o) {
          if (r(o) === i && w.init(o), n === i) s === i && (s = C.getPropertyValue(o, e));
          else {
            var a = C.setPropertyValue(o, e, n);
            "transform" === a[0] && w.CSS.flushTransformCache(o), s = a
          }
        }), s
      };
      var E = function() {
        function t() {
          return p ? S.promise || null : m
        }

        function l(t, o) {
          function s(s) {
            var h, p;
            if (l.begin && 0 === A) try {
              l.begin.call(y, y)
            } catch (t) {
              setTimeout(function() {
                throw t
              }, 1)
            }
            if ("scroll" === k) {
              var m, g, b, E = /^x$/i.test(l.axis) ? "Left" : "Top",
                T = parseFloat(l.offset) || 0;
              l.container ? v.isWrapped(l.container) || v.isNode(l.container) ? (l.container = l.container[0] || l.container, b = (m = l.container["scroll" + E]) + f(t).position()[E.toLowerCase()] + T) : l.container = null : (m = w.State.scrollAnchor[w.State["scrollProperty" + E]], g = w.State.scrollAnchor[w.State["scrollProperty" + ("Left" === E ? "Top" : "Left")]], b = f(t).offset()[E.toLowerCase()] + T), d = {
                scroll: {
                  rootPropertyValue: !1,
                  startValue: m,
                  currentValue: m,
                  endValue: b,
                  unitType: "",
                  easing: l.easing,
                  scrollData: {
                    container: l.container,
                    direction: E,
                    alternateValue: g
                  }
                },
                element: t
              }, w.debug && console.log("tweensContainer (scroll): ", d.scroll, t)
            } else if ("reverse" === k) {
              if (!(h = r(t))) return;
              if (!h.tweensContainer) return void f.dequeue(t, l.queue);
              for (var D in "none" === h.opts.display && (h.opts.display = "auto"), "hidden" === h.opts.visibility && (h.opts.visibility = "visible"), h.opts.loop = !1, h.opts.begin = null, h.opts.complete = null, x.easing || delete l.easing, x.duration || delete l.duration, l = f.extend({}, h.opts, l), p = f.extend(!0, {}, h ? h.tweensContainer : null))
                if (p.hasOwnProperty(D) && "element" !== D) {
                  var L = p[D].startValue;
                  p[D].startValue = p[D].currentValue = p[D].endValue, p[D].endValue = L, v.isEmptyObject(x) || (p[D].easing = l.easing), w.debug && console.log("reverse tweensContainer (" + D + "): " + JSON.stringify(p[D]), t)
                } d = p
            } else if ("start" === k) {
              (h = r(t)) && h.tweensContainer && !0 === h.isAnimating && (p = h.tweensContainer);
              var O = function(e, n) {
                  var i, r, s;
                  return v.isFunction(e) && (e = e.call(t, o, I)), v.isArray(e) ? (i = e[0], !v.isArray(e[1]) && /^[\d-]/.test(e[1]) || v.isFunction(e[1]) || C.RegEx.isHex.test(e[1]) ? s = e[1] : v.isString(e[1]) && !C.RegEx.isHex.test(e[1]) && w.Easings[e[1]] || v.isArray(e[1]) ? (r = n ? e[1] : u(e[1], l.duration), s = e[2]) : s = e[1] || e[2]) : i = e, n || (r = r || l.easing), v.isFunction(i) && (i = i.call(t, o, I)), v.isFunction(s) && (s = s.call(t, o, I)), [i || 0, r, s]
                },
                P = function(o, r) {
                  var s, u = C.Hooks.getRoot(o),
                    c = !1,
                    m = r[0],
                    g = r[1],
                    y = r[2];
                  if (h && h.isSVG || "tween" === u || !1 !== C.Names.prefixCheck(u)[1] || C.Normalizations.registered[u] !== i) {
                    (l.display !== i && null !== l.display && "none" !== l.display || l.visibility !== i && "hidden" !== l.visibility) && /opacity|filter/.test(o) && !y && 0 !== m && (y = 0), l._cacheValues && p && p[o] ? (y === i && (y = p[o].endValue + p[o].unitType), c = h.rootPropertyValueCache[u]) : C.Hooks.registered[o] ? y === i ? (c = C.getPropertyValue(t, u), y = C.getPropertyValue(t, o, c)) : c = C.Hooks.templates[u][1] : y === i && (y = C.getPropertyValue(t, o));
                    var b, _, x, E = !1,
                      T = function(t, e) {
                        var n, i;
                        return i = (e || "0").toString().toLowerCase().replace(/[%A-z]+$/, function(t) {
                          return n = t, ""
                        }), n || (n = C.Values.getUnitType(t)), [i, n]
                      };
                    if (y !== m && v.isString(y) && v.isString(m)) {
                      s = "";
                      var S = 0,
                        k = 0,
                        I = [],
                        A = [],
                        D = 0,
                        L = 0,
                        O = 0;
                      for (y = C.Hooks.fixColors(y), m = C.Hooks.fixColors(m); S < y.length && k < m.length;) {
                        var P = y[S],
                          N = m[k];
                        if (/[\d\.-]/.test(P) && /[\d\.-]/.test(N)) {
                          for (var j = P, M = N, $ = ".", R = "."; ++S < y.length;) {
                            if ((P = y[S]) === $) $ = "..";
                            else if (!/\d/.test(P)) break;
                            j += P
                          }
                          for (; ++k < m.length;) {
                            if ((N = m[k]) === R) R = "..";
                            else if (!/\d/.test(N)) break;
                            M += N
                          }
                          var F = C.Hooks.getUnit(y, S),
                            B = C.Hooks.getUnit(m, k);
                          if (S += F.length, k += B.length, F === B) j === M ? s += j + F : (s += "{" + I.length + (L ? "!" : "") + "}" + F, I.push(parseFloat(j)), A.push(parseFloat(M)));
                          else {
                            var H = parseFloat(j),
                              W = parseFloat(M);
                            s += (D < 5 ? "calc" : "") + "(" + (H ? "{" + I.length + (L ? "!" : "") + "}" : "0") + F + " + " + (W ? "{" + (I.length + (H ? 1 : 0)) + (L ? "!" : "") + "}" : "0") + B + ")", H && (I.push(H), A.push(0)), W && (I.push(0), A.push(W))
                          }
                        } else {
                          if (P !== N) {
                            D = 0;
                            break
                          }
                          s += P, S++, k++, 0 === D && "c" === P || 1 === D && "a" === P || 2 === D && "l" === P || 3 === D && "c" === P || D >= 4 && "(" === P ? D++ : (D && D < 5 || D >= 4 && ")" === P && --D < 5) && (D = 0), 0 === L && "r" === P || 1 === L && "g" === P || 2 === L && "b" === P || 3 === L && "a" === P || L >= 3 && "(" === P ? (3 === L && "a" === P && (O = 1), L++) : O && "," === P ? ++O > 3 && (L = O = 0) : (O && L < (O ? 5 : 4) || L >= (O ? 4 : 3) && ")" === P && --L < (O ? 5 : 4)) && (L = O = 0)
                        }
                      }
                      S === y.length && k === m.length || (w.debug && console.error('Trying to pattern match mis-matched strings ["' + m + '", "' + y + '"]'), s = i), s && (I.length ? (w.debug && console.log('Pattern found "' + s + '" -> ', I, A, "[" + y + "," + m + "]"), y = I, m = A, _ = x = "") : s = i)
                    }
                    s || (y = (b = T(o, y))[0], x = b[1], m = (b = T(o, m))[0].replace(/^([+-\/*])=/, function(t, e) {
                      return E = e, ""
                    }), _ = b[1], y = parseFloat(y) || 0, m = parseFloat(m) || 0, "%" === _ && (/^(fontSize|lineHeight)$/.test(o) ? (m /= 100, _ = "em") : /^scale/.test(o) ? (m /= 100, _ = "") : /(Red|Green|Blue)$/i.test(o) && (m = m / 100 * 255, _ = "")));
                    if (/[\/*]/.test(E)) _ = x;
                    else if (x !== _ && 0 !== y)
                      if (0 === m) _ = x;
                      else {
                        a = a || function() {
                          var i = {
                              myParent: t.parentNode || n.body,
                              position: C.getPropertyValue(t, "position"),
                              fontSize: C.getPropertyValue(t, "fontSize")
                            },
                            o = i.position === z.lastPosition && i.myParent === z.lastParent,
                            r = i.fontSize === z.lastFontSize;
                          z.lastParent = i.myParent, z.lastPosition = i.position, z.lastFontSize = i.fontSize;
                          var s = 100,
                            a = {};
                          if (r && o) a.emToPx = z.lastEmToPx, a.percentToPxWidth = z.lastPercentToPxWidth, a.percentToPxHeight = z.lastPercentToPxHeight;
                          else {
                            var l = h && h.isSVG ? n.createElementNS("http://www.w3.org/2000/svg", "rect") : n.createElement("div");
                            w.init(l), i.myParent.appendChild(l), f.each(["overflow", "overflowX", "overflowY"], function(t, e) {
                              w.CSS.setPropertyValue(l, e, "hidden")
                            }), w.CSS.setPropertyValue(l, "position", i.position), w.CSS.setPropertyValue(l, "fontSize", i.fontSize), w.CSS.setPropertyValue(l, "boxSizing", "content-box"), f.each(["minWidth", "maxWidth", "width", "minHeight", "maxHeight", "height"], function(t, e) {
                              w.CSS.setPropertyValue(l, e, s + "%")
                            }), w.CSS.setPropertyValue(l, "paddingLeft", s + "em"), a.percentToPxWidth = z.lastPercentToPxWidth = (parseFloat(C.getPropertyValue(l, "width", null, !0)) || 1) / s, a.percentToPxHeight = z.lastPercentToPxHeight = (parseFloat(C.getPropertyValue(l, "height", null, !0)) || 1) / s, a.emToPx = z.lastEmToPx = (parseFloat(C.getPropertyValue(l, "paddingLeft")) || 1) / s, i.myParent.removeChild(l)
                          }
                          return null === z.remToPx && (z.remToPx = parseFloat(C.getPropertyValue(n.body, "fontSize")) || 16), null === z.vwToPx && (z.vwToPx = parseFloat(e.innerWidth) / 100, z.vhToPx = parseFloat(e.innerHeight) / 100), a.remToPx = z.remToPx, a.vwToPx = z.vwToPx, a.vhToPx = z.vhToPx, w.debug >= 1 && console.log("Unit ratios: " + JSON.stringify(a), t), a
                        }();
                        var q = /margin|padding|left|right|width|text|word|letter/i.test(o) || /X$/.test(o) || "x" === o ? "x" : "y";
                        switch (x) {
                          case "%":
                            y *= "x" === q ? a.percentToPxWidth : a.percentToPxHeight;
                            break;
                          case "px":
                            break;
                          default:
                            y *= a[x + "ToPx"]
                        }
                        switch (_) {
                          case "%":
                            y *= 1 / ("x" === q ? a.percentToPxWidth : a.percentToPxHeight);
                            break;
                          case "px":
                            break;
                          default:
                            y *= 1 / a[_ + "ToPx"]
                        }
                      } switch (E) {
                      case "+":
                        m = y + m;
                        break;
                      case "-":
                        m = y - m;
                        break;
                      case "*":
                        m *= y;
                        break;
                      case "/":
                        m = y / m
                    }
                    d[o] = {
                      rootPropertyValue: c,
                      startValue: y,
                      currentValue: y,
                      endValue: m,
                      unitType: _,
                      easing: g
                    }, s && (d[o].pattern = s), w.debug && console.log("tweensContainer (" + o + "): " + JSON.stringify(d[o]), t)
                  } else w.debug && console.log("Skipping [" + u + "] due to a lack of browser support.")
                };
              for (var N in _)
                if (_.hasOwnProperty(N)) {
                  var j = C.Names.camelCase(N),
                    $ = O(_[N]);
                  if (C.Lists.colors.indexOf(j) >= 0) {
                    var R = $[0],
                      F = $[1],
                      B = $[2];
                    if (C.RegEx.isHex.test(R)) {
                      for (var H = ["Red", "Green", "Blue"], W = C.Values.hexToRgb(R), q = B ? C.Values.hexToRgb(B) : i, V = 0; V < H.length; V++) {
                        var U = [W[V]];
                        F && U.push(F), q !== i && U.push(q[V]), P(j + H[V], U)
                      }
                      continue
                    }
                  }
                  P(j, $)
                } d.element = t
            }
            d.element && (C.Values.addClass(t, "velocity-animating"), M.push(d), (h = r(t)) && ("" === l.queue && (h.tweensContainer = d, h.opts = l), h.isAnimating = !0), A === I - 1 ? (w.State.calls.push([M, y, l, null, S.resolver, null, 0]), !1 === w.State.isTicking && (w.State.isTicking = !0, c())) : A++)
          }
          var a, l = f.extend({}, w.defaults, x),
            d = {};
          switch (r(t) === i && w.init(t), parseFloat(l.delay) && !1 !== l.queue && f.queue(t, l.queue, function(e) {
            w.velocityQueueEntryFlag = !0;
            var n = w.State.delayedElements.count++;
            w.State.delayedElements[n] = t;
            var i = function(t) {
              return function() {
                w.State.delayedElements[t] = !1, e()
              }
            }(n);
            r(t).delayBegin = (new Date).getTime(), r(t).delay = parseFloat(l.delay), r(t).delayTimer = {
              setTimeout: setTimeout(e, parseFloat(l.delay)),
              next: i
            }
          }), l.duration.toString().toLowerCase()) {
            case "fast":
              l.duration = 200;
              break;
            case "normal":
              l.duration = b;
              break;
            case "slow":
              l.duration = 600;
              break;
            default:
              l.duration = parseFloat(l.duration) || 1
          }
          if (!1 !== w.mock && (!0 === w.mock ? l.duration = l.delay = 1 : (l.duration *= parseFloat(w.mock) || 1, l.delay *= parseFloat(w.mock) || 1)), l.easing = u(l.easing, l.duration), l.begin && !v.isFunction(l.begin) && (l.begin = null), l.progress && !v.isFunction(l.progress) && (l.progress = null), l.complete && !v.isFunction(l.complete) && (l.complete = null), l.display !== i && null !== l.display && (l.display = l.display.toString().toLowerCase(), "auto" === l.display && (l.display = w.CSS.Values.getDisplayType(t))), l.visibility !== i && null !== l.visibility && (l.visibility = l.visibility.toString().toLowerCase()), l.mobileHA = l.mobileHA && w.State.isMobile && !w.State.isGingerbread, !1 === l.queue)
            if (l.delay) {
              var h = w.State.delayedElements.count++;
              w.State.delayedElements[h] = t;
              var p = function(t) {
                return function() {
                  w.State.delayedElements[t] = !1, s()
                }
              }(h);
              r(t).delayBegin = (new Date).getTime(), r(t).delay = parseFloat(l.delay), r(t).delayTimer = {
                setTimeout: setTimeout(s, parseFloat(l.delay)),
                next: p
              }
            } else s();
          else f.queue(t, l.queue, function(t, e) {
            return !0 === e ? (S.promise && S.resolver(y), !0) : (w.velocityQueueEntryFlag = !0, void s())
          });
          "" !== l.queue && "fx" !== l.queue || "inprogress" === f.queue(t)[0] || f.dequeue(t)
        }
        var h, p, m, g, y, _, x, T = arguments[0] && (arguments[0].p || f.isPlainObject(arguments[0].properties) && !arguments[0].properties.names || v.isString(arguments[0].properties));
        v.isWrapped(this) ? (p = !1, g = 0, y = this, m = this) : (p = !0, g = 1, y = T ? arguments[0].elements || arguments[0].e : arguments[0]);
        var S = {
          promise: null,
          resolver: null,
          rejecter: null
        };
        if (p && w.Promise && (S.promise = new w.Promise(function(t, e) {
            S.resolver = t, S.rejecter = e
          })), T ? (_ = arguments[0].properties || arguments[0].p, x = arguments[0].options || arguments[0].o) : (_ = arguments[g], x = arguments[g + 1]), y = o(y)) {
          var k, I = y.length,
            A = 0;
          if (!/^(stop|finish|finishAll|pause|resume)$/i.test(_) && !f.isPlainObject(x)) {
            x = {};
            for (var D = g + 1; D < arguments.length; D++) v.isArray(arguments[D]) || !/^(fast|normal|slow)$/i.test(arguments[D]) && !/^\d/.test(arguments[D]) ? v.isString(arguments[D]) || v.isArray(arguments[D]) ? x.easing = arguments[D] : v.isFunction(arguments[D]) && (x.complete = arguments[D]) : x.duration = arguments[D]
          }
          switch (_) {
            case "scroll":
              k = "scroll";
              break;
            case "reverse":
              k = "reverse";
              break;
            case "pause":
              var L = (new Date).getTime();
              return f.each(y, function(t, e) {
                s(e, L)
              }), f.each(w.State.calls, function(t, e) {
                var n = !1;
                e && f.each(e[1], function(t, o) {
                  var r = x === i ? "" : x;
                  return !0 !== r && e[2].queue !== r && (x !== i || !1 !== e[2].queue) || (f.each(y, function(t, i) {
                    if (i === o) return e[5] = {
                      resume: !1
                    }, n = !0, !1
                  }), !n && void 0)
                })
              }), t();
            case "resume":
              return f.each(y, function(t, e) {
                a(e)
              }), f.each(w.State.calls, function(t, e) {
                var n = !1;
                e && f.each(e[1], function(t, o) {
                  var r = x === i ? "" : x;
                  return !0 !== r && e[2].queue !== r && (x !== i || !1 !== e[2].queue) || !e[5] || (f.each(y, function(t, i) {
                    if (i === o) return e[5].resume = !0, n = !0, !1
                  }), !n && void 0)
                })
              }), t();
            case "finish":
            case "finishAll":
            case "stop":
              f.each(y, function(t, e) {
                r(e) && r(e).delayTimer && (clearTimeout(r(e).delayTimer.setTimeout), r(e).delayTimer.next && r(e).delayTimer.next(), delete r(e).delayTimer), "finishAll" !== _ || !0 !== x && !v.isString(x) || (f.each(f.queue(e, v.isString(x) ? x : ""), function(t, e) {
                  v.isFunction(e) && e()
                }), f.queue(e, v.isString(x) ? x : "", []))
              });
              var O = [];
              return f.each(w.State.calls, function(t, e) {
                e && f.each(e[1], function(n, o) {
                  var s = x === i ? "" : x;
                  return !0 !== s && e[2].queue !== s && (x !== i || !1 !== e[2].queue) || void f.each(y, function(n, i) {
                    if (i === o)
                      if ((!0 === x || v.isString(x)) && (f.each(f.queue(i, v.isString(x) ? x : ""), function(t, e) {
                          v.isFunction(e) && e(null, !0)
                        }), f.queue(i, v.isString(x) ? x : "", [])), "stop" === _) {
                        var a = r(i);
                        a && a.tweensContainer && !1 !== s && f.each(a.tweensContainer, function(t, e) {
                          e.endValue = e.currentValue
                        }), O.push(t)
                      } else "finish" !== _ && "finishAll" !== _ || (e[2].duration = 1)
                  })
                })
              }), "stop" === _ && (f.each(O, function(t, e) {
                d(e, !0)
              }), S.promise && S.resolver(y)), t();
            default:
              if (!f.isPlainObject(_) || v.isEmptyObject(_)) {
                if (v.isString(_) && w.Redirects[_]) {
                  var P = (h = f.extend({}, x)).duration,
                    N = h.delay || 0;
                  return !0 === h.backwards && (y = f.extend(!0, [], y).reverse()), f.each(y, function(t, e) {
                    parseFloat(h.stagger) ? h.delay = N + parseFloat(h.stagger) * t : v.isFunction(h.stagger) && (h.delay = N + h.stagger.call(e, t, I)), h.drag && (h.duration = parseFloat(P) || (/^(callout|transition)/.test(_) ? 1e3 : b), h.duration = Math.max(h.duration * (h.backwards ? 1 - t / I : (t + 1) / I), .75 * h.duration, 200)), w.Redirects[_].call(e, e, h || {}, t, I, y, S.promise ? S : i)
                  }), t()
                }
                var j = "Velocity: First argument (" + _ + ") was not a property map, a known action, or a registered redirect. Aborting.";
                return S.promise ? S.rejecter(new Error(j)) : console.log(j), t()
              }
              k = "start"
          }
          var z = {
              lastParent: null,
              lastPosition: null,
              lastFontSize: null,
              lastPercentToPxWidth: null,
              lastPercentToPxHeight: null,
              lastEmToPx: null,
              remToPx: null,
              vwToPx: null,
              vhToPx: null
            },
            M = [];
          f.each(y, function(t, e) {
            v.isNode(e) && l(e, t)
          }), (h = f.extend({}, w.defaults, x)).loop = parseInt(h.loop, 10);
          var $ = 2 * h.loop - 1;
          if (h.loop)
            for (var R = 0; R < $; R++) {
              var F = {
                delay: h.delay,
                progress: h.progress
              };
              R === $ - 1 && (F.display = h.display, F.visibility = h.visibility, F.complete = h.complete), E(y, "reverse", F)
            }
          return t()
        }
        S.promise && (_ && x && !1 === x.promiseRejectEmpty ? S.resolver() : S.rejecter())
      };
      (w = f.extend(E, w)).animate = E;
      var T = e.requestAnimationFrame || p;
      if (!w.State.isMobile && n.hidden !== i) {
        var S = function() {
          n.hidden ? (T = function(t) {
            return setTimeout(function() {
              t(!0)
            }, 16)
          }, c()) : T = e.requestAnimationFrame || p
        };
        S(), n.addEventListener("visibilitychange", S)
      }
      return t.Velocity = w, t !== e && (t.fn.velocity = E, t.fn.velocity.defaults = w.defaults), f.each(["Down", "Up"], function(t, e) {
        w.Redirects["slide" + e] = function(t, n, o, r, s, a) {
          var l = f.extend({}, n),
            u = l.begin,
            c = l.complete,
            d = {},
            h = {
              height: "",
              marginTop: "",
              marginBottom: "",
              paddingTop: "",
              paddingBottom: ""
            };
          l.display === i && (l.display = "Down" === e ? "inline" === w.CSS.Values.getDisplayType(t) ? "inline-block" : "block" : "none"), l.begin = function() {
            for (var n in 0 === o && u && u.call(s, s), h)
              if (h.hasOwnProperty(n)) {
                d[n] = t.style[n];
                var i = C.getPropertyValue(t, n);
                h[n] = "Down" === e ? [i, 0] : [0, i]
              } d.overflow = t.style.overflow, t.style.overflow = "hidden"
          }, l.complete = function() {
            for (var e in d) d.hasOwnProperty(e) && (t.style[e] = d[e]);
            o === r - 1 && (c && c.call(s, s), a && a.resolver(s))
          }, w(t, h, l)
        }
      }), f.each(["In", "Out"], function(t, e) {
        w.Redirects["fade" + e] = function(t, n, o, r, s, a) {
          var l = f.extend({}, n),
            u = l.complete,
            c = {
              opacity: "In" === e ? 1 : 0
            };
          0 !== o && (l.begin = null), l.complete = o !== r - 1 ? null : function() {
            u && u.call(s, s), a && a.resolver(s)
          }, l.display === i && (l.display = "In" === e ? "auto" : "none"), w(this, c, l)
        }
      }), w
    }
    jQuery.fn.velocity = jQuery.fn.animate
  }(window.jQuery || window.Zepto || window, window, window ? window.document : void 0)
});
var componentLoader = {
  components: {},
  loadedElements: {
    document: []
  },
  presets: {
    "hero-slider-section": ["owlCarousel", "countDowns"],
    "featured-collection-section": ["owlCarousel", "isotopGrid", "productBadges"],
    "collection-list-section": ["isotopGrid", "owlCarousel"],
    "social-instagram-section": ["instagramFeed"]
  },
  loadEl: function(t) {
    var e = (t = t || document) == document;
    t = e ? $(document) : t;
    var n = Object.keys(this.components),
      i = e ? "document" : t.attr("id");
    if (!e)
      for (var o in this.presets) t.find("section").hasClass(o) && (n = this.presets[o]);
    var r = this.components,
      s = this.loadedElements;
    s[i] = [], n.forEach(function(e) {
      var n = jQuery.extend(!0, {}, r[e]);
      n.$el = t, n.hasComponent() && (n.load(), s[i].push(n))
    })
  },
  unloadEl: function(t) {
    var e = (t = t || document) == document ? "document" : t.attr("id"),
      n = this.loadedElements[e];
    void 0 === n || n.size <= 0 || (n.forEach(function(t) {
      t.unload()
    }), delete this.loadedElements[e])
  }
};
componentLoader.components.scrollbarWidthAdjust = {
  $el: $(document),
  hasComponent: function() {
    if ("number" == typeof window.innerWidth) return window.innerWidth > document.documentElement.clientWidth;
    var t, e, n = document.documentElement || document.body;
    void 0 !== n.currentStyle && (t = n.currentStyle.overflow), t = t || window.getComputedStyle(n, "").overflow, void 0 !== n.currentStyle && (e = n.currentStyle.overflowY), e = e || window.getComputedStyle(n, "").overflowY;
    var i = n.scrollHeight > n.clientHeight,
      o = /^(visible|auto)$/.test(t) || /^(visible|auto)$/.test(e);
    return i && o || ("scroll" === t || "scroll" === e)
  },
  load: function() {
    this.$el.find("body").addClass("hasScrollbar")
  },
  unload: function() {
    this.$el.find("body").removeClass("hasScrollbar")
  }
}, componentLoader.components.dummylinks = {
  selector: 'a[href="#"]',
  $el: $(document),
  hasComponent: function() {
    return this.$el.find(this.selector).length > 0
  },
  $emptyLink: function() {
    return this.$el.find(this.selector)
  },
  clickDefaultHandler: function(t) {
    t.preventDefault()
  },
  load: function() {
    this.$emptyLink().on("click", this.clickDefaultHandler)
  },
  unload: function() {
    this.$emptyLink().off("click", this.clickDefaultHandler)
  }
}, componentLoader.components.stickyHeader = {
  $el: $(document),
  selector: ".navbar",
  hasComponent: function() {
    return this.$el.find(this.selector).length > 0
  },
  prevDisplay: "",
  scrollFunc: function() {
    var t = ".dragondrop-anoun-bar",
      e = $(".navbar"),
      n = $(".navbar-sticky"),
      i = $(".topbar").not(t),
      o = i.outerHeight(),
      r = $("body"),
      s = e.find(".site-logo"),
      a = $(t),
      l = $(".dragondrop-anoun-bar-sticky"),
      u = 0,
      c = 0;
    if (a.length > 0)
      for (var d = 0; d < a.length; d++) {
        var f = $(a[d]),
          h = f.outerHeight();
        f.hasClass("dragondrop-anoun-bar-sticky") && h > c && (c = h), h > u && (u = h)
      }
    var p = e.outerHeight(),
      m = n.outerHeight();
    if ($(this).scrollTop() > p + c + o) {
      if (n.length <= 0 && l.length <= 0) return;
      n.addClass("navbar-stuck"), l.addClass("anoun-fixed"), e.css("top", c), e.hasClass("navbar-ghost") ? s.addClass("logo-stuck") : r.css("padding-top", m + c)
    } else n.removeClass("navbar-stuck"), l.removeClass("anoun-fixed"), r.css("padding-top", 0), e.hasClass("navbar-ghost") ? (e.css("top", u + o), i.css("top", u), s.removeClass("logo-stuck")) : e.css("top", 0)
  },
  galleryOpened: function() {
    $(".topbar").css("display", "none")
  },
  galleryClosed: function() {
    $(".topbar").css("display", "")
  },
  load: function() {
    $(window).on("scroll", this.scrollFunc), $(window).on("load", this.scrollFunc), $(document).on("pswp.init", this.galleryOpened), $(document).on("pswp.close", this.galleryClosed)
  },
  unload: function() {
    $(window).off("scroll", this.scrollFunc), $(window).off("load", this.scrollFunc), $(document).off("pswp.init", this.galleryOpened), $(document).off("pswp.close", this.galleryClosed)
  }
}, componentLoader.components.siteSearch = {
  selector: ".toolbar .tools .search",
  $el: $(document),
  hasComponent: function() {
    return this.$el.find(this.selector).length > 0
  },
  load: function() {
    var t = this.$el;
    this.openTrigger = openTrigger = this.selector, this.closeTrigger = closeTrigger = ".close-search", this.clearTrigger = clearTrigger = ".clear-search", this.target = target = ".site-search", this.openTriggerHandler = function() {
      $(target).addClass("search-visible"), setTimeout(function() {
        $(target + " > input").focus()
      }, 200)
    }, t.find(openTrigger).on("click", this.openTriggerHandler), this.closeTriggerHandler = function() {
      $(target).removeClass("search-visible")
    }, t.find(closeTrigger).on("click", this.closeTriggerHandler), this.clearTriggerHandler = function() {
      $(target + " > input").val(""), setTimeout(function() {
        $(target + " > input").focus()
      }, 200)
    }, t.find(clearTrigger).on("click", this.clearTriggerHandler)
  },
  unload: function() {
    $(this.openTrigger).off("click", this.openTriggerHandler), $(this.closeTrigger).off("click", this.closeTriggerHandler), $(this.clearTrigger).off("click", this.clearTriggerHandler)
  }
}, componentLoader.components.langCurrencySwitcher = {
  selector: ".lang-currency-switcher-wrap",
  $el: $(document),
  hasComponent: function() {
    return this.$el.find(this.selector).length > 0
  },
  load: function() {
    var t = this.$el;
    this.currencyActivate = function() {
      t.find(this.selector).find(".currency-picker").trigger("click")
    }, t.find(this.selector).find("i").on("click", this.currencyActivate)
  },
  unload: function() {
    $el.find(this.selector).find("i").off("click", this.currencyActivate)
  }
}, componentLoader.components.sideCartContainer = {
  selector: '[data-toggle="sidecart"]',
  $el: $(document),
  hasComponent: function() {
    return this.$el.find(this.selector).length > 0
  },
  load: function() {
    this.sideCartOpen = function(t) {
      var e = $("body"),
        n = $(t.target).attr("href");
      $(n).addClass("active"), e.css("overflow", "hidden"), e.addClass("sidecart-open"), t.preventDefault()
    }, this.$el.find('[data-toggle="sidecart"]').on("click", this.sideCartOpen), this.sideCartClose = function() {
      var t = $("body");
      t.removeClass("sidecart-open"), setTimeout(function() {
        t.css("overflow", "visible"), $(".sidecart-container").removeClass("active")
      }, 450)
    }, this.$el.find(".site-backdrop").on("click", this.sideCartClose)
  },
  unload: function() {
    this.$el.find('[data-toggle="sidecart"]').off("click", this.sideCartOpen), this.$el.find(".site-backdrop").off("click", this.sideCartClose)
  }
}, componentLoader.components.offCanvasContainer = {
  selector: '[data-toggle="offcanvas"]',
  $el: $(document),
  hasComponent: function() {
    return this.$el.find(this.selector).length > 0
  },
  load: function() {
    this.offcanvasOpen = function(t) {
      var e = $("body"),
        n = $(t.target).attr("href");
      $(n).addClass("active"), e.css("overflow", "hidden"), e.addClass("offcanvas-open"), t.preventDefault()
    }, this.$el.find('[data-toggle="offcanvas"]').on("click", this.offcanvasOpen), this.offcanvasClose = function() {
      var t = $("body");
      t.removeClass("offcanvas-open"), setTimeout(function() {
        t.css("overflow", "visible"), $(".offcanvas-container").removeClass("active")
      }, 450)
    }, this.$el.find(".site-backdrop").on("click", this.offcanvasClose)
  },
  unload: function() {
    this.$el.find('[data-toggle="offcanvas"]').off("click", this.offcanvasOpen), this.$el.find(".site-backdrop").off("click", this.offcanvasClose)
  }
};
var backBtnText = "Back",
  subMenu = $(".offcanvas-menu .offcanvas-submenu"),
  hasChildLink = $(".has-children .sub-menu-toggle");
if (componentLoader.components.offCanvasMenu = {
    selector: ".offcanvas-menu .menu",
    backSelector: "li.back-btn",
    menuInitHeight: 0,
    $el: $(document),
    hasComponent: function() {
      return this.$el.find(this.selector).length > 0
    },
    load: function() {
      var t = this;
      subMenu.find(this.backSelector).length <= 0 && subMenu.each(function() {
        $(this).prepend('<li class="back-btn"><a href="#">' + backBtnText + "</a></li>")
      }), this.backButtonHandler = function(e) {
        var n = $(this).closest(".menu, .offcanvas-submenu"),
          i = n.parents(".menu, .offcanvas-submenu").first(),
          o = $(this).closest(".menu");
        n.removeClass("in-view"), i.removeClass("off-view"), "menu" === i.attr("class") ? o.css("height", t.menuInitHeight) : o.css("height", i.height()), e.preventDefault()
      }, this.$el.find(this.backSelector).on("click", this.backButtonHandler), this.childLinkHandler = function(e) {
        var n = this,
          i = $(n).closest(".menu, .offcanvas-submenu"),
          o = $(n).closest("li.has-children").find(".offcanvas-submenu").first(),
          r = $(n).parents(".menu");
        return "menu" === i.attr("class") && (t.menuInitHeight = i.height()), i.addClass("off-view"), o.addClass("in-view"), r.css("height", o.height()), e.preventDefault(), !1
      }, hasChildLink.on("click", this.childLinkHandler)
    },
    unload: function() {
      backBtn.off("click", this.backButtonHandler), hasChildLink.off("click", this.childLinkHandler)
    }
  }, componentLoader.components.scrollToTopButton = {
    selector: ".scroll-to-top-btn",
    $el: $(document),
    hasComponent: function() {
      return this.$el.find(this.selector).length > 0
    },
    $scrollTop: function() {
      return this.$el.find(this.selector)
    },
    load: function() {
      var t = this.$scrollTop();
      this.scrollHandler = function() {
        $(this).scrollTop() > 600 ? t.addClass("visible") : t.removeClass("visible")
      }, this.scrollTopClickHandler = function(t) {
        t.preventDefault(), $("html").velocity("scroll", {
          offset: 0,
          duration: 1200,
          easing: "easeOutExpo",
          mobileHA: !1
        })
      }, t.length > 0 && ($(window).on("scroll", this.scrollHandler), t.on("click", this.scrollTopClickHandler))
    },
    unload: function() {
      $scrollTop.length > 0 && ($(window).off("scroll", this.scrollHandler), $scrollTop.off("click", this.scrollTopClickHandler))
    }
  }, componentLoader.components.smoothScroll = {
    selector: ".scroll-to",
    $el: $(document),
    hasComponent: function() {
      return this.$el.find(this.selector).length > 0
    },
    scrollHandler: function(t) {
      var e = $(this).attr("href");
      if ("#" === e) return !1;
      var n = $(e);
      if (n.length > 0) {
        var i = n.data("offset-top") || 70;
        $("html").velocity("scroll", {
          offset: $(this.hash).offset().top - i,
          duration: 1e3,
          easing: "easeOutExpo",
          mobileHA: !1
        })
      }
      t.preventDefault()
    },
    load: function() {
      $(document).on("click", this.selector, this.scrollHandler)
    },
    unload: function() {
      $(document).off("click", this.selector, this.scrollHandler)
    }
  }, componentLoader.components.countDowns = {
    selector: ".countdown",
    $el: $(document),
    hasComponent: function() {
      return this.$el.find(this.selector).length > 0
    },
    intervalArray: [],
    load: function() {
      var t = this.intervalArray;
      ! function(e, n) {
        e.each(function() {
          var e = $(this),
            i = $(this).data("date-time"),
            o = i.substring(0, 10).split("-"),
            r = i.substr(11),
            s = new Date(o[1] + "/" + o[2] + "/" + o[0] + " " + r),
            a = (n || e).downCount({
              date: s,
              offset: s.getTimezoneOffset() / 60 * -1
            });
          t.push(a)
        })
      }(this.$el.find(this.selector))
    },
    unload: function() {
      this.intervalArray.forEach(function(t) {
        clearInterval(t)
      })
    }
  }, componentLoader.components.iziToastNotifications = {
    selector: "[data-toast]",
    $el: $(document),
    hasComponent: function() {
      return this.$el.find(this.selector).length > 0
    },
    toastHandler: function(t) {
      var e = $(this),
        n = e.data("toast-type"),
        i = e.data("toast-icon"),
        o = e.data("toast-position"),
        r = e.data("toast-title"),
        s = e.data("toast-message"),
        a = "";
      switch (o) {
        case "topRight":
          a = {
            class: "iziToast-" + n || "",
            title: r || "Title",
            message: s || "toast message",
            animateInside: !1,
            position: "topRight",
            progressBar: !1,
            icon: i,
            timeout: 3200,
            transitionIn: "fadeInLeft",
            transitionOut: "fadeOut",
            transitionInMobile: "fadeIn",
            transitionOutMobile: "fadeOut"
          };
          break;
        case "bottomRight":
          a = {
            class: "iziToast-" + n || "",
            title: r || "Title",
            message: s || "toast message",
            animateInside: !1,
            position: "bottomRight",
            progressBar: !1,
            icon: i,
            timeout: 3200,
            transitionIn: "fadeInLeft",
            transitionOut: "fadeOut",
            transitionInMobile: "fadeIn",
            transitionOutMobile: "fadeOut"
          };
          break;
        case "topLeft":
          a = {
            class: "iziToast-" + n || "",
            title: r || "Title",
            message: s || "toast message",
            animateInside: !1,
            position: "topLeft",
            progressBar: !1,
            icon: i,
            timeout: 3200,
            transitionIn: "fadeInRight",
            transitionOut: "fadeOut",
            transitionInMobile: "fadeIn",
            transitionOutMobile: "fadeOut"
          };
          break;
        case "bottomLeft":
          a = {
            class: "iziToast-" + n || "",
            title: r || "Title",
            message: s || "toast message",
            animateInside: !1,
            position: "bottomLeft",
            progressBar: !1,
            icon: i,
            timeout: 3200,
            transitionIn: "fadeInRight",
            transitionOut: "fadeOut",
            transitionInMobile: "fadeIn",
            transitionOutMobile: "fadeOut"
          };
          break;
        case "topCenter":
          a = {
            class: "iziToast-" + n || "",
            title: r || "Title",
            message: s || "toast message",
            animateInside: !1,
            position: "topCenter",
            progressBar: !1,
            icon: i,
            timeout: 3200,
            transitionIn: "fadeInDown",
            transitionOut: "fadeOut",
            transitionInMobile: "fadeIn",
            transitionOutMobile: "fadeOut"
          };
          break;
        case "bottomCenter":
          a = {
            class: "iziToast-" + n || "",
            title: r || "Title",
            message: s || "toast message",
            animateInside: !1,
            position: "bottomCenter",
            progressBar: !1,
            icon: i,
            timeout: 3200,
            transitionIn: "fadeInUp",
            transitionOut: "fadeOut",
            transitionInMobile: "fadeIn",
            transitionOutMobile: "fadeOut"
          };
          break;
        default:
          a = {
            class: "iziToast-" + n || "",
            title: r || "Title",
            message: s || "toast message",
            animateInside: !1,
            position: "topRight",
            progressBar: !1,
            icon: i,
            timeout: 3200,
            transitionIn: "fadeInLeft",
            transitionOut: "fadeOut",
            transitionInMobile: "fadeIn",
            transitionOutMobile: "fadeOut"
          }
      }
      iziToast.show(a)
    },
    load: function() {
      this.$el.find(this.selector).on("click", this.toastHandler)
    },
    unload: function() {
      this.$el.find(this.selector).off("click", this.toastHandler)
    }
  }, componentLoader.components.wishList = {
    selector: ".btn-wishlist",
    hasComponent: function() {
      return this.$el.find(this.selector).length > 0
    },
    wishlistClickHandler: function(t) {
      t.preventDefault();
      var e = $(this).data("iteration") || 1,
        n = {
          title: "Product",
          animateInside: !1,
          position: "topRight",
          progressBar: !1,
          timeout: 3200,
          transitionIn: "fadeInLeft",
          transitionOut: "fadeOut",
          transitionInMobile: "fadeIn",
          transitionOutMobile: "fadeOut"
        };
      switch (e) {
        case 1:
          $(this).addClass("active"), n.class = "iziToast-info", n.message = "added to your wishlist!", n.icon = "icon-bell";
          break;
        case 2:
          $(this).removeClass("active"), n.class = "iziToast-danger", n.message = "removed from your wishlist!", n.icon = "icon-ban"
      }
      iziToast.show(n), ++e > 2 && (e = 1), $(this).data("iteration", e)
    },
    load: function() {
      this.$el.find(this.selector).on("click", this.wishlistClickHandler)
    },
    unload: function() {
      this.$el.find(this.selector).off("click", this.wishlistClickHandler)
    }
  }, componentLoader.components.isotopGrid = {
    selector: ".isotope-grid",
    $el: $(document),
    hasComponent: function() {
      return this.$el.find(this.selector).length > 0
    },
    load: function() {
      var t = this.$el,
        e = t.find(".isotope-grid").imagesLoaded(function() {
          e.isotope({
            itemSelector: ".grid-item",
            layoutMode: "fitRows",
            fitRows: {
              columnWidth: ".grid-sizer",
              gutter: ".gutter-sizer"
            },
            transitionDuration: "0s"
          })
        });
      if (t.find(".filter-grid").length > 0) {
        var n = t.find(".filter-grid");
        t.find(".nav-pills").on("click", "a", function(e) {
          e.preventDefault(), t.find(".nav-pills a").removeClass("active"), $(this).addClass("active");
          var i = $(this).attr("data-filter");
          n.isotope({
            filter: i
          })
        })
      }
    },
    unload: function() {
      var t = this.$el;
      t.find(".filter-grid").length > 0 && t.find(".nav-pills").off("click", "a"), t.find(".isotope-grid").length && t.find(".isotope-grid").isotope("destroy")
    }
  }, componentLoader.components.productBadges = {
    selector: ".curved-overlay.overlay-bottom",
    $el: $(document),
    hasComponent: function() {
      return this.$el.find(this.selector).length > 0
    },
    load: function() {
      var t, e = this.$el,
        n = this.selector;
      this.badgePosition = function() {
        clearTimeout(t), t = setTimeout(function() {
          e.find(n).each(function(t, e) {
            var n = $(e),
              i = n.closest(".product-thumb").find("img").height();
            n.css("top", i + "px")
          })
        }, 250)
      }, $(window).resize(this.badgePosition), $(this.selector).closest(".product-thumb").find("img").on("load", this.badgePosition)
    },
    unload: function() {
      $(window).off("resize", this.badgePosition)
    }
  }, componentLoader.components.categoryChildMenu = {
    selector: ".widget-categories .has-children > a",
    $el: $(document),
    hasComponent: function() {
      return this.$el.find(this.selector).length > 0
    },
    categoryToggle: function() {
      return this.$el.find(this.selector)
    },
    load: function() {
      function t() {
        e.parent().removeClass("expanded")
      }
      var e = this.categoryToggle();
      this.clickHandler = function(e) {
        $(e.target).parent().is(".expanded") ? t() : (t(), $(this).parent().addClass("expanded"))
      }, e.on("click", this.clickHandler)
    },
    unload: function() {
      categoryToggle.off("click", this.clickHandler)
    }
  }, componentLoader.components.bootstrapToolTips = {
    selector: '[data-toggle="tooltip"]',
    $el: $(document),
    hasComponent: function() {
      return this.$el.find(this.selector).length > 0
    },
    load: function() {},
    unload: function() {
      this.$el.find(this.selector).tooltip("destroy")
    }
  }, componentLoader.components.bootstrapPopovers = {
    selector: '[data-toggle="popover"]',
    $el: $(document),
    hasComponent: function() {
      return this.$el.find(this.selector).length > 0
    },
    load: function() {
      this.$el.find(this.selector).popover()
    },
    unload: function() {
      this.$el.find(this.selector).popover("destroy")
    }
  }, componentLoader.components.photoSwipeGallery = {
    selector: ".gallery-wrapper",
    $el: $(document),
    hasComponent: function() {
      return this.$el.find(this.selector).length > 0
    },
    initPhotoSwipeFromDOM: function(t) {
      for (var e = function(t) {
          (t = t || window.event).preventDefault ? t.preventDefault() : t.returnValue = !1;
          var e = function t(e, n) {
            return e && (n(e) ? e : t(e.parentNode, n))
          }(t.target || t.srcElement, function(t) {
            return function(t, e) {
              return (" " + t.className + " ").indexOf(" " + e + " ") > -1
            }(t, "gallery-item")
          });
          if (e) {
            for (var i, o = e.closest(".gallery-wrapper"), r = $(e.closest(".gallery-wrapper")).find(".gallery-item:not(.isotope-hidden)").get(), s = r.length, a = 0, l = 0; l < s; l++)
              if (1 === r[l].nodeType) {
                if (r[l] === e) {
                  i = a;
                  break
                }
                a++
              } return i >= 0 && n(i, o), !1
          }
        }, n = function(t, e, n, i) {
          var o, r, s, a = document.querySelectorAll(".pswp")[0];
          if (s = function(t) {
              for (var e, n, i, o, r = $(t).find(".gallery-item:not(.isotope-hidden)").get(), s = r.length, a = [], l = 0; l < s; l++) 1 === (e = r[l]).nodeType && (n = e.children[0], "video" == $(n).data("type") ? o = {
                html: $(n).data("video")
              } : (i = n.getAttribute("data-size").split("x"), o = {
                src: n.getAttribute("href"),
                w: parseInt(i[0], 10),
                h: parseInt(i[1], 10)
              }), e.children.length > 1 && (o.title = $(e).find(".caption").html()), n.children.length > 0 && (o.msrc = n.children[0].getAttribute("src")), o.el = e, a.push(o));
              return a
            }(e), r = {
              closeOnScroll: !1,
              galleryUID: e.getAttribute("data-pswp-uid"),
              getThumbBoundsFn: function(t) {
                var e = s[t].el.getElementsByTagName("img")[0];
                if ($(e).length > 0) {
                  var n = window.pageYOffset || document.documentElement.scrollTop,
                    i = e.getBoundingClientRect();
                  return {
                    x: i.left,
                    y: i.top + n,
                    w: i.width
                  }
                }
              }
            }, i)
            if (r.galleryPIDs) {
              for (var l = 0; l < s.length; l++)
                if (s[l].pid == t) {
                  r.index = l;
                  break
                }
            } else r.index = parseInt(t, 10) - 1;
          else r.index = parseInt(t, 10);
          isNaN(r.index) || (n && (r.showAnimationDuration = 0), (o = new PhotoSwipe(a, PhotoSwipeUI_Default, s, r)).listen("afterInit", function() {
            $(document).trigger("pswp.init")
          }), o.init(), $(".navbar").hide(), o.listen("beforeChange", function() {
            var t = $(o.currItem.container);
            $(".pswp__video").removeClass("active"), t.find(".pswp__video").addClass("active"), $(".pswp__video").each(function() {
              $(this).hasClass("active") || $(this).attr("src", $(this).attr("src"))
            })
          }), o.listen("close", function() {
            $(".navbar").show(), $(".pswp__video").each(function() {
              $(this).attr("src", $(this).attr("src"))
            }), $(document).trigger("pswp.close")
          }))
        }, i = this.$el.find(t), o = 0, r = i.length; o < r; o++) i[o].setAttribute("data-pswp-uid", o + 1), i[o].onclick = e;
      var s = function() {
        var t = window.location.hash.substring(1),
          e = {};
        if (t.length < 5) return e;
        for (var n = t.split("&"), i = 0; i < n.length; i++)
          if (n[i]) {
            var o = n[i].split("=");
            o.length < 2 || (e[o[0]] = o[1])
          } return e.gid && (e.gid = parseInt(e.gid, 10)), e
      }();
      s.pid && s.gid && n(s.pid, i[s.gid - 1], !0, !0)
    },
    load: function() {
      this.initPhotoSwipeFromDOM(this.selector)
    },
    unload: function() {}
  }, componentLoader.components.owlCarousel = {
    selector: ".owl-carousel",
    $el: $(document),
    hasComponent: function() {
      return this.$el.find(this.selector).length > 0
    },
    load: function() {
      var t = (this.$el.find(this.selector), {
        items: 1,
        loop: !1,
        nav: !1,
        navText: [],
        dots: !0,
        slideBy: 1,
        lazyLoad: !1,
        autoplay: !1,
        autoplayTimeout: 4e3,
        responsive: {},
        animateOut: !1,
        animateIn: !1,
        smartSpeed: 450,
        navSpeed: 450
      });
      $.each(this.$el.find(this.selector), function(e, n) {
        var i = $(n),
          o = $.extend(!0, {}, t, i.data("owl-carousel"));
        i.owlCarousel(o)
      })
    },
    unload: function() {
      this.$el.find(this.selector).owlCarousel("destroy")
    }
  }, componentLoader.components.productThumbGallery = {
    selector: ".product-carousel",
    $el: $(document),
    hasComponent: function() {
      return this.$el.find(this.selector).length > 0
    },
    load: function() {
      return simply.initProductSlider(), !1
    },
    unload: function() {
      return simply.distroyProductSlider(), !1
    }
  }, componentLoader.components.googleMaps = {
    selector: ".google-map",
    $el: $(document),
    hasComponent: function() {
      return this.$el.find(this.selector).length > 0
    },
    load: function() {
      var t = this.$el.find(this.selector);
      t.length && t.each(function() {
        var t = $(this).data("height"),
          e = $(this).data("address"),
          n = $(this).data("zoom"),
          i = $(this).data("disable-controls"),
          o = $(this).data("scrollwheel"),
          r = $(this).data("marker"),
          s = $(this).data("marker-title"),
          a = $(this).data("styles");
        $(this).height(t), $(this).gmap3({
          marker: {
            address: e,
            data: s,
            options: {
              icon: r
            },
            events: {
              mouseover: function(t, e, n) {
                var i = $(this).gmap3("get"),
                  o = $(this).gmap3({
                    get: {
                      name: "infowindow"
                    }
                  });
                o ? (o.open(i, t), o.setContent(n.data)) : $(this).gmap3({
                  infowindow: {
                    anchor: t,
                    options: {
                      content: n.data
                    }
                  }
                })
              },
              mouseout: function() {
                var t = $(this).gmap3({
                  get: {
                    name: "infowindow"
                  }
                });
                t && t.close()
              }
            }
          },
          map: {
            options: {
              zoom: n,
              disableDefaultUI: i,
              scrollwheel: o,
              styles: a
            }
          }
        })
      })
    },
    unload: function() {
      this.$el.find(this.selector).gmap3("destroy")
    }
  }, componentLoader.components.instagramFeed = {
    selector: ".instagram-feed",
    $el: $(document),
    hasComponent: function() {
      return 0 < this.$el.find(this.selector).length
    },
    load: function() {
      return !1
    },
    unload: function() {
      this.$el.find(this.selector).isotope("destroy"), this.$el.find(this.selector).find(".grid-item").remove()
    }
  }, componentLoader.components.autoComplete = {
    selector: ".non-existent",
    $el: $(document),
    hasComponent: function() {
      return this.$el.find(this.selector).length > 0
    },
    load: function() {
      if (this.$el.find(this.selector).length) {
        var t = null;
        $('form[action="/search"]').each(function() {
          var e = $(this).find('input[name="q"]'),
            n = e.position().top + e.innerHeight();
          $('<ul class="search-results"></ul>').css({
            position: "absolute",
            left: "0px",
            top: n
          }).appendTo($(this)).hide(), e.attr("autocomplete", "off").bind("keyup change", function() {
            var e = $(this).val(),
              n = $(this).closest("form"),
              i = "/search?type=product&q=" + e,
              o = n.find(".search-results");
            e.length > 2 && ($(this).attr("data-old-term", e), null != t && t.abort(), t = $.getJSON(i + "&view=json", function(t) {
              o.empty(), 0 == t.results_count ? o.hide() : ($.each(t.results, function(t, e) {
                var n = $("<a></a>").attr("href", e.url);
                n.append('<span class="thumbnail"><img src="' + e.thumbnail + '" /></span>');
                var i = $('<div class="description-container"></div>');
                n.append(i), i.append('<span class="title h5">' + e.title + "</span>"), i.append('<span class="price h6">' + e.price + "</span>"), i.append('<p class="description">' + e.description + "</p>"), n.wrap("<li></li>"), o.append(n.parent())
              }), t.results_count > 10 && o.append('<li><span class="title"><a href="' + i + '">See all results (' + t.results_count + ")</a></span></li>"), o.fadeIn(200))
            }))
          })
        });
        $("body").bind("click", function() {
          $(".search-results").hide()
        })
      }
    },
    unload: function() {}
  }, componentLoader.components.loadMorePagination = {
    selector: ".btn-loadmore-pagination",
    $el: $(document),
    hasComponent: function() {
      return this.$el.find(this.selector).length > 0
    },
    load: function() {
      var t = this.$el,
        e = !1,
        n = t.find(this.selector),
        i = $(".loadmore-container"),
        o = i.hasClass("isotope-grid");
      this.clickHandler = function(t) {
        var n = $(this),
          r = n.data("url");
        e || (console.log("loading more items"), $.ajax({
          url: r,
          beforeSend: function() {
            n.find("i.fa").show(), n.attr("disabled", !0), e = !0
          },
          success: function(t) {
            var e = $(t),
              r = e.find(".loadmore-container"),
              s = e.find(".btn-loadmore-pagination"),
              a = [];
            o ? (a = r.find(".grid-item"), i.isotope("insert", a), i.imagesLoaded(function() {
              i.isotope("layout")
            })) : (a = r.find(".product-item"), i.append(a));
            var l = s.data("url");
            n.data("url", l), "#" === l && n.hide()
          },
          complete: function() {
            e = !1, n.find("i.fa").hide(), n.removeAttr("disabled")
          }
        }))
      }, n.on("click", this.clickHandler)
    },
    unload: function() {
      $loadMoreBtn.off("click", this.clickHandler)
    }
  }, jQuery(document).ready(function(t) {
    "use strict";
    componentLoader.loadEl(document), t(document).on("shopify:section:load", function(e) {
      var n = t("#shopify-section-" + e.detail.sectionId);
      componentLoader.loadEl(n)
    }), t(document).on("shopify:section:unload", function(e) {
      var n = (t(e.target), t("#shopify-section-" + e.detail.sectionId));
      componentLoader.unloadEl(n)
    })
  }), jQuery.cookie = function(t, e, n) {
    if (void 0 === e) {
      var i = null;
      if (document.cookie && "" != document.cookie)
        for (var o = document.cookie.split(";"), r = 0; r < o.length; r++) {
          var s = jQuery.trim(o[r]);
          if (s.substring(0, t.length + 1) == t + "=") {
            i = decodeURIComponent(s.substring(t.length + 1));
            break
          }
        }
      return i
    }
    n = n || {}, null === e && (e = "", n.expires = -1);
    var a, l = "";
    n.expires && ("number" == typeof n.expires || n.expires.toUTCString) && ("number" == typeof n.expires ? (a = new Date).setTime(a.getTime() + 24 * n.expires * 60 * 60 * 1e3) : a = n.expires, l = "; expires=" + a.toUTCString());
    var u = n.path ? "; path=" + n.path : "",
      c = n.domain ? "; domain=" + n.domain : "",
      d = n.secure ? "; secure" : "";
    document.cookie = [t, "=", encodeURIComponent(e), l, u, c, d].join("")
  }, void 0 === Currency) var Currency = {};
Currency.cookie = {
    configuration: {
      expires: 365,
      path: "/",
      domain: window.location.hostname
    },
    name: "currency",
    write: function(t) {
      jQuery.cookie(this.name, t, this.configuration)
    },
    read: function() {
      return jQuery.cookie(this.name)
    },
    destroy: function() {
      jQuery.cookie(this.name, null, this.configuration)
    }
  }, Currency.moneyFormats = {
    USD: {
      money_format: "${{amount}}",
      money_with_currency_format: "${{amount}} USD"
    },
    EUR: {
      money_format: "&euro;{{amount}}",
      money_with_currency_format: "&euro;{{amount}} EUR"
    },
    GBP: {
      money_format: "&pound;{{amount}}",
      money_with_currency_format: "&pound;{{amount}} GBP"
    },
    CAD: {
      money_format: "${{amount}}",
      money_with_currency_format: "${{amount}} CAD"
    },
    ALL: {
      money_format: "Lek {{amount}}",
      money_with_currency_format: "Lek {{amount}} ALL"
    },
    DZD: {
      money_format: "DA {{amount}}",
      money_with_currency_format: "DA {{amount}} DZD"
    },
    AOA: {
      money_format: "Kz{{amount}}",
      money_with_currency_format: "Kz{{amount}} AOA"
    },
    ARS: {
      money_format: "${{amount_with_comma_separator}}",
      money_with_currency_format: "${{amount_with_comma_separator}} ARS"
    },
    AMD: {
      money_format: "{{amount}} AMD",
      money_with_currency_format: "{{amount}} AMD"
    },
    AWG: {
      money_format: "Afl{{amount}}",
      money_with_currency_format: "Afl{{amount}} AWG"
    },
    AUD: {
      money_format: "${{amount}}",
      money_with_currency_format: "${{amount}} AUD"
    },
    BBD: {
      money_format: "${{amount}}",
      money_with_currency_format: "${{amount}} Bds"
    },
    AZN: {
      money_format: "m.{{amount}}",
      money_with_currency_format: "m.{{amount}} AZN"
    },
    BDT: {
      money_format: "Tk {{amount}}",
      money_with_currency_format: "Tk {{amount}} BDT"
    },
    BSD: {
      money_format: "BS${{amount}}",
      money_with_currency_format: "BS${{amount}} BSD"
    },
    BHD: {
      money_format: "{{amount}}0 BD",
      money_with_currency_format: "{{amount}}0 BHD"
    },
    BYR: {
      money_format: "Br {{amount}}",
      money_with_currency_format: "Br {{amount}} BYR"
    },
    BZD: {
      money_format: "BZ${{amount}}",
      money_with_currency_format: "BZ${{amount}} BZD"
    },
    BTN: {
      money_format: "Nu {{amount}}",
      money_with_currency_format: "Nu {{amount}} BTN"
    },
    BAM: {
      money_format: "KM {{amount_with_comma_separator}}",
      money_with_currency_format: "KM {{amount_with_comma_separator}} BAM"
    },
    BRL: {
      money_format: "R$ {{amount_with_comma_separator}}",
      money_with_currency_format: "R$ {{amount_with_comma_separator}} BRL"
    },
    BOB: {
      money_format: "Bs{{amount_with_comma_separator}}",
      money_with_currency_format: "Bs{{amount_with_comma_separator}} BOB"
    },
    BWP: {
      money_format: "P{{amount}}",
      money_with_currency_format: "P{{amount}} BWP"
    },
    BND: {
      money_format: "${{amount}}",
      money_with_currency_format: "${{amount}} BND"
    },
    BGN: {
      money_format: "{{amount}} лв",
      money_with_currency_format: "{{amount}} лв BGN"
    },
    MMK: {
      money_format: "K{{amount}}",
      money_with_currency_format: "K{{amount}} MMK"
    },
    KHR: {
      money_format: "KHR{{amount}}",
      money_with_currency_format: "KHR{{amount}}"
    },
    KYD: {
      money_format: "${{amount}}",
      money_with_currency_format: "${{amount}} KYD"
    },
    XAF: {
      money_format: "FCFA{{amount}}",
      money_with_currency_format: "FCFA{{amount}} XAF"
    },
    CLP: {
      money_format: "${{amount_no_decimals}}",
      money_with_currency_format: "${{amount_no_decimals}} CLP"
    },
    CNY: {
      money_format: "&#165;{{amount}}",
      money_with_currency_format: "&#165;{{amount}} CNY"
    },
    COP: {
      money_format: "${{amount_with_comma_separator}}",
      money_with_currency_format: "${{amount_with_comma_separator}} COP"
    },
    CRC: {
      money_format: "&#8353; {{amount_with_comma_separator}}",
      money_with_currency_format: "&#8353; {{amount_with_comma_separator}} CRC"
    },
    HRK: {
      money_format: "{{amount_with_comma_separator}} kn",
      money_with_currency_format: "{{amount_with_comma_separator}} kn HRK"
    },
    CZK: {
      money_format: "{{amount_with_comma_separator}} K&#269;",
      money_with_currency_format: "{{amount_with_comma_separator}} K&#269;"
    },
    DKK: {
      money_format: "{{amount_with_comma_separator}}",
      money_with_currency_format: "kr.{{amount_with_comma_separator}}"
    },
    DOP: {
      money_format: "RD$ {{amount}}",
      money_with_currency_format: "RD$ {{amount}}"
    },
    XCD: {
      money_format: "${{amount}}",
      money_with_currency_format: "EC${{amount}}"
    },
    EGP: {
      money_format: "LE {{amount}}",
      money_with_currency_format: "LE {{amount}} EGP"
    },
    ETB: {
      money_format: "Br{{amount}}",
      money_with_currency_format: "Br{{amount}} ETB"
    },
    XPF: {
      money_format: "{{amount_no_decimals_with_comma_separator}} XPF",
      money_with_currency_format: "{{amount_no_decimals_with_comma_separator}} XPF"
    },
    FJD: {
      money_format: "${{amount}}",
      money_with_currency_format: "FJ${{amount}}"
    },
    GMD: {
      money_format: "D {{amount}}",
      money_with_currency_format: "D {{amount}} GMD"
    },
    GHS: {
      money_format: "GH&#8373;{{amount}}",
      money_with_currency_format: "GH&#8373;{{amount}}"
    },
    GTQ: {
      money_format: "Q{{amount}}",
      money_with_currency_format: "{{amount}} GTQ"
    },
    GYD: {
      money_format: "G${{amount}}",
      money_with_currency_format: "${{amount}} GYD"
    },
    GEL: {
      money_format: "{{amount}} GEL",
      money_with_currency_format: "{{amount}} GEL"
    },
    HNL: {
      money_format: "L {{amount}}",
      money_with_currency_format: "L {{amount}} HNL"
    },
    HKD: {
      money_format: "${{amount}}",
      money_with_currency_format: "HK${{amount}}"
    },
    HUF: {
      money_format: "{{amount_no_decimals_with_comma_separator}}",
      money_with_currency_format: "{{amount_no_decimals_with_comma_separator}} Ft"
    },
    ISK: {
      money_format: "{{amount_no_decimals}} kr",
      money_with_currency_format: "{{amount_no_decimals}} kr ISK"
    },
    INR: {
      money_format: "Rs. {{amount}}",
      money_with_currency_format: "Rs. {{amount}}"
    },
    IDR: {
      money_format: "{{amount_with_comma_separator}}",
      money_with_currency_format: "Rp {{amount_with_comma_separator}}"
    },
    ILS: {
      money_format: "{{amount}} NIS",
      money_with_currency_format: "{{amount}} NIS"
    },
    JMD: {
      money_format: "${{amount}}",
      money_with_currency_format: "${{amount}} JMD"
    },
    JPY: {
      money_format: "&#165;{{amount_no_decimals}}",
      money_with_currency_format: "&#165;{{amount_no_decimals}} JPY"
    },
    JEP: {
      money_format: "&pound;{{amount}}",
      money_with_currency_format: "&pound;{{amount}} JEP"
    },
    JOD: {
      money_format: "{{amount}}0 JD",
      money_with_currency_format: "{{amount}}0 JOD"
    },
    KZT: {
      money_format: "{{amount}} KZT",
      money_with_currency_format: "{{amount}} KZT"
    },
    KES: {
      money_format: "KSh{{amount}}",
      money_with_currency_format: "KSh{{amount}}"
    },
    KWD: {
      money_format: "{{amount}}0 KD",
      money_with_currency_format: "{{amount}}0 KWD"
    },
    KGS: {
      money_format: "лв{{amount}}",
      money_with_currency_format: "лв{{amount}}"
    },
    LVL: {
      money_format: "Ls {{amount}}",
      money_with_currency_format: "Ls {{amount}} LVL"
    },
    LBP: {
      money_format: "L&pound;{{amount}}",
      money_with_currency_format: "L&pound;{{amount}} LBP"
    },
    LTL: {
      money_format: "{{amount}} Lt",
      money_with_currency_format: "{{amount}} Lt"
    },
    MGA: {
      money_format: "Ar {{amount}}",
      money_with_currency_format: "Ar {{amount}} MGA"
    },
    MKD: {
      money_format: "ден {{amount}}",
      money_with_currency_format: "ден {{amount}} MKD"
    },
    MOP: {
      money_format: "MOP${{amount}}",
      money_with_currency_format: "MOP${{amount}}"
    },
    MVR: {
      money_format: "Rf{{amount}}",
      money_with_currency_format: "Rf{{amount}} MRf"
    },
    MXN: {
      money_format: "$ {{amount}}",
      money_with_currency_format: "$ {{amount}} MXN"
    },
    MYR: {
      money_format: "RM{{amount}} MYR",
      money_with_currency_format: "RM{{amount}} MYR"
    },
    MUR: {
      money_format: "Rs {{amount}}",
      money_with_currency_format: "Rs {{amount}} MUR"
    },
    MDL: {
      money_format: "{{amount}} MDL",
      money_with_currency_format: "{{amount}} MDL"
    },
    MAD: {
      money_format: "{{amount}} dh",
      money_with_currency_format: "Dh {{amount}} MAD"
    },
    MNT: {
      money_format: "{{amount_no_decimals}} &#8366",
      money_with_currency_format: "{{amount_no_decimals}} MNT"
    },
    MZN: {
      money_format: "{{amount}} Mt",
      money_with_currency_format: "Mt {{amount}} MZN"
    },
    NAD: {
      money_format: "N${{amount}}",
      money_with_currency_format: "N${{amount}} NAD"
    },
    NPR: {
      money_format: "Rs{{amount}}",
      money_with_currency_format: "Rs{{amount}} NPR"
    },
    ANG: {
      money_format: "&fnof;{{amount}}",
      money_with_currency_format: "{{amount}} NA&fnof;"
    },
    NZD: {
      money_format: "${{amount}}",
      money_with_currency_format: "${{amount}} NZD"
    },
    NIO: {
      money_format: "C${{amount}}",
      money_with_currency_format: "C${{amount}} NIO"
    },
    NGN: {
      money_format: "&#8358;{{amount}}",
      money_with_currency_format: "&#8358;{{amount}} NGN"
    },
    NOK: {
      money_format: "kr {{amount_with_comma_separator}}",
      money_with_currency_format: "kr {{amount_with_comma_separator}} NOK"
    },
    OMR: {
      money_format: "{{amount_with_comma_separator}} OMR",
      money_with_currency_format: "{{amount_with_comma_separator}} OMR"
    },
    PKR: {
      money_format: "Rs.{{amount}}",
      money_with_currency_format: "Rs.{{amount}} PKR"
    },
    PGK: {
      money_format: "K {{amount}}",
      money_with_currency_format: "K {{amount}} PGK"
    },
    PYG: {
      money_format: "Gs. {{amount_no_decimals_with_comma_separator}}",
      money_with_currency_format: "Gs. {{amount_no_decimals_with_comma_separator}} PYG"
    },
    PEN: {
      money_format: "S/. {{amount}}",
      money_with_currency_format: "S/. {{amount}} PEN"
    },
    PHP: {
      money_format: "&#8369;{{amount}}",
      money_with_currency_format: "&#8369;{{amount}} PHP"
    },
    PLN: {
      money_format: "{{amount_with_comma_separator}} zl",
      money_with_currency_format: "{{amount_with_comma_separator}} zl PLN"
    },
    QAR: {
      money_format: "QAR {{amount_with_comma_separator}}",
      money_with_currency_format: "QAR {{amount_with_comma_separator}}"
    },
    RON: {
      money_format: "{{amount_with_comma_separator}} lei",
      money_with_currency_format: "{{amount_with_comma_separator}} lei RON"
    },
    RUB: {
      money_format: "&#1088;&#1091;&#1073;{{amount_with_comma_separator}}",
      money_with_currency_format: "&#1088;&#1091;&#1073;{{amount_with_comma_separator}} RUB"
    },
    RWF: {
      money_format: "{{amount_no_decimals}} RF",
      money_with_currency_format: "{{amount_no_decimals}} RWF"
    },
    WST: {
      money_format: "WS$ {{amount}}",
      money_with_currency_format: "WS$ {{amount}} WST"
    },
    SAR: {
      money_format: "{{amount}} SR",
      money_with_currency_format: "{{amount}} SAR"
    },
    STD: {
      money_format: "Db {{amount}}",
      money_with_currency_format: "Db {{amount}} STD"
    },
    RSD: {
      money_format: "{{amount}} RSD",
      money_with_currency_format: "{{amount}} RSD"
    },
    SCR: {
      money_format: "Rs {{amount}}",
      money_with_currency_format: "Rs {{amount}} SCR"
    },
    SGD: {
      money_format: "${{amount}}",
      money_with_currency_format: "${{amount}} SGD"
    },
    SYP: {
      money_format: "S&pound;{{amount}}",
      money_with_currency_format: "S&pound;{{amount}} SYP"
    },
    ZAR: {
      money_format: "R {{amount}}",
      money_with_currency_format: "R {{amount}} ZAR"
    },
    KRW: {
      money_format: "&#8361;{{amount_no_decimals}}",
      money_with_currency_format: "&#8361;{{amount_no_decimals}} KRW"
    },
    LKR: {
      money_format: "Rs {{amount}}",
      money_with_currency_format: "Rs {{amount}} LKR"
    },
    SEK: {
      money_format: "{{amount_no_decimals}} kr",
      money_with_currency_format: "{{amount_no_decimals}} kr SEK"
    },
    CHF: {
      money_format: "SFr. {{amount}}",
      money_with_currency_format: "SFr. {{amount}} CHF"
    },
    TWD: {
      money_format: "${{amount}}",
      money_with_currency_format: "${{amount}} TWD"
    },
    THB: {
      money_format: "{{amount}} &#xe3f;",
      money_with_currency_format: "{{amount}} &#xe3f; THB"
    },
    TZS: {
      money_format: "{{amount}} TZS",
      money_with_currency_format: "{{amount}} TZS"
    },
    TTD: {
      money_format: "${{amount}}",
      money_with_currency_format: "${{amount}} TTD"
    },
    TND: {
      money_format: "{{amount}}",
      money_with_currency_format: "{{amount}} DT"
    },
    TRY: {
      money_format: "{{amount}}TL",
      money_with_currency_format: "{{amount}}TL"
    },
    UGX: {
      money_format: "Ush {{amount_no_decimals}}",
      money_with_currency_format: "Ush {{amount_no_decimals}} UGX"
    },
    UAH: {
      money_format: "₴{{amount}}",
      money_with_currency_format: "₴{{amount}} UAH"
    },
    AED: {
      money_format: "Dhs. {{amount}}",
      money_with_currency_format: "Dhs. {{amount}} AED"
    },
    UYU: {
      money_format: "${{amount_with_comma_separator}}",
      money_with_currency_format: "${{amount_with_comma_separator}} UYU"
    },
    VUV: {
      money_format: "${{amount}}",
      money_with_currency_format: "${{amount}}VT"
    },
    VEF: {
      money_format: "Bs. {{amount_with_comma_separator}}",
      money_with_currency_format: "Bs. {{amount_with_comma_separator}} VEF"
    },
    VND: {
      money_format: "{{amount_no_decimals_with_comma_separator}}&#8363;",
      money_with_currency_format: "{{amount_no_decimals_with_comma_separator}} VND"
    },
    XBT: {
      money_format: "{{amount_no_decimals}} BTC",
      money_with_currency_format: "{{amount_no_decimals}} BTC"
    },
    XOF: {
      money_format: "CFA{{amount}}",
      money_with_currency_format: "CFA{{amount}} XOF"
    },
    ZMW: {
      money_format: "K{{amount_no_decimals_with_comma_separator}}",
      money_with_currency_format: "ZMW{{amount_no_decimals_with_comma_separator}}"
    }
  }, Currency.formatMoney = function(t, e) {
    function n(t, e) {
      return void 0 === t ? e : t
    }

    function i(t, e, i, o) {
      if (e = n(e, 2), i = n(i, ","), o = n(o, "."), isNaN(t) || null == t) return 0;
      var r = (t = (t / 100).toFixed(e)).split(".");
      return r[0].replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1" + i) + (r[1] ? o + r[1] : "")
    }
    if ("function" == typeof Shopify.formatMoney) return Shopify.formatMoney(t, e);
    "string" == typeof t && (t = t.replace(".", ""));
    var o = "",
      r = /\{\{\s*(\w+)\s*\}\}/,
      s = e || "${{amount}}";
    switch (s.match(r)[1]) {
      case "amount":
        o = i(t, 2);
        break;
      case "amount_no_decimals":
        o = i(t, 0);
        break;
      case "amount_with_comma_separator":
        o = i(t, 2, ".", ",");
        break;
      case "amount_no_decimals_with_comma_separator":
        o = i(t, 0, ".", ",")
    }
    return s.replace(r, o)
  }, Currency.currentCurrency = "", Currency.format = "money_with_currency_format", Currency.convertAll = function(t, e, n, i) {
    jQuery(n || "span.money").each(function() {
      if (jQuery(this).attr("data-currency") !== e) {
        if (jQuery(this).attr("data-currency-" + e)) jQuery(this).html(jQuery(this).attr("data-currency-" + e));
        else {
          var n, o = Currency.moneyFormats[t][i || Currency.format] || "{{amount}}",
            r = Currency.moneyFormats[e][i || Currency.format] || "{{amount}}";
          n = -1 !== o.indexOf("amount_no_decimals") ? Currency.convert(100 * parseInt(jQuery(this).html().replace(/[^0-9]/g, ""), 10), t, e) : "JOD" === t || "KWD" == t || "BHD" == t ? Currency.convert(parseInt(jQuery(this).html().replace(/[^0-9]/g, ""), 10) / 10, t, e) : Currency.convert(parseInt(jQuery(this).html().replace(/[^0-9]/g, ""), 10), t, e);
          var s = Currency.formatMoney(n, r);
          jQuery(this).html(s), jQuery(this).attr("data-currency-" + e, s)
        }
        jQuery(this).attr("data-currency", e)
      }
    }), this.currentCurrency = e, this.cookie.write(e)
  },
  function() {
    function t(t, n, i, o) {
      return new e(t, n, i, o)
    }

    function e(t, e, i, o) {
      this.options = o || {}, this.options.adapters = this.options.adapters || {}, this.obj = t, this.keypath = e, this.callback = i, this.objectPath = [], this.parse(), n(this.target = this.realize()) && this.set(!0, this.key, this.target, this.callback)
    }

    function n(t) {
      return "object" == typeof t && null !== t
    }

    function i(t) {
      throw new Error("[sightglass] " + t)
    }
    t.adapters = {}, e.tokenize = function(t, e, n) {
      var i, o, r = [],
        s = {
          i: n,
          path: ""
        };
      for (i = 0; i < t.length; i++) o = t.charAt(i), ~e.indexOf(o) ? (r.push(s), s = {
        i: o,
        path: ""
      }) : s.path += o;
      return r.push(s), r
    }, e.prototype.parse = function() {
      var n, o, r = this.interfaces();
      r.length || i("Must define at least one adapter interface."), ~r.indexOf(this.keypath[0]) ? (n = this.keypath[0], o = this.keypath.substr(1)) : (void 0 === (n = this.options.root || t.root) && i("Must define a default root adapter."), o = this.keypath), this.tokens = e.tokenize(o, r, n), this.key = this.tokens.pop()
    }, e.prototype.realize = function() {
      var t, e = this.obj,
        i = !1;
      return this.tokens.forEach(function(o, r) {
        n(e) ? (void 0 !== this.objectPath[r] ? e !== (t = this.objectPath[r]) && (this.set(!1, o, t, this.update.bind(this)), this.set(!0, o, e, this.update.bind(this)), this.objectPath[r] = e) : (this.set(!0, o, e, this.update.bind(this)), this.objectPath[r] = e), e = this.get(o, e)) : (!1 === i && (i = r), (t = this.objectPath[r]) && this.set(!1, o, t, this.update.bind(this)))
      }, this), !1 !== i && this.objectPath.splice(i), e
    }, e.prototype.update = function() {
      var t, e;
      (t = this.realize()) !== this.target && (n(this.target) && this.set(!1, this.key, this.target, this.callback), n(t) && this.set(!0, this.key, t, this.callback), e = this.value(), this.target = t, this.value() !== e && this.callback())
    }, e.prototype.value = function() {
      if (n(this.target)) return this.get(this.key, this.target)
    }, e.prototype.setValue = function(t) {
      n(this.target) && this.adapter(this.key).set(this.target, this.key.path, t)
    }, e.prototype.get = function(t, e) {
      return this.adapter(t).get(e, t.path)
    }, e.prototype.set = function(t, e, n, i) {
      var o = t ? "observe" : "unobserve";
      this.adapter(e)[o](n, e.path, i)
    }, e.prototype.interfaces = function() {
      var e = Object.keys(this.options.adapters);
      return Object.keys(t.adapters).forEach(function(t) {
        ~e.indexOf(t) || e.push(t)
      }), e
    }, e.prototype.adapter = function(e) {
      return this.options.adapters[e.i] || t.adapters[e.i]
    }, e.prototype.unobserve = function() {
      var t;
      this.tokens.forEach(function(e, n) {
        (t = this.objectPath[n]) && this.set(!1, e, t, this.update.bind(this))
      }, this), n(this.target) && this.set(!1, this.key, this.target, this.callback)
    }, "undefined" != typeof module && module.exports ? module.exports = t : "function" == typeof define && define.amd ? define([], function() {
      return this.sightglass = t
    }) : this.sightglass = t
  }.call(this),
  function() {
    var t, e, n, i, o = function(t, e) {
        return function() {
          return t.apply(e, arguments)
        }
      },
      r = [].slice,
      s = {}.hasOwnProperty,
      a = function(t, e) {
        function n() {
          this.constructor = t
        }
        for (var i in e) s.call(e, i) && (t[i] = e[i]);
        return n.prototype = e.prototype, t.prototype = new n, t.__super__ = e.prototype, t
      },
      l = [].indexOf || function(t) {
        for (var e = 0, n = this.length; e < n; e++)
          if (e in this && this[e] === t) return e;
        return -1
      };
    t = {
      options: ["prefix", "templateDelimiters", "rootInterface", "preloadData", "handler"],
      extensions: ["binders", "formatters", "components", "adapters"],
      public: {
        binders: {},
        components: {},
        formatters: {},
        adapters: {},
        prefix: "rv",
        templateDelimiters: ["{", "}"],
        rootInterface: ".",
        preloadData: !0,
        handler: function(t, e, n) {
          return this.call(t, e, n.view.models)
        },
        configure: function(e) {
          var n, i, o, r;
          for (o in null == e && (e = {}), e)
            if (r = e[o], "binders" === o || "components" === o || "formatters" === o || "adapters" === o)
              for (i in r) n = r[i], t[o][i] = n;
            else t.public[o] = r
        },
        bind: function(e, n, i) {
          var o;
          return null == n && (n = {}), null == i && (i = {}), (o = new t.View(e, n, i)).bind(), o
        },
        init: function(e, n, i) {
          var o, r;
          return null == i && (i = {}), null == n && (n = document.createElement("div")), e = t.public.components[e], n.innerHTML = e.template.call(this, n), o = e.initialize.call(this, n, i), (r = new t.View(n, o)).bind(), r
        }
      }
    }, window.jQuery || window.$ ? (i = "on" in jQuery.prototype ? ["on", "off"] : ["bind", "unbind"], e = i[0], n = i[1], t.Util = {
      bindEvent: function(t, n, i) {
        return jQuery(t)[e](n, i)
      },
      unbindEvent: function(t, e, i) {
        return jQuery(t)[n](e, i)
      },
      getInputValue: function(t) {
        var e;
        return "checkbox" === (e = jQuery(t)).attr("type") ? e.is(":checked") : e.val()
      }
    }) : t.Util = {
      bindEvent: "addEventListener" in window ? function(t, e, n) {
        return t.addEventListener(e, n, !1)
      } : function(t, e, n) {
        return t.attachEvent("on" + e, n)
      },
      unbindEvent: "removeEventListener" in window ? function(t, e, n) {
        return t.removeEventListener(e, n, !1)
      } : function(t, e, n) {
        return t.detachEvent("on" + e, n)
      },
      getInputValue: function(t) {
        var e, n, i, o;
        if ("checkbox" === t.type) return t.checked;
        if ("select-multiple" === t.type) {
          for (o = [], n = 0, i = t.length; n < i; n++)(e = t[n]).selected && o.push(e.value);
          return o
        }
        return t.value
      }
    }, t.TypeParser = function() {
      function t() {}
      return t.types = {
        primitive: 0,
        keypath: 1
      }, t.parse = function(t) {
        return /^'.*'$|^".*"$/.test(t) ? {
          type: this.types.primitive,
          value: t.slice(1, -1)
        } : "true" === t ? {
          type: this.types.primitive,
          value: !0
        } : "false" === t ? {
          type: this.types.primitive,
          value: !1
        } : "null" === t ? {
          type: this.types.primitive,
          value: null
        } : "undefined" === t ? {
          type: this.types.primitive,
          value: void 0
        } : !1 === isNaN(Number(t)) ? {
          type: this.types.primitive,
          value: Number(t)
        } : {
          type: this.types.keypath,
          value: t
        }
      }, t
    }(), t.TextTemplateParser = function() {
      function t() {}
      return t.types = {
        text: 0,
        binding: 1
      }, t.parse = function(t, e) {
        var n, i, o, r, s, a, l;
        for (a = [], r = t.length, n = 0, i = 0; i < r;) {
          if ((n = t.indexOf(e[0], i)) < 0) {
            a.push({
              type: this.types.text,
              value: t.slice(i)
            });
            break
          }
          if (n > 0 && i < n && a.push({
              type: this.types.text,
              value: t.slice(i, n)
            }), i = n + e[0].length, (n = t.indexOf(e[1], i)) < 0) {
            s = t.slice(i - e[1].length), (null != (o = a[a.length - 1]) ? o.type : void 0) === this.types.text ? o.value += s : a.push({
              type: this.types.text,
              value: s
            });
            break
          }
          l = t.slice(i, n).trim(), a.push({
            type: this.types.binding,
            value: l
          }), i = n + e[1].length
        }
        return a
      }, t
    }(), t.View = function() {
      function e(e, n, i) {
        var r, s, a, l, u, c, d, f, h, p, m, g, v;
        for (this.els = e, this.models = n, null == i && (i = {}), this.update = o(this.update, this), this.publish = o(this.publish, this), this.sync = o(this.sync, this), this.unbind = o(this.unbind, this), this.bind = o(this.bind, this), this.select = o(this.select, this), this.traverse = o(this.traverse, this), this.build = o(this.build, this), this.buildBinding = o(this.buildBinding, this), this.bindingRegExp = o(this.bindingRegExp, this), this.options = o(this.options, this), this.els.jquery || this.els instanceof Array || (this.els = [this.els]), u = 0, d = (h = t.extensions).length; u < d; u++) {
          if (this[s = h[u]] = {}, i[s])
            for (r in p = i[s]) a = p[r], this[s][r] = a;
          for (r in m = t.public[s]) a = m[r], null == (l = this[s])[r] && (l[r] = a)
        }
        for (c = 0, f = (g = t.options).length; c < f; c++) this[s = g[c]] = null != (v = i[s]) ? v : t.public[s];
        this.build()
      }
      return e.prototype.options = function() {
        var e, n, i, o, r;
        for (n = {}, i = 0, o = (r = t.extensions.concat(t.options)).length; i < o; i++) n[e = r[i]] = this[e];
        return n
      }, e.prototype.bindingRegExp = function() {
        return new RegExp("^" + this.prefix + "-")
      }, e.prototype.buildBinding = function(e, n, i, o) {
        var r, s, a, l, u, c, d;
        return u = {}, d = function() {
          var t, e, n, i;
          for (i = [], t = 0, e = (n = o.split("|")).length; t < e; t++) c = n[t], i.push(c.trim());
          return i
        }(), l = (r = function() {
          var t, e, n, i;
          for (i = [], t = 0, e = (n = d.shift().split("<")).length; t < e; t++) s = n[t], i.push(s.trim());
          return i
        }()).shift(), u.formatters = d, (a = r.shift()) && (u.dependencies = a.split(/\s+/)), this.bindings.push(new t[e](this, n, i, l, u))
      }, e.prototype.build = function() {
        var e, n, i, o, r;
        for (this.bindings = [], n = function(e) {
            return function(i) {
              var o, r, s, a, l, u, c, d, f, h, p, m, g, v;
              if (3 === i.nodeType) {
                if (l = t.TextTemplateParser, (s = e.templateDelimiters) && (d = l.parse(i.data, s)).length && (1 !== d.length || d[0].type !== l.types.text)) {
                  for (f = 0, p = d.length; f < p; f++) c = d[f], u = document.createTextNode(c.value), i.parentNode.insertBefore(u, i), 1 === c.type && e.buildBinding("TextBinding", u, null, c.value);
                  i.parentNode.removeChild(i)
                }
              } else 1 === i.nodeType && (o = e.traverse(i));
              if (!o) {
                for (v = [], h = 0, m = (g = function() {
                    var t, e, n, o;
                    for (o = [], t = 0, e = (n = i.childNodes).length; t < e; t++) a = n[t], o.push(a);
                    return o
                  }()).length; h < m; h++) r = g[h], v.push(n(r));
                return v
              }
            }
          }(this), i = 0, o = (r = this.els).length; i < o; i++) e = r[i], n(e);
        this.bindings.sort(function(t, e) {
          var n, i;
          return ((null != (n = e.binder) ? n.priority : void 0) || 0) - ((null != (i = t.binder) ? i.priority : void 0) || 0)
        })
      }, e.prototype.traverse = function(e) {
        var n, i, o, r, s, a, l, u, c, d, f, h, p, m, g;
        for (r = this.bindingRegExp(), s = "SCRIPT" === e.nodeName || "STYLE" === e.nodeName, c = 0, f = (p = e.attributes).length; c < f; c++)
          if (n = p[c], r.test(n.name)) {
            if (l = n.name.replace(r, ""), !(o = this.binders[l]))
              for (a in m = this.binders) u = m[a], "*" !== a && -1 !== a.indexOf("*") && (new RegExp("^" + a.replace(/\*/g, ".+") + "$").test(l) && (o = u));
            o || (o = this.binders["*"]), o.block && (s = !0, i = [n])
          } for (d = 0, h = (g = i || e.attributes).length; d < h; d++) n = g[d], r.test(n.name) && (l = n.name.replace(r, ""), this.buildBinding("Binding", e, l, n.value));
        return s || (l = e.nodeName.toLowerCase(), this.components[l] && !e._bound && (this.bindings.push(new t.ComponentBinding(this, e, l)), s = !0)), s
      }, e.prototype.select = function(t) {
        var e, n, i, o, r;
        for (r = [], n = 0, i = (o = this.bindings).length; n < i; n++) t(e = o[n]) && r.push(e);
        return r
      }, e.prototype.bind = function() {
        var t, e, n, i, o;
        for (o = [], e = 0, n = (i = this.bindings).length; e < n; e++) t = i[e], o.push(t.bind());
        return o
      }, e.prototype.unbind = function() {
        var t, e, n, i, o;
        for (o = [], e = 0, n = (i = this.bindings).length; e < n; e++) t = i[e], o.push(t.unbind());
        return o
      }, e.prototype.sync = function() {
        var t, e, n, i, o;
        for (o = [], e = 0, n = (i = this.bindings).length; e < n; e++) t = i[e], o.push("function" == typeof t.sync ? t.sync() : void 0);
        return o
      }, e.prototype.publish = function() {
        var t, e, n, i, o;
        for (i = this.select(function(t) {
            var e;
            return null != (e = t.binder) ? e.publishes : void 0
          }), o = [], e = 0, n = i.length; e < n; e++) t = i[e], o.push(t.publish());
        return o
      }, e.prototype.update = function(t) {
        var e, n, i, o, r, s, a;
        for (n in null == t && (t = {}), t) i = t[n], this.models[n] = i;
        for (a = [], o = 0, r = (s = this.bindings).length; o < r; o++) e = s[o], a.push("function" == typeof e.update ? e.update(t) : void 0);
        return a
      }, e
    }(), t.Binding = function() {
      function e(t, e, n, i, r) {
        this.view = t, this.el = e, this.type = n, this.keypath = i, this.options = null != r ? r : {}, this.getValue = o(this.getValue, this), this.update = o(this.update, this), this.unbind = o(this.unbind, this), this.bind = o(this.bind, this), this.publish = o(this.publish, this), this.sync = o(this.sync, this), this.set = o(this.set, this), this.eventHandler = o(this.eventHandler, this), this.formattedValue = o(this.formattedValue, this), this.parseTarget = o(this.parseTarget, this), this.observe = o(this.observe, this), this.setBinder = o(this.setBinder, this), this.formatters = this.options.formatters || [], this.dependencies = [], this.formatterObservers = {}, this.model = void 0, this.setBinder()
      }
      return e.prototype.setBinder = function() {
        var t, e, n;
        if (!(this.binder = this.view.binders[this.type]))
          for (t in n = this.view.binders) e = n[t], "*" !== t && -1 !== t.indexOf("*") && (new RegExp("^" + t.replace(/\*/g, ".+") + "$").test(this.type) && (this.binder = e, this.args = new RegExp("^" + t.replace(/\*/g, "(.+)") + "$").exec(this.type), this.args.shift()));
        if (this.binder || (this.binder = this.view.binders["*"]), this.binder instanceof Function) return this.binder = {
          routine: this.binder
        }
      }, e.prototype.observe = function(e, n, i) {
        return t.sightglass(e, n, i, {
          root: this.view.rootInterface,
          adapters: this.view.adapters
        })
      }, e.prototype.parseTarget = function() {
        var e;
        return 0 === (e = t.TypeParser.parse(this.keypath)).type ? this.value = e.value : (this.observer = this.observe(this.view.models, this.keypath, this.sync), this.model = this.observer.target)
      }, e.prototype.formattedValue = function(e) {
        var n, i, o, s, a, l, u, c, d, f, h, p, m, g;
        for (s = f = 0, p = (g = this.formatters).length; f < p; s = ++f) {
          for (a = g[s], l = (o = a.match(/[^\s']+|'([^']|'[^\s])*'|"([^"]|"[^\s])*"/g)).shift(), a = this.view.formatters[l], o = function() {
              var e, n, r;
              for (r = [], e = 0, n = o.length; e < n; e++) i = o[e], r.push(t.TypeParser.parse(i));
              return r
            }(), c = [], n = h = 0, m = o.length; h < m; n = ++h) i = o[n], c.push(0 === i.type ? i.value : ((d = this.formatterObservers)[s] || (d[s] = {}), (u = this.formatterObservers[s][n]) || (u = this.observe(this.view.models, i.value, this.sync), this.formatterObservers[s][n] = u), u.value()));
          (null != a ? a.read : void 0) instanceof Function ? e = a.read.apply(a, [e].concat(r.call(c))) : a instanceof Function && (e = a.apply(null, [e].concat(r.call(c))))
        }
        return e
      }, e.prototype.eventHandler = function(t) {
        var e, n;
        return n = (e = this).view.handler,
          function(i) {
            return n.call(t, this, i, e)
          }
      }, e.prototype.set = function(t) {
        var e;
        return t = t instanceof Function && !this.binder.function ? this.formattedValue(t.call(this.model)) : this.formattedValue(t), null != (e = this.binder.routine) ? e.call(this, this.el, t) : void 0
      }, e.prototype.sync = function() {
        var t, e;
        return this.set(function() {
          var n, i, o, r, s, a, l;
          if (this.observer) {
            if (this.model !== this.observer.target) {
              for (n = 0, o = (s = this.dependencies).length; n < o; n++)(e = s[n]).unobserve();
              if (this.dependencies = [], null != (this.model = this.observer.target) && (null != (a = this.options.dependencies) ? a.length : void 0))
                for (i = 0, r = (l = this.options.dependencies).length; i < r; i++) t = l[i], e = this.observe(this.model, t, this.sync), this.dependencies.push(e)
            }
            return this.observer.value()
          }
          return this.value
        }.call(this))
      }, e.prototype.publish = function() {
        var t, e, n, i, o, s, a, l;
        if (this.observer) {
          for (n = this.getValue(this.el), i = 0, o = (s = this.formatters.slice(0).reverse()).length; i < o; i++) e = (t = s[i].split(/\s+/)).shift(), (null != (a = this.view.formatters[e]) ? a.publish : void 0) && (n = (l = this.view.formatters[e]).publish.apply(l, [n].concat(r.call(t))));
          return this.observer.setValue(n)
        }
      }, e.prototype.bind = function() {
        var t, e, n, i, o, r, s;
        if (this.parseTarget(), null != (o = this.binder.bind) && o.call(this, this.el), null != this.model && (null != (r = this.options.dependencies) ? r.length : void 0))
          for (n = 0, i = (s = this.options.dependencies).length; n < i; n++) t = s[n], e = this.observe(this.model, t, this.sync), this.dependencies.push(e);
        if (this.view.preloadData) return this.sync()
      }, e.prototype.unbind = function() {
        var t, e, n, i, o, r, s, a, l;
        for (null != (r = this.binder.unbind) && r.call(this, this.el), null != (s = this.observer) && s.unobserve(), i = 0, o = (a = this.dependencies).length; i < o; i++) a[i].unobserve();
        for (n in this.dependencies = [], l = this.formatterObservers)
          for (t in e = l[n]) e[t].unobserve();
        return this.formatterObservers = {}
      }, e.prototype.update = function(t) {
        var e, n;
        return null == t && (t = {}), this.model = null != (e = this.observer) ? e.target : void 0, null != (n = this.binder.update) ? n.call(this, t) : void 0
      }, e.prototype.getValue = function(e) {
        return this.binder && null != this.binder.getValue ? this.binder.getValue.call(this, e) : t.Util.getInputValue(e)
      }, e
    }(), t.ComponentBinding = function(e) {
      function n(t, e, n) {
        var i, r, s, a, u, c, d;
        for (this.view = t, this.el = e, this.type = n, this.unbind = o(this.unbind, this), this.bind = o(this.bind, this), this.locals = o(this.locals, this), this.component = this.view.components[this.type], this.static = {}, this.observers = {}, this.upstreamObservers = {}, r = t.bindingRegExp(), a = 0, u = (c = this.el.attributes || []).length; a < u; a++) i = c[a], r.test(i.name) || (s = this.camelCase(i.name), l.call(null != (d = this.component.static) ? d : [], s) >= 0 ? this.static[s] = i.value : this.observers[s] = i.value)
      }
      return a(n, e), n.prototype.sync = function() {}, n.prototype.update = function() {}, n.prototype.publish = function() {}, n.prototype.locals = function() {
        var t, e, n, i, o, r;
        for (t in n = {}, o = this.static) i = o[t], n[t] = i;
        for (t in r = this.observers) e = r[t], n[t] = e.value();
        return n
      }, n.prototype.camelCase = function(t) {
        return t.replace(/-([a-z])/g, function(t) {
          return t[1].toUpperCase()
        })
      }, n.prototype.bind = function() {
        var e, n, i, o, r, s, a, l, u, c, d, f, h, p, m, g, v, y, b, _, w;
        if (!this.bound) {
          for (n in p = this.observers) i = p[n], this.observers[n] = this.observe(this.view.models, i, function(t) {
            return function(e) {
              return function() {
                return t.componentView.models[e] = t.observers[e].value()
              }
            }
          }(this).call(this, n));
          this.bound = !0
        }
        if (null != this.componentView) return this.componentView.bind();
        for (this.el.innerHTML = this.component.template.call(this), a = this.component.initialize.call(this, this.el, this.locals()), this.el._bound = !0, s = {}, c = 0, f = (m = t.extensions).length; c < f; c++) {
          if (s[r = m[c]] = {}, this.component[r])
            for (e in g = this.component[r]) l = g[e], s[r][e] = l;
          for (e in v = this.view[r]) l = v[e], null == (u = s[r])[e] && (u[e] = l)
        }
        for (d = 0, h = (y = t.options).length; d < h; d++) s[r = y[d]] = null != (b = this.component[r]) ? b : this.view[r];
        for (n in this.componentView = new t.View(this.el, a, s), this.componentView.bind(), w = [], _ = this.observers) o = _[n], w.push(this.upstreamObservers[n] = this.observe(this.componentView.models, n, function(t) {
          return function(e, n) {
            return function() {
              return n.setValue(t.componentView.models[e])
            }
          }
        }(this).call(this, n, o)));
        return w
      }, n.prototype.unbind = function() {
        var t, e, n, i;
        for (t in e = this.upstreamObservers) e[t].unobserve();
        for (t in n = this.observers) n[t].unobserve();
        return null != (i = this.componentView) ? i.unbind.call(this) : void 0
      }, n
    }(t.Binding), t.TextBinding = function(t) {
      function e(t, e, n, i, r) {
        this.view = t, this.el = e, this.type = n, this.keypath = i, this.options = null != r ? r : {}, this.sync = o(this.sync, this), this.formatters = this.options.formatters || [], this.dependencies = [], this.formatterObservers = {}
      }
      return a(e, t), e.prototype.binder = {
        routine: function(t, e) {
          return t.data = null != e ? e : ""
        }
      }, e.prototype.sync = function() {
        return e.__super__.sync.apply(this, arguments)
      }, e
    }(t.Binding), t.public.binders.text = function(t, e) {
      return null != t.textContent ? t.textContent = null != e ? e : "" : t.innerText = null != e ? e : ""
    }, t.public.binders.html = function(t, e) {
      return t.innerHTML = null != e ? e : ""
    }, t.public.binders.show = function(t, e) {
      return t.style.display = e ? "" : "none"
    }, t.public.binders.hide = function(t, e) {
      return t.style.display = e ? "none" : ""
    }, t.public.binders.enabled = function(t, e) {
      return t.disabled = !e
    }, t.public.binders.disabled = function(t, e) {
      return t.disabled = !!e
    }, t.public.binders.checked = {
      publishes: !0,
      priority: 2e3,
      bind: function(e) {
        return t.Util.bindEvent(e, "change", this.publish)
      },
      unbind: function(e) {
        return t.Util.unbindEvent(e, "change", this.publish)
      },
      routine: function(t, e) {
        var n;
        return "radio" === t.type ? t.checked = (null != (n = t.value) ? n.toString() : void 0) === (null != e ? e.toString() : void 0) : t.checked = !!e
      }
    }, t.public.binders.unchecked = {
      publishes: !0,
      priority: 2e3,
      bind: function(e) {
        return t.Util.bindEvent(e, "change", this.publish)
      },
      unbind: function(e) {
        return t.Util.unbindEvent(e, "change", this.publish)
      },
      routine: function(t, e) {
        var n;
        return "radio" === t.type ? t.checked = (null != (n = t.value) ? n.toString() : void 0) !== (null != e ? e.toString() : void 0) : t.checked = !e
      }
    }, t.public.binders.value = {
      publishes: !0,
      priority: 3e3,
      bind: function(e) {
        if ("INPUT" !== e.tagName || "radio" !== e.type) return this.event = "SELECT" === e.tagName ? "change" : "input", t.Util.bindEvent(e, this.event, this.publish)
      },
      unbind: function(e) {
        if ("INPUT" !== e.tagName || "radio" !== e.type) return t.Util.unbindEvent(e, this.event, this.publish)
      },
      routine: function(t, e) {
        var n, i, o, r, s, a, u;
        if ("INPUT" === t.tagName && "radio" === t.type) return t.setAttribute("value", e);
        if (null != window.jQuery) {
          if (t = jQuery(t), (null != e ? e.toString() : void 0) !== (null != (r = t.val()) ? r.toString() : void 0)) return t.val(null != e ? e : "")
        } else if ("select-multiple" === t.type) {
          if (null != e) {
            for (u = [], i = 0, o = t.length; i < o; i++) n = t[i], u.push(n.selected = (s = n.value, l.call(e, s) >= 0));
            return u
          }
        } else if ((null != e ? e.toString() : void 0) !== (null != (a = t.value) ? a.toString() : void 0)) return t.value = null != e ? e : ""
      }
    }, t.public.binders.if = {
      block: !0,
      priority: 4e3,
      bind: function(t) {
        var e, n;
        if (null == this.marker) return e = [this.view.prefix, this.type].join("-").replace("--", "-"), n = t.getAttribute(e), this.marker = document.createComment(" rivets: " + this.type + " " + n + " "), this.bound = !1, t.removeAttribute(e), t.parentNode.insertBefore(this.marker, t), t.parentNode.removeChild(t)
      },
      unbind: function() {
        var t;
        return null != (t = this.nested) ? t.unbind() : void 0
      },
      routine: function(e, n) {
        var i, o, r, s;
        if (!!n == !this.bound) {
          if (n) {
            for (i in r = {}, s = this.view.models) o = s[i], r[i] = o;
            return (this.nested || (this.nested = new t.View(e, r, this.view.options()))).bind(), this.marker.parentNode.insertBefore(e, this.marker.nextSibling), this.bound = !0
          }
          return e.parentNode.removeChild(e), this.nested.unbind(), this.bound = !1
        }
      },
      update: function(t) {
        var e;
        return null != (e = this.nested) ? e.update(t) : void 0
      }
    }, t.public.binders.unless = {
      block: !0,
      priority: 4e3,
      bind: function(e) {
        return t.public.binders.if.bind.call(this, e)
      },
      unbind: function() {
        return t.public.binders.if.unbind.call(this)
      },
      routine: function(e, n) {
        return t.public.binders.if.routine.call(this, e, !n)
      },
      update: function(e) {
        return t.public.binders.if.update.call(this, e)
      }
    }, t.public.binders["on-*"] = {
      function: !0,
      priority: 1e3,
      unbind: function(e) {
        if (this.handler) return t.Util.unbindEvent(e, this.args[0], this.handler)
      },
      routine: function(e, n) {
        return this.handler && t.Util.unbindEvent(e, this.args[0], this.handler), t.Util.bindEvent(e, this.args[0], this.handler = this.eventHandler(n))
      }
    }, t.public.binders["each-*"] = {
      block: !0,
      priority: 4e3,
      bind: function(t) {
        var e, n, i, o;
        if (null == this.marker) e = [this.view.prefix, this.type].join("-").replace("--", "-"), this.marker = document.createComment(" rivets: " + this.type + " "), this.iterated = [], t.removeAttribute(e), t.parentNode.insertBefore(this.marker, t), t.parentNode.removeChild(t);
        else
          for (n = 0, i = (o = this.iterated).length; n < i; n++) o[n].bind()
      },
      unbind: function(t) {
        var e, n, i, o, r;
        if (null != this.iterated) {
          for (r = [], n = 0, i = (o = this.iterated).length; n < i; n++) e = o[n], r.push(e.unbind());
          return r
        }
      },
      routine: function(e, n) {
        var i, o, r, s, a, l, u, c, d, f, h, p, m, g, v, y, b, _, w, x;
        if (l = this.args[0], n = n || [], this.iterated.length > n.length)
          for (h = 0, g = (b = Array(this.iterated.length - n.length)).length; h < g; h++) b[h], (f = this.iterated.pop()).unbind(), this.marker.parentNode.removeChild(f.els[0]);
        for (r = p = 0, v = n.length; p < v; r = ++p)
          if (a = n[r], (o = {
              index: r
            })[l] = a, null == this.iterated[r]) {
            for (s in _ = this.view.models) a = _[s], null == o[s] && (o[s] = a);
            c = this.iterated.length ? this.iterated[this.iterated.length - 1].els[0] : this.marker, (u = this.view.options()).preloadData = !0, d = e.cloneNode(!0), (f = new t.View(d, o, u)).bind(), this.iterated.push(f), this.marker.parentNode.insertBefore(d, c.nextSibling)
          } else this.iterated[r].models[l] !== a && this.iterated[r].update(o);
        if ("OPTION" === e.nodeName) {
          for (x = [], m = 0, y = (w = this.view.bindings).length; m < y; m++)(i = w[m]).el === this.marker.parentNode && "value" === i.type ? x.push(i.sync()) : x.push(void 0);
          return x
        }
      },
      update: function(t) {
        var e, n, i, o, r, s, a, l;
        for (n in e = {}, t) i = t[n], n !== this.args[0] && (e[n] = i);
        for (l = [], r = 0, s = (a = this.iterated).length; r < s; r++) o = a[r], l.push(o.update(e));
        return l
      }
    }, t.public.binders["class-*"] = function(t, e) {
      var n;
      if (!e == (-1 !== (n = " " + t.className + " ").indexOf(" " + this.args[0] + " "))) return t.className = e ? t.className + " " + this.args[0] : n.replace(" " + this.args[0] + " ", " ").trim()
    }, t.public.binders["*"] = function(t, e) {
      return null != e ? t.setAttribute(this.type, e) : t.removeAttribute(this.type)
    }, t.public.adapters["."] = {
      id: "_rv",
      counter: 0,
      weakmap: {},
      weakReference: function(t) {
        var e, n, i;
        return t.hasOwnProperty(this.id) || (e = this.counter++, Object.defineProperty(t, this.id, {
          value: e
        })), (n = this.weakmap)[i = t[this.id]] || (n[i] = {
          callbacks: {}
        })
      },
      cleanupWeakReference: function(t, e) {
        if (!(Object.keys(t.callbacks).length || t.pointers && Object.keys(t.pointers).length)) return delete this.weakmap[e]
      },
      stubFunction: function(t, e) {
        var n, i, o;
        return i = t[e], n = this.weakReference(t), o = this.weakmap, t[e] = function() {
          var e, r, s, a, l, u, c, d, f;
          for (r in s = i.apply(t, arguments), u = n.pointers)
            for (e = u[r], a = 0, l = (f = null != (c = null != (d = o[r]) ? d.callbacks[e] : void 0) ? c : []).length; a < l; a++)(0, f[a])();
          return s
        }
      },
      observeMutations: function(t, e, n) {
        var i, o, r, s, a, u;
        if (Array.isArray(t)) {
          if (null == (r = this.weakReference(t)).pointers)
            for (r.pointers = {}, a = 0, u = (o = ["push", "pop", "shift", "unshift", "sort", "reverse", "splice"]).length; a < u; a++) i = o[a], this.stubFunction(t, i);
          if (null == (s = r.pointers)[e] && (s[e] = []), l.call(r.pointers[e], n) < 0) return r.pointers[e].push(n)
        }
      },
      unobserveMutations: function(t, e, n) {
        var i, o, r;
        if (Array.isArray(t) && null != t[this.id] && (o = this.weakmap[t[this.id]]) && (r = o.pointers[e])) return (i = r.indexOf(n)) >= 0 && r.splice(i, 1), r.length || delete o.pointers[e], this.cleanupWeakReference(o, t[this.id])
      },
      observe: function(t, e, n) {
        var i, o, r;
        return null == (i = this.weakReference(t).callbacks)[e] && (i[e] = [], (null != (o = Object.getOwnPropertyDescriptor(t, e)) ? o.get : void 0) || (null != o ? o.set : void 0) || (r = t[e], Object.defineProperty(t, e, {
          enumerable: !0,
          get: function() {
            return r
          },
          set: function(o) {
            return function(s) {
              var a, u, c, d;
              if (s !== r && (o.unobserveMutations(r, t[o.id], e), r = s, a = o.weakmap[t[o.id]])) {
                if ((i = a.callbacks)[e])
                  for (u = 0, c = (d = i[e].slice()).length; u < c; u++) n = d[u], l.call(i[e], n) >= 0 && n();
                return o.observeMutations(s, t[o.id], e)
              }
            }
          }(this)
        }))), l.call(i[e], n) < 0 && i[e].push(n), this.observeMutations(t[e], t[this.id], e)
      },
      unobserve: function(t, e, n) {
        var i, o, r;
        if ((r = this.weakmap[t[this.id]]) && (i = r.callbacks[e])) return (o = i.indexOf(n)) >= 0 && (i.splice(o, 1), i.length || delete r.callbacks[e]), this.unobserveMutations(t[e], t[this.id], e), this.cleanupWeakReference(r, t[this.id])
      },
      get: function(t, e) {
        return t[e]
      },
      set: function(t, e, n) {
        return t[e] = n
      }
    }, t.factory = function(e) {
      return t.sightglass = e, t.public._ = t, t.public
    }, "object" == typeof("undefined" != typeof module && null !== module ? module.exports : void 0) ? module.exports = t.factory(require("sightglass")) : "function" == typeof define && define.amd ? define(["sightglass"], function(e) {
      return this.rivets = t.factory(e)
    }) : this.rivets = t.factory(sightglass)
  }.call(this),
  function() {
    var t, e, n, i, o, r, s = function(t, e) {
      return function() {
        return t.apply(e, arguments)
      }
    };
    e = function() {
      function t() {
        this.update = s(this.update, this)
      }
      return t.prototype.update = function(t) {
        var e, n, o;
        for (n in t) o = t[n], "items" !== n && (this[n] = o);
        return this.items = function() {
          var n, o, r, s;
          for (s = [], n = 0, o = (r = t.items).length; n < o; n++) e = r[n], s.push(new i(e));
          return s
        }()
      }, t
    }(), i = function() {
      function t(t) {
        this.propertyArray = s(this.propertyArray, this), this.update = s(this.update, this), this.update(t)
      }
      return t.prototype.update = function(t) {
        var e, i;
        for (e in t) i = t[e], "properties" !== e && (this[e] = i);
        return this.properties = n.Utils.extend({}, t.properties)
      }, t.prototype.propertyArray = function() {
        var t, e, n, i;
        for (t in i = [], n = this.properties) e = n[t], i.push({
          name: t,
          value: e
        });
        return i
      }, t
    }(), (n = {
      settings: {
        debug: !1,
        dataAPI: !0,
        requestBodyClass: null,
        rivetsModels: {},
        currency: null,
        moneyFormat: null,
        moneyWithCurrencyFormat: null,
        weightUnit: "g",
        weightPrecision: 0
      },
      cart: new e
    }).init = function(t, e) {
      return null == e && (e = {}), n.configure(e), n.Utils.log("Initialising CartJS."), n.cart.update(t), n.settings.dataAPI && (n.Utils.log('"dataAPI" setting is true, initialising Data API.'), n.Data.init()), n.settings.requestBodyClass && (n.Utils.log('"requestBodyClass" set, adding event listeners.'), jQuery(document).on("cart.requestStarted", function() {
        return jQuery("body").addClass(n.settings.requestBodyClass)
      }), jQuery(document).on("cart.requestComplete", function() {
        return jQuery("body").removeClass(n.settings.requestBodyClass)
      })), n.Rivets.init(), jQuery(document).trigger("cart.ready", [n.cart])
    }, n.configure = function(t) {
      return null == t && (t = {}), n.Utils.extend(n.settings, t)
    }, null == window.console && (window.console = {}, window.console.log = function() {}), n.Utils = {
      log: function() {
        return n.Utils.console(console.log, arguments)
      },
      error: function() {
        return n.Utils.console(console.error, arguments)
      },
      console: function(t, e) {
        if (n.settings.debug && "undefined" != typeof console && null !== console) return (e = Array.prototype.slice.call(e)).unshift("[CartJS]:"), t.apply(console, e)
      },
      wrapKeys: function(t, e, n) {
        var i, o, r;
        for (i in null == e && (e = "properties"), r = {}, t) o = t[i], r[e + "[" + i + "]"] = null != n ? n : o;
        return r
      },
      unwrapKeys: function(t, e, n) {
        var i, o, r;
        for (i in null == e && (e = "properties"), o = {}, t) r = t[i], o[i.replace(e + "[", "").replace("]", "")] = null != n ? n : r;
        return o
      },
      extend: function(t, e) {
        var n, i;
        for (n in e) i = e[n], t[n] = i;
        return t
      },
      clone: function(t) {
        var e, n;
        if (null == t || "object" != typeof t) return t;
        for (e in n = new t.constructor, t) n[e] = clone(t[e]);
        return n
      },
      isArray: Array.isArray || function(t) {
        return "[object Array]" === {}.toString.call(t)
      },
      ensureArray: function(t) {
        return n.Utils.isArray(t) ? t : null != t ? [t] : []
      },
      formatMoney: function(t, e, i, o) {
        var r, s;
        return null == o && (o = ""), o || (o = n.settings.currency), null != window.Currency && o !== n.settings.currency && (t = Currency.convert(t, n.settings.currency, o), null != (null != (r = window.Currency) ? r.moneyFormats : void 0) && o in window.Currency.moneyFormats && (e = window.Currency.moneyFormats[o][i])), null != (null != (s = window.Shopify) ? s.formatMoney : void 0) ? Shopify.formatMoney(t, e) : t
      },
      getSizedImageUrl: function(t, e) {
        var n, i;
        return null != (null != (n = window.Shopify) && null != (i = n.Image) ? i.getSizedImageUrl : void 0) ? t ? Shopify.Image.getSizedImageUrl(t, e) : Shopify.Image.getSizedImageUrl("https://cdn.shopify.com/s/images/admin/no-image-.gif", e).replace("-_", "-") : t || "https://cdn.shopify.com/s/images/admin/no-image-large.gif"
      }
    }, r = [], o = !1, n.Queue = {
      add: function(t, e, i) {
        var s;
        if (null == i && (i = {}), s = {
            url: t,
            data: e,
            type: i.type || "POST",
            dataType: i.dataType || "json",
            success: n.Utils.ensureArray(i.success),
            error: n.Utils.ensureArray(i.error),
            complete: n.Utils.ensureArray(i.complete)
          }, i.updateCart && s.success.push(n.cart.update), r.push(s), !o) return jQuery(document).trigger("cart.requestStarted", [n.cart, e]), n.Queue.process()
      },
      process: function() {
        var t;
        return r.length ? (o = !0, (t = r.shift()).complete = n.Queue.process, t.success.push(function() {
          jQuery(document).trigger("cart.requestSuccess", [n.cart, t.data])
        }), t.error = function(t, e, n) {
          jQuery(document).trigger("cart.requestError", [t, e, n])
        }, jQuery.ajax(t)) : (o = !1, void jQuery(document).trigger("cart.requestComplete", [n.cart]))
      }
    }, n.Core = {
      getCart: function(t) {
        return null == t && (t = {}), t.type = "GET", t.updateCart = !0, n.Queue.add("/cart.js", {}, t)
      },
      addItem: function(t, e, i, o) {
        if($(".send_to_checkout",o).length > 0){
          simply.ajaxDirectCheckout(o);
          return false;
        }  
        if($(".send_to_cart",o).length > 0){
          simply.ajaxDirectCheckout(o,"cart");
          return false;
        }
        var r;
        return null == e && (e = 1), null == i && (i = {}), null == o && (o = {}), (r = n.Utils.wrapKeys(i)).id = t, r.quantity = e, n.Queue.add("/cart/add.js", r, o), n.Core.getCart()
      },
      updateItem: function(t, e, i, o) {
        var r;
        return null == i && (i = {}), null == o && (o = {}), (r = n.Utils.wrapKeys(i)).line = t, null != e && (r.quantity = e), o.updateCart = !0, n.Queue.add("/cart/change.js", r, o)
      },
      removeItem: function(t, e) {
        return null == e && (e = {}), n.Core.updateItem(t, 0, {}, e)
      },
      updateItemById: function(t, e, i, o) {
        var r;
        return null == i && (i = {}), null == o && (o = {}), (r = n.Utils.wrapKeys(i)).id = t, null != e && (r.quantity = e), o.updateCart = !0, n.Queue.add("/cart/change.js", r, o)
      },
      updateItemQuantitiesById: function(t, e) {
        return null == t && (t = {}), null == e && (e = {}), e.updateCart = !0, n.Queue.add("/cart/update.js", {
          updates: t
        }, e)
      },
      removeItemById: function(t, e) {
        var i;
        return null == e && (e = {}), i = {
          id: t,
          quantity: 0
        }, e.updateCart = !0, n.Queue.add("/cart/change.js", i, e)
      },
      clear: function(t) {
        return null == t && (t = {}), t.updateCart = !0, n.Queue.add("/cart/clear.js", {}, t)
      },
      getAttribute: function(t, e) {
        return t in n.cart.attributes ? n.cart.attributes[t] : e
      },
      setAttribute: function(t, e, i) {
        var o;
        return null == i && (i = {}), (o = {})[t] = e, n.Core.setAttributes(o, i)
      },
      getAttributes: function() {
        return n.cart.attributes
      },
      setAttributes: function(t, e) {
        return null == t && (t = {}), null == e && (e = {}), e.updateCart = !0, n.Queue.add("/cart/update.js", n.Utils.wrapKeys(t, "attributes"), e)
      },
      clearAttributes: function(t) {
        return null == t && (t = {}), t.updateCart = !0, n.Queue.add("/cart/update.js", n.Utils.wrapKeys(n.Core.getAttributes(), "attributes", ""), t)
      },
      getNote: function() {
        return n.cart.note
      },
      setNote: function(t, e) {
        return null == e && (e = {}), e.updateCart = !0, n.Queue.add("/cart/update.js", {
          note: t
        }, e)
      }
    }, t = null, n.Data = {
      init: function() {
        return t = jQuery(document), n.Data.setEventListeners("on"), n.Data.render(null, n.cart)
      },
      destroy: function() {
        return n.Data.setEventListeners("off")
      },
      setEventListeners: function(e) {
        return t[e]("click", "[data-cart-add]", n.Data.add), t[e]("click", "[data-cart-remove]", n.Data.remove), t[e]("click", "[data-cart-remove-id]", n.Data.removeById), t[e]("click", "[data-cart-update]", n.Data.update), t[e]("click", "[data-cart-update-id]", n.Data.updateById), t[e]("click", "[data-cart-clear]", n.Data.clear), t[e]("change", "[data-cart-toggle]", n.Data.toggle), t[e]("change", "[data-cart-toggle-attribute]", n.Data.toggleAttribute), t[e]("submit", "[data-cart-submit]", n.Data.submit), t[e]("cart.requestComplete", n.Data.render)
      },
      add: function(t) {
        var e;
        return t.preventDefault(), e = jQuery(this), n.Core.addItem(e.attr("data-cart-add"), e.attr("data-cart-quantity"))
      },
      remove: function(t) {
        var e;
        return t.preventDefault(), e = jQuery(this), n.Core.removeItem(e.attr("data-cart-remove"))
      },
      removeById: function(t) {
        var e;
        return t.preventDefault(), e = jQuery(this), n.Core.removeItemById(e.attr("data-cart-remove-id"))
      },
      update: function(t) {
        var e;
        return t.preventDefault(), e = jQuery(this), n.Core.updateItem(e.attr("data-cart-update"), e.attr("data-cart-quantity"))
      },
      updateById: function(t) {
        var e;
        return t.preventDefault(), e = jQuery(this), n.Core.updateItemById(e.attr("data-cart-update-id"), e.attr("data-cart-quantity"))
      },
      clear: function(t) {
        return t.preventDefault(), n.Core.clear()
      },
      toggle: function(t) {
        var e, i;
        return i = (e = jQuery(this)).attr("data-cart-toggle"), e.is(":checked") ? n.Core.addItem(i) : n.Core.removeItemById(i)
      },
      toggleAttribute: function(t) {
        var e, i;
        return i = (e = jQuery(this)).attr("data-cart-toggle-attribute"), n.Core.setAttribute(i, e.is(":checked") ? "Yes" : "")
      },
      submit: function(t) {
        var e, i, o, r;
        return t.preventDefault(), e = jQuery(this).serializeArray(), i = void 0, r = void 0, o = {}, jQuery.each(e, function(t, e) {
          return "id" === e.name ? i = e.value : "quantity" === e.name ? r = e.value : e.name.match(/^properties\[\w+\]$/) ? o[e.name] = e.value : void 0
        }), n.Core.addItem(i, r, n.Utils.unwrapKeys(o),$(this))
      },
      render: function(t, e) {
        var i;
        return i = {
          item_count: e.item_count,
          total_price: e.total_price,
          total_price_money: n.Utils.formatMoney(e.total_price, n.settings.moneyFormat, "money_format", null != (null != Currency ? Currency.currentCurrency : void 0) ? Currency.currentCurrency : void 0),
          total_price_money_with_currency: n.Utils.formatMoney(e.total_price, n.settings.moneyWithCurrencyFormat, "money_with_currency_format", null != (null != Currency ? Currency.currentCurrency : void 0) ? Currency.currentCurrency : void 0)
        }, jQuery("[data-cart-render]").each(function() {
          var t;
          return (t = jQuery(this)).html(i[t.attr("data-cart-render")])
        })
      }
    }, "rivets" in window ? (n.Rivets = {
      model: null,
      boundViews: [],
      init: function() {
        return n.Rivets.bindViews()
      },
      destroy: function() {
        return n.Rivets.unbindViews()
      },
      bindViews: function() {
        return n.Utils.log("Rivets.js is present, binding views."), n.Rivets.unbindViews(), n.Rivets.model = n.Utils.extend({
          cart: n.cart
        }, n.settings.rivetsModels), null != window.Currency && (n.Rivets.model.Currency = window.Currency), jQuery("[data-cart-view]").each(function() {
          var t;
          return t = rivets.bind(jQuery(this), n.Rivets.model), n.Rivets.boundViews.push(t)
        })
      },
      unbindViews: function() {
        var t, e, i;
        for (t = 0, e = (i = n.Rivets.boundViews).length; t < e; t++) i[t].unbind();
        return n.Rivets.boundViews = []
      }
    }, rivets.binders["href-cart-remove"] = function(t, e) {
      t.setAttribute("href", "/cart/change?line=" + e + "&amp;quantity=0")
    }, rivets.formatters.eq = function(t, e) {
      return t === e
    }, rivets.formatters.includes = function(t, e) {
      return t.indexOf(e) >= 0
    }, rivets.formatters.match = function(t, e, n) {
      return t.match(new RegExp(e, n))
    }, rivets.formatters.lt = function(t, e) {
      return t < e
    }, rivets.formatters.gt = function(t, e) {
      return t > e
    }, rivets.formatters.not = function(t) {
      return !t
    }, rivets.formatters.empty = function(t) {
      return !t.length
    }, rivets.formatters.plus = function(t, e) {
      return parseInt(t) + parseInt(e)
    }, rivets.formatters.minus = function(t, e) {
      return parseInt(t) - parseInt(e)
    }, rivets.formatters.times = function(t, e) {
      return t * e
    }, rivets.formatters.divided_by = function(t, e) {
      return t / e
    }, rivets.formatters.modulo = function(t, e) {
      return t % e
    }, rivets.formatters.prepend = function(t, e) {
      return e + t
    }, rivets.formatters.append = function(t, e) {
      return t + e
    }, rivets.formatters.slice = function(t, e, n) {
      return t.slice(e, n)
    }, rivets.formatters.pluralize = function(t, e, i) {
      return null == i && (i = e + "s"), n.Utils.isArray(t) && (t = t.length), 1 === t ? e : i
    }, rivets.formatters.array_element = function(t, e) {
      return t[e]
    }, rivets.formatters.array_first = function(t) {
      return t[0]
    }, rivets.formatters.array_last = function(t) {
      return t[t.length - 1]
    }, rivets.formatters.money = function(t, e) {
      return n.Utils.formatMoney(t, n.settings.moneyFormat, "money_format", e)
    }, rivets.formatters.money_with_currency = function(t, e) {
      return n.Utils.formatMoney(t, n.settings.moneyWithCurrencyFormat, "money_with_currency_format", e)
    }, rivets.formatters.weight = function(t) {
      switch (n.settings.weightUnit) {
        case "kg":
          return (t / 1e3).toFixed(n.settings.weightPrecision);
        case "oz":
          return (.035274 * t).toFixed(n.settings.weightPrecision);
        case "lb":
          return (.00220462 * t).toFixed(n.settings.weightPrecision);
        default:
          return t.toFixed(n.settings.weightPrecision)
      }
    }, rivets.formatters.weight_with_unit = function(t) {
      return rivets.formatters.weight(t) + n.settings.weightUnit
    }, rivets.formatters.product_image_size = function(t, e) {
      return n.Utils.getSizedImageUrl(t, e)
    }, rivets.formatters.moneyWithCurrency = rivets.formatters.money_with_currency, rivets.formatters.weightWithUnit = rivets.formatters.weight_with_unit, rivets.formatters.productImageSize = rivets.formatters.product_image_size) : n.Rivets = {
      init: function() {},
      destroy: function() {}
    }, n.factory = function(t) {
      return t.init = n.init, t.configure = n.configure, t.cart = n.cart, t.settings = n.settings, t.getCart = n.Core.getCart, t.addItem = n.Core.addItem, t.updateItem = n.Core.updateItem, t.updateItemById = n.Core.updateItemById, t.updateItemQuantitiesById = n.Core.updateItemQuantitiesById, t.removeItem = n.Core.removeItem, t.removeItemById = n.Core.removeItemById, t.clear = n.Core.clear, t.getAttribute = n.Core.getAttribute, t.setAttribute = n.Core.setAttribute, t.getAttributes = n.Core.getAttributes, t.setAttributes = n.Core.setAttributes, t.clearAttributes = n.Core.clearAttributes, t.getNote = n.Core.getNote, t.setNote = n.Core.setNote, t.render = n.Data.render
    }, "object" == typeof exports ? n.factory(exports) : "function" == typeof define && define.amd ? define(["exports"], function(t) {
      return n.factory(this.CartJS = t), t
    }) : n.factory(this.CartJS = {})
  }.call(this), void 0 === Shopify.Cart && (Shopify.Cart = {}), Shopify.Cart.ShippingCalculator = function() {
    var _config = {
        submitButton: "Calculate shipping",
        submitButtonDisabled: "Calculating...",
        templateId: "shipping-calculator-response-template",
        wrapperId: "wrapper-response",
        customerIsLoggedIn: !1,
        moneyFormat: "${{amount}}"
      },
      _render = function(t) {
        var e = jQuery("#" + _config.templateId),
          n = jQuery("#" + _config.wrapperId);
        if (e.length && n.length) {
          var i = jQuery(e.html());
          console.log(i), console.log(t);
          var o = i.appendTo(n);
          rivets.bind(o, {
            response: t
          }), !0 === dragonDropPlugins.topbar.multiCurrency && Currency.convertAll(dragonDropPlugins.topbar.shopCurrency, Currency.currentCurrency)
        }
      },
      _enableButtons = function() {
        jQuery(".get-rates").removeAttr("disabled").removeClass("disabled").val(_config.submitButton)
      },
      _disableButtons = function() {
        jQuery(".get-rates").val(_config.submitButtonDisabled).attr("disabled", "disabled").addClass("disabled")
      },
      _getCartShippingRatesForDestination = function(t) {
        var e = {
          type: "POST",
          url: "/cart/prepare_shipping_rates",
          data: jQuery.param({
            shipping_address: t
          }),
          success: _pollForCartShippingRatesForDestination(t),
          error: _onError
        };
        jQuery.ajax(e)
      },
      _pollForCartShippingRatesForDestination = function(t) {
        var e = function() {
          jQuery.ajax("/cart/async_shipping_rates", {
            dataType: "json",
            success: function(n, i, o) {
              200 === o.status ? _onCartShippingRatesUpdate(n.shipping_rates, t) : setTimeout(e, 500)
            },
            error: _onError
          })
        };
        return e
      },
      _fullMessagesFromErrors = function(t) {
        var e = [];
        return jQuery.each(t, function(t, n) {
          jQuery.each(n, function(n, i) {
            e.push(t + " " + i)
          })
        }), e
      },
      _onError = function(XMLHttpRequest, textStatus) {
        jQuery("#estimated-shipping").hide(), jQuery("#estimated-shipping em").empty(), _enableButtons();
        var feedback = "",
          data = eval("(" + XMLHttpRequest.responseText + ")");
        feedback = data.message ? data.message + "(" + data.status + "): " + data.description : "Error : " + _fullMessagesFromErrors(data).join("; ") + ".", "Error : country is not supported." === feedback && (feedback = "We do not ship to this destination."), _render({
          rates: [],
          rates_length: 0,
          errorFeedback: feedback,
          success: !1
        }), jQuery("#" + _config.wrapperId).show()
      },
      _onCartShippingRatesUpdate = function(t, e) {
        _enableButtons();
        var n = "";
        e.zip && (n += e.zip + ", "), e.province && (n += e.province + ", "), n += e.country, _render({
          rates: t,
          rates_length: t.length,
          address: n,
          success: !0
        }), jQuery("#" + _config.wrapperId + ", #estimated-shipping").fadeIn()
      },
      _formatRate = function(t) {
        function e(t, e) {
          return void 0 === t ? e : t
        }

        function n(t, n, i, o) {
          if (n = e(n, 2), i = e(i, ","), o = e(o, "."), isNaN(t) || null == t) return 0;
          var r = (t = (t / 100).toFixed(n)).split(".");
          return r[0].replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1" + i) + (r[1] ? o + r[1] : "")
        }
        if ("function" == typeof Shopify.formatMoney) return Shopify.formatMoney(t, _config.moneyFormat);
        "string" == typeof t && (t = t.replace(".", ""));
        var i = "",
          o = /\{\{\s*(\w+)\s*\}\}/,
          r = _config.moneyFormat;
        switch (r.match(o)[1]) {
          case "amount":
            i = n(t, 2);
            break;
          case "amount_no_decimals":
            i = n(t, 0);
            break;
          case "amount_with_comma_separator":
            i = n(t, 2, ".", ",");
            break;
          case "amount_no_decimals_with_comma_separator":
            i = n(t, 0, ".", ",")
        }
        return r.replace(o, i)
      };
    return _init = function() {
      new Shopify.CountryProvinceSelector("address_country", "address_province", {
        hideElement: "address_province_container"
      }), jQuery("#address_country"), jQuery("#address_province_label").get(0), jQuery(".get-rates").click(function() {
        _disableButtons(), jQuery("#" + _config.wrapperId).empty().hide();
        var t = {};
        t.zip = jQuery("#address_zip").val() || "", t.country = jQuery("#address_country").val() || "", t.province = jQuery("#address_province").val() || "", _getCartShippingRatesForDestination(t)
      }), _config.customerIsLoggedIn && jQuery(".get-rates:eq(0)").trigger("click"), rivets.binders.echo = function(t, e) {
        console.log(e)
      }
    }, {
      show: function(t) {
        t = t || {}, jQuery.extend(_config, t), jQuery(function() {
          _init()
        })
      },
      getConfig: function() {
        return _config
      },
      formatRate: function(t) {
        return _formatRate(t)
      }
    }
  }(),
  function(t) {
    if ("object" == typeof exports && "undefined" != typeof module) module.exports = t();
    else if ("function" == typeof define && define.amd) define([], t);
    else {
      ("undefined" != typeof window ? window : "undefined" != typeof global ? global : "undefined" != typeof self ? self : this).store = t()
    }
  }(function() {
    var define, module, exports;
    return function t(e, n, i) {
      function o(s, a) {
        if (!n[s]) {
          if (!e[s]) {
            var l = "function" == typeof require && require;
            if (!a && l) return l(s, !0);
            if (r) return r(s, !0);
            var u = new Error("Cannot find module '" + s + "'");
            throw u.code = "MODULE_NOT_FOUND", u
          }
          var c = n[s] = {
            exports: {}
          };
          e[s][0].call(c.exports, function(t) {
            var n = e[s][1][t];
            return o(n || t)
          }, c, c.exports, t, e, n, i)
        }
        return n[s].exports
      }
      for (var r = "function" == typeof require && require, s = 0; s < i.length; s++) o(i[s]);
      return o
    }({
      1: [function(t, e, n) {
        "use strict";
        var i = t("../src/store-engine"),
          o = t("../storages/all"),
          r = [t("../plugins/json2")];
        e.exports = i.createStore(o, r)
      }, {
        "../plugins/json2": 2,
        "../src/store-engine": 4,
        "../storages/all": 6
      }],
      2: [function(t, e, n) {
        "use strict";
        e.exports = function() {
          return t("./lib/json2"), {}
        }
      }, {
        "./lib/json2": 3
      }],
      3: [function(require, module, exports) {
        "use strict";
        var _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function(t) {
          return typeof t
        } : function(t) {
          return t && "function" == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype ? "symbol" : typeof t
        };
        "object" !== ("undefined" == typeof JSON ? "undefined" : _typeof(JSON)) && (JSON = {}),
        function() {
          function f(t) {
            return t < 10 ? "0" + t : t
          }

          function this_value() {
            return this.valueOf()
          }

          function quote(t) {
            return rx_escapable.lastIndex = 0, rx_escapable.test(t) ? '"' + t.replace(rx_escapable, function(t) {
              var e = meta[t];
              return "string" == typeof e ? e : "\\u" + ("0000" + t.charCodeAt(0).toString(16)).slice(-4)
            }) + '"' : '"' + t + '"'
          }

          function str(t, e) {
            var n, i, o, r, s, a = gap,
              l = e[t];
            switch (l && "object" === (void 0 === l ? "undefined" : _typeof(l)) && "function" == typeof l.toJSON && (l = l.toJSON(t)), "function" == typeof rep && (l = rep.call(e, t, l)), void 0 === l ? "undefined" : _typeof(l)) {
              case "string":
                return quote(l);
              case "number":
                return isFinite(l) ? String(l) : "null";
              case "boolean":
              case "null":
                return String(l);
              case "object":
                if (!l) return "null";
                if (gap += indent, s = [], "[object Array]" === Object.prototype.toString.apply(l)) {
                  for (r = l.length, n = 0; n < r; n += 1) s[n] = str(n, l) || "null";
                  return o = 0 === s.length ? "[]" : gap ? "[\n" + gap + s.join(",\n" + gap) + "\n" + a + "]" : "[" + s.join(",") + "]", gap = a, o
                }
                if (rep && "object" === (void 0 === rep ? "undefined" : _typeof(rep)))
                  for (r = rep.length, n = 0; n < r; n += 1) "string" == typeof rep[n] && ((o = str(i = rep[n], l)) && s.push(quote(i) + (gap ? ": " : ":") + o));
                else
                  for (i in l) Object.prototype.hasOwnProperty.call(l, i) && ((o = str(i, l)) && s.push(quote(i) + (gap ? ": " : ":") + o));
                return o = 0 === s.length ? "{}" : gap ? "{\n" + gap + s.join(",\n" + gap) + "\n" + a + "}" : "{" + s.join(",") + "}", gap = a, o
            }
          }
          var rx_one = /^[\],:{}\s]*$/,
            rx_two = /\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g,
            rx_three = /"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g,
            rx_four = /(?:^|:|,)(?:\s*\[)+/g,
            rx_escapable = /[\\"\u0000-\u001f\u007f-\u009f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
            rx_dangerous = /[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
            gap, indent, meta, rep;
          "function" != typeof Date.prototype.toJSON && (Date.prototype.toJSON = function() {
            return isFinite(this.valueOf()) ? this.getUTCFullYear() + "-" + f(this.getUTCMonth() + 1) + "-" + f(this.getUTCDate()) + "T" + f(this.getUTCHours()) + ":" + f(this.getUTCMinutes()) + ":" + f(this.getUTCSeconds()) + "Z" : null
          }, Boolean.prototype.toJSON = this_value, Number.prototype.toJSON = this_value, String.prototype.toJSON = this_value), "function" != typeof JSON.stringify && (meta = {
            "\b": "\\b",
            "\t": "\\t",
            "\n": "\\n",
            "\f": "\\f",
            "\r": "\\r",
            '"': '\\"',
            "\\": "\\\\"
          }, JSON.stringify = function(t, e, n) {
            var i;
            if (gap = "", indent = "", "number" == typeof n)
              for (i = 0; i < n; i += 1) indent += " ";
            else "string" == typeof n && (indent = n);
            if (rep = e, e && "function" != typeof e && ("object" !== (void 0 === e ? "undefined" : _typeof(e)) || "number" != typeof e.length)) throw new Error("JSON.stringify");
            return str("", {
              "": t
            })
          }), "function" != typeof JSON.parse && (JSON.parse = function(text, reviver) {
            function walk(t, e) {
              var n, i, o = t[e];
              if (o && "object" === (void 0 === o ? "undefined" : _typeof(o)))
                for (n in o) Object.prototype.hasOwnProperty.call(o, n) && (void 0 !== (i = walk(o, n)) ? o[n] = i : delete o[n]);
              return reviver.call(t, e, o)
            }
            var j;
            if (text = String(text), rx_dangerous.lastIndex = 0, rx_dangerous.test(text) && (text = text.replace(rx_dangerous, function(t) {
                return "\\u" + ("0000" + t.charCodeAt(0).toString(16)).slice(-4)
              })), rx_one.test(text.replace(rx_two, "@").replace(rx_three, "]").replace(rx_four, ""))) return j = eval("(" + text + ")"), "function" == typeof reviver ? walk({
              "": j
            }, "") : j;
            throw new SyntaxError("JSON.parse")
          })
        }()
      }, {}],
      4: [function(t, e, n) {
        "use strict";

        function i(t, e, n) {
          n || (n = ""), t && !c(t) && (t = [t]), e && !c(e) && (e = [e]);
          var i = n ? "__storejs_" + n + "_" : "",
            o = n ? new RegExp("^" + i) : null;
          if (!/^[a-zA-Z0-9_\-]*$/.test(n)) throw new Error("store.js namespaces can only have alphanumerics + underscores and dashes");
          var p = u({
            _namespacePrefix: i,
            _namespaceRegexp: o,
            _testStorage: function(t) {
              try {
                var e = "__storejs__test__";
                t.write(e, e);
                var n = t.read(e) === e;
                return t.remove(e), n
              } catch (t) {
                return !1
              }
            },
            _assignPluginFnProp: function(t, e) {
              var n = this[e];
              this[e] = function() {
                var e = r(arguments, 0),
                  i = this,
                  o = [function() {
                    if (n) return a(arguments, function(t, n) {
                      e[n] = t
                    }), n.apply(i, e)
                  }].concat(e);
                return t.apply(i, o)
              }
            },
            _serialize: function(t) {
              return JSON.stringify(t)
            },
            _deserialize: function(t, e) {
              if (!t) return e;
              var n = "";
              try {
                n = JSON.parse(t)
              } catch (e) {
                n = t
              }
              return void 0 !== n ? n : e
            },
            _addStorage: function(t) {
              this.enabled || this._testStorage(t) && (this.storage = t, this.enabled = !0)
            },
            _addPlugin: function(t) {
              var e = this;
              if (c(t)) a(t, function(t) {
                e._addPlugin(t)
              });
              else if (!s(this.plugins, function(e) {
                  return t === e
                })) {
                if (this.plugins.push(t), !d(t)) throw new Error("Plugins must be function values that return objects");
                var n = t.call(this);
                if (!f(n)) throw new Error("Plugins must return an object of function properties");
                a(n, function(n, i) {
                  if (!d(n)) throw new Error("Bad plugin property: " + i + " from plugin " + t.name + ". Plugins should only return functions.");
                  e._assignPluginFnProp(n, i)
                })
              }
            },
            addStorage: function(t) {
              (function() {
                var t = "undefined" == typeof console ? null : console;
                t && (t.warn ? t.warn : t.log).apply(t, arguments)
              })("store.addStorage(storage) is deprecated. Use createStore([storages])"), this._addStorage(t)
            }
          }, h, {
            plugins: []
          });
          return p.raw = {}, a(p, function(t, e) {
            d(t) && (p.raw[e] = l(p, t))
          }), a(t, function(t) {
            p._addStorage(t)
          }), a(e, function(t) {
            p._addPlugin(t)
          }), p
        }
        var o = t("./util"),
          r = o.slice,
          s = o.pluck,
          a = o.each,
          l = o.bind,
          u = o.create,
          c = o.isList,
          d = o.isFunction,
          f = o.isObject;
        e.exports = {
          createStore: i
        };
        var h = {
          version: "2.0.12",
          enabled: !1,
          get: function(t, e) {
            var n = this.storage.read(this._namespacePrefix + t);
            return this._deserialize(n, e)
          },
          set: function(t, e) {
            return void 0 === e ? this.remove(t) : (this.storage.write(this._namespacePrefix + t, this._serialize(e)), e)
          },
          remove: function(t) {
            this.storage.remove(this._namespacePrefix + t)
          },
          each: function(t) {
            var e = this;
            this.storage.each(function(n, i) {
              t.call(e, e._deserialize(n), (i || "").replace(e._namespaceRegexp, ""))
            })
          },
          clearAll: function() {
            this.storage.clearAll()
          },
          hasNamespace: function(t) {
            return this._namespacePrefix == "__storejs_" + t + "_"
          },
          createStore: function() {
            return i.apply(this, arguments)
          },
          addPlugin: function(t) {
            this._addPlugin(t)
          },
          namespace: function(t) {
            return i(this.storage, this.plugins, t)
          }
        }
      }, {
        "./util": 5
      }],
      5: [function(t, e, n) {
        (function(t) {
          "use strict";

          function n(t, e) {
            return Array.prototype.slice.call(t, e || 0)
          }

          function i(t, e) {
            o(t, function(t, n) {
              return e(t, n), !1
            })
          }

          function o(t, e) {
            if (r(t)) {
              for (var n = 0; n < t.length; n++)
                if (e(t[n], n)) return t[n]
            } else
              for (var i in t)
                if (t.hasOwnProperty(i) && e(t[i], i)) return t[i]
          }

          function r(t) {
            return null != t && "function" != typeof t && "number" == typeof t.length
          }
          var s = Object.assign ? Object.assign : function(t, e, n, o) {
              for (var r = 1; r < arguments.length; r++) i(Object(arguments[r]), function(e, n) {
                t[n] = e
              });
              return t
            },
            a = function() {
              if (Object.create) return function(t, e, i, o) {
                var r = n(arguments, 1);
                return s.apply(this, [Object.create(t)].concat(r))
              };
              var t = function() {};
              return function(e, i, o, r) {
                var a = n(arguments, 1);
                return t.prototype = e, s.apply(this, [new t].concat(a))
              }
            }(),
            l = String.prototype.trim ? function(t) {
              return String.prototype.trim.call(t)
            } : function(t) {
              return t.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, "")
            },
            u = "undefined" != typeof window ? window : t;
          e.exports = {
            assign: s,
            create: a,
            trim: l,
            bind: function(t, e) {
              return function() {
                return e.apply(t, Array.prototype.slice.call(arguments, 0))
              }
            },
            slice: n,
            each: i,
            map: function(t, e) {
              var n = r(t) ? [] : {};
              return o(t, function(t, i) {
                return n[i] = e(t, i), !1
              }), n
            },
            pluck: o,
            isList: r,
            isFunction: function(t) {
              return t && "[object Function]" === {}.toString.call(t)
            },
            isObject: function(t) {
              return t && "[object Object]" === {}.toString.call(t)
            },
            Global: u
          }
        }).call(this, "undefined" != typeof global ? global : "undefined" != typeof self ? self : "undefined" != typeof window ? window : {})
      }, {}],
      6: [function(t, e, n) {
        "use strict";
        e.exports = [t("./localStorage"), t("./oldFF-globalStorage"), t("./oldIE-userDataStorage"), t("./cookieStorage"), t("./sessionStorage"), t("./memoryStorage")]
      }, {
        "./cookieStorage": 7,
        "./localStorage": 8,
        "./memoryStorage": 9,
        "./oldFF-globalStorage": 10,
        "./oldIE-userDataStorage": 11,
        "./sessionStorage": 12
      }],
      7: [function(t, e, n) {
        "use strict";

        function i(t) {
          for (var e = u.cookie.split(/; ?/g), n = e.length - 1; n >= 0; n--)
            if (l(e[n])) {
              var i = e[n].split("="),
                o = unescape(i[0]);
              t(unescape(i[1]), o)
            }
        }

        function o(t) {
          t && r(t) && (u.cookie = escape(t) + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT; path=/")
        }

        function r(t) {
          return new RegExp("(?:^|;\\s*)" + escape(t).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=").test(u.cookie)
        }
        var s = t("../src/util"),
          a = s.Global,
          l = s.trim;
        e.exports = {
          name: "cookieStorage",
          read: function(t) {
            if (!t || !r(t)) return null;
            var e = "(?:^|.*;\\s*)" + escape(t).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=\\s*((?:[^;](?!;))*[^;]?).*";
            return unescape(u.cookie.replace(new RegExp(e), "$1"))
          },
          write: function(t, e) {
            t && (u.cookie = escape(t) + "=" + escape(e) + "; expires=Tue, 19 Jan 2038 03:14:07 GMT; path=/")
          },
          each: i,
          remove: o,
          clearAll: function() {
            i(function(t, e) {
              o(e)
            })
          }
        };
        var u = a.document
      }, {
        "../src/util": 5
      }],
      8: [function(t, e, n) {
        "use strict";

        function i() {
          return r.localStorage
        }

        function o(t) {
          return i().getItem(t)
        }
        var r = t("../src/util").Global;
        e.exports = {
          name: "localStorage",
          read: o,
          write: function(t, e) {
            return i().setItem(t, e)
          },
          each: function(t) {
            for (var e = i().length - 1; e >= 0; e--) {
              var n = i().key(e);
              t(o(n), n)
            }
          },
          remove: function(t) {
            return i().removeItem(t)
          },
          clearAll: function() {
            return i().clear()
          }
        }
      }, {
        "../src/util": 5
      }],
      9: [function(t, e, n) {
        "use strict";
        e.exports = {
          name: "memoryStorage",
          read: function(t) {
            return i[t]
          },
          write: function(t, e) {
            i[t] = e
          },
          each: function(t) {
            for (var e in i) i.hasOwnProperty(e) && t(i[e], e)
          },
          remove: function(t) {
            delete i[t]
          },
          clearAll: function(t) {
            i = {}
          }
        };
        var i = {}
      }, {}],
      10: [function(t, e, n) {
        "use strict";

        function i(t) {
          for (var e = r.length - 1; e >= 0; e--) {
            var n = r.key(e);
            t(r[n], n)
          }
        }
        var o = t("../src/util").Global;
        e.exports = {
          name: "oldFF-globalStorage",
          read: function(t) {
            return r[t]
          },
          write: function(t, e) {
            r[t] = e
          },
          each: i,
          remove: function(t) {
            return r.removeItem(t)
          },
          clearAll: function() {
            i(function(t, e) {
              delete r[t]
            })
          }
        };
        var r = o.globalStorage
      }, {
        "../src/util": 5
      }],
      11: [function(t, e, n) {
        "use strict";

        function i(t) {
          return t.replace(/^\d/, "___$&").replace(u, "___")
        }
        var o = t("../src/util").Global;
        e.exports = {
          name: "oldIE-userDataStorage",
          write: function(t, e) {
            if (!l) {
              var n = i(t);
              a(function(t) {
                t.setAttribute(n, e), t.save(r)
              })
            }
          },
          read: function(t) {
            if (!l) {
              var e = i(t),
                n = null;
              return a(function(t) {
                n = t.getAttribute(e)
              }), n
            }
          },
          each: function(t) {
            a(function(e) {
              for (var n = e.XMLDocument.documentElement.attributes, i = n.length - 1; i >= 0; i--) {
                var o = n[i];
                t(e.getAttribute(o.name), o.name)
              }
            })
          },
          remove: function(t) {
            var e = i(t);
            a(function(t) {
              t.removeAttribute(e), t.save(r)
            })
          },
          clearAll: function() {
            a(function(t) {
              var e = t.XMLDocument.documentElement.attributes;
              t.load(r);
              for (var n = e.length - 1; n >= 0; n--) t.removeAttribute(e[n].name);
              t.save(r)
            })
          }
        };
        var r = "storejs",
          s = o.document,
          a = function() {
            if (!s || !s.documentElement || !s.documentElement.addBehavior) return null;
            var t, e, n, i = "script";
            try {
              (e = new ActiveXObject("htmlfile")).open(), e.write("<" + i + ">document.w=window</" + i + '><iframe src="/favicon.ico"></iframe>'), e.close(), t = e.w.frames[0].document, n = t.createElement("div")
            } catch (e) {
              n = s.createElement("div"), t = s.body
            }
            return function(e) {
              var i = [].slice.call(arguments, 0);
              i.unshift(n), t.appendChild(n), n.addBehavior("#default#userData"), n.load(r), e.apply(this, i), t.removeChild(n)
            }
          }(),
          l = (o.navigator ? o.navigator.userAgent : "").match(/ (MSIE 8|MSIE 9|MSIE 10)\./),
          u = new RegExp("[!\"#$%&'()*+,/\\\\:;<=>?@[\\]^`{|}~]", "g")
      }, {
        "../src/util": 5
      }],
      12: [function(t, e, n) {
        "use strict";

        function i() {
          return r.sessionStorage
        }

        function o(t) {
          return i().getItem(t)
        }
        var r = t("../src/util").Global;
        e.exports = {
          name: "sessionStorage",
          read: o,
          write: function(t, e) {
            return i().setItem(t, e)
          },
          each: function(t) {
            for (var e = i().length - 1; e >= 0; e--) {
              var n = i().key(e);
              t(o(n), n)
            }
          },
          remove: function(t) {
            return i().removeItem(t)
          },
          clearAll: function() {
            return i().clear()
          }
        }
      }, {
        "../src/util": 5
      }]
    }, {}, [1])(1)
  }),
  function(t, e) {
    var n = function() {
      e(t.lazySizes), t.removeEventListener("lazyunveilread", n, !0)
    };
    e = e.bind(null, t, t.document), "object" == typeof module && module.exports ? e(require("lazysizes")) : t.lazySizes ? n() : t.addEventListener("lazyunveilread", n, !0)
  }(window, function(t, e, n) {
    "use strict";
    if (t.addEventListener) {
      var i = /\s+/g,
        o = /\s*\|\s+|\s+\|\s*/g,
        r = /^(.+?)(?:\s+\[\s*(.+?)\s*\])(?:\s+\[\s*(.+?)\s*\])?$/,
        s = /^\s*\(*\s*type\s*:\s*(.+?)\s*\)*\s*$/,
        a = /\(|\)|'/,
        l = {
          contain: 1,
          cover: 1
        },
        u = function(t, e) {
          if (e) {
            var n = e.match(s);
            n && n[1] ? t.setAttribute("type", n[1]) : t.setAttribute("media", lazySizesConfig.customMedia[e] || e)
          }
        },
        c = function(t, n, s) {
          var a = e.createElement("picture"),
            l = n.getAttribute(lazySizesConfig.sizesAttr),
            c = n.getAttribute("data-ratio"),
            d = n.getAttribute("data-optimumx");
          n._lazybgset && n._lazybgset.parentNode == n && n.removeChild(n._lazybgset), Object.defineProperty(s, "_lazybgset", {
            value: n,
            writable: !0
          }), Object.defineProperty(n, "_lazybgset", {
            value: a,
            writable: !0
          }), t = t.replace(i, " ").split(o), a.style.display = "none", s.className = lazySizesConfig.lazyClass, 1 != t.length || l || (l = "auto"), t.forEach(function(t) {
            var n, i = e.createElement("source");
            l && "auto" != l && i.setAttribute("sizes", l), (n = t.match(r)) ? (i.setAttribute(lazySizesConfig.srcsetAttr, n[1]), u(i, n[2]), u(i, n[3])) : i.setAttribute(lazySizesConfig.srcsetAttr, t), a.appendChild(i)
          }), l && (s.setAttribute(lazySizesConfig.sizesAttr, l), n.removeAttribute(lazySizesConfig.sizesAttr), n.removeAttribute("sizes")), d && s.setAttribute("data-optimumx", d), c && s.setAttribute("data-ratio", c), a.appendChild(s), n.appendChild(a)
        },
        d = function(t) {
          if (t.target._lazybgset) {
            var e = t.target,
              i = e._lazybgset,
              o = e.currentSrc || e.src;
            if (o) {
              var r = n.fire(i, "bgsetproxy", {
                src: o,
                useSrc: a.test(o) ? JSON.stringify(o) : o
              });
              r.defaultPrevented || (i.style.backgroundImage = "url(" + r.detail.useSrc + ")")
            }
            e._lazybgsetLoading && (n.fire(i, "_lazyloaded", {}, !1, !0), delete e._lazybgsetLoading)
          }
        };
      addEventListener("lazybeforeunveil", function(t) {
        var i, o, r;
        !t.defaultPrevented && (i = t.target.getAttribute("data-bgset")) && (r = t.target, (o = e.createElement("img")).alt = "", o._lazybgsetLoading = !0, t.detail.firesLoad = !0, c(i, r, o), setTimeout(function() {
          n.loader.unveil(o), n.rAF(function() {
            n.fire(o, "_lazyloaded", {}, !0, !0), o.complete && d({
              target: o
            })
          })
        }))
      }), e.addEventListener("load", d, !0), t.addEventListener("lazybeforesizes", function(t) {
        if (t.detail.instance == n && t.target._lazybgset && t.detail.dataAttr) {
          var e = function(t) {
            var e;
            return e = (getComputedStyle(t) || {
              getPropertyValue: function() {}
            }).getPropertyValue("background-size"), !l[e] && l[t.style.backgroundSize] && (e = t.style.backgroundSize), e
          }(t.target._lazybgset);
          l[e] && (t.target._lazysizesParentFit = e, n.rAF(function() {
            t.target.setAttribute("data-parent-fit", e), t.target._lazysizesParentFit && delete t.target._lazysizesParentFit
          }))
        }
      }, !0), e.documentElement.addEventListener("lazybeforesizes", function(t) {
        !t.defaultPrevented && t.target._lazybgset && t.detail.instance == n && (t.detail.width = function(t) {
          var e = n.gW(t, t.parentNode);
          return (!t._lazysizesWidth || e > t._lazysizesWidth) && (t._lazysizesWidth = e), t._lazysizesWidth
        }(t.target._lazybgset))
      })
    }
  }),
  function(t, e) {
    var n = function() {
      e(t.lazySizes), t.removeEventListener("lazyunveilread", n, !0)
    };
    e = e.bind(null, t, t.document), "object" == typeof module && module.exports ? e(require("lazysizes")) : t.lazySizes ? n() : t.addEventListener("lazyunveilread", n, !0)
  }(window, function(t, e, n) {
    "use strict";

    function i(e, n) {
      var i, o, r, s, a = t.getComputedStyle(e);
      for (i in o = e.parentNode, s = {
          isPicture: !(!o || !d.test(o.nodeName || ""))
        }, r = function(t, n) {
          var i = e.getAttribute("data-" + t);
          if (!i) {
            var o = a.getPropertyValue("--ls-" + t);
            o && (i = o.trim())
          }
          if (i) {
            if ("true" == i) i = !0;
            else if ("false" == i) i = !1;
            else if (c.test(i)) i = parseFloat(i);
            else if ("function" == typeof l[t]) i = l[t](e, i);
            else if (m.test(i)) try {
              i = JSON.parse(i)
            } catch (t) {}
            s[t] = i
          } else t in l && "function" != typeof l[t] ? s[t] = l[t] : n && "function" == typeof l[t] && (s[t] = l[t](e, i))
        }, l) r(i);
      return n.replace(p, function(t, e) {
        e in s || r(e, !0)
      }), s
    }

    function o(t, n, i) {
      var o = 0,
        r = 0,
        s = i;
      if (t) {
        if ("container" === n.ratio) {
          for (o = s.scrollWidth, r = s.scrollHeight; !(o && r || s === e);) o = (s = s.parentNode).scrollWidth, r = s.scrollHeight;
          o && r && (n.ratio = r / o)
        }(t = function(t, e) {
          var n = [];
          return "string" == typeof e.widths && (e.widths = JSON.parse(e.widths)), n.srcset = [], e.absUrl && (v.setAttribute("href", t), t = v.href), t = ((e.prefix || "") + t + (e.postfix || "")).replace(p, function(t, n) {
            return u[typeof e[n]] ? e[n] : t
          }), e.widths.forEach(function(i) {
            var o = e.widthmap[i] || i,
              r = {
                u: t.replace(f, o).replace(h, e.ratio ? Math.round(i * e.ratio) : ""),
                w: i
              };
            n.push(r), n.srcset.push(r.c = r.u + " " + i + "w")
          }), n
        }(t, n)).isPicture = n.isPicture, b && "IMG" == i.nodeName.toUpperCase() ? i.removeAttribute(a.srcsetAttr) : i.setAttribute(a.srcsetAttr, t.srcset.join(", ")), Object.defineProperty(i, "_lazyrias", {
          value: t,
          writable: !0
        })
      }
    }

    function r(t, e) {
      var o = i(t, e);
      return l.modifyOptions.call(t, {
        target: t,
        details: o,
        detail: o
      }), n.fire(t, "lazyriasmodifyoptions", o), o
    }

    function s(t) {
      return t.getAttribute(t.getAttribute("data-srcattr") || l.srcAttr) || t.getAttribute(a.srcsetAttr) || t.getAttribute(a.srcAttr) || t.getAttribute("data-pfsrcset") || ""
    }
    var a, l, u = {
        string: 1,
        number: 1
      },
      c = /^\-*\+*\d+\.*\d*$/,
      d = /^picture$/i,
      f = /\s*\{\s*width\s*\}\s*/i,
      h = /\s*\{\s*height\s*\}\s*/i,
      p = /\s*\{\s*([a-z0-9]+)\s*\}\s*/gi,
      m = /^\[.*\]|\{.*\}$/,
      g = /^(?:auto|\d+(px)?)$/,
      v = e.createElement("a"),
      y = e.createElement("img"),
      b = "srcset" in y && !("sizes" in y),
      _ = !!t.HTMLPictureElement && !b;
    ! function() {
      var e, i = {
        prefix: "",
        postfix: "",
        srcAttr: "data-src",
        absUrl: !1,
        modifyOptions: function() {},
        widthmap: {},
        ratio: !1
      };
      for (e in (a = n && n.cfg || t.lazySizesConfig) || (a = {}, t.lazySizesConfig = a), a.supportsType || (a.supportsType = function(t) {
          return !t
        }), a.rias || (a.rias = {}), "widths" in (l = a.rias) || (l.widths = [], function(t) {
          for (var e, n = 0; !e || 3e3 > e;)(n += 5) > 30 && (n += 1), e = 36 * n, t.push(e)
        }(l.widths)), i) e in l || (l[e] = i[e])
    }(), addEventListener("lazybeforesizes", function(t) {
      var e, i, u, c, d, h, p, m, v, y, b, x, C;
      if (t.detail.instance == n && (e = t.target, t.detail.dataAttr && !t.defaultPrevented && !l.disabled && (v = e.getAttribute(a.sizesAttr) || e.getAttribute("sizes")) && g.test(v))) {
        if (u = r(e, i = s(e)), b = f.test(u.prefix) || f.test(u.postfix), u.isPicture && (c = e.parentNode))
          for (h = 0, p = (d = c.getElementsByTagName("source")).length; p > h; h++)(b || f.test(m = s(d[h]))) && (o(m, u, d[h]), x = !0);
        b || f.test(i) ? (o(i, u, e), x = !0) : x && ((C = []).srcset = [], C.isPicture = !0, Object.defineProperty(e, "_lazyrias", {
          value: C,
          writable: !0
        })), x && (_ ? e.removeAttribute(a.srcAttr) : "auto" != v && (y = {
          width: parseInt(v, 10)
        }, w({
          target: e,
          detail: y
        })))
      }
    }, !0);
    var w = function() {
      var i = function(t, e) {
          return t.w - e.w
        },
        o = function(t, e) {
          var i;
          return !t._lazyrias && n.pWS && (i = n.pWS(t.getAttribute(a.srcsetAttr || ""))).length && (Object.defineProperty(t, "_lazyrias", {
            value: i,
            writable: !0
          }), e && t.parentNode && (i.isPicture = "PICTURE" == t.parentNode.nodeName.toUpperCase())), t._lazyrias
        },
        r = function(e) {
          var i = t.devicePixelRatio || 1,
            o = n.getX && n.getX(e);
          return Math.min(o || i, 2.4, i)
        },
        s = function(e, n) {
          var s, a, l, u, c, d;
          if ((c = e._lazyrias).isPicture && t.matchMedia)
            for (a = 0, l = (s = e.parentNode.getElementsByTagName("source")).length; l > a; a++)
              if (o(s[a]) && !s[a].getAttribute("type") && (!(u = s[a].getAttribute("media")) || (matchMedia(u) || {}).matches)) {
                c = s[a]._lazyrias;
                break
              } return (!c.w || c.w < n) && (c.w = n, c.d = r(e), d = function(t) {
            for (var e, n, i = t.length, o = t[i - 1], r = 0; i > r; r++)
              if ((o = t[r]).d = o.w / t.w, o.d >= t.d) {
                !o.cached && (e = t[r - 1]) && e.d > t.d - .13 * Math.pow(t.d, 2.2) && (n = Math.pow(e.d - .6, 1.6), e.cached && (e.d += .15 * n), e.d + (o.d - t.d) * n > t.d && (o = e));
                break
              } return o
          }(c.sort(i))), d
        },
        l = function(i) {
          if (i.detail.instance == n) {
            var r, u = i.target;
            return !b && (t.respimage || t.picturefill || lazySizesConfig.pf) ? void e.removeEventListener("lazybeforesizes", l) : void(("_lazyrias" in u || i.detail.dataAttr && o(u, !0)) && (r = s(u, i.detail.width), r && r.u && u._lazyrias.cur != r.u && (u._lazyrias.cur = r.u, r.cached = !0, n.rAF(function() {
              u.setAttribute(a.srcAttr, r.u), u.setAttribute("src", r.u)
            }))))
          }
        };
      return _ ? l = function() {} : addEventListener("lazybeforesizes", l), l
    }()
  }),
  function(t, e) {
    var n = function(t, e) {
      "use strict";
      if (e.getElementsByClassName) {
        var n, i, o = e.documentElement,
          r = t.Date,
          s = t.HTMLPictureElement,
          a = "addEventListener",
          l = "getAttribute",
          u = t[a],
          c = t.setTimeout,
          d = t.requestAnimationFrame || c,
          f = t.requestIdleCallback,
          h = /^picture$/i,
          p = ["load", "error", "lazyincluded", "_lazyloaded"],
          m = {},
          g = Array.prototype.forEach,
          v = function(t, e) {
            return m[e] || (m[e] = new RegExp("(\\s|^)" + e + "(\\s|$)")), m[e].test(t[l]("class") || "") && m[e]
          },
          y = function(t, e) {
            v(t, e) || t.setAttribute("class", (t[l]("class") || "").trim() + " " + e)
          },
          b = function(t, e) {
            var n;
            (n = v(t, e)) && t.setAttribute("class", (t[l]("class") || "").replace(n, " "))
          },
          _ = function(t, e, n) {
            var i = n ? a : "removeEventListener";
            n && _(t, e), p.forEach(function(n) {
              t[i](n, e)
            })
          },
          w = function(t, i, o, r, s) {
            var a = e.createEvent("Event");
            return o || (o = {}), o.instance = n, a.initEvent(i, !r, !s), a.detail = o, t.dispatchEvent(a), a
          },
          x = function(e, n) {
            var o;
            !s && (o = t.picturefill || i.pf) ? (n && n.src && !e[l]("srcset") && e.setAttribute("srcset", n.src), o({
              reevaluate: !0,
              elements: [e]
            })) : n && n.src && (e.src = n.src)
          },
          C = function(t, e) {
            return (getComputedStyle(t, null) || {})[e]
          },
          E = function(t, e, n) {
            for (n = n || t.offsetWidth; n < i.minSize && e && !t._lazysizesWidth;) n = e.offsetWidth, e = e.parentNode;
            return n
          },
          T = function() {
            var t, n, i = [],
              o = [],
              r = i,
              s = function() {
                var e = r;
                for (r = i.length ? o : i, t = !0, n = !1; e.length;) e.shift()();
                t = !1
              },
              a = function(i, o) {
                t && !o ? i.apply(this, arguments) : (r.push(i), n || (n = !0, (e.hidden ? c : d)(s)))
              };
            return a._lsFlush = s, a
          }(),
          S = function(t, e) {
            return e ? function() {
              T(t)
            } : function() {
              var e = this,
                n = arguments;
              T(function() {
                t.apply(e, n)
              })
            }
          },
          k = function(t) {
            var e, n = 0,
              o = i.throttleDelay,
              s = i.ricTimeout,
              a = function() {
                e = !1, n = r.now(), t()
              },
              l = f && s > 49 ? function() {
                f(a, {
                  timeout: s
                }), s !== i.ricTimeout && (s = i.ricTimeout)
              } : S(function() {
                c(a)
              }, !0);
            return function(t) {
              var i;
              (t = !0 === t) && (s = 33), e || (e = !0, 0 > (i = o - (r.now() - n)) && (i = 0), t || 9 > i ? l() : c(l, i))
            }
          },
          I = function(t) {
            var e, n, i = function() {
                e = null, t()
              },
              o = function() {
                var t = r.now() - n;
                99 > t ? c(o, 99 - t) : (f || i)(i)
              };
            return function() {
              n = r.now(), e || (e = c(o, 99))
            }
          };
        ! function() {
          var e, n = {
            lazyClass: "lazyload",
            loadedClass: "lazyloaded",
            loadingClass: "lazyloading",
            preloadClass: "lazypreload",
            errorClass: "lazyerror",
            autosizesClass: "lazyautosizes",
            srcAttr: "data-src",
            srcsetAttr: "data-srcset",
            sizesAttr: "data-sizes",
            minSize: 40,
            customMedia: {},
            init: !0,
            expFactor: 1.5,
            hFac: .8,
            loadMode: 2,
            loadHidden: !0,
            ricTimeout: 0,
            throttleDelay: 125
          };
          for (e in i = t.lazySizesConfig || t.lazysizesConfig || {}, n) e in i || (i[e] = n[e]);
          t.lazySizesConfig = i, c(function() {
            i.init && L()
          })
        }();
        var A = function() {
            var s, d, f, p, m, E, A, L, O, P, N, j, z, M, $ = /^img$/i,
              R = /^iframe$/i,
              F = "onscroll" in t && !/(gle|ing)bot/.test(navigator.userAgent),
              B = 0,
              H = 0,
              W = -1,
              q = function(t) {
                H--, t && t.target && _(t.target, q), (!t || 0 > H || !t.target) && (H = 0)
              },
              V = function(t, n) {
                var i, r = t,
                  s = "hidden" == C(e.body, "visibility") || "hidden" != C(t.parentNode, "visibility") && "hidden" != C(t, "visibility");
                for (L -= n, N += n, O -= n, P += n; s && (r = r.offsetParent) && r != e.body && r != o;)(s = (C(r, "opacity") || 1) > 0) && "visible" != C(r, "overflow") && (i = r.getBoundingClientRect(), s = P > i.left && O < i.right && N > i.top - 1 && L < i.bottom + 1);
                return s
              },
              U = function() {
                var t, r, a, u, c, f, h, m, g, v = n.elements;
                if ((p = i.loadMode) && 8 > H && (t = v.length)) {
                  r = 0, W++, null == z && ("expand" in i || (i.expand = o.clientHeight > 500 && o.clientWidth > 500 ? 500 : 370), j = i.expand, z = j * i.expFactor), z > B && 1 > H && W > 2 && p > 2 && !e.hidden ? (B = z, W = 0) : B = p > 1 && W > 1 && 6 > H ? j : 0;
                  for (; t > r; r++)
                    if (v[r] && !v[r]._lazyRace)
                      if (F)
                        if ((m = v[r][l]("data-expand")) && (f = 1 * m) || (f = B), g !== f && (E = innerWidth + f * M, A = innerHeight + f, h = -1 * f, g = f), a = v[r].getBoundingClientRect(), (N = a.bottom) >= h && (L = a.top) <= A && (P = a.right) >= h * M && (O = a.left) <= E && (N || P || O || L) && (i.loadHidden || "hidden" != C(v[r], "visibility")) && (d && 3 > H && !m && (3 > p || 4 > W) || V(v[r], f))) {
                          if (J(v[r]), c = !0, H > 9) break
                        } else !c && d && !u && 4 > H && 4 > W && p > 2 && (s[0] || i.preloadAfterLoad) && (s[0] || !m && (N || P || O || L || "auto" != v[r][l](i.sizesAttr))) && (u = s[0] || v[r]);
                  else J(v[r]);
                  u && !c && J(u)
                }
              },
              Q = k(U),
              G = function(t) {
                y(t.target, i.loadedClass), b(t.target, i.loadingClass), _(t.target, Y), w(t.target, "lazyloaded")
              },
              K = S(G),
              Y = function(t) {
                K({
                  target: t.target
                })
              },
              Z = function(t) {
                var e, n = t[l](i.srcsetAttr);
                (e = i.customMedia[t[l]("data-media") || t[l]("media")]) && t.setAttribute("media", e), n && t.setAttribute("srcset", n)
              },
              X = S(function(t, e, n, o, r) {
                var s, a, u, d, p, m;
                (p = w(t, "lazybeforeunveil", e)).defaultPrevented || (o && (n ? y(t, i.autosizesClass) : t.setAttribute("sizes", o)), a = t[l](i.srcsetAttr), s = t[l](i.srcAttr), r && (u = t.parentNode, d = u && h.test(u.nodeName || "")), m = e.firesLoad || "src" in t && (a || s || d), p = {
                  target: t
                }, m && (_(t, q, !0), clearTimeout(f), f = c(q, 2500), y(t, i.loadingClass), _(t, Y, !0)), d && g.call(u.getElementsByTagName("source"), Z), a ? t.setAttribute("srcset", a) : s && !d && (R.test(t.nodeName) ? function(t, e) {
                  try {
                    t.contentWindow.location.replace(e)
                  } catch (n) {
                    t.src = e
                  }
                }(t, s) : t.src = s), r && (a || d) && x(t, {
                  src: s
                })), t._lazyRace && delete t._lazyRace, b(t, i.lazyClass), T(function() {
                  (!m || t.complete && t.naturalWidth > 1) && (m ? q(p) : H--, G(p))
                }, !0)
              }),
              J = function(t) {
                var e, n = $.test(t.nodeName),
                  o = n && (t[l](i.sizesAttr) || t[l]("sizes")),
                  r = "auto" == o;
                (!r && d || !n || !t[l]("src") && !t.srcset || t.complete || v(t, i.errorClass) || !v(t, i.lazyClass)) && (e = w(t, "lazyunveilread").detail, r && D.updateElem(t, !0, t.offsetWidth), t._lazyRace = !0, H++, X(t, e, r, o, n))
              },
              tt = function() {
                if (!d) {
                  if (r.now() - m < 999) return void c(tt, 999);
                  var t = I(function() {
                    i.loadMode = 3, Q()
                  });
                  d = !0, i.loadMode = 3, Q(), u("scroll", function() {
                    3 == i.loadMode && (i.loadMode = 2), t()
                  }, !0)
                }
              };
            return {
              _: function() {
                m = r.now(), n.elements = e.getElementsByClassName(i.lazyClass), s = e.getElementsByClassName(i.lazyClass + " " + i.preloadClass), M = i.hFac, u("scroll", Q, !0), u("resize", Q, !0), t.MutationObserver ? new MutationObserver(Q).observe(o, {
                  childList: !0,
                  subtree: !0,
                  attributes: !0
                }) : (o[a]("DOMNodeInserted", Q, !0), o[a]("DOMAttrModified", Q, !0), setInterval(Q, 999)), u("hashchange", Q, !0), ["focus", "mouseover", "click", "load", "transitionend", "animationend", "webkitAnimationEnd"].forEach(function(t) {
                  e[a](t, Q, !0)
                }), /d$|^c/.test(e.readyState) ? tt() : (u("load", tt), e[a]("DOMContentLoaded", Q), c(tt, 2e4)), n.elements.length ? (U(), T._lsFlush()) : Q()
              },
              checkElems: Q,
              unveil: J
            }
          }(),
          D = function() {
            var t, n = S(function(t, e, n, i) {
                var o, r, s;
                if (t._lazysizesWidth = i, i += "px", t.setAttribute("sizes", i), h.test(e.nodeName || ""))
                  for (o = e.getElementsByTagName("source"), r = 0, s = o.length; s > r; r++) o[r].setAttribute("sizes", i);
                n.detail.dataAttr || x(t, n.detail)
              }),
              o = function(t, e, i) {
                var o, r = t.parentNode;
                r && (i = E(t, r, i), (o = w(t, "lazybeforesizes", {
                  width: i,
                  dataAttr: !!e
                })).defaultPrevented || (i = o.detail.width) && i !== t._lazysizesWidth && n(t, r, o, i))
              },
              r = I(function() {
                var e, n = t.length;
                if (n)
                  for (e = 0; n > e; e++) o(t[e])
              });
            return {
              _: function() {
                t = e.getElementsByClassName(i.autosizesClass), u("resize", r)
              },
              checkElems: r,
              updateElem: o
            }
          }(),
          L = function() {
            L.i || (L.i = !0, D._(), A._())
          };
        return n = {
          cfg: i,
          autoSizer: D,
          loader: A,
          init: L,
          uP: x,
          aC: y,
          rC: b,
          hC: v,
          fire: w,
          gW: E,
          rAF: T
        }
      }
    }(t, t.document);
    t.lazySizes = n, "object" == typeof module && module.exports && (module.exports = n)
  }(window),
  function(t) {
    t.fn.instagramLite = function(e) {
      if (!this.length) return this;
      plugin = this, plugin.defaults = {
        accessToken: null,
        limit: null,
        list: !0,
        videos: !1,
        urls: !1,
        captions: !1,
        date: !1,
        likes: !1,
        comments_count: !1,
        error: function() {},
        success: function() {}
      };
      var n = t.extend({}, plugin.defaults, e),
        i = t(this),
        o = function(t) {
          for (var e = t.split(" "), n = "", i = 0; i < e.length; i++) {
            var o;
            if ("@" == e[i][0]) o = '<a href="http://twitter.com/' + e[i].replace("@", "").toLowerCase() + '" target="_blank">' + e[i] + "</a>";
            else if ("#" == e[i][0]) {
              o = '<a href="http://twitter.com/hashtag/' + e[i].replace("#", "").toLowerCase() + '" target="_blank">' + e[i] + "</a>"
            } else o = e[i];
            n += o + " "
          }
          return n
        };
      ! function() {
        if (n.accessToken) {
          var e = "https://api.instagram.com/v1/users/self/media/recent/?access_token=" + n.accessToken + "&count=" + n.limit;
          t.ajax({
            type: "GET",
            url: e,
            dataType: "jsonp",
            success: function(t) {
              200 === t.meta.code && t.data.length ? (function(t) {
                for (var e = 0; e < t.length; e++) {
                  var r, s, a = t[e];
                  if ("image" === a.type || !n.videos) {
                    if (r = '<div class="insta-img" style="background:url(' + a.images.standard_resolution.url + ')" alt="Instagram Image" data-filter="' + a.filter + '" />', n.urls && (r = '<a href="' + a.link + '" target="_blank">' + r + "</a>"), (n.captions || n.date || n.likes || n.comments_count) && (r += '<div class="il-photo__meta">'), n.captions && a.caption && (r += '<div class="il-photo__caption" data-caption-id="' + a.caption.id + '">' + o(a.caption.text) + "</div>"), n.date) {
                      var l = new Date(1e3 * a.created_time),
                        u = l.getDate(),
                        c = l.getMonth() + 1,
                        d = l.getFullYear();
                      l.getHours(), l.getMinutes(), l.getSeconds(), r += '<div class="il-photo__date">' + (l = c + "/" + u + "/" + d) + "</div>"
                    }
                    n.likes && (r += '<div class="il-photo__likes">' + a.likes.count + "</div>"), n.comments_count && a.comments && (r += '<div class="il-photo__comments">' + a.comments.count + "</div>"), (n.captions || n.date || n.likes || n.comments_count) && (r += "</div>")
                  }
                  "video" === a.type && n.videos && a.videos && (a.videos.standard_resolution ? s = a.videos.standard_resolution.url : a.videos.low_resolution ? s = a.videos.low_resolution.url : a.videos.low_bandwidth && (s = a.videos.low_bandwidth.url), r = '<video poster="' + a.images.standard_resolution.url + '" controls>', r += '<source src="' + s + '" type="video/mp4;"></source>', r += "</video>"), n.list && r && (r = '<li class="il-item" data-instagram-id="' + a.id + '">' + r + "</li>"), "" !== r && i.append(r)
                }
              }(t.data), n.success.call(this)) : n.error.call(this)
            },
            error: function() {
              n.error.call(this)
            }
          })
        }
      }()  
    }
  }(jQuery);