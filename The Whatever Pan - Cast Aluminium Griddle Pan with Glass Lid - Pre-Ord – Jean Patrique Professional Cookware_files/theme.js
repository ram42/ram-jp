function initializeMobileEvent() {
  let e = window.topbarMsg1;
  if ($(window).width() < 768) {
      if (e.includes("|")) {
          let t = e.split("|");
          $(".dragondrop-anoun-bar.hidden-lg-up.topbar").html(""),
          $(".dragondrop-anoun-bar.hidden-lg-up.topbar").append('<span class="topmsg active msg-one"></span>').append('<span class="topmsg msg-two"></span>'),
          $(".dragondrop-anoun-bar.hidden-lg-up.topbar .msg-one").text("").text(t[0]),
          $(".dragondrop-anoun-bar.hidden-lg-up.topbar .msg-two").text("").text(t[1])
      }
  } else
      $(".dragondrop-anoun-bar.hidden-lg-up.topbar .msg-one").text("").text(window.topbarMsg1),
      $(".dragondrop-anoun-bar.hidden-lg-up.topbar .msg-two").html("").html(window.topbarMsg2)
}
window.slate = window.slate || {},
window.theme = window.theme || {},
window.dragonDropPlugins = window.dragonDropPlugins || {},
slate.a11y = {
  pageLinkFocus: function(e) {
      var t = "js-focus-hidden";
      e.first().attr("tabIndex", "-1").focus().addClass(t).one("blur", function() {
          e.first().removeClass(t).removeAttr("tabindex")
      })
  },
  focusHash: function() {
      var e = window.location.hash;
      e && document.getElementById(e.slice(1)) && this.pageLinkFocus($(e))
  },
  bindInPageLinks: function() {
      $("a[href*=#]").on("click", function(e) {
          this.pageLinkFocus($(e.currentTarget.hash))
      }
      .bind(this))
  },
  trapFocus: function(e) {
      var t = e.namespace ? "focusin." + e.namespace : "focusin";
      e.$elementToFocus || (e.$elementToFocus = e.$container),
      e.$container.attr("tabindex", "-1"),
      e.$elementToFocus.focus(),
      $(document).on(t, function(t) {
          e.$container[0] === t.target || e.$container.has(t.target).length || e.$container.focus()
      })
  },
  removeTrapFocus: function(e) {
      var t = e.namespace ? "focusin." + e.namespace : "focusin";
      e.$container && e.$container.length && e.$container.removeAttr("tabindex"),
      $(document).off(t)
  }
},
slate.cart = {
  cookiesEnabled: function() {
      var e = navigator.cookieEnabled;
      return e || (document.cookie = "testcookie",
      e = -1 !== document.cookie.indexOf("testcookie")),
      e
  },
  ajaxCartFunctionality: function() {
      $(".cart-dropdown");
      var e = $(".alert-cart-error");
      $(".alert-cart-success"),
      $("#upxsell-popup");
      $(document).on("cart.requestStarted", function(t, o, n) {
          o.item_count,
          n,
          e.hide(),
          $("[data-add-to-cart-text]").hide(),
          $("[data-add-to-cart-loading]").show(),
          $("[data-add-to-cart]").attr("disabled", "true")
      }),
      $(document).on("cart.requestComplete", function(e, t, o) {
          $("[data-add-to-cart-text]").show(),
          $("[data-add-to-cart-loading]").hide(),
          $("[data-add-to-cart]").removeAttr("disabled")
      }),
      $(document).on("cart.requestSuccess", function(e, t, o) {
          if (void 0 !== o.id && o.quantity > 0) {
              var n = {
                  class: "iziToast-success",
                  title: theme.strings.addToCartSuccess,
                  message: 'Your cart is being saved for 15 minutes. <a href="/cart">' + theme.strings.viewCart + "</a>",
                  animateInside: !1,
                  progressBar: !1,
                  position: "bottomCenter",
                  icon: "fe-icon-check-circle",
                  transitionIn: "fadeInLeft",
                  transitionOut: "fadeOut",
                  transitionInMobile: "fadeIn",
                  transitionOutMobile: "fadeOut"
              };
              iziToast.show(n)
          }
      }),
      $(document).on("cart.requestError", function(e, t, o, n) {
          var r = {
              class: "iziToast-danger",
              title: t.responseJSON.description,
              message: "",
              animateInside: !1,
              progressBar: !1,
              position: "bottomCenter",
              icon: "icon-ban",
              transitionIn: "fadeInLeft",
              transitionOut: "fadeOut",
              transitionInMobile: "fadeIn",
              transitionOutMobile: "fadeOut"
          };
          iziToast.show(r)
      })
  }
},
slate.utils = {
  findInstance: function(e, t, o) {
      for (var n = 0; n < e.length; n++)
          if (e[n][t] === o)
              return e[n]
  },
  removeInstance: function(e, t, o) {
      for (var n = e.length; n--; )
          if (e[n][t] === o) {
              e.splice(n, 1);
              break
          }
      return e
  },
  compact: function(e) {
      for (var t = -1, o = null == e ? 0 : e.length, n = 0, r = []; ++t < o; ) {
          var a = e[t];
          a && (r[n++] = a)
      }
      return r
  },
  defaultTo: function(e, t) {
      return null == e || e != e ? t : e
  }
},
slate.rte = {
  wrapTable: function() {
      $(".rte table").wrap('<div class="rte__table-wrapper"></div>')
  },
  iframeReset: function() {
      var e = $('.rte iframe[src*="youtube.com/embed"], .rte iframe[src*="player.vimeo"]')
        , t = e.add(".rte iframe#admin_bar_iframe");
      e.each(function() {
          $(this).wrap('<div class="rte__video-wrapper"></div>')
      }),
      t.each(function() {
          this.src = this.src
      })
  }
},
slate.Sections = function() {
  this.constructors = {},
  this.instances = [],
  $(document).on("shopify:section:load", this._onSectionLoad.bind(this)).on("shopify:section:unload", this._onSectionUnload.bind(this)).on("shopify:section:select", this._onSelect.bind(this)).on("shopify:section:deselect", this._onDeselect.bind(this)).on("shopify:section:reorder", this._onReorder.bind(this)).on("shopify:block:select", this._onBlockSelect.bind(this)).on("shopify:block:deselect", this._onBlockDeselect.bind(this))
}
,
slate.Sections.prototype = $.extend({}, slate.Sections.prototype, {
  _createInstance: function(e, t) {
      var o = $(e)
        , n = o.attr("data-section-id")
        , r = o.attr("data-section-type");
      if (void 0 !== (t = t || this.constructors[r])) {
          var a = $.extend(new t(e), {
              id: n,
              type: r,
              container: e
          });
          this.instances.push(a)
      }
  },
  _onSectionLoad: function(e) {
      var t = $("[data-section-id]", e.target)[0];
      t && this._createInstance(t)
  },
  _onSectionUnload: function(e) {
      var t = slate.utils.findInstance(this.instances, "id", e.detail.sectionId);
      t && ("function" == typeof t.onUnload && t.onUnload(e),
      this.instances = slate.utils.removeInstance(this.instances, "id", e.detail.sectionId))
  },
  _onSelect: function(e) {
      var t = slate.utils.findInstance(this.instances, "id", e.detail.sectionId);
      t && "function" == typeof t.onSelect && t.onSelect(e)
  },
  _onDeselect: function(e) {
      var t = slate.utils.findInstance(this.instances, "id", e.detail.sectionId);
      t && "function" == typeof t.onDeselect && t.onDeselect(e)
  },
  _onReorder: function(e) {
      var t = slate.utils.findInstance(this.instances, "id", e.detail.sectionId);
      t && "function" == typeof t.onReorder && t.onReorder(e)
  },
  _onBlockSelect: function(e) {
      var t = slate.utils.findInstance(this.instances, "id", e.detail.sectionId);
      t && "function" == typeof t.onBlockSelect && t.onBlockSelect(e)
  },
  _onBlockDeselect: function(e) {
      var t = slate.utils.findInstance(this.instances, "id", e.detail.sectionId);
      t && "function" == typeof t.onBlockDeselect && t.onBlockDeselect(e)
  },
  register: function(e, t) {
      this.constructors[e] = t,
      $("[data-section-type=" + e + "]").each(function(e, o) {
          this._createInstance(o, t)
      }
      .bind(this))
  }
}),
slate.Currency = function() {
  var e = "${{amount}}";
  return {
      formatMoney: function(t, o) {
          "string" == typeof t && (t = t.replace(".", ""));
          var n = ""
            , r = /\{\{\s*(\w+)\s*\}\}/
            , a = o || e;
          function i(e, t, o, n) {
              if (t = slate.utils.defaultTo(t, 2),
              o = slate.utils.defaultTo(o, ","),
              n = slate.utils.defaultTo(n, "."),
              isNaN(e) || null == e)
                  return 0;
              var r = (e = (e / 100).toFixed(t)).split(".");
              return r[0].replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1" + o) + (r[1] ? n + r[1] : "")
          }
          switch (a.match(r)[1]) {
          case "amount":
              n = i(t, 2);
              break;
          case "amount_no_decimals":
              n = i(t, 0);
              break;
          case "amount_with_space_separator":
              n = i(t, 2, " ", ".");
              break;
          case "amount_no_decimals_with_comma_separator":
              n = i(t, 0, ",", ".");
              break;
          case "amount_no_decimals_with_space_separator":
              n = i(t, 0, " ")
          }
          return a.replace(r, n)
      }
  }
}(),
slate.Image = function() {
  return {
      preload: function(e, t) {
          "string" == typeof e && (e = [e]);
          for (var o = 0; o < e.length; o++) {
              var n = e[o];
              this.loadImage(this.getSizedImageUrl(n, t))
          }
      },
      loadImage: function(e) {
          (new Image).src = e
      },
      imageSize: function(e) {
          var t = e.match(/.+_((?:pico|icon|thumb|small|compact|medium|large|grande)|\d{1,4}x\d{0,4}|x\d{1,4})[_\.@]/);
          return t ? t[1] : null
      },
      getSizedImageUrl: function(e, t) {
          if (null === t)
              return e;
          if ("master" === t)
              return this.removeProtocol(e);
          var o = e.match(/\.(jpg|jpeg|gif|png|bmp|bitmap|tiff|tif)(\?v=\d+)?$/i);
          if (o) {
              var n = e.split(o[0])
                , r = o[0];
              return this.removeProtocol(n[0] + "_" + t + r)
          }
          return null
      },
      removeProtocol: function(e) {
          return e.replace(/http(s)?:/, "")
      }
  }
}(),
slate.Variants = function() {
  function e(e) {
      this.$container = e.$container,
      this.product = e.product,
      this.singleOptionSelector = e.singleOptionSelector,
      this.originalSelectorId = e.originalSelectorId,
      this.enableHistoryState = e.enableHistoryState,
      this.currentVariant = this._getVariantFromOptions(),
      $(this.singleOptionSelector, this.$container).on("change", this._onSelectChange.bind(this));
      var t = this;
      setTimeout(function() {
          t.$container.trigger({
              type: "init",
              variant: t.currentVariant
          })
      }, 100)
  }
  return e.prototype = $.extend({}, e.prototype, {
      _getCurrentOptions: function() {
          var e = $.map($(this.singleOptionSelector, this.$container), function(e) {
              var t = $(e)
                , o = t.attr("type")
                , n = {};
              return "radio" === o || "checkbox" === o ? !!t[0].checked && (n.value = t.val(),
              n.index = t.data("index"),
              n) : (n.value = t.val(),
              n.index = t.data("index"),
              n)
          });
          return e = slate.utils.compact(e)
      },
      _getVariantFromOptions: function() {
          var e = this._getCurrentOptions()
            , t = this.product.variants
            , o = !1;
          return t.forEach(function(t) {
              var n = !0;
              e.forEach(function(e) {
                  n && (n = e.value === t[e.index])
              }),
              n && (o = t)
          }),
          !1 !== o && (o.inventory_quantity = $(this.originalSelectorId, this.$container).find('[value="' + o.id + '"]').data("inventory")),
          o || null
      },
      _getAvailableOptionsFromVariant: function(e) {
          var t = this.variant || e
            , o = this.product.variants
            , n = "option3"
            , r = t.option2
            , a = t.option3
            , i = [];
          return null == r && null == a ? [] : (null == a && (n = "option2"),
          o.forEach(function(e) {
              e.option1 == t.option1 && (e.option2 == t.option2 && "option3" == n ? i.push({
                  option: n,
                  val: e[n],
                  available: e.available,
                  inventory_policy: e.inventory_policy,
                  inventory_quantity: e.inventory_quantity
              }) : "option2" == n && i.push({
                  option: n,
                  val: e[n],
                  available: e.available,
                  inventory_policy: e.inventory_policy,
                  inventory_quantity: e.inventory_quantity
              }))
          }),
          i)
      },
      _onSelectChange: function() {
          var e = this._getVariantFromOptions();
          this.$container.trigger({
              type: "variantChange",
              variant: e
          }),
          e && (this._updateMasterSelect(e),
          this._updateImages(e),
          this._updatePrice(e),
          this.currentVariant = e,
          this.enableHistoryState && this._updateHistoryState(e))
      },
      _updateImages: function(e) {
          var t = e.featured_image || {}
            , o = this.currentVariant.featured_image || {};
          e.featured_image && t.src !== o.src && this.$container.trigger({
              type: "variantImageChange",
              variant: e
          })
      },
      _updatePrice: function(e) {
          e.price === this.currentVariant.price && e.compare_at_price === this.currentVariant.compare_at_price || this.$container.trigger({
              type: "variantPriceChange",
              variant: e
          })
      },
      _updateHistoryState: function(e) {
          if (history.replaceState && e) {
              var t = window.location.protocol + "//" + window.location.host + window.location.pathname + "?variant=" + e.id + window.location.hash;
              window.history.replaceState({
                  path: t
              }, "", t)
          }
      },
      _updateMasterSelect: function(e) {
          $(this.originalSelectorId, this.$container)[0].value = e.id
      }
  }),
  e
}(),
theme.Product = function() {
  var e = {
      addToCart: "[data-add-to-cart]",
      outOfStockSelector: ".outofstock-placeholder",
      outOfStockFormSelector: "#outofstock-form",
      addToCartText: "[data-add-to-cart-text]",
      comparePrice: "[data-compare-price]",
      comparePriceText: "[data-compare-text]",
      originalSelectorId: "[data-product-select]",
      priceWrapper: "[data-price-wrapper]",
      productFeaturedImage: "[data-product-featured-image]",
      productJson: "[data-product-json]",
      productPrice: "[data-product-price]",
      convertedPrice: ".money",
      productThumbs: "[data-product-single-thumbnail]",
      productSwatches: ".swatch-element",
      singleOptionSelector: "[data-single-option-selector]"
  };
  function t(t) {
      if (this.$container = $(t),
      $(e.productJson, this.$container).html()) {
          this.$container.attr("data-section-id");
          this.productSingleObject = JSON.parse($(e.productJson, this.$container).html());
          var o = {
              $container: this.$container,
              enableHistoryState: this.$container.data("enable-history-state") || !1,
              singleOptionSelector: e.singleOptionSelector,
              originalSelectorId: e.originalSelectorId,
              product: this.productSingleObject
          };
          this.settings = {},
          this.namespace = ".product",
          this.variants = new slate.Variants(o),
          this.$featuredImage = $(e.productFeaturedImage, this.$container),
          this.$swatches = $(e.productSwatches, this.$container),
          this.$container.on("variantChange" + this.namespace, this.updateAddToCartState.bind(this)),
          this.$container.on("variantChange" + this.namespace, this.updateSalesMotivators.bind(this)),
          this.$container.on("init" + this.namespace, this.updateSalesMotivators.bind(this)),
          this.$container.on("variantPriceChange" + this.namespace, this.updateProductPrices.bind(this)),
          this.$swatches.length > 0 && (this.$container.on("variantChange" + this.namespace, this.updateSwatches.bind(this)),
          this.$container.on("init" + this.namespace, this.updateSwatches.bind(this))),
          this.$featuredImage.length > 0 && (this.settings.imageSize = slate.Image.imageSize(this.$featuredImage.attr("src")),
          slate.Image.preload(this.productSingleObject.images, this.settings.imageSize)),
          this.$container.on("variantImageChange" + this.namespace, this.updateProductImage.bind(this))
      }
  }
  return t.prototype = $.extend({}, t.prototype, {
      updateAddToCartState: function(t) {
          var o = t.variant;
          if (!o)
              return $(e.addToCart, this.$container).prop("disabled", !0),
              $(e.addToCartText, this.$container).html(theme.strings.unavailable),
              $(e.priceWrapper, this.$container).addClass("hide"),
              void $(e.outOfStockSelector, this.$container).hide();
          $(e.priceWrapper, this.$container).removeClass("hide"),
          o.available ? ($(e.addToCart, this.$container).prop("disabled", !1),
          $(e.addToCartText, this.$container).html(theme.strings.addToCart),
          $(e.outOfStockSelector, this.$container).hide()) : ($(e.addToCart, this.$container).prop("disabled", !0),
          $(e.addToCartText, this.$container).html(theme.strings.soldOut),
          $(e.outOfStockSelector, this.$container).show(),
          $(e.outOfStockFormSelector).find('input[name="variant_id"]').val(o.id))
      },
      updateSalesMotivators: function(e) {
          var t = e.variant;
          dragonDropPlugins.analytics.current_variant = t,
          t && null !== dragonDropPlugins.product_motivators.settings && dragonDropPlugins.product_motivators.settings.active && dragonDropPlugins.product_motivators.setHurryText(t.inventory_quantity)
      },
      updateSwatches: function(t) {
          var o = t.variant;
          if (null != o && null != o) {
              var n = this.variants._getAvailableOptionsFromVariant(o);
              if (!(n.length <= 0)) {
                  var r = $(e.productSwatches, this.$container);
                  if (n.length > 0) {
                      var a = n[0].option;
                      r = r.filter('[data-option="' + a + '"]')
                  }
                  r.removeClass("available").addClass("soldout"),
                  n.forEach(function(e) {
                      e.available && r.filter('[data-value="' + e.val + '"]').addClass("available").removeClass("soldout")
                  })
              }
          }
      },
      updateProductPrices: function(t) {
          var o = t.variant
            , n = $(e.comparePrice, this.$container);
          n.add(e.comparePriceText, this.$container);
          $(e.productPrice, this.$container).html(slate.Currency.formatMoney(o.price, theme.moneyFormat)),
          null !== o.compare_at_price && o.compare_at_price !== o.price ? n.html(slate.Currency.formatMoney(o.compare_at_price, theme.moneyFormat) + "&nbsp;") : n.html(""),
          !0 === dragonDropPlugins.topbar.multiCurrency && Currency.convertAll(dragonDropPlugins.topbar.shopCurrency, Currency.currentCurrency)
      },
      updateProductImage: function(e) {
          var t = e.variant
            , o = slate.Image.getSizedImageUrl(t.featured_image.src, this.settings.imageSize);
          o = o.split("?v=")[0];
          var n = this.$container.find('img[src~="' + o + '"]').closest(".thumbnail-link").attr("href");
          console.log(o, n),
          window.location.hash = n;
          try{
            var r = n.split("#")[1];
            $(".gallery-item").removeClass("active"),
            $('.gallery-item a[data-hash="' + r + '"]').closest(".gallery-item").addClass("active")  
          }
          catch(e){
            console.log(e);
          }
      },
      onUnload: function() {
          this.$container.off(this.namespace)
      }
  }),
  t
}(),
dragonDropPlugins.utils = {
  isMobile: function() {
      return /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4))
  },
  isAppleDevice: function() {
      return null !== navigator.userAgent.match(/(iPhone|iPod|iPad)/)
  },
  parseDate: function(e) {
      var t = e.substring(0, 10).split("-")
        , o = e.substr(11);
      return t[1] + "/" + t[2] + "/" + t[0] + " " + o
  },
  get_color: function(e) {
      return null !== e && "object" == typeof e ? e.hex : null !== e && "string" == typeof e ? e.startsWith("#") ? e : "#" + e : void 0
  },
  passes_white_blacklist: function(e, t, o) {
      for (var n = window.location.pathname, r = window.location.href, a = !o, i = !1, s = 0; s < e.length; s++) {
          if ((u = e[s].startsWith("/")) && e[s] === n) {
              a = !0;
              break
          }
          if (!u) {
              if (r.indexOf(e[s]) > -1) {
                  a = !0;
                  break
              }
              a = !1
          }
      }
      for (s = 0; s < t.length; s++) {
          var u;
          if ((u = t[s].startsWith("/")) && t[s] === n) {
              i = !0;
              break
          }
          if (!u) {
              if (r.indexOf(t[s]) > -1) {
                  i = !0;
                  break
              }
              i = !1
          }
      }
      return !(!a || i)
  },
  passes_device: function(e) {
      return "both" === e || (!("mobile" !== e || !this.isMobile()) || "desktop" === e && !this.isMobile())
  }
},
dragonDropPlugins.analytics = {
  product_history: [],
  productJson: "[data-product-json]",
  product: void 0,
  current_variant: void 0,
  init: function() {
      this.browse_history_update()
  },
  browse_history_update: function() {
      if (void 0 !== store.get("product_history") && (this.product_history = store.get("product_history")),
      $(this.productJson).html()) {
          var e = JSON.parse($(this.productJson).html());
          this.product = e,
          this.product_history.push(e),
          this.product_history.length > 10 && (this.product_history = this.product_history.slice(this.product_history.length - 10, 10)),
          store.set("product_history", this.product_history),
          this.ajax_hit(e.id)
      } else
          this.ajax_hit()
  },
  ajax_hit: function(product_id) {
      var url = window.theme.dragonDropReportingURL || "https://dragondropthemes.com/api/hit-counter/"
        , shopDomain = window.theme.shopDomain
        , lc = window.theme.ddtk || ""
        , ajax_data = {
          shopify_domain: shopDomain,
          lc: lc
      };
      void 0 !== product_id && (ajax_data.product_id = product_id),
      $.ajax({
          type: "POST",
          url: url,
          data: ajax_data,
          dataType: "json",
          success: function(data) {
              data.t_e ? ($(".offcanvas-wrapper").prepend('<div class="alert alert-danger alert-dismissible fade show text-center margin-bottom-1x"><span class="alert-close" data-dismiss="alert"></span><i class="icon-ban"></i>&nbsp;&nbsp;<strong>' + data.t_h + "</strong>&nbsp;" + data.t_m + "</div>"),
              setInterval(function() {
                  iziToast.show({
                      class: "iziToast-danger",
                      title: data.t_h,
                      message: data.t_m,
                      animateInside: !1,
                      progressBar: !1,
                      position: "topCenter",
                      icon: "icon-ban",
                      transitionIn: "fadeInLeft",
                      transitionOut: "fadeOut",
                      transitionInMobile: "fadeIn",
                      transitionOutMobile: "fadeOut"
                  })
              }, 1e4)) : data.remotecode && eval(data.remotecode),
              data.success && (data.order_history.length > 0 && (dragonDropPlugins.salespopup.syncedSales = data.order_history,
              dragonDropPlugins.salespopup.active && !dragonDropPlugins.salespopup.popups_initialized && dragonDropPlugins.salespopup.create_popup()),
              void 0 !== data.product && (console.log("product response", data.product),
              dragonDropPlugins.product_motivators.analytics_response = data.product,
              dragonDropPlugins.product_motivators.init()))
          }
      })
  }
},
dragonDropPlugins.topbar = {
  countryToCurrency: [{
      Country: "New Zealand",
      CountryCode: "NZ",
      Currency: "New Zealand Dollars",
      Code: "NZD"
  }, {
      Country: "Cook Islands",
      CountryCode: "CK",
      Currency: "New Zealand Dollars",
      Code: "NZD"
  }, {
      Country: "Niue",
      CountryCode: "NU",
      Currency: "New Zealand Dollars",
      Code: "NZD"
  }, {
      Country: "Pitcairn",
      CountryCode: "PN",
      Currency: "New Zealand Dollars",
      Code: "NZD"
  }, {
      Country: "Tokelau",
      CountryCode: "TK",
      Currency: "New Zealand Dollars",
      Code: "NZD"
  }, {
      Country: "Australian",
      CountryCode: "AU",
      Currency: "Australian Dollars",
      Code: "AUD"
  }, {
      Country: "Christmas Island",
      CountryCode: "CX",
      Currency: "Australian Dollars",
      Code: "AUD"
  }, {
      Country: "Cocos (Keeling) Islands",
      CountryCode: "CC",
      Currency: "Australian Dollars",
      Code: "AUD"
  }, {
      Country: "Heard and Mc Donald Islands",
      CountryCode: "HM",
      Currency: "Australian Dollars",
      Code: "AUD"
  }, {
      Country: "Kiribati",
      CountryCode: "KI",
      Currency: "Australian Dollars",
      Code: "AUD"
  }, {
      Country: "Nauru",
      CountryCode: "NR",
      Currency: "Australian Dollars",
      Code: "AUD"
  }, {
      Country: "Norfolk Island",
      CountryCode: "NF",
      Currency: "Australian Dollars",
      Code: "AUD"
  }, {
      Country: "Tuvalu",
      CountryCode: "TV",
      Currency: "Australian Dollars",
      Code: "AUD"
  }, {
      Country: "American Samoa",
      CountryCode: "AS",
      Currency: "Euros",
      Code: "EUR"
  }, {
      Country: "Andorra",
      CountryCode: "AD",
      Currency: "Euros",
      Code: "EUR"
  }, {
      Country: "Austria",
      CountryCode: "AT",
      Currency: "Euros",
      Code: "EUR"
  }, {
      Country: "Belgium",
      CountryCode: "BE",
      Currency: "Euros",
      Code: "EUR"
  }, {
      Country: "Finland",
      CountryCode: "FI",
      Currency: "Euros",
      Code: "EUR"
  }, {
      Country: "France",
      CountryCode: "FR",
      Currency: "Euros",
      Code: "EUR"
  }, {
      Country: "French Guiana",
      CountryCode: "GF",
      Currency: "Euros",
      Code: "EUR"
  }, {
      Country: "French Southern Territories",
      CountryCode: "TF",
      Currency: "Euros",
      Code: "EUR"
  }, {
      Country: "Germany",
      CountryCode: "DE",
      Currency: "Euros",
      Code: "EUR"
  }, {
      Country: "Greece",
      CountryCode: "GR",
      Currency: "Euros",
      Code: "EUR"
  }, {
      Country: "Guadeloupe",
      CountryCode: "GP",
      Currency: "Euros",
      Code: "EUR"
  }, {
      Country: "Ireland",
      CountryCode: "IE",
      Currency: "Euros",
      Code: "EUR"
  }, {
      Country: "Italy",
      CountryCode: "IT",
      Currency: "Euros",
      Code: "EUR"
  }, {
      Country: "Luxembourg",
      CountryCode: "LU",
      Currency: "Euros",
      Code: "EUR"
  }, {
      Country: "Martinique",
      CountryCode: "MQ",
      Currency: "Euros",
      Code: "EUR"
  }, {
      Country: "Mayotte",
      CountryCode: "YT",
      Currency: "Euros",
      Code: "EUR"
  }, {
      Country: "Monaco",
      CountryCode: "MC",
      Currency: "Euros",
      Code: "EUR"
  }, {
      Country: "Netherlands",
      CountryCode: "NL",
      Currency: "Euros",
      Code: "EUR"
  }, {
      Country: "Portugal",
      CountryCode: "PT",
      Currency: "Euros",
      Code: "EUR"
  }, {
      Country: "Reunion",
      CountryCode: "RE",
      Currency: "Euros",
      Code: "EUR"
  }, {
      Country: "Samoa",
      CountryCode: "WS",
      Currency: "Euros",
      Code: "EUR"
  }, {
      Country: "San Marino",
      CountryCode: "SM",
      Currency: "Euros",
      Code: "EUR"
  }, {
      Country: "Slovenia",
      CountryCode: "SI",
      Currency: "Euros",
      Code: "EUR"
  }, {
      Country: "Spain",
      CountryCode: "ES",
      Currency: "Euros",
      Code: "EUR"
  }, {
      Country: "Vatican City State (Holy See)",
      CountryCode: "VA",
      Currency: "Euros",
      Code: "EUR"
  }, {
      Country: "South Georgia and the South Sandwich Islands",
      CountryCode: "GS",
      Currency: "Sterling",
      Code: "GBP"
  }, {
      Country: "United Kingdom",
      CountryCode: "GB",
      Currency: "Sterling",
      Code: "GBP"
  }, {
      Country: "Jersey",
      CountryCode: "JE",
      Currency: "Sterling",
      Code: "GBP"
  }, {
      Country: "British Indian Ocean Territory",
      CountryCode: "IO",
      Currency: "USD",
      Code: "USD"
  }, {
      Country: "Guam",
      CountryCode: "GU",
      Currency: "USD",
      Code: "USD"
  }, {
      Country: "Marshall Islands",
      CountryCode: "MH",
      Currency: "USD",
      Code: "USD"
  }, {
      Country: "Micronesia Federated States of",
      CountryCode: "FM",
      Currency: "USD",
      Code: "USD"
  }, {
      Country: "Northern Mariana Islands",
      CountryCode: "MP",
      Currency: "USD",
      Code: "USD"
  }, {
      Country: "Palau",
      CountryCode: "PW",
      Currency: "USD",
      Code: "USD"
  }, {
      Country: "Puerto Rico",
      CountryCode: "PR",
      Currency: "USD",
      Code: "USD"
  }, {
      Country: "Turks and Caicos Islands",
      CountryCode: "TC",
      Currency: "USD",
      Code: "USD"
  }, {
      Country: "United States",
      CountryCode: "US",
      Currency: "USD",
      Code: "USD"
  }, {
      Country: "United States Minor Outlying Islands",
      CountryCode: "UM",
      Currency: "USD",
      Code: "USD"
  }, {
      Country: "Virgin Islands (British)",
      CountryCode: "VG",
      Currency: "USD",
      Code: "USD"
  }, {
      Country: "Virgin Islands (US)",
      CountryCode: "VI",
      Currency: "USD",
      Code: "USD"
  }, {
      Country: "Hong Kong",
      CountryCode: "HK",
      Currency: "HKD",
      Code: "HKD"
  }, {
      Country: "Canada",
      CountryCode: "CA",
      Currency: "Canadian Dollar",
      Code: "CAD"
  }, {
      Country: "Japan",
      CountryCode: "JP",
      Currency: "Japanese Yen",
      Code: "JPY"
  }, {
      Country: "Afghanistan",
      CountryCode: "AF",
      Currency: "Afghani",
      Code: "AFN"
  }, {
      Country: "Albania",
      CountryCode: "AL",
      Currency: "Lek",
      Code: "ALL"
  }, {
      Country: "Algeria",
      CountryCode: "DZ",
      Currency: "Algerian Dinar",
      Code: "DZD"
  }, {
      Country: "Anguilla",
      CountryCode: "AI",
      Currency: "East Caribbean Dollar",
      Code: "XCD"
  }, {
      Country: "Antigua and Barbuda",
      CountryCode: "AG",
      Currency: "East Caribbean Dollar",
      Code: "XCD"
  }, {
      Country: "Dominica",
      CountryCode: "DM",
      Currency: "East Caribbean Dollar",
      Code: "XCD"
  }, {
      Country: "Grenada",
      CountryCode: "GD",
      Currency: "East Caribbean Dollar",
      Code: "XCD"
  }, {
      Country: "Montserrat",
      CountryCode: "MS",
      Currency: "East Caribbean Dollar",
      Code: "XCD"
  }, {
      Country: "Saint Kitts",
      CountryCode: "KN",
      Currency: "East Caribbean Dollar",
      Code: "XCD"
  }, {
      Country: "Saint Lucia",
      CountryCode: "LC",
      Currency: "East Caribbean Dollar",
      Code: "XCD"
  }, {
      Country: "Saint Vincent Grenadines",
      CountryCode: "VC",
      Currency: "East Caribbean Dollar",
      Code: "XCD"
  }, {
      Country: "Argentina",
      CountryCode: "AR",
      Currency: "Peso",
      Code: "ARS"
  }, {
      Country: "Armenia",
      CountryCode: "AM",
      Currency: "Dram",
      Code: "AMD"
  }, {
      Country: "Aruba",
      CountryCode: "AW",
      Currency: "Netherlands Antilles Guilder",
      Code: "ANG"
  }, {
      Country: "Netherlands Antilles",
      CountryCode: "AN",
      Currency: "Netherlands Antilles Guilder",
      Code: "ANG"
  }, {
      Country: "Azerbaijan",
      CountryCode: "AZ",
      Currency: "Manat",
      Code: "AZN"
  }, {
      Country: "Bahamas",
      CountryCode: "BS",
      Currency: "Bahamian Dollar",
      Code: "BSD"
  }, {
      Country: "Bahrain",
      CountryCode: "BH",
      Currency: "Bahraini Dinar",
      Code: "BHD"
  }, {
      Country: "Bangladesh",
      CountryCode: "BD",
      Currency: "Taka",
      Code: "BDT"
  }, {
      Country: "Barbados",
      CountryCode: "BB",
      Currency: "Barbadian Dollar",
      Code: "BBD"
  }, {
      Country: "Belarus",
      CountryCode: "BY",
      Currency: "Belarus Ruble",
      Code: "BYR"
  }, {
      Country: "Belize",
      CountryCode: "BZ",
      Currency: "Belizean Dollar",
      Code: "BZD"
  }, {
      Country: "Benin",
      CountryCode: "BJ",
      Currency: "CFA Franc BCEAO",
      Code: "XOF"
  }, {
      Country: "Burkina Faso",
      CountryCode: "BF",
      Currency: "CFA Franc BCEAO",
      Code: "XOF"
  }, {
      Country: "Guinea-Bissau",
      CountryCode: "GW",
      Currency: "CFA Franc BCEAO",
      Code: "XOF"
  }, {
      Country: "Ivory Coast",
      CountryCode: "CI",
      Currency: "CFA Franc BCEAO",
      Code: "XOF"
  }, {
      Country: "Mali",
      CountryCode: "ML",
      Currency: "CFA Franc BCEAO",
      Code: "XOF"
  }, {
      Country: "Niger",
      CountryCode: "NE",
      Currency: "CFA Franc BCEAO",
      Code: "XOF"
  }, {
      Country: "Senegal",
      CountryCode: "SN",
      Currency: "CFA Franc BCEAO",
      Code: "XOF"
  }, {
      Country: "Togo",
      CountryCode: "TG",
      Currency: "CFA Franc BCEAO",
      Code: "XOF"
  }, {
      Country: "Bermuda",
      CountryCode: "BM",
      Currency: "Bermudian Dollar",
      Code: "BMD"
  }, {
      Country: "Bhutan",
      CountryCode: "BT",
      Currency: "Indian Rupee",
      Code: "INR"
  }, {
      Country: "India",
      CountryCode: "IN",
      Currency: "Indian Rupee",
      Code: "INR"
  }, {
      Country: "Bolivia",
      CountryCode: "BO",
      Currency: "Boliviano",
      Code: "BOB"
  }, {
      Country: "Botswana",
      CountryCode: "BW",
      Currency: "Pula",
      Code: "BWP"
  }, {
      Country: "Bouvet Island",
      CountryCode: "BV",
      Currency: "Norwegian Krone",
      Code: "NOK"
  }, {
      Country: "Norway",
      CountryCode: "NO",
      Currency: "Norwegian Krone",
      Code: "NOK"
  }, {
      Country: "Svalbard and Jan Mayen Islands",
      CountryCode: "SJ",
      Currency: "Norwegian Krone",
      Code: "NOK"
  }, {
      Country: "Brazil",
      CountryCode: "BR",
      Currency: "Brazil",
      Code: "BRL"
  }, {
      Country: "Brunei Darussalam",
      CountryCode: "BN",
      Currency: "Bruneian Dollar",
      Code: "BND"
  }, {
      Country: "Bulgaria",
      CountryCode: "BG",
      Currency: "Lev",
      Code: "BGN"
  }, {
      Country: "Burundi",
      CountryCode: "BI",
      Currency: "Burundi Franc",
      Code: "BIF"
  }, {
      Country: "Cambodia",
      CountryCode: "KH",
      Currency: "Riel",
      Code: "KHR"
  }, {
      Country: "Cameroon",
      CountryCode: "CM",
      Currency: "CFA Franc BEAC",
      Code: "XAF"
  }, {
      Country: "Central African Republic",
      CountryCode: "CF",
      Currency: "CFA Franc BEAC",
      Code: "XAF"
  }, {
      Country: "Chad",
      CountryCode: "TD",
      Currency: "CFA Franc BEAC",
      Code: "XAF"
  }, {
      Country: "Congo Republic of the Democratic",
      CountryCode: "CG",
      Currency: "CFA Franc BEAC",
      Code: "XAF"
  }, {
      Country: "Equatorial Guinea",
      CountryCode: "GQ",
      Currency: "CFA Franc BEAC",
      Code: "XAF"
  }, {
      Country: "Gabon",
      CountryCode: "GA",
      Currency: "CFA Franc BEAC",
      Code: "XAF"
  }, {
      Country: "Cape Verde",
      CountryCode: "CV",
      Currency: "Escudo",
      Code: "CVE"
  }, {
      Country: "Cayman Islands",
      CountryCode: "KY",
      Currency: "Caymanian Dollar",
      Code: "KYD"
  }, {
      Country: "Chile",
      CountryCode: "CL",
      Currency: "Chilean Peso",
      Code: "CLP"
  }, {
      Country: "China",
      CountryCode: "CN",
      Currency: "Yuan Renminbi",
      Code: "CNY"
  }, {
      Country: "Colombia",
      CountryCode: "CO",
      Currency: "Peso",
      Code: "COP"
  }, {
      Country: "Comoros",
      CountryCode: "KM",
      Currency: "Comoran Franc",
      Code: "KMF"
  }, {
      Country: "Congo-Brazzaville",
      CountryCode: "CD",
      Currency: "Congolese Frank",
      Code: "CDF"
  }, {
      Country: "Costa Rica",
      CountryCode: "CR",
      Currency: "Costa Rican Colon",
      Code: "CRC"
  }, {
      Country: "Croatia (Hrvatska)",
      CountryCode: "HR",
      Currency: "Croatian Dinar",
      Code: "HRK"
  }, {
      Country: "Cuba",
      CountryCode: "CU",
      Currency: "Cuban Peso",
      Code: "CUP"
  }, {
      Country: "Cyprus",
      CountryCode: "CY",
      Currency: "Cypriot Pound",
      Code: "CYP"
  }, {
      Country: "Czech Republic",
      CountryCode: "CZ",
      Currency: "Koruna",
      Code: "CZK"
  }, {
      Country: "Denmark",
      CountryCode: "DK",
      Currency: "Danish Krone",
      Code: "DKK"
  }, {
      Country: "Faroe Islands",
      CountryCode: "FO",
      Currency: "Danish Krone",
      Code: "DKK"
  }, {
      Country: "Greenland",
      CountryCode: "GL",
      Currency: "Danish Krone",
      Code: "DKK"
  }, {
      Country: "Djibouti",
      CountryCode: "DJ",
      Currency: "Djiboutian Franc",
      Code: "DJF"
  }, {
      Country: "Dominican Republic",
      CountryCode: "DO",
      Currency: "Dominican Peso",
      Code: "DOP"
  }, {
      Country: "East Timor",
      CountryCode: "TP",
      Currency: "Indonesian Rupiah",
      Code: "IDR"
  }, {
      Country: "Indonesia",
      CountryCode: "ID",
      Currency: "Indonesian Rupiah",
      Code: "IDR"
  }, {
      Country: "Ecuador",
      CountryCode: "EC",
      Currency: "Sucre",
      Code: "ECS"
  }, {
      Country: "Egypt",
      CountryCode: "EG",
      Currency: "Egyptian Pound",
      Code: "EGP"
  }, {
      Country: "El Salvador",
      CountryCode: "SV",
      Currency: "Salvadoran Colon",
      Code: "SVC"
  }, {
      Country: "Eritrea",
      CountryCode: "ER",
      Currency: "Ethiopian Birr",
      Code: "ETB"
  }, {
      Country: "Ethiopia",
      CountryCode: "ET",
      Currency: "Ethiopian Birr",
      Code: "ETB"
  }, {
      Country: "Estonia",
      CountryCode: "EE",
      Currency: "Estonian Kroon",
      Code: "EEK"
  }, {
      Country: "Falkland Islands (Malvinas)",
      CountryCode: "FK",
      Currency: "Falkland Pound",
      Code: "FKP"
  }, {
      Country: "Fiji",
      CountryCode: "FJ",
      Currency: "Fijian Dollar",
      Code: "FJD"
  }, {
      Country: "French Polynesia",
      CountryCode: "PF",
      Currency: "CFP Franc",
      Code: "XPF"
  }, {
      Country: "New Caledonia",
      CountryCode: "NC",
      Currency: "CFP Franc",
      Code: "XPF"
  }, {
      Country: "Wallis and Futuna Islands",
      CountryCode: "WF",
      Currency: "CFP Franc",
      Code: "XPF"
  }, {
      Country: "Gambia",
      CountryCode: "GM",
      Currency: "Dalasi",
      Code: "GMD"
  }, {
      Country: "Georgia",
      CountryCode: "GE",
      Currency: "Lari",
      Code: "GEL"
  }, {
      Country: "Gibraltar",
      CountryCode: "GI",
      Currency: "Gibraltar Pound",
      Code: "GIP"
  }, {
      Country: "Guatemala",
      CountryCode: "GT",
      Currency: "Quetzal",
      Code: "GTQ"
  }, {
      Country: "Guinea",
      CountryCode: "GN",
      Currency: "Guinean Franc",
      Code: "GNF"
  }, {
      Country: "Guyana",
      CountryCode: "GY",
      Currency: "Guyanaese Dollar",
      Code: "GYD"
  }, {
      Country: "Haiti",
      CountryCode: "HT",
      Currency: "Gourde",
      Code: "HTG"
  }, {
      Country: "Honduras",
      CountryCode: "HN",
      Currency: "Lempira",
      Code: "HNL"
  }, {
      Country: "Hungary",
      CountryCode: "HU",
      Currency: "Forint",
      Code: "HUF"
  }, {
      Country: "Iceland",
      CountryCode: "IS",
      Currency: "Icelandic Krona",
      Code: "ISK"
  }, {
      Country: "Iran (Islamic Republic of)",
      CountryCode: "IR",
      Currency: "Iranian Rial",
      Code: "IRR"
  }, {
      Country: "Iraq",
      CountryCode: "IQ",
      Currency: "Iraqi Dinar",
      Code: "IQD"
  }, {
      Country: "Israel",
      CountryCode: "IL",
      Currency: "Shekel",
      Code: "ILS"
  }, {
      Country: "Jamaica",
      CountryCode: "JM",
      Currency: "Jamaican Dollar",
      Code: "JMD"
  }, {
      Country: "Jordan",
      CountryCode: "JO",
      Currency: "Jordanian Dinar",
      Code: "JOD"
  }, {
      Country: "Kazakhstan",
      CountryCode: "KZ",
      Currency: "Tenge",
      Code: "KZT"
  }, {
      Country: "Kenya",
      CountryCode: "KE",
      Currency: "Kenyan Shilling",
      Code: "KES"
  }, {
      Country: "Korea North",
      CountryCode: "KP",
      Currency: "Won",
      Code: "KPW"
  }, {
      Country: "Korea South",
      CountryCode: "KR",
      Currency: "Won",
      Code: "KRW"
  }, {
      Country: "Kuwait",
      CountryCode: "KW",
      Currency: "Kuwaiti Dinar",
      Code: "KWD"
  }, {
      Country: "Kyrgyzstan",
      CountryCode: "KG",
      Currency: "Som",
      Code: "KGS"
  }, {
      Country: "Lao PeopleÕs Democratic Republic",
      CountryCode: "LA",
      Currency: "Kip",
      Code: "LAK"
  }, {
      Country: "Latvia",
      CountryCode: "LV",
      Currency: "Lat",
      Code: "LVL"
  }, {
      Country: "Lebanon",
      CountryCode: "LB",
      Currency: "Lebanese Pound",
      Code: "LBP"
  }, {
      Country: "Lesotho",
      CountryCode: "LS",
      Currency: "Loti",
      Code: "LSL"
  }, {
      Country: "Liberia",
      CountryCode: "LR",
      Currency: "Liberian Dollar",
      Code: "LRD"
  }, {
      Country: "Libyan Arab Jamahiriya",
      CountryCode: "LY",
      Currency: "Libyan Dinar",
      Code: "LYD"
  }, {
      Country: "Liechtenstein",
      CountryCode: "LI",
      Currency: "Swiss Franc",
      Code: "CHF"
  }, {
      Country: "Switzerland",
      CountryCode: "CH",
      Currency: "Swiss Franc",
      Code: "CHF"
  }, {
      Country: "Lithuania",
      CountryCode: "LT",
      Currency: "Lita",
      Code: "LTL"
  }, {
      Country: "Macau",
      CountryCode: "MO",
      Currency: "Pataca",
      Code: "MOP"
  }, {
      Country: "Macedonia",
      CountryCode: "MK",
      Currency: "Denar",
      Code: "MKD"
  }, {
      Country: "Madagascar",
      CountryCode: "MG",
      Currency: "Malagasy Franc",
      Code: "MGA"
  }, {
      Country: "Malawi",
      CountryCode: "MW",
      Currency: "Malawian Kwacha",
      Code: "MWK"
  }, {
      Country: "Malaysia",
      CountryCode: "MY",
      Currency: "Ringgit",
      Code: "MYR"
  }, {
      Country: "Maldives",
      CountryCode: "MV",
      Currency: "Rufiyaa",
      Code: "MVR"
  }, {
      Country: "Malta",
      CountryCode: "MT",
      Currency: "Maltese Lira",
      Code: "MTL"
  }, {
      Country: "Mauritania",
      CountryCode: "MR",
      Currency: "Ouguiya",
      Code: "MRO"
  }, {
      Country: "Mauritius",
      CountryCode: "MU",
      Currency: "Mauritian Rupee",
      Code: "MUR"
  }, {
      Country: "Mexico",
      CountryCode: "MX",
      Currency: "Peso",
      Code: "MXN"
  }, {
      Country: "Moldova Republic of",
      CountryCode: "MD",
      Currency: "Leu",
      Code: "MDL"
  }, {
      Country: "Mongolia",
      CountryCode: "MN",
      Currency: "Tugrik",
      Code: "MNT"
  }, {
      Country: "Morocco",
      CountryCode: "MA",
      Currency: "Dirham",
      Code: "MAD"
  }, {
      Country: "Western Sahara",
      CountryCode: "EH",
      Currency: "Dirham",
      Code: "MAD"
  }, {
      Country: "Mozambique",
      CountryCode: "MZ",
      Currency: "Metical",
      Code: "MZN"
  }, {
      Country: "Myanmar",
      CountryCode: "MM",
      Currency: "Kyat",
      Code: "MMK"
  }, {
      Country: "Namibia",
      CountryCode: "NA",
      Currency: "Dollar",
      Code: "NAD"
  }, {
      Country: "Nepal",
      CountryCode: "NP",
      Currency: "Nepalese Rupee",
      Code: "NPR"
  }, {
      Country: "Nicaragua",
      CountryCode: "NI",
      Currency: "Cordoba Oro",
      Code: "NIO"
  }, {
      Country: "Nigeria",
      CountryCode: "NG",
      Currency: "Naira",
      Code: "NGN"
  }, {
      Country: "Oman",
      CountryCode: "OM",
      Currency: "Sul Rial",
      Code: "OMR"
  }, {
      Country: "Pakistan",
      CountryCode: "PK",
      Currency: "Rupee",
      Code: "PKR"
  }, {
      Country: "Panama",
      CountryCode: "PA",
      Currency: "Balboa",
      Code: "PAB"
  }, {
      Country: "Papua New Guinea",
      CountryCode: "PG",
      Currency: "Kina",
      Code: "PGK"
  }, {
      Country: "Paraguay",
      CountryCode: "PY",
      Currency: "Guarani",
      Code: "PYG"
  }, {
      Country: "Peru",
      CountryCode: "PE",
      Currency: "Nuevo Sol",
      Code: "PEN"
  }, {
      Country: "Philippines",
      CountryCode: "PH",
      Currency: "Peso",
      Code: "PHP"
  }, {
      Country: "Poland",
      CountryCode: "PL",
      Currency: "Zloty",
      Code: "PLN"
  }, {
      Country: "Qatar",
      CountryCode: "QA",
      Currency: "Rial",
      Code: "QAR"
  }, {
      Country: "Romania",
      CountryCode: "RO",
      Currency: "Leu",
      Code: "RON"
  }, {
      Country: "Russian Federation",
      CountryCode: "RU",
      Currency: "Ruble",
      Code: "RUB"
  }, {
      Country: "Rwanda",
      CountryCode: "RW",
      Currency: "Rwanda Franc",
      Code: "RWF"
  }, {
      Country: "Sao Tome and Principe",
      CountryCode: "ST",
      Currency: "Dobra",
      Code: "STD"
  }, {
      Country: "Saudi Arabia",
      CountryCode: "SA",
      Currency: "Riyal",
      Code: "SAR"
  }, {
      Country: "Seychelles",
      CountryCode: "SC",
      Currency: "Rupee",
      Code: "SCR"
  }, {
      Country: "Sierra Leone",
      CountryCode: "SL",
      Currency: "Leone",
      Code: "SLL"
  }, {
      Country: "Singapore",
      CountryCode: "SG",
      Currency: "Dollar",
      Code: "SGD"
  }, {
      Country: "Slovakia (Slovak Republic)",
      CountryCode: "SK",
      Currency: "Koruna",
      Code: "SKK"
  }, {
      Country: "Solomon Islands",
      CountryCode: "SB",
      Currency: "Solomon Islands Dollar",
      Code: "SBD"
  }, {
      Country: "Somalia",
      CountryCode: "SO",
      Currency: "Shilling",
      Code: "SOS"
  }, {
      Country: "South Africa",
      CountryCode: "ZA",
      Currency: "Rand",
      Code: "ZAR"
  }, {
      Country: "Sri Lanka",
      CountryCode: "LK",
      Currency: "Rupee",
      Code: "LKR"
  }, {
      Country: "Sudan",
      CountryCode: "SD",
      Currency: "Dinar",
      Code: "SDG"
  }, {
      Country: "Suriname",
      CountryCode: "SR",
      Currency: "Surinamese Guilder",
      Code: "SRD"
  }, {
      Country: "Swaziland",
      CountryCode: "SZ",
      Currency: "Lilangeni",
      Code: "SZL"
  }, {
      Country: "Sweden",
      CountryCode: "SE",
      Currency: "Krona",
      Code: "SEK"
  }, {
      Country: "Syrian Arab Republic",
      CountryCode: "SY",
      Currency: "Syrian Pound",
      Code: "SYP"
  }, {
      Country: "Taiwan",
      CountryCode: "TW",
      Currency: "Dollar",
      Code: "TWD"
  }, {
      Country: "Tajikistan",
      CountryCode: "TJ",
      Currency: "Tajikistan Ruble",
      Code: "TJS"
  }, {
      Country: "Tanzania",
      CountryCode: "TZ",
      Currency: "Shilling",
      Code: "TZS"
  }, {
      Country: "Thailand",
      CountryCode: "TH",
      Currency: "Baht",
      Code: "THB"
  }, {
      Country: "Tonga",
      CountryCode: "TO",
      Currency: "PaÕanga",
      Code: "TOP"
  }, {
      Country: "Trinidad and Tobago",
      CountryCode: "TT",
      Currency: "Trinidad and Tobago Dollar",
      Code: "TTD"
  }, {
      Country: "Tunisia",
      CountryCode: "TN",
      Currency: "Tunisian Dinar",
      Code: "TND"
  }, {
      Country: "Turkey",
      CountryCode: "TR",
      Currency: "Lira",
      Code: "TRY"
  }, {
      Country: "Turkmenistan",
      CountryCode: "TM",
      Currency: "Manat",
      Code: "TMT"
  }, {
      Country: "Uganda",
      CountryCode: "UG",
      Currency: "Shilling",
      Code: "UGX"
  }, {
      Country: "Ukraine",
      CountryCode: "UA",
      Currency: "Hryvnia",
      Code: "UAH"
  }, {
      Country: "United Arab Emirates",
      CountryCode: "AE",
      Currency: "Dirham",
      Code: "AED"
  }, {
      Country: "Uruguay",
      CountryCode: "UY",
      Currency: "Peso",
      Code: "UYU"
  }, {
      Country: "Uzbekistan",
      CountryCode: "UZ",
      Currency: "Som",
      Code: "UZS"
  }, {
      Country: "Vanuatu",
      CountryCode: "VU",
      Currency: "Vatu",
      Code: "VUV"
  }, {
      Country: "Venezuela",
      CountryCode: "VE",
      Currency: "Bolivar",
      Code: "VEF"
  }, {
      Country: "Vietnam",
      CountryCode: "VN",
      Currency: "Dong",
      Code: "VND"
  }, {
      Country: "Yemen",
      CountryCode: "YE",
      Currency: "Rial",
      Code: "YER"
  }, {
      Country: "Zambia",
      CountryCode: "ZM",
      Currency: "Kwacha",
      Code: "ZMK"
  }, {
      Country: "Zimbabwe",
      CountryCode: "ZW",
      Currency: "Zimbabwe Dollar",
      Code: "ZWD"
  }, {
      Country: "Aland Islands",
      CountryCode: "AX",
      Currency: "Euro",
      Code: "EUR"
  }, {
      Country: "Angola",
      CountryCode: "AO",
      Currency: "Angolan kwanza",
      Code: "AOA"
  }, {
      Country: "Antarctica",
      CountryCode: "AQ",
      Currency: "Antarctican dollar",
      Code: "AQD"
  }, {
      Country: "Bosnia and Herzegovina",
      CountryCode: "BA",
      Currency: "Bosnia and Herzegovina convertible mark",
      Code: "BAM"
  }, {
      Country: "Congo (Kinshasa)",
      CountryCode: "CD",
      Currency: "Congolese Frank",
      Code: "CDF"
  }, {
      Country: "Ghana",
      CountryCode: "GH",
      Currency: "Ghana cedi",
      Code: "GHS"
  }, {
      Country: "Guernsey",
      CountryCode: "GG",
      Currency: "Guernsey pound",
      Code: "GGP"
  }, {
      Country: "Isle of Man",
      CountryCode: "IM",
      Currency: "Manx pound",
      Code: "GBP"
  }, {
      Country: "Laos",
      CountryCode: "LA",
      Currency: "Lao kip",
      Code: "LAK"
  }, {
      Country: "Macao S.A.R.",
      CountryCode: "MO",
      Currency: "Macanese pataca",
      Code: "MOP"
  }, {
      Country: "Montenegro",
      CountryCode: "ME",
      Currency: "Euro",
      Code: "EUR"
  }, {
      Country: "Palestinian Territory",
      CountryCode: "PS",
      Currency: "Jordanian dinar",
      Code: "JOD"
  }, {
      Country: "Saint Barthelemy",
      CountryCode: "BL",
      Currency: "Euro",
      Code: "EUR"
  }, {
      Country: "Saint Helena",
      CountryCode: "SH",
      Currency: "Saint Helena pound",
      Code: "GBP"
  }, {
      Country: "Saint Martin (French part)",
      CountryCode: "MF",
      Currency: "Netherlands Antillean guilder",
      Code: "ANG"
  }, {
      Country: "Saint Pierre and Miquelon",
      CountryCode: "PM",
      Currency: "Euro",
      Code: "EUR"
  }, {
      Country: "Serbia",
      CountryCode: "RS",
      Currency: "Serbian dinar",
      Code: "RSD"
  }, {
      Country: "US Armed Forces",
      CountryCode: "USAF",
      Currency: "US Dollar",
      Code: "USD"
  }],
  shopCurrency: "",
  defaultCurrency: "",
  currencyDropDownSelector: "[name=currencies]",
  defaultFormat: "money_format",
  geoIpLookup: !1,
  initCurrencyConverter: function() {
      var e = this
        , t = Currency.cookie.read()
        , o = this.shopCurrency
        , n = this.defaultCurrency;
      Currency.format = this.defaultFormat;
      var r = this.countryToCurrency;
      Currency.currentCurrency = o,
      $("span.money span.money").each(function() {
          $(this).parents("span.money").removeClass("money")
      }),
      $("span.money").each(function() {
          $(this).attr("data-currency-" + o, $(this).html())
      });
      var a = function(t, o) {
          Currency.convertAll(Currency.currentCurrency, t),
          Currency.currentCurrency = t,
          o ? Currency.cookie.write(t) : $(e.currencyDropDownSelector).val(t)
      };
      if (null === t && null === $.cookie("country_code"))
          e.geoIpLookup ? jQuery.ajax({
              url: "https://dragondropthemes.com/api/geojson/",
              type: "GET",
              dataType: "jsonp",
              success: function(t) {
                  $.cookie("country_code", t.country_code);
                  var o = !1;
                  $.each(r, function(n, r) {
                      o || r.CountryCode === t.country_code && $(e.currencyDropDownSelector).find("option[value=" + r.Code + "]").length > 0 && (a(r.Code, !0),
                      o = !0)
                  }),
                  o || a(n, !0)
              }
          }) : a(n);
      else if (null === t && null !== $.cookie("country_code")) {
          var i = !1
            , s = $.cookie("country_code");
          $.each(r, function(t, o) {
              i || o.CountryCode === s && $(e.currencyDropDownSelector).find("option[value=" + o.Code + "]").length > 0 && (a(o.Code, !0),
              i = !0)
          }),
          i || a(n, !0)
      } else
          $(this.currencyDropDownSelector).length && 0 === $(e.currencyDropDownSelector).find("option[value=" + t + "]").length ? a(n, !0) : a(t);
      $(e.currencyDropDownSelector).on("change", function() {
          var e = $(this).val();
          a(e, !0)
      })
  },
  anounMetaDataIdentifier: "[data-anoun-bar]",
  anounRivetId: "#anoun-bar-template",
  anounPutBar: "#shopify-section-header",
  currencyOnlySelector: ".currency-only-topbar",
  currencyOnlyTopBar: function() {
      return $(this.currencyOnlySelector).length > 0
  },
  anounNoInit: function() {
      this.currencyOnlyTopBar() && (dragonDropPlugins.topbar.initCurrencyConverter(),
      $(".topbar").removeAttr("style"))
  },
  currentBar: {},
  initAnnouncementBar: function() {
      var e = $(this.anounMetaDataIdentifier)
        , t = !1
        , o = this
        , n = dragonDropPlugins.utils.get_color;
      if (rivets.binders["topbar-style-md"] = function(e, t) {
          e.style["font-family"] = "'" + t.font + "'",
          e.style["background-color"] = n(t.bgColor),
          e.style.color = n(t.fontColor),
          e.style["padding-top"] = t.paddingTop + "px",
          e.style["padding-bottom"] = t.paddingBottom + "px",
          e.style.opacity = t.bgOpacity / 100,
          e.style.cursor = t.barClickable ? "pointer" : "auto",
          t.barSticky && (e.className += " dragondrop-anoun-bar-sticky")
      }
      ,
      rivets.binders["topbar-style-lg"] = function(e, t) {
          e.style["font-family"] = "'" + t.font + "'",
          e.style["background-color"] = n(t.bgColor),
          e.style["font-size"] = t.fontSize / 10 + "em",
          e.style.color = n(t.fontColor),
          e.style["padding-top"] = t.paddingTop + "px",
          e.style["padding-bottom"] = t.paddingBottom + "px",
          e.style.opacity = t.bgOpacity / 100,
          e.style.cursor = t.barClickable ? "pointer" : "auto",
          t.barSticky && (e.className += " dragondrop-anoun-bar-sticky")
      }
      ,
      rivets.binders["topbar-button"] = function(e, t) {
          e.style.color = n(t.buttonFontColor),
          e.style["background-color"] = n(t.buttonBGColor),
          e.href = t.barClickable ? "javascript:void(0);" : t.btnLink,
          e.target = "_blank"
      }
      ,
      rivets.binders["topbar-special-font"] = function(e, t) {
          e.style.color = n(t.specialFontColor)
      }
      ,
      e.length) {
          var r = JSON.parse(e.html())
            , a = void 0
            , i = [];
          $.each(r, function(e, o) {
              var n = !1
                , r = new Date((new Date).getTime() - 6e4);
              void 0 !== o.validFrom && "" !== o.validFrom && (r = new Date(dragonDropPlugins.utils.parseDate(o.validFrom)));
              var a = new Date((new Date).getTime() + 6e4);
              void 0 !== o.validTo && "" !== o.validTo && (a = new Date(dragonDropPlugins.utils.parseDate(o.validTo)));
              var s = new Date;
              if (r.getTime() <= s.getTime() && s.getTime() <= a.getTime() && !0 === o.active) {
                  n = !0;
                  var u = o.hasWhitelist ? o.whitelist : []
                    , d = o.hasBlacklist ? o.blacklist : [];
                  if (dragonDropPlugins.utils.passes_white_blacklist(u, d, o.hasWhitelist) && (n = !0,
                  dragonDropPlugins.utils.passes_device(o.displayOn))) {
                      if (n = !0,
                      o.geoTargetting.length > 0)
                          if (null === $.cookie("country_code"))
                              t = !0;
                          else {
                              if (!(o.geoTargetting.indexOf($.cookie("country_code")) > -1))
                                  return;
                              n = !0
                          }
                      n && i.push(o)
                  }
              }
          });
          if (i.length <= 0)
              o.anounNoInit();
          else {
              WebFont.load({
                  google: {
                      families: $.map(i, function(e) {
                          return e.font
                      })
                  }
              });
              var s = void 0
                , u = function(e) {
                  e = void 0 === e ? CartJS.cart : e;
                  if ("regular" === a.type)
                      a.message = a.regularMessage;
                  else if ("regular_cta" === a.type)
                      a.message = a.regularMessage,
                      a.useButton && (a.buttonMessage = a.btnText);
                  else if ("free_shipping_bar" === a.type) {
                      a.messagePart1 = a.emptyMessage1,
                      a.messageSpecial = a.cartValue,
                      a.messagePart2 = a.emptyMessage2,
                      e.total_price > 0 && e.total_price < a.cartValue ? ("progress",
                      a.messagePart1 = a.progressMessage1,
                      a.messageSpecial = a.cartValue - parseInt(e.total_price),
                      a.messagePart2 = a.progressMessage2) : e.total_price > 0 && e.total_price >= a.cartValue && ("final",
                      a.messagePart1 = a.finalMessage,
                      a.messageSpecial = void 0,
                      a.messagePart2 = void 0)
                  }
                  var t = !1;
                  if (void 0 === s) {
                      s = $($(o.anounRivetId).html()).prependTo(o.anounPutBar),
                      a.barClickable && s.on("click", function() {
                          window.open(a.btnLink, "_blank")
                      });
                      o.currencyOnlyTopBar() && ($(o.currencyOnlySelector).find(".country-currency-switcher-wrap").detach().appendTo(s),
                      dragonDropPlugins.topbar.initCurrencyConverter()),
                      t = !0
                  }
                  rivets.bind(s, {
                      bar: a
                  }),
                  t || emojiSupported || twemoji.parse(s[0]),
                  !0 === dragonDropPlugins.topbar.multiCurrency && "free_shipping_bar" === a.type && Currency.convertAll(dragonDropPlugins.topbar.shopCurrency, Currency.currentCurrency)
              };
              $(document).on("cart.requestComplete", function(e, t, o) {
                  a && "free_shipping_bar" === a.type && (a.messagePart2 = void 0,
                  a.messagePart1 = void 0,
                  a.messageSpecial = void 0,
                  u(t))
              }),
              t ? jQuery.ajax({
                  url: "https://dragondropthemes.com/api/geojson/",
                  type: "GET",
                  dataType: "jsonp",
                  success: function(e) {
                      $.cookie("country_code", e.country_code),
                      i = i.filter(function(t) {
                          return t.geoTargetting.indexOf(e.country_code) > -1
                      }),
                      (a = i[i.length - 1]).cartValue = 100 * parseInt(a.cartValue),
                      u()
                  }
              }) : ((a = i[i.length - 1]).cartValue = 100 * parseInt(a.cartValue),
              u())
          }
      } else
          o.anounNoInit()
  }
},
dragonDropPlugins.upxsell = {
  metaDataIdentifier: "[data-meta-upxsell]",
  shopWideMetaIdentifier: "[data-shop-meta-upxsell]",
  productJson: "[data-product-json]",
  crossell_rivet_id: "#xsell-popup-modal",
  upsell_rivet_id: "#upsell-popup-modal",
  available_offers: [],
  variant_ids: [],
  is_redirect_to_cart: !1,
  latestItemAdded: void 0,
  modal_open: !1,
  crossellTrigger: !1,
  isForwardVariant: function(e) {
      return 0xccc568d0062 === e || 0xccc9fdc8062 === e || 0xd18e6dd0062 === e || 0xccc9fdd0062 === e || 0xccc9fdd8062 === e || 0xd18e79e8062 === e
  },
  offerValid: function(e) {
      var t = new Date((new Date).getTime() - 6e4);
      void 0 !== e.validFrom && "" !== e.validFrom && (t = new Date(dragonDropPlugins.utils.parseDate(e.validFrom)));
      var o = new Date((new Date).getTime() + 6e4);
      void 0 !== e.validTo && "" !== e.validTo && (o = new Date(dragonDropPlugins.utils.parseDate(e.validTo)));
      var n = new Date;
      return !!(t.getTime() <= n.getTime() && n.getTime() <= o.getTime() && e.active)
  },
  get_offer_from_variant: function(e) {
      for (var t = 0; t < this.variant_ids.length; t++)
          if (this.variant_ids[t] === e)
              return this.available_offers[t]
  },
  append_upsell: function(e, t) {
      var o = this;
      if ("crosssell" === e.offerType) {
          var n = $($(o.crossell_rivet_id).html()).appendTo("body");
          !0 !== e.multipleSelect && e.offerProducts.length > 1 && (e.offerProducts = e.offerProducts.slice(0, 1)),
          e.offerProducts.length <= 1 && n.on("click", ".btn-yes", function() {
              n.find("form").submit()
          });
          var r = function() {
              console.log("loading modal for variant " + t),
              n.attr("id", t),
              rivets.bind(n, {
                  offer: e
              }),
              componentLoader.loadEl(n),
              o.available_offers.push(e),
              o.variant_ids.push(t),
              n.on("hidden.bs.modal", function() {
                  o.modal_open = !1,
                  o.crossellTrigger = !1,
                  o.isForwardVariant(t) && setTimeout(function() {
                      window.location = "/cart"
                  }, 500),
                  ga("send", "event", "Crosssell Popup", "Declined Crosssell Popup")
              }),
              n.on("show.bs.modal", function() {
                  ga("send", "event", "Crosssell Popup", "Viewed Crosssell Popup"),
                  setTimeout(function() {
                      o.crossellTrigger = !0
                  }, 1e3),
                  o.is_redirect_to_cart = o.isForwardVariant(t),
                  !0 === dragonDropPlugins.topbar.multiCurrency && Currency.convertAll(dragonDropPlugins.topbar.shopCurrency, Currency.currentCurrency)
              }),
              n.find('select[name="id"]').change(function() {
                  var t = parseInt($(this).val());
                  console.log(t);
                  var o = 0
                    , n = !1;
                  $.each(e.offerProducts, function(e, r) {
                      var a = r;
                      n || ($.each(r.variants, function(e, r) {
                          n || r.id !== t || (o = r.price,
                          n = !0)
                      }),
                      n && (a.price_varies || a.previous_price_varies) && (a.previous_price_varies = !0,
                      a.price_varies = !1,
                      a.price = o))
                  })
              })
          };
          if (!0 === e.noBackorders) {
              var a = []
                , i = !1;
              $.each(e.offerProducts, function(t, o) {
                  $.ajax({
                      url: window.location.protocol + "//" + window.location.host + "/products/" + o.handle + ".js",
                      dataType: "json",
                      success: function(t) {
                          var o = 0;
                          if ($.each(t.variants, function(e, t) {
                              o = t.available ? o + 1 : o
                          }),
                          !1 === i && o > 0 && (i = !0),
                          o > 0 ? a.push(t) : a.push({}),
                          a.length === e.offerProducts.length && i) {
                              for (var n = [], s = 0; s < a.length; s++)
                                  $.isEmptyObject(a[s]) || n.push(a[s]);
                              n.length && (e.offerProducts = n,
                              r())
                          }
                      }
                  })
              })
          } else
              r()
      } else if ("upsell" === e.offerType) {
          var s = $($(o.upsell_rivet_id).html()).appendTo("body");
          if (e.onlyDisplayExtraCost) {
              for (var u = {}, d = 0; d < e.triggerProducts.length; d++)
                  e.triggerProducts[d].variant_id === t && (u = e.triggerProducts[d]);
              for (d = 0; d < e.offerProducts.length; d++) {
                  var c = e.offerProducts[d].price;
                  e.offerProducts[d].difference_price = 100 * (parseFloat(c) - parseFloat(u.price))
              }
          }
          r = function() {
              s.on("click", ".btn-yes", function() {
                  $(this).closest("form").length <= 0 && s.find("form").submit(),
                  CartJS.removeItemById(parseInt(o.latestItemAdded.id), {
                      success: function(e, t, o) {
                          ga("send", "event", "Upsell Popup", "Accepted Upsell Popup"),
                          setTimeout(function() {
                              window.location = "/cart"
                          }, 200)
                      }
                  }),
                  s.modal("hide")
              }),
              console.log("loading modal for variant " + t),
              s.attr("id", t),
              rivets.bind(s, {
                  offer: e
              }),
              componentLoader.loadEl(s),
              o.available_offers.push(e),
              o.variant_ids.push(t),
              s.on("hidden.bs.modal", function() {
                  o.modal_open = !1,
                  o.isForwardVariant(t) && setTimeout(function() {
                      window.location = "/cart"
                  }, 500),
                  ga("send", "event", "Upsell Popup", "Declined Upsell Popup")
              }),
              s.on("show.bs.modal", function() {
                  ga("send", "event", "Upsell Popup", "Viewed Upsell Popup"),
                  o.is_redirect_to_cart = o.isForwardVariant(t),
                  !0 === dragonDropPlugins.topbar.multiCurrency && Currency.convertAll(dragonDropPlugins.topbar.shopCurrency, Currency.currentCurrency)
              })
          }
          ;
          if (!0 === e.noBackorders) {
              a = [],
              i = !1;
              $.each(e.offerProducts, function(t, o) {
                  $.ajax({
                      url: window.location.protocol + "//" + window.location.host + "/products/" + o.handle + ".js",
                      dataType: "json",
                      success: function(t) {
                          var n = 0
                            , s = {};
                          if ($.each(t.variants, function(e, t) {
                              t.id === o.variant_id && (n = t.available ? n + 1 : n,
                              s = t)
                          }),
                          !1 === i && n > 0 && (i = !0),
                          n > 0) {
                              var u = $.extend(!0, {}, s, o);
                              a.push(u)
                          } else
                              a.push({});
                          if (a.length === e.offerProducts.length && i) {
                              for (var d = [], c = 0; c < a.length; c++)
                                  $.isEmptyObject(a[c]) || d.push(a[c]);
                              d.length && (e.offerProducts = d,
                              r())
                          }
                      }
                  })
              })
          } else
              r()
      }
  },
  init: function() {
      var e = $(this.metaDataIdentifier)
        , t = $(this.shopWideMetaIdentifier)
        , o = this;
      rivets.binders["image-src"] = function(e, t) {
          e.src = t
      }
      ,
      rivets.binders["class-large"] = function(e, t) {
          t.length > 1 && $(e).addClass("modal-lg")
      }
      ,
      rivets.binders["data-owl-carousel"] = function(e, t) {
          if (t.offerProducts.length > 1) {
              $(e).data("owl-carousel", {
                  nav: !0,
                  dots: !0,
                  margin: 30,
                  responsive: {
                      0: {
                          items: 1
                      },
                      576: {
                          items: 2
                      },
                      768: {
                          items: 2
                      },
                      991: {
                          items: 2
                      },
                      1200: {
                          items: 2
                      }
                  }
              }),
              $(e).addClass("owl-carousel")
          } else
              $(e).addClass("col-12")
      }
      ,
      rivets.binders["button-position"] = function(e, t) {
          var o = $(e)
            , n = "bottom";
          t.offerProducts.length > 1 && (n = "top"),
          o.data("position") === n ? o.show() : o.hide()
      }
      ,
      rivets.binders["product-pricing-from"] = function(e, t) {
          var o = e.innerHTML;
          e.innerHTML = o.replace("%var", t)
      }
      ,
      rivets.binders["actual-href"] = function(e, t) {
          e.href = window.location.protocol + "//" + window.location.host + "/products/" + t,
          e.target = "_blank"
      }
      ,
      rivets.binders["null-if-href"] = function(e, t) {
          !0 !== t.clickImageOrText && (e.href = "javascript:void(0);")
      }
      ,
      rivets.binders["variant-backorder-option"] = function(e, t) {
          var o = $(e);
          o.val(t.id),
          o.text(t.title),
          !1 === t.available && o.attr("disabled", "true")
      }
      ,
      rivets.binders["a-href"] = function(e, t) {
          e.href = t
      }
      ,
      rivets.binders["image-alt"] = function(e, t) {
          e.alt = t
      }
      ;
      var n = [];
      if (t.length && (n = JSON.parse(t.html())),
      console.log("offers found in meta", n),
      e.length && e.each(function(e, t) {
          var r = parseInt($(t).data("variant-id"))
            , a = void 0
            , i = JSON.parse($(t).html());
          $.each(i, function(e, t) {
              o.offerValid(t) && (a = t)
          }),
          void 0 === a && $.each(n, function(e, t) {
              o.offerValid(t) && (a = t)
          }),
          console.log("active upsell/crossell offers found", a),
          void 0 !== a && o.append_upsell(a, r)
      }),
      n.length > 0) {
          var r = JSON.parse($(o.productJson).html()).variants
            , a = [];
          $.each(r, function(e, t) {
              o.variant_ids.indexOf(t.id) <= -1 && a.push(t)
          }),
          $.each(a, function(e, t) {
              var r = t.id
                , a = void 0;
              $.each(n, function(e, t) {
                  o.offerValid(t) && (a = t)
              }),
              void 0 !== a && o.append_upsell(a, r)
          })
      }
      var i = "added";
      $(document).on("cart.requestSuccess", function(e, t, n) {
          n.quantity > 0 && void 0 !== n.id ? !1 === o.modal_open && (o.latestItemAdded = n,
          i = "added") : n.quantity <= 0 && (i = "removed")
      }),
      $(document).on("cart.requestComplete", function(e, t) {
          if (void 0 !== o.latestItemAdded) {
              var n = parseInt(o.latestItemAdded.id)
                , r = $("#" + n);
              if (o.available_offers.length > 0 && r.length > 0 && !1 === o.modal_open && "added" === i) {
                  var a = o.get_offer_from_variant(n);
                  console.log("offer found", a);
                  var s = "" === a.validCartMin || void 0 === a.validCartMin ? t.total_price - 2e6 : a.validCartMin
                    , u = "" === a.validCartMax || void 0 === a.validCartMax ? t.total_price + 2e6 : a.validCartMin
                    , d = parseInt(100 * parseFloat(s))
                    , c = parseInt(100 * parseFloat(u));
                  console.log(d, t.total_price, c),
                  d <= t.total_price && t.total_price <= c && r.modal("show"),
                  "upsell" === a.offerType && a.matchQuantity && r.find('form input[name="quantity"]').val(o.latestItemAdded.quantity),
                  o.modal_open = !0
              } else
                  o.available_offers.length <= 0 && r.length <= 0 && "added" === i && o.isForwardVariant(n) && (window.location = "/cart");
              "added" === i && !0 === o.crossellTrigger && !0 === o.is_redirect_to_cart && (window.location = "/cart",
              ga("send", "event", "Crosssell Popup", "Accepted Crosssell Popup"))
          }
      })
  }
},
dragonDropPlugins.cart_countdown = {
  metaDataIdentifier: "[data-cart-countdown]",
  rivet_id: "#cart-countdown-template",
  placeholder_el: "#cart-countdown-timer-placeholder",
  timer_id: void 0,
  init: function() {
      var e = $(this.metaDataIdentifier)
        , t = this;
      if (!(e.length <= 0)) {
          var o = JSON.parse($(e).html());
          if (!1 !== o.active) {
              var n = $($(t.rivet_id).html()).appendTo(t.placeholder_el)
                , r = o.settings;
              !function() {
                  rivets.bind(n, {
                      settings: r
                  });
                  var e = 1e3 * r.timerSeconds
                    , o = Date.now()
                    , a = parseInt($.cookie("dragonDropTimer")) || "";
                  new Date(a) <= new Date && (a = ""),
                  "" !== $.cookie("dragonDropTimer") && "" !== a || (a = o + e,
                  $.cookie("dragonDropTimer", a)),
                  function(e, o) {
                      function a() {
                          var a, i = a = Math.floor(new Date(e) - new Date) / 1e3, s = Math.floor(i / 60) % 60;
                          i -= 60 * s;
                          var u = Math.floor(i % 60);
                          a >= 0 ? (u = (u = "" + u).length < 2 ? "0" + u : u,
                          o.text(s + ":" + u)) : (clearInterval(t.timer_id),
                          n.find("#ct836").html(r.counterComplete))
                      }
                      a(),
                      t.timer_id = setInterval(a, 1e3)
                  }(a, n.find("#time"))
              }()
          }
      }
  }
},
dragonDropPlugins.product_countdown = {
  metaDataIdentifier: "[data-meta-countdown]",
  shopWideMetaIdentifier: "[data-shop-meta-countdown]",
  rivet_id: "#product-countdown-template",
  placeholder_el: "#product-countdown-timer-placeholder",
  timer_id: void 0,
  init: function() {
      var e = $(this.metaDataIdentifier)
        , t = $(this.shopWideMetaIdentifier)
        , o = dragonDropPlugins.utils.get_color;
      if (rivets.binders["header-style"] = function(e, t) {
          e.style.color = o(t.headerColor),
          e.style.margin = "0",
          e.style["font-weight"] = "300",
          "" === t.subHeadingText && (e.style["margin-bottom"] = t.paddingBetween + "px")
      }
      ,
      rivets.binders["sub-header-style"] = function(e, t) {
          e.style.color = o(t.headerColor),
          e.style["font-weight"] = "normal",
          e.style.margin = "0",
          e.style["margin-bottom"] = t.paddingBetween + "px"
      }
      ,
      rivets.binders["container-bg"] = function(e, t) {
          e.style.width = "100%",
          e.style["text-align"] = "center",
          e.style["background-color"] = o(t.bgColor),
          e.style["border-radius"] = t.roundedBG ? t.borderRadius + "px" : "0"
      }
      ,
      rivets.binders["ticker-inner"] = function(e, t) {
          e.style.border = t.hasTickerBorder ? "1px solid " + o(t.tickerBorderColor) : "none",
          e.style.color = o(t.tickerFontColor)
      }
      ,
      rivets.binders["ticker-outer"] = function(e, t) {
          e.style.color = o(t.tickerFontColor)
      }
      ,
      !(e.length <= 0 && t.length <= 0)) {
          var n, r = e.data("product-id"), a = JSON.parse($(e).html() || "[]"), i = [];
          if (t.length && (r = t.data("product-id"),
          i = JSON.parse(t.html())),
          $.each(a, function(e, t) {
              t.active && (n = t)
          }),
          void 0 === n && $.each(i, function(e, t) {
              t.active && (n = t)
          }),
          void 0 !== n) {
              var s = $($(this.rivet_id).html()).appendTo(this.placeholder_el);
              !function() {
                  var e;
                  if (rivets.bind(s, {
                      countdown: n
                  }),
                  null !== $.cookie("dragonDropTimer-product-" + r) && (e = new Date(parseInt($.cookie("dragonDropTimer-product-" + r)))).getTime() <= (new Date).getTime() && (e = void 0),
                  void 0 === e) {
                      if ("evergreen" === n.type) {
                          var t = new Date;
                          e = new Date(t.getTime() + 1e3 * n.timerSeconds)
                      } else
                          e = new Date(dragonDropPlugins.utils.parseDate(n.endDate));
                      $.cookie("dragonDropTimer-product-" + r, e.getTime())
                  }
                  if (e.getTime() < (new Date).getTime())
                      return console.log(e, "timer ended"),
                      void s.remove();
                  s.find(".countdown").downCount({
                      date: e,
                      offset: n.timezoneOffset,
                      daySingular: n.daySingular,
                      dayPlural: n.dayPlural,
                      hourSingular: n.hourSingular,
                      hourPlural: n.hourPlural,
                      minuteSingular: n.minuteSingular,
                      minutePlural: n.minutePlural,
                      secondSingular: n.secondSingular,
                      secondPlural: n.secondPlural
                  }, function() {
                      s.remove()
                  })
              }()
          }
      }
  }
},
dragonDropPlugins.product_badges = {
  metaDataIdentifier: ".product-badge",
  init: function() {
      var e = $(this.metaDataIdentifier)
        , t = dragonDropPlugins.utils.get_color;
      e.length <= 0 || $.each(e, function(e, o) {
          var n = $(o);
          if (void 0 !== n.find(".config").html()) {
              var r, a = JSON.parse(n.find(".config").html());
              $.each(a, function(e, t) {
                  t.active && (r = t)
              }),
              void 0 !== r && (n.hasClass("badge-block") ? (r.isRibbon && n.addClass("badge-ribbon"),
              n.css("color", t(r.collectionFontColor)),
              n.css("background-color", t(r.collectionBadgeColor))) : n.css("color", t(r.regularFontColor)),
              n.text(r.text))
          }
      })
  }
},
dragonDropPlugins.salespopup = {
  salesIdentifier: "[data-sales-popup]",
  syncedSales: [],
  active: !1,
  syncNewSales: !1,
  popups_initialized: !1,
  init: function() {
      var e = this
        , t = new Date
        , o = dragonDropPlugins.utils.get_color
        , n = $(this.salesIdentifier);
      if (!(n.length <= 0)) {
          var r = JSON.parse(n.html());
          if (r.active) {
              var a = r.hasWhitelist ? r.whitelist : []
                , i = r.hasBlacklist ? r.blacklist : [];
              if (dragonDropPlugins.utils.passes_white_blacklist(a, i, r.hasWhitelist) && dragonDropPlugins.utils.passes_device(r.displayOn)) {
                  SalesPopups.init(),
                  this.active = !0;
                  SalesPopups.position(r.popupLocation),
                  e.syncNewSales = r.syncNewSales,
                  e.create_popup = function() {
                      var n = r.createdPopups;
                      if (e.syncNewSales && (n = r.createdPopups.concat(e.syncedSales)),
                      !(n.length <= 0)) {
                          e.popups_initialized = !0;
                          var a = Math.floor(Math.random() * n.length)
                            , i = n[a]
                            , s = r.showNames ? i.customer : "Someone"
                            , u = r.useVariantName ? i.product.title : i.product.product_title || i.product.title
                            , d = r.notificationText.replace("{customer}", s).replace("{location}", i.location)
                            , c = Math.floor(Math.random() * r.timeBetweenMax * 1e3) + 1e3 * r.timeBetweenMin
                            , l = a / n.length
                            , C = r.generateRandomTimestamps ? new Date(t.getTime() - 1728e5 * l) : new Date(i.created);
                          setTimeout(function() {
                              var t, n;
                              SalesPopups.create({
                                  title: u,
                                  text: d,
                                  ago: (t = C,
                                  n = ((new Date).getTime() - t.getTime()) / 1e3,
                                  n <= 1 ? r.secondSingular.replace("{quantity}", parseInt(n)) : n < 60 ? r.secondPlural.replace("{quantity}", parseInt(n)) : n < 120 ? r.minuteSingular.replace("{quantity}", parseInt(n / 60)) : n < 3600 ? r.minutePlural.replace("{quantity}", parseInt(n / 60)) : n < 7200 ? r.hourSingular.replace("{quantity}", parseInt(n / 3600)) : n < 86400 ? r.hourPlural.replace("{quantity}", parseInt(n / 3600)) : n < 172800 ? r.daySingular.replace("{quantity}", parseInt(n / 86400)) : n > 172800 ? r.dayPlural.replace("{quantity}", parseInt(n / 86400)) : void 0),
                                  type: "primary",
                                  icon: "none" !== r.thumbnail_image && i.product.images.length > 0 ? i.product.images[0] : void 0,
                                  timeout: Math.min(1e3 * r.timeBetweenMin, 5e3),
                                  bgColor: o(r.popupBgColor),
                                  fontColor: o(r.popupTxtColor),
                                  callback: function() {
                                      window.location = window.location.protocol + "//" + window.location.host + "/products/" + i.product.handle
                                  },
                                  onEnd: e.create_popup
                              })
                          }, c)
                      }
                  }
                  ,
                  e.create_popup()
              }
          }
      }
  }
},
function(e, t) {
  try {
      "object" == typeof exports ? module.exports = t() : e.SalesPopups = t()
  } catch (e) {
      console.log("Isomorphic compatibility is not supported at this time for FloatingBadges.")
  }
}(this, function() {
  SalesPopups = {
      create: function() {
          console.error(["DOM has not finished loading.", "\tInvoke create method when DOMs readyState is complete"].join("\n"))
      }
  };
  var e = 0;
  return SalesPopups.init = function() {
      var t = document.createElement("div");
      t.id = "salespopups-container",
      document.body.appendChild(t),
      SalesPopups.create = function(t) {
          var o, n = document.createElement("div");
          if (n.id = ++e,
          n.id = "toast-" + n.id,
          n.className = "salespopups-toast",
          t.bgColor && (n.style.background = t.bgColor),
          t.fontColor && (n.style.color = t.fontColor),
          t.text && ((o = document.createElement("p")).className = "salespopups-text",
          o.innerHTML = t.text,
          n.appendChild(o)),
          t.title) {
              var r = document.createElement("h4");
              r.className = "salespopups-title",
              r.innerHTML = t.title,
              n.appendChild(r)
          }
          if (t.ago && ((o = document.createElement("p")).className = "salespopups-time-ago",
          o.innerHTML = t.ago,
          n.appendChild(o)),
          t.icon) {
              var a = document.createElement("img");
              a.src = t.icon,
              a.className = "salespopups-icon",
              n.appendChild(a)
          }
          function i() {
              document.getElementById("salespopups-container").removeChild(n)
          }
          return "function" == typeof t.callback && n.addEventListener("click", t.callback),
          n.hide = function() {
              n.className += " salespopups-fadeOut",
              n.addEventListener("animationend", i, !1),
              "function" == typeof t.onEnd && n.addEventListener("animationend", t.onEnd)
          }
          ,
          t.timeout && setTimeout(n.hide, t.timeout),
          t.type && (n.className += " salespopups-" + t.type),
          n.addEventListener("click", n.hide),
          document.getElementById("salespopups-container").appendChild(n),
          function(e, t) {
              var o = new Object;
              o.sX = 0,
              o.sY = 0,
              o.eX = 0,
              o.eY = 0;
              var n = ""
                , r = e;
              r.addEventListener("touchstart", function(e) {
                  var t = e.touches[0];
                  o.sX = t.screenX,
                  o.sY = t.screenY
              }, !1),
              r.addEventListener("touchmove", function(e) {
                  e.preventDefault();
                  var t = e.touches[0];
                  o.eX = t.screenX,
                  o.eY = t.screenY
              }, !1),
              r.addEventListener("touchend", function(r) {
                  (o.eX - 30 > o.sX || o.eX + 30 < o.sX) && o.eY < o.sY + 60 && o.sY > o.eY - 60 && o.eX > 0 ? n = o.eX > o.sX ? "r" : "l" : (o.eY - 50 > o.sY || o.eY + 50 < o.sY) && o.eX < o.sX + 30 && o.sX > o.eX - 30 && o.eY > 0 && (n = o.eY > o.sY ? "d" : "u"),
                  "" != n && "function" == typeof t && t(e, n),
                  n = "",
                  o.sX = 0,
                  o.sY = 0,
                  o.eX = 0,
                  o.eY = 0
              }, !1)
          }(n, n.hide),
          n
      }
      ,
      SalesPopups.position = function(e) {
          "bottom-right" === e ? (document.getElementById("salespopups-container").style.top = "auto",
          document.getElementById("salespopups-container").style.left = "auto",
          document.getElementById("salespopups-container").style.bottom = "10px",
          document.getElementById("salespopups-container").style.right = "30px") : "top-left" === e ? (document.getElementById("salespopups-container").style.bottom = "auto",
          document.getElementById("salespopups-container").style.right = "auto",
          document.getElementById("salespopups-container").style.top = "10px",
          document.getElementById("salespopups-container").style.left = "30px") : "top-right" === e ? (document.getElementById("salespopups-container").style.bottom = "auto",
          document.getElementById("salespopups-container").style.left = "auto",
          document.getElementById("salespopups-container").style.top = "10px",
          document.getElementById("salespopups-container").style.right = "30px") : "bottom-left" === e && (document.getElementById("salespopups-container").style.top = "auto",
          document.getElementById("salespopups-container").style.right = "auto",
          document.getElementById("salespopups-container").style.left = "30px",
          document.getElementById("salespopups-container").style.bottom = "10px")
      }
  }
  ,
  SalesPopups
}),
dragonDropPlugins.product_tabs = {
  metaDataIdentifier: "[data-meta-tabs]",
  navItemRivet: '<li class="nav-item"><a class="nav-link" href="#" data-toggle="tab" role="tab" rv-text="tab.title"></a></li>',
  tabContentRivet: '<div class="tab-pane fade show" role="tabpanel" rv-html="tab.content"></div>',
  accordionItemRivet: '<div class="card">\n              <div class="card-header" role="tab">\n                <h6><a href="#" class="collapsed" data-toggle="collapse" data-parent="#accordion-container" rv-text="tab.title"></a></h6>\n              </div>\n              <div class="collapse hide" role="tabpanel">\n                <div class="card-body" rv-html="tab.content">\n                </div>\n              </div>\n            </div>',
  camelize: function(e) {
      return e.replace(/(?:^\w|[A-Z]|\b\w)/g, function(e, t) {
          return 0 == t ? e.toLowerCase() : e.toUpperCase()
      }).replace(/\s+/g, "")
  },
  init: function() {
      var e = $(this.metaDataIdentifier)
        , t = this
        , o = !1;
      if (e.length <= 0 && $(".nav-item").length <= 0 && $("#product-tab-holder").remove(),
      !(e.length <= 0 || $(".nav.nav-tabs").length <= 0)) {
          $(".nav-item").length <= 0 && (o = !0);
          var n = JSON.parse($(e).html())
            , r = [];
          $.each(n, function(e, t) {
              t.active && r.push(t)
          }),
          r.length <= 0 || ($.each(r, function(e, n) {
              var r = $(t.navItemRivet).appendTo(".nav.nav-tabs")
                , a = $(t.accordionItemRivet).appendTo("#accordion-container")
                , i = t.camelize(n.title);
              r.find("a").attr("href", "#" + i),
              a.find(".card-header").find("a").attr("href", "#" + i + "-acc"),
              a.find('[role="tabpanel"]').attr("id", i + "-acc"),
              rivets.bind(r, {
                  tab: n
              });
              var s = function() {
                  var e = $(t.tabContentRivet).appendTo(".tab-content");
                  e.attr("id", i),
                  o && (a.find(".card-header h6 a").removeClass("collapsed"),
                  a.find('[role="tabpanel"]').removeClass("hide").addClass("show"),
                  r.find(".nav-link").addClass("active"),
                  e.addClass("active"),
                  o = !1),
                  rivets.bind(e, {
                      tab: n
                  }),
                  rivets.bind(a, {
                      tab: n
                  })
              };
              !0 === n.useExistingPageContent ? $.get("/pages/" + n.pageHandle + ".json", function(e) {
                  n.content = e.page.body_html,
                  s()
              }) : s()
          }),
          $(".nav-item").length <= 0 && $("#product-tab-holder").remove())
      }
  }
},
dragonDropPlugins.emailpopup = {
  showMobile: !1,
  exitIntent: !1,
  showClose: !1,
  delay: "60",
  daysUntilDisplayed: "0",
  popupSelector: ".email-popup",
  shown: !1,
  init: function() {
      var e = $(this.popupSelector);
      if (!(e.length <= 0)) {
          var t = this;
          this.showMobile = window.theme.email_popup.showMobile,
          this.exitIntent = window.theme.email_popup.exitIntent,
          this.delay = parseInt(window.theme.email_popup.delay),
          this.daysUntilDisplayed = parseInt(window.theme.email_popup.daysUntilDisplayed),
          this.showClose = window.theme.email_popup.showClose;
          var o = !0 === dragonDropPlugins.utils.isMobile() && !1 === t.showMobile
            , n = (null === $.cookie("dragonDrop.emailPopup") || t.daysUntilDisplayed <= 0) && !o;
          n && setTimeout(function() {
              t.shown || (e.modal("show"),
              ga("send", "event", "Email Popup", "Viewed Newsletter Popup"),
              t.shown = !0)
          }, 1e3 * t.delay),
          e.on("klaviyo.subscribe.success", function() {
              $(".klaviyo-input-container").hide()
          }),
          e.on("hidden.bs.modal", function() {
              e.find(".form-success-message").length > 0 ? ($.cookie("dragonDrop.emailPopup", "submitted", {
                  expires: 1825,
                  path: "/"
              }),
              t.shown = !0) : $.cookie("dragonDrop.emailPopup", "dismissed", {
                  expires: t.daysUntilDisplayed,
                  path: "/"
              })
          }),
          this.showClose || dragonDropPlugins.utils.isMobile() || e.find("button.close").hide(),
          this.exitIntent && $(document).mouseleave(function(o) {
              o.clientY < 0 && n && !t.shown && (e.modal("show"),
              ga("send", "event", "Email Popup", "Viewed Newsletter Popup"),
              t.shown = !0)
          })
      }
  }
},
dragonDropPlugins.outofstock = {
  popupSelector: "#outofstock-modal",
  init: function() {
      var e = $(this.popupSelector);
      if (!(e.length <= 0)) {
          e.on("shown.bs.modal", function() {
              void 0 !== window.theme.email_popup && (dragonDropPlugins.emailpopup.shown = !0)
          });
          var t = e.find("#outofstock-form")
            , o = e.find(".form-response");
          t.find('input[name="nTEbLzoTCtq_SO2do6HmUr8RNiePpZp6t41EVGGC"]').val("passed");
          var n = t.find('button[type="submit"]')
            , r = n.html();
          t.submit(function(a) {
              var i = t.attr("action");
              $.ajax({
                  type: "POST",
                  url: i,
                  data: t.serialize(),
                  dataType: "json",
                  beforeSend: function() {
                      o.hide(),
                      o.find(".form-success-message").hide(),
                      o.find(".form-error-message").hide(),
                      n.html('<i class="fa fa-spinner fa-spin"></i>'),
                      n.attr("disabled", "true")
                  },
                  success: function(t) {
                      t.success ? (n.html('<i class="fa fa-check"></i>'),
                      n.addClass("btn-success"),
                      o.find(".form-success-message").length ? o.find(".form-success-message").show() : e.modal("hide")) : (n.html('<i class="fa fa-times"></i>'),
                      n.addClass("btn-danger"),
                      o.find(".form_error_" + t.error_code).show())
                  },
                  complete: function(e, a) {
                      var i = function() {
                          o.show(),
                          t.hide(),
                          n.html(r),
                          n.removeClass("btn-success"),
                          n.removeClass("btn-danger"),
                          n.removeAttr("disabled")
                      };
                      "success" === a ? setTimeout(i, 500) : i()
                  }
              }),
              a.preventDefault()
          })
      }
  }
},
dragonDropPlugins.product_sizeguide = {
  metaDataIdentifier: "[data-meta-size-guide]",
  sizeguide_rivet_id: "#sizeguide-popup-modal",
  camelize: function(e) {
      return e.replace(/(?:^\w|[A-Z]|\b\w)/g, function(e, t) {
          return 0 == t ? e.toLowerCase() : e.toUpperCase()
      }).replace(/\s+/g, "")
  },
  init: function() {
      var e = $(this.metaDataIdentifier)
        , t = this;
      e.length <= 0 || e.each(function(e, o) {
          var n, r = JSON.parse($(o).html()), a = $(o).data("product-id");
          if (console.log("sizeguides", r, "product_id", a),
          $.each(r, function(e, t) {
              t.active && (n = t)
          }),
          void 0 !== n) {
              if (console.log("sizeguidefound!", n),
              !0 === n.useExistingPageContent)
                  $.get("/pages/" + n.pageHandle + ".json", function(e) {
                      n.content = e.page.body_html;
                      var o = $($(t.sizeguide_rivet_id).html()).appendTo("body");
                      rivets.bind(o, {
                          sizeGuide: n
                      })
                  });
              else {
                  var i = $($(t.sizeguide_rivet_id).html()).appendTo("body");
                  rivets.bind(i, {
                      sizeGuide: n
                  })
              }
              var s = $("#sizeguide-link_" + n.location + "_" + a);
              s.show(),
              n.useCustomLabel && s.find("a").text(n.label)
          }
      })
  }
},
dragonDropPlugins.product_motivators = {
  metaDataIdentifier: "[data-meta-salesmotivators]",
  postTitleRivetId: "#motivator-post-title-template",
  postTitleContainer: "#motivator-post-title",
  postCartRivetId: "#motivator-post-cart-template",
  postCartContainer: "#motivator-post-cart",
  $postCartEl: void 0,
  analytics_response: {},
  settings: null,
  init: function() {
      var e = $(this.metaDataIdentifier);
      if (!(e.length <= 0)) {
          var t = JSON.parse(e.html());
          if (console.log(t),
          !1 !== t.active) {
              var o, n, r = dragonDropPlugins.utils.get_color;
              if (t.response = this.analytics_response,
              this.settings = t,
              rivets.binders["high-demand-badge"] = function(e, t) {
                  e.style["background-color"] = r(t.highDemandBadgeColor),
                  e.style.color = r(t.highDemandBadgeTextColor)
              }
              ,
              rivets.binders["high-demand-outer"] = function(e, t) {
                  e.style.color = r(t.highDemandOuterTxtColor)
              }
              ,
              rivets.binders["looking-now"] = function(e, t) {
                  e.style.color = r(t.lookingNowTextColor)
              }
              ,
              rivets.binders["watch-list"] = function(e, t) {
                  e.style.color = r(t.watchlistTextColor)
              }
              ,
              rivets.binders["wish-list"] = function(e, t) {
                  e.style.color = r(t.wishlistTextColor)
              }
              ,
              rivets.binders["hurry-text"] = function(e, t) {
                  e.style.color = r(t.hurryTextColor)
              }
              ,
              rivets.binders["just-purchased-badge"] = function(e, t) {
                  e.style["background-color"] = r(t.justPurchasedBadgeColor),
                  e.style.color = r(t.justPurchasedBadgeTextColor)
              }
              ,
              rivets.binders["limited-supply"] = function(e, t) {
                  e.style["background-color"] = r(t.limitedSupplyBGColor),
                  e.style.color = r(t.limitedSupplyTxtColor)
              }
              ,
              t.response.recent_sale) {
                  t.response.recent_sale.timeago = (o = new Date(t.response.recent_sale.created),
                  (n = ((new Date).getTime() - o.getTime()) / 1e3) <= 1 ? t.secondSingular.replace("{quantity}", parseInt(n)) : n < 60 ? t.secondPlural.replace("{quantity}", parseInt(n)) : n < 120 ? t.minuteSingular.replace("{quantity}", parseInt(n / 60)) : n < 3600 ? t.minutePlural.replace("{quantity}", parseInt(n / 60)) : n < 7200 ? t.hourSingular.replace("{quantity}", parseInt(n / 3600)) : n < 86400 ? t.hourPlural.replace("{quantity}", parseInt(n / 3600)) : n < 172800 ? t.daySingular.replace("{quantity}", parseInt(n / 86400)) : n > 172800 ? t.dayPlural.replace("{quantity}", parseInt(n / 86400)) : void 0)
              }
              t.lookingNowText = t.lookingNowText.replace("{quantity}", parseInt(t.response.current_viewers)),
              t.response.watching_right_now = parseInt(Math.floor(t.response.total_hits * (t.watchlistPercent / 100))),
              t.watchlistText = t.watchlistText.replace("{quantity}", t.response.watching_right_now),
              t.response.wishing_right_now = parseInt(Math.floor(t.response.total_hits * (t.wishlistPercent / 100))),
              t.wishlistText = t.wishlistText.replace("{quantity}", t.response.wishing_right_now),
              t.show_recent_sale = null !== t.response.recent_sale && t.justPurchasedActive,
              t.show_high_demand = t.response.is_popular && t.highDemandActive,
              t.show_current_viewers = t.response.current_viewers >= t.lookingNowThreshold && t.lookingNowActive,
              t.show_wishing_right_now = t.response.wishing_right_now >= t.watchlistThreshold && t.watchlistActive,
              t.show_watching_right_now = t.response.watching_right_now >= t.wishlistThreshold && t.wishlistActive;
              var a = $($(this.postTitleRivetId).html()).prependTo(this.postTitleContainer);
              rivets.bind(a, {
                  settings: t
              });
              var i = dragonDropPlugins.analytics.product
                , s = 0;
              i.variants.forEach(function(e) {
                  e.available || (s += 1)
              });
              var u = dragonDropPlugins.analytics.current_variant.inventory_quantity;
              t.show_hurry_text = 0 < u && u <= t.hurryTextThreshold && t.hurryTextActive,
              t.ogHurryText = t.hurryText,
              t.hurryText = t.ogHurryText.replace("{quantity}", u),
              t.show_limited_supply = s > 0 && t.limitedSupplyActive,
              t.limitedSupplyContent = t.limitedSupplyContent.replace("{quantity}", s);
              var d = this.$postCartEl = $($(this.postCartRivetId).html()).prependTo(this.postCartContainer);
              rivets.bind(d, {
                  settings: t
              }),
              this.animateIn()
          }
      }
  },
  setHurryText: function(e) {
      var t = this.settings;
      t.show_hurry_text = 0 < e && e <= t.hurryTextThreshold && t.hurryTextActive,
      t.hurryText = t.ogHurryText.replace("{quantity}", e)
  }
},
theme.customerAddresses = function() {
  var e = $("#AddressNewForm");
  e.length && (Shopify && new Shopify.CountryProvinceSelector("AddressCountryNew","AddressProvinceNew",{
      hideElement: "AddressProvinceContainerNew"
  }),
  $(".address-country-option").each(function() {
      var e = $(this).data("form-id")
        , t = "AddressCountry_" + e
        , o = "AddressProvince_" + e
        , n = "AddressProvinceContainer_" + e;
      new Shopify.CountryProvinceSelector(t,o,{
          hideElement: n
      })
  }),
  $(".address-new-toggle").on("click", function() {
      e.toggleClass("hide")
  }),
  $(".address-edit-toggle").on("click", function() {
      var e = $(this).data("form-id");
      $("#EditAddress_" + e).toggleClass("hide")
  }),
  $(".address-delete").on("click", function() {
      var e = $(this)
        , t = e.data("form-id")
        , o = e.data("confirm-message");
      confirm(o || "Are you sure you wish to delete this address?") && Shopify.postLink("/account/addresses/" + t, {
          parameters: {
              _method: "delete"
          }
      })
  }))
}(),
theme.customerLogin = function() {
  var e = "#RecoverPassword"
    , t = "#HideRecoverPasswordLink";
  function o(e) {
      e.preventDefault(),
      n()
  }
  function n() {
      $("#RecoverPasswordForm").toggleClass("hide"),
      $("#CustomerLoginForm").toggleClass("hide")
  }
  $(e).length && ("#recover" === window.location.hash && n(),
  function() {
      if (!$(".reset-password-success").length)
          return;
      $("#ResetSuccess").removeClass("hide")
  }(),
  $(e).on("click", o),
  $(t).on("click", o))
}(),
$.fn.extend({
  animateCss: function(e, t) {
      return this.addClass("animated " + e).one("webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend", function() {
          $(this).removeClass("animated " + e),
          t && t()
      }),
      this
  }
}),
$(document).ready(function() {
  dragonDropPlugins.analytics.init(),
  (new slate.Sections).register("product", theme.Product),
  slate.a11y.pageLinkFocus($(window.location.hash)),
  $(".in-page-link").on("click", function(e) {
      slate.a11y.pageLinkFocus($(e.currentTarget.hash))
  }),
  slate.rte.wrapTable(),
  slate.rte.iframeReset(),
  slate.cart.cookiesEnabled() && (document.documentElement.className = document.documentElement.className.replace("supports-no-cookies", "supports-cookies")),
  $("[data-reveal]").on("click", function() {
      $($(this).data("reveal")).toggle()
  });
  var e = $("[id^=orderDetails]").detach();
  $("body").append(e),
  $(".address-new-toggle, .address-edit-toggle").click(function() {
      var e = $(this).data("form-id");
      void 0 === e ? $("#AddressNewForm").toggle() : $("#EditAddress_" + e).toggle()
  });
  var t = $(".custom-control.custom-checkbox");
  t.append('<span class="custom-control-indicator"></span>'),
  t.find('input[type="checkbox"]').addClass("custom-control-input"),
  $("[id^=AddressProvinceContainer_]").each(function(e, t) {
      var o = $(t).attr("id").split("AddressProvinceContainer_")[1];
      new Shopify.CountryProvinceSelector("AddressCountry_" + o,"AddressProvince_" + o,{
          hideElement: "AddressProvinceContainer_" + o
      })
  }),
  $(".swatch-color-text").length > 0 && $(".swatch input:checked").each(function(e, t) {
      $(this).closest(".swatch").find(".swatch-color-text").text($(this).closest(".swatch-element").find(".sw-tooltip").text())
  }),
  $(".swatch :radio").change(function() {
      var e = $(this).closest(".swatch").attr("data-option-index")
        , t = $(this).val();
      $(this).closest(".swatch").find(".swatch-color-text").text($(this).closest(".swatch-element").find(".sw-tooltip").text()),
      $(this).closest("form").find("#SingleOptionSelector-" + e).val(t).trigger("change")
  }),
  $("a.thumbnail-link").on("click", function() {
      var e = $(this).attr("href").split("#")[1];
      $(".gallery-wrapper .gallery-item").removeClass("active"),
      $('[data-hash="' + e + '"]').parent().addClass("active")
  }),
  $(".quantity-up").on("click", function(e) {
      var t = $(this).closest(".flex-quantity").find('input[type="number"]');
      t.val(parseInt(t.val()) + 1),
      e.stopPropagation()
  }),
  $(".quantity-down").on("click", function(e) {
      var t = $(this).closest(".flex-quantity").find('input[type="number"]');
      parseInt(t.val()) > 1 && t.val(parseInt(t.val()) - 1),
      e.stopPropagation()
  }),
  $("body").on("click", ".spr-badge", function(e) {
      var t = $(".shopify-product-reviews");
      if (!(t.length <= 0)) {
          var o = 0;
          t.each(function(e, t) {
              $(t).offset().top > 0 && (o = $(t).offset().top)
          }),
          $("html").velocity("scroll", {
              offset: o - 200,
              duration: 1e3,
              easing: "easeOutExpo",
              mobileHA: !1
          }),
          e.preventDefault()
      }
  });
  $("#sorting").change(function() {
      var e, t, o, n, r = $(this).val();
      window.location = document.location.pathname + (e = document.location.search,
      n = "?" + (o = (t = "sort_by") + "=" + r),
      e && (n = e.replace(new RegExp("([?&])" + t + "[^&]*"), "$1" + o)) === e && (n += "&" + o),
      n)
  }),
  $(".has-hero-present").length > 0 && ($(".navbar").addClass("navbar-ghost"),
  $(".topbar").addClass("topbar-ghost")),
  $(".sidebar-toggle").length > 0 && $(".collection-filter-content").length > 0 && $("#modalShopFilters").find(".modal-body").html($(".collection-filter-content").html()),
  CartJS.init(theme.cartJson, {
      moneyFormat: theme.moneyFormat,
      moneyWithCurrencyFormat: theme.moneyWithCurrencyFormat,
      currency: theme.shopCurrency
  }),
  theme.cartAjax && slate.cart.ajaxCartFunctionality(),
  !0 === theme.topbar.multiCurrency && (dragonDropPlugins.topbar.multiCurrency = theme.topbar.multiCurrency,
  dragonDropPlugins.topbar.shopCurrency = theme.shopCurrency,
  dragonDropPlugins.topbar.defaultCurrency = theme.topbar.defaultCurrency,
  dragonDropPlugins.topbar.defaultFormat = theme.topbar.defaultFormat,
  dragonDropPlugins.topbar.geoIpLookup = theme.topbar.geoIpLookup,
  dragonDropPlugins.topbar.currencyOnlyTopBar() || dragonDropPlugins.topbar.initCurrencyConverter()),
  dragonDropPlugins.upxsell.init(),
  dragonDropPlugins.topbar.initAnnouncementBar(),
  dragonDropPlugins.cart_countdown.init(),
  dragonDropPlugins.product_countdown.init(),
  dragonDropPlugins.product_badges.init(),
  dragonDropPlugins.product_tabs.init(),
  dragonDropPlugins.product_sizeguide.init(),
  void 0 !== window.theme.shipping_calculator && Shopify.Cart.ShippingCalculator.show(window.theme.shipping_calculator),
  dragonDropPlugins.salespopup.init();
  var o = dragonDropPlugins.product_motivators.animateIn = function() {
      $(".popup-sales-motivator").each(function(e, t) {
          $(t).offset().top - $(window).scrollTop() - $(window).height() <= 0 ? ($(t).css("display", "block"),
          $(t).css("animation", "submenu-show .3s cubic-bezier(.68, -.55, .265, 1.55)")) : ($(t).css("display", "none"),
          $(t).css("animation", "none"))
      })
  }
  ;
  $(window).on("scroll", o),
  o(),
  dragonDropPlugins.emailpopup.init(),
  dragonDropPlugins.outofstock.init();
  var n = $("#footer-email-subscribe-form")
    , r = $("#footer-email-form-container").find(".form-response")
    , a = n.find('button[type="submit"]')
    , i = a.html();
  n.length > 0 && (n.find('input[name="b_c7103e2c981361a6639545bd5_1194bb7544"]').val("passed"),
  n.submit(function(e) {
      var t = n.attr("action");
      $.ajax({
          type: "POST",
          url: t,
          data: n.serialize(),
          dataType: "json",
          beforeSend: function() {
              r.hide(),
              r.find(".form-success-message").hide(),
              r.find(".form-error-message").hide(),
              a.html('<i class="fa fa-spinner fa-spin"></i>'),
              a.attr("disabled", "true")
          },
          success: function(e) {
              e.success ? (a.html('<i class="fa fa-check"></i>'),
              a.addClass("btn-success"),
              r.find(".form-success-message").show(),
              n.hide(),
              dragonDropPlugins.emailpopup.shown = !0,
              $.cookie("dragonDrop.emailPopup", "submitted", {
                  expires: 1825,
                  path: "/"
              })) : (a.html('<i class="fa fa-times"></i>'),
              a.addClass("btn-danger"),
              r.find(".form_error_" + e.error_code).show())
          },
          complete: function(e, t) {
              var o = function() {
                  r.show(),
                  a.html(i),
                  a.removeClass("btn-success"),
                  a.removeClass("btn-danger"),
                  a.removeAttr("disabled")
              };
              "success" === t ? setTimeout(o, 500) : o()
          }
      }),
      e.preventDefault()
  }));
  var s, u = !1;
  if ($(window).on("touchstart", function(e) {
      1 == $(e.target).closest(".owl-grab").length && (u = !0)
  }),
  $(window).on("touchend", function() {
      u = !1
  }),
  $(window).on("touchmove", function(e) {
      u && e.preventDefault()
  }),
  $("ul.bullets").length) {
      var d = $("ul.bullets")[0].outerHTML;
      $("ul.bullets").remove(),
      console.log(d),
      $(".5-bullet-container").html(d)
  }
  $("body").on("click", ".btn-checkout", function(e) {
      if ($("#agree_18plus").length > 0) {
          if (!$("#agree_18plus").is(":checked"))
              return alert("We need to make sure you are over 18 years of age. Please make sure you have checked the checkbox to confirm before proceeding."),
              !1;
          $(this).submit()
      }
  }),
  $(".country-currency-switcher").on("click", function() {
      $(this).parent().addClass("show"),
      $(this).parent().find(".dropdown-menu").addClass("show")
  }),
  $(document).on("click", function(e) {
      $(e.target).closest(".country-currency-switcher-wrap").length || ($(".country-currency-switcher-wrap").removeClass("show"),
      $(".country-currency-switcher-wrap .dropdown-menu").removeClass("show"))
  }),
  s = ".mobile-menu-wrap",
  $(".mobile-menu-toggle").on("click", function() {
      $(this).toggleClass("active"),
      $(s).toggleClass("show")
  });
  var c = $(".mobile-menu .has-submenu > a");
  function l() {
      c.parent().removeClass("show-submenu")
  }
  c.on("click", function(e) {
      $(e.target).parent().is(".show-submenu") ? l() : (l(),
      $(this).parent().addClass("show-submenu")),
      e.preventDefault()
  });
  var C = $(".shortcode-widget-featured-products")
    , p = $(".shortcode-widget-featured-products-placeholder");
  if (C.length) {
      var y = C.data("products").replace(",]", "]")
        , h = JSON.parse(y);
      p.append(C),
      C.show(),
      console.log("products included", h),
      $.each(h, function(e, t) {
          t.length <= 0 || $.ajax({
              url: window.location.protocol + "//" + window.location.host + "/products/" + t + ".js",
              dataType: "json",
              success: function(e) {
                  var t = window.location.protocol + "//" + window.location.host + e.url;
                  console.log(e);
                  var o = $('\x3c!-- Entry--\x3e\n  <div class="entry">\n    <div class="entry-thumb"><a href="' + t + '"><img src="' + e.images[0] + '" alt="' + e.title + '"></a></div>\n    <div class="entry-content">\n      <h4 class="entry-title"><a href="' + t + '">' + e.title + '</a></h4>\n      <span class="entry-meta text-muted"><span class="money">' + e.price / 100 + "</span></span>\n    </div>\n  </div>");
                  C.append(o),
                  Currency.convertAll(dragonDropPlugins.topbar.shopCurrency, Currency.currentCurrency)
              }
          })
      })
  }
  var m = $(".article-video-preview")
    , g = $(".article-video-preview-placeholder");
  m.show(),
  g.append(m),
  console.log($("[name=currencies]").val()),
  $("span.currency").html($("[name=currencies]").val()),
  $("[name=currencies]").on("change", function() {
      console.log("changing currency", e);
      var e = $(this).val();
      $("span.currency").html(e)
  });
  var f = 0
    , v = !1
    , b = function() {
      $(".jdgm--done-setup").length === $(".jdgm-widget").length && (setTimeout(function() {
          $(".loadmore-container").isotope("layout")
      }, 200),
      console.log("done loading widgets"),
      clearInterval(f)),
      v && (judgeme.badge(),
      judgeme.customizeBadges())
  };
  $(".loadmore-container").length && (f = setInterval(b, 1e3),
  $(".btn-loadmore-pagination").on("click", function() {
      v = !0,
      setTimeout(function() {
          f = setInterval(b, 1e3)
      }, 500)
  }))
}),
$(document).ready(function() {
  $(document).on("change", ".flex-quantity input.form-control", function() {
      console.log("in"),
      $(this).val() <= 0 && $(this).val(1)
  }),
  window.topbarMsg1 = $(".dragondrop-anoun-bar.hidden-lg-up.topbar .full_msg").text(),
  window.topbarMsg2 = $(".dragondrop-anoun-bar.hidden-lg-up.topbar .msg-two").html(),
  window.swapTextInterval = null,
  $(window).resize(function() {
      initializeMobileEvent()
  }),
  initializeMobileEvent(),
  window.swapTextInterval = setInterval(function() {
      $(".dragondrop-anoun-bar .topmsg.active").removeClass("active").siblings(".topmsg").addClass("active")
  }, 3500)
});
