(function (document, $) {
    if (window['OptiMonkPreloadStarted'] || typeof localStorage !== 'object') {
        return;
    }

    window.OptiMonkPreloadStarted = true;

    var acc = '92770';
    var optimonkUrl = 'https://front.optimonk.com';
    var assetsVersion = '213';

    function reloadCart(){OptiMonk.$.getJSON("/cart.js",function(t){var n=OptiMonk.Visitor.createAdapter();n.Cart.clear(),OptiMonk.$.each(t.items,function(t,e){n.Cart.add(e.id,{handle:e.handle,quantity:e.quantity,price:e.price/100,line_price:e.line_price/100,product_id:e.product_id,product_title:e.product_title,sku:e.sku,title:e.title,url:e.url})})})}function addListener(t,e,n){t.addEventListener?t.addEventListener(e,n,!1):t.attachEvent?t.attachEvent("on"+e,function(){n.apply(t,new Array(window.event))}):t["on"+e]=n}function addListenerToHtml(t,e){addListener(document.querySelector("html"),t,e)}function runPreload(){"undefined"!=typeof $?($(document).on("optimonk#ready",reloadCart),$(document).ajaxComplete(function(t,e,n){n&&("/cart/add.js"===n.url&&"POST"===n.type||/^\/cart\/add\.js/.exec(n.url)&&"GET"===n.type)&&reloadCart()})):document.querySelector("html").addEventListener("optimonk#ready",function(){reloadCart(),OptiMonk.$(document).ajaxComplete(function(t,e,n){n&&("/cart/add.js"===n.url&&"POST"===n.type||/^\/cart\/add\.js/.exec(n.url)&&"GET"===n.type)&&reloadCart()})});var t=optimonkUrl+"/public/"+acc+"/js/preload.js",e=document.createElement("script");e.type="text/javascript",e.charset="utf-8",e.src=t+"?"+assetsVersion,document.querySelector("head").appendChild(e)}
    var body = document.querySelector('body');
    if (body) {
        var styleTag = document.createElement('style')
        styleTag.innerText = 'html.wf-loading,html.wf-active,html.wf-inactive{visibility: visible;opacity: 1}'
        body.appendChild(styleTag)
    }

    if (document.readyState === "complete") {
        runPreload();
    } else {
        addListener(window, 'load', function () {
            runPreload();
        });
    }
    // checkout and Checkout are seperate objects
    var isCheckoutPage = Shopify.Checkout && Shopify.Checkout.page === 'thank_you'
    if (isCheckoutPage) {
        var filledCookieMatch = document.cookie.match(new RegExp('omLastFilled=([^;]+)'));
        if (filledCookieMatch) {
            var filledCookieData = JSON.parse(decodeURIComponent(filledCookieMatch[1]))
            params = {
                accountId: acc,
                creativeId: filledCookieData.creativeId,
                showedTs: filledCookieData.ts,
                orderId: Shopify.checkout.order_id,
                totalPrice: Shopify.checkout.total_price,
                currency: Shopify.checkout.currency,
                href: window.location.href
            }
            const http = new XMLHttpRequest()
            http.open('POST', optimonkUrl + '/shopify/checkout')
            http.setRequestHeader('Content-type', 'application/json')
            http.send(JSON.stringify(params))
        } else {
            console.log('no om session found')
        }
    }

}(document, window.jQuery));
